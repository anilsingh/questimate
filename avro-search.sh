#!/bin/sh

mem=800m

java "-Xmx$mem"  -cp ".:dist/Questimate.jar:lib/*:lib-nodistrib/*" org.limsi.cm.avro.AvroSearch $@
