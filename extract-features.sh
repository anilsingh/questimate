#!/bin/sh

# Change the java heap size, if necessary
mem=800m

java "-Xmx$mem"  -cp ".:dist/Questimate.jar:lib/*:lib-nodistrib/*" org.limsi.cm.workflow.FeatureExtractionFlow $@

#USAGE:
# sh extract-features.sh <arguments>

# Data paths are located at: props/core-data-paths.txt
# External tool commands are located at: props/tool-commands.txt 

#Examples of arguments (commands you can try)
#sh extract-features.sh -sl en -tl fr -nb /vol/work/anil/wmt13/nbest-sample.txt -a tmp/nbest-features.avro -o tmp/nbest-features.svmlight -ft SOUL_LM  /vol/work/anil/wmt13/en-sample.txt /vol/work/anil/wmt13/fr-sample.txt

#sh extract-features.sh -sl en -tl fr -nb /vol/work/anil/wmt13/nbest-sample.txt -a tmp/nbest-features.avro -o tmp/nbest-features.svmlight -ft SURFACE,POSCOUNTS  /vol/work/anil/wmt13/en-sample.txt /vol/work/anil/wmt13/fr-sample.txt

#sh extract-features.sh -sl en -tl fr -nb /vol/work/anil/wmt13/nbest-sample.txt -a tmp/nbest-features.avro -o tmp/nbest-features.svmlight -ft NGRAM_LM,NGRAM_LM_POS,NGRAM_COUNTS -lmo 5 /vol/work/anil/wmt13/en-sample.txt /vol/work/anil/wmt13/fr-sample.txt

#List of available feature types (-ft argument):
# SURFACE
# NGRAM_LM
# NGRAM_LM_POS
# NGRAM_LM_POS_NOLEX
# NGRAM_COUNTS
# NGRAM_COUNTS_POS
# NGRAM_COUNTS_POS_NOLEX
# POSCOUNTS
# IBM1
# SOUL_LM
# MODEL: For n-best lists
# LABEL: For the label to be predicted/classified (currently only hTER supported)

# Options and arguments:
# -cs: Charset (UTF-8 by default)
# -sl: Source language (two letter or three letter code or full name)
# -tl: Target language (two letter or three letter code or full name)
# -o: Output text file path
# -a: Output avro file path (-a and -o are likely to be combined together into -o with the argument for format type)
# -f: The format of text feature file to be created (SVMLIGHT_FORMAT or ARFF_FORMAT)
# -ft: List of feature types, separated by commas
# -lmo: N-gram LM order (only for NGRAM_LM, NGRAM_LM_POS and NGRAM_LM_POS_NOLEX)
# -nb: The input n-best list
# -b: Flag for batch mode (read job specifications from one or more files)
# -s: Tokenized source file (for batch mode, use -s, otherwise without -s)
# -t: Tokenized target file (for batch mode, use -t, otherwise without -t)
# Last two arguments: Source and target text files (*should not be lowercased*)

