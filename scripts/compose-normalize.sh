#!/bin/sh

composeNormalizeFST() {
   composeFSTs "$1" "$2"

   normalizeFST "$1.$2.composed"
}
