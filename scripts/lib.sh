#!/bin/sh

JCMD="java -Xmx10g -Djava.ext.dirs=lib:lib-nodistrib -cp .:dist/Questimate.jar"

SRCNAME=`basename $SRCFILE`
TGTNAME=`basename $TGTFILE`
PEDITNAME=`basename $PEDITFILE`

simpleTask(){
  if [ -f "$1" ];
  then
     echo "File $1 exists."
  else
     eval "$2"
  fi
}

simpleTaskEx () {
  simpleTask "$1" "$2"

  sed -i -e "s/:NaN/:1/g" "$1"
  sed -i -e "s/:Infinity/:2/g" "$1"
}

runTagger() {

  if [ -f "$1.pos" ];
  then
     echo "File $1.pos exists."
  else
     CMD="cp $2 $1.tt"
     eval "$CMD"

     CMD="sed -i -e \"s/$/<SENTENCE-END>/g\" $1.tt"
     eval "$CMD"

     if [ "$3" == "SRC" ];
        then
        eval "$SRCTAGGERCMD < $1.tt > $1.tagged"
     elif [ "$3" == "TGT"  ];
     then
        eval "$TGTTAGGERCMD < $1.tt > $1.tagged"
     fi

     CMD="rm $1.tt"
     eval "$CMD"

     CMD="$JCMD org.limsi.cm.conv.POSTaggedFormatConvertor -cs UTF-8 -if TREE_TAGGER -of SINGLE_LINE_UNDERSCORE -o $1.pos $1.tagged"
     cd ..
     eval "$CMD"
     cd -

     CMD="cat $1.pos | awk '{gsub(/[^_^ ]*_/,\"\");print}' > $1.pos.nolex"
     eval "$CMD"

     CMD="sed -i -e \"s/_/\//g\" $1.pos"
     eval "$CMD"

     CMD="rm $1.tagged"
     eval "$CMD"
  fi
}

getLMFeatures(){
  simpleTask "$1" "$4"

  grep logprob "$1" > "$2"

  CMD="sed -i -e \"s/[0-9]* zeroprobs, //g\" $2"
  eval "$CMD"

  CMD="sed -i -e \"s/= /:/g; s/logprob/$3Logprob/g; s/ppl/$3Ppl/g\" $2"
  eval "$CMD"

  sed -i '$d' "$2"
}

getLMFeaturesST(){
  cd ..

  if [ -f "$2" ];
  then
     echo "File $2 exists"
  elif [ "$1" == "lex" ];
  then
     echo "Getting LM features for the source file"

     getLMFeatures "$WORKDIR/$SRCNAME.ppl" "$FEATUREDIR/$SRCNAME.lm" "srcLM" "$SRCLMPCMD"

     echo "Getting LM features for the target file"

     getLMFeatures "$WORKDIR/$TGTNAME.ppl" "$FEATUREDIR/$TGTNAME.lm" "tgtLM" "$TGTLMPCMD"

     paste -d ' ' "$FEATUREDIR/$SRCNAME.lm" "$FEATUREDIR/$TGTNAME.lm" > "$SRCNAME.tmp"

     mv "$SRCNAME.tmp" "$FEATUREDIR/$SRCNAME.lm"

    CMD="$JCMD org.limsi.mt.cm.features.impl.NormalizeFeatures -cs UTF-8 -fn srcLMLogprob,srcLMPpl,srcLMPpl1,tgtLMLogprob,tgtLMPpl,tgtLMPpl1 -o $FEATUREDIR/$SRCNAME.lm.norm $SRCFILE $FEATUREDIR/$SRCNAME.lm"

     simpleTaskEx "$FEATUREDIR/$SRCNAME.lm.norm" "$CMD"

     CMD="$JCMD org.limsi.mt.cm.features.impl.RatioFeatures -cs UTF-8 -fn srcLMLogprob,srcLMPpl,srcLMPpl1,tgtLMLogprob,tgtLMPpl,tgtLMPpl1 -o $FEATUREDIR/$SRCNAME.lm.ratio $FEATUREDIR/$SRCNAME.lm $FEATUREDIR/$TGTNAME.lm "

     simpleTaskEx "$FEATUREDIR/$SRCNAME.lm.ratio" "$CMD"

     rm "$FEATUREDIR/$TGTNAME.lm"

  elif [ "$1" == "pos" ];
  then
     # POS LM features

     echo "Getting POS tagged LM features for the source file"

     getLMFeatures "$WORKDIR/$SRCNAME.pos.ppl" "$FEATUREDIR/$SRCNAME.pos.lm" "srcPOSLM" "$SRCPOSLMPCMD"

     echo "Getting POS tagged LM features for the target file"

     getLMFeatures "$WORKDIR/$TGTNAME.pos.ppl" "$FEATUREDIR/$TGTNAME.pos.lm" "tgtPOSLM" "$TGTPOSLMPCMD"

     paste -d ' ' "$FEATUREDIR/$SRCNAME.pos.lm" "$FEATUREDIR/$TGTNAME.pos.lm" > "$SRCNAME.tmp"

     mv "$SRCNAME.tmp" "$FEATUREDIR/$SRCNAME.pos.lm"

     CMD="$JCMD org.limsi.mt.cm.features.impl.NormalizeFeatures -cs UTF-8 -fn srcPOSLMLogprob,srcPOSLMPpl,srcPOSLMPpl1,tgtPOSLMLogprob,tgtPOSLMPpl,tgtPOSLMPpl1 -o $FEATUREDIR/$SRCNAME.pos.lm.norm $SRCFILE $FEATUREDIR/$SRCNAME.pos.lm"

     simpleTaskEx "$FEATUREDIR/$SRCNAME.pos.lm.norm" "$CMD"

     CMD="$JCMD org.limsi.mt.cm.features.impl.RatioFeatures -cs UTF-8 -fn srcPOSLMLogprob,srcPOSLMPpl,srcPOSLMPpl1,tgtPOSLMLogprob,tgtPOSLMPpl,tgtPOSLMPpl1 -o $FEATUREDIR/$SRCNAME.pos.lm.ratio $FEATUREDIR/$SRCNAME.pos.lm $FEATUREDIR/$TGTNAME.pos.lm "

     simpleTaskEx "$FEATUREDIR/$SRCNAME.pos.lm.ratio" "$CMD"

     rm "$FEATUREDIR/$TGTNAME.pos.lm"

  elif [ "$1" == "nolex" ];
  then
    # POS nolex LM features

     echo "Getting POS tagged nolex LM features for the source file"

     getLMFeatures "$WORKDIR/$SRCNAME.pos.nolex.ppl" "$FEATUREDIR/$SRCNAME.pos.nolex.lm" "srcPOSLMNolex" "$SRCPOSLMNLPCMD"

     echo "Getting POS tagged nolex LM features for the target file"

     getLMFeatures "$WORKDIR/$TGTNAME.pos.nolex.ppl" "$FEATUREDIR/$TGTNAME.pos.nolex.lm" "tgtPOSLMNolex" "$TGTPOSLMNLPCMD"

     paste -d ' ' "$FEATUREDIR/$SRCNAME.pos.nolex.lm" "$FEATUREDIR/$TGTNAME.pos.nolex.lm" > "$SRCNAME.tmp"

     mv "$SRCNAME.tmp" "$FEATUREDIR/$SRCNAME.pos.nolex.lm"

     CMD="$JCMD org.limsi.mt.cm.features.impl.NormalizeFeatures -cs UTF-8 -fn srcPOSLMNolexLogprob,srcPOSLMNolexPpl,srcPOSLMNolexPpl1,tgtPOSLMNolexLogprob,tgtPOSLMNolexPpl,tgtPOSLMNolexPpl1 -o $FEATUREDIR/$SRCNAME.pos.nolex.lm.norm $SRCFILE $FEATUREDIR/$SRCNAME.pos.nolex.lm"

     simpleTaskEx "$FEATUREDIR/$SRCNAME.pos.nolex.lm.norm" "$CMD"

     CMD="$JCMD org.limsi.mt.cm.features.impl.RatioFeatures -cs UTF-8 -fn srcPOSLMNolexLogprob,srcPOSLMNolexPpl,srcPOSLMNolexPpl1,tgtPOSLMNolexLogprob,tgtPOSLMNolexPpl,tgtPOSLMNolexPpl1 -o $FEATUREDIR/$SRCNAME.pos.nolex.lm.ratio $FEATUREDIR/$SRCNAME.pos.nolex.lm $FEATUREDIR/$TGTNAME.pos.nolex.lm "

     simpleTaskEx "$FEATUREDIR/$SRCNAME.pos.nolex.lm.ratio" "$CMD"

     rm "$FEATUREDIR/$TGTNAME.pos.nolex.lm"
  fi

  cd -
}

getSOULFeatures() {
  if [ -f "$FEATUREDIR/$SRCNAME.soul" ];
  then
     echo "File $FEATUREDIR/$SRCNAME.soul exists."
  elif [ ! -f "$SOULCMD" ];
  then
     echo "File $SOULCMD does not exist."
  elif [ ! -f "$SRCSOULMODEL" ];
  then
     echo "File $SRCSOULMODEL does not exist."
  elif [ ! -f "$TGTSOULMODEL" ];
  then
     echo "File $TGTSOULMODEL does not exist."
  else
     CMD="$SOULCMD $SRCFILE $SRCSOULMODEL $WORKDIR/$SRCNAME.soul-"
     eval "$CMD"

     CMD="$SOULCMD $TGTFILE $TGTSOULMODEL $WORKDIR/$TGTNAME.soul-"
     eval "$CMD"

     CMD="sed -i -e \"s/^/srcSoulLM:/\" $WORKDIR/$SRCNAME.soul-sentprobs"
     eval "$CMD"

     CMD="sed -i -e \"s/^/tgtSoulLM:/\" $WORKDIR/$TGTNAME.soul-sentprobs"
     eval "$CMD"

     paste -d ' ' "$WORKDIR/$SRCNAME.soul-sentprobs" "$WORKDIR/$TGTNAME.soul-sentprobs" > "$FEATUREDIR/$SRCNAME.soul"

     cd ..

     CMD="$JCMD org.limsi.mt.cm.features.impl.NormalizeFeatures -cs UTF-8 -fn srcSoulLM,tgtSoulLM -o $FEATUREDIR/$SRCNAME.soul.norm $SRCFILE $FEATUREDIR/$SRCNAME.soul"

     simpleTaskEx "$FEATUREDIR/$SRCNAME.soul.norm" "$CMD"

     CMD="$JCMD org.limsi.mt.cm.features.impl.RatioFeatures -cs UTF-8 -fn srcSoulLM,tgtSoulLM -o $FEATUREDIR/$SRCNAME.soul.ratio $WORKDIR/$SRCNAME.soul-sentprobs $WORKDIR/$TGTNAME.soul-sentprobs"

     simpleTaskEx "$FEATUREDIR/$SRCNAME.soul.ratio" "$CMD"

     cd -

  fi
}

 
