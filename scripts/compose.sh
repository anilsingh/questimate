#!/bin/bash

# First argument: the lattice file name prefix (leaving out only the extensions, e.g. leaving out .fst.txt)
# Second argument: the n-gram order, e.g. bg (bigram) or tg (trigram) or fg (four gram)

composeFSTs() {
   CMD="fstcompile --isymbols=$1.input.syms --osymbols=$1.output.syms --keep_isymbols --keep_osymbols $1.fst.txt $1.fst"
   eval "$CMD"
   CMD="fstprint $1.fst $1.fst.print"
   eval "$CMD"

   CMD="fstcompile --isymbols=$1.$2.input.syms --osymbols=$1.$2.output.syms --keep_isymbols --keep_osymbols $1.$2.fst.txt $1.$2.fst"
   eval "$CMD"

   CMD="fstprint $1.$2.fst $1.$2.fst.print"
   eval "$CMD"


   CMD="fstmap --map_type=to_log $1.fst $1.log.fst"
   eval "$CMD"

   CMD="fstmap --map_type=to_log $1.$2.fst $1.$2.log.fst"
   eval "$CMD"


   CMD="mv $1.log.fst $1.fst"
   eval "$CMD"

   CMD="mv $1.$2.log.fst $1.$2.fst"
   eval "$CMD"


   CMD="fstarcsort --sort_type=olabel $1.fst $1.fst.output.sorted" 
   eval "$CMD"
   #fstprint $1.log.fst.output.sorted $1.fst.output.sorted.print

   CMD="fstarcsort --sort_type=ilabel $1.$2.fst $1.$2.fst.input.sorted"
   eval "$CMD"
   #fstprint $1.$2.log.fst.input.sorted $1.$2.fst.input.sorted.print


   CMD="mv $1.fst.output.sorted $1.fst"
   eval "$CMD"

   CMD="mv $1.$2.fst.input.sorted $1.$2.fst"
   eval "$CMD"


   CMD="fstcompose $3 $1.fst $1.$2.fst $1.$2.composed.fst"
   eval "$CMD"
   #fstprint $1.$2.log.composed.fst $1.$2.log.composed.fst.print


   CMD="fstinfo $1.$2.composed.fst"
   eval "$CMD"
}
