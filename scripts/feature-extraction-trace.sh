#!/bin/sh

source config-feature-extraction-trace.sh
#source config-prediction.sh
source lib.sh

if [ "$REVERSE" == "REV" ];
then
   read SRCLMDATAPATH TGTLMDATAPATH <<<"$TGTLMDATAPATH $SRCLMDATAPATH"
   read SRCTAGSET TGTTAGSET <<<"$TGTTAGSET $SRCTAGSET"
   read STWDTRANSPATH TSWDTRANSPATH <<<"$TSWDTRANSPATH $STWDTRANSPATH"
   read SRCSOULMODEL TGTSOULMODEL <<<"$TGTSOULMODEL $SRCSOULMODEL"
   read SRCTAGGERCMD TGTTAGGERCMD <<<"$TGTTAGGERCMD $SRCTAGGERCMD"
   read SRCLMPATH TGTLMPATH <<<"$TGTLMPATH $SRCLMPATH"
   read SRCPOSLMPATH TGTPOSLMPATH <<<"$TGTPOSLMPATH $SRCPOSLMPATH"
   read SRCPOSLMNLPATH TGTPOSLMNLPATH <<<"$TGTPOSLMNLPATH $SRCPOSLMNLPATH"
fi

SRCLMDATA=`basename $SRCLMDATAPATH`
TGTLMDATA=`basename $TGTLMDATAPATH`

SRCLMPATH="/people/anil/work/data/LM/en/englishlm.en.3g.arpa.gz"
TGTLMPATH="/people/anil/work/data/LM/fr/frenchlm.fr.3g.arpa.gz"

SRCPOSLMPATH="/people/anil/work/data/poslm-train/poslm-train-data.en.tagged.lm5g"
TGTPOSLMPATH="/people/anil/work/data/poslm-train/poslm-train-data.fr.tagged.lm5g"

SRCPOSLMNLPATH="/people/anil/work/data/poslm-train/poslm-train-data.en.tagged.nolex.lm5g"
TGTPOSLMNLPATH="/people/anil/work/data/poslm-train/poslm-train-data.fr.tagged.nolex.lm5g"

SRCLMCCMD="$SRILM/bin/*/ngram-count -unk -order $LMORDER -kndiscount -interpolate -text $SRCLMDATAPATH -lm $SRCLMPATH"
TGTLMCCMD="$SRILM/bin/*/ngram-count -unk -order $LMORDER -kndiscount -interpolate -text $TGTLMDATAPATH -lm $TGTLMPATH"

SRCPOSLMCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$SRCLMDATA.pos -lm $SRCPOSLMPATH"
TGTPOSLMCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$TGTLMDATA.pos -lm $TGTPOSLMPATH"

SRCPOSLMNLCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$SRCLMDATA.pos.nolex -lm $SRCPOSLMNLPATH"
TGTPOSLMNLCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$TGTLMDATA.pos.nolex -lm $TGTPOSLMNLPATH"

if [ "$REVERSE" == "REV" ];
then
   SRCLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $TGTLMPATH -debug 1 -ppl $SRCFILE > $WORKDIR/$SRCNAME.ppl"
   TGTLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $SRCLMPATH -debug 1 -ppl $TGTFILE > $WORKDIR/$TGTNAME.ppl"

   SRCPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMPATH -debug 1 -ppl $WORKDIR/$SRCNAME.pos > $WORKDIR/$SRCNAME.pos.ppl"
   TGTPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMPATH -debug 1 -ppl $WORKDIR/$TGTNAME.pos > $WORKDIR/$TGTNAME.pos.ppl"

   SRCPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMNLPATH -debug 1 -ppl $WORKDIR/$SRCNAME.pos.nolex > $WORKDIR/$SRCNAME.pos.nolex.ppl"
   TGTPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMNLPATH -debug 1 -ppl $WORKDIR/$TGTNAME.pos.nolex > $WORKDIR/$TGTNAME.pos.nolex.ppl"
else
   SRCLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $SRCLMPATH -debug 1 -ppl $SRCFILE > $WORKDIR/$SRCNAME.ppl"
   TGTLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $TGTLMPATH -debug 1 -ppl $TGTFILE > $WORKDIR/$TGTNAME.ppl"

   SRCPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMPATH -debug 1 -ppl $WORKDIR/$SRCNAME.pos > $WORKDIR/$SRCNAME.pos.ppl"
   TGTPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMPATH -debug 1 -ppl $WORKDIR/$TGTNAME.pos > $WORKDIR/$TGTNAME.pos.ppl"

   SRCPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMNLPATH -debug 1 -ppl $WORKDIR/$SRCNAME.pos.nolex > $WORKDIR/$SRCNAME.pos.nolex.ppl"
   TGTPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMNLPATH -debug 1 -ppl $WORKDIR/$TGTNAME.pos.nolex > $WORKDIR/$TGTNAME.pos.nolex.ppl"
fi

# Assumption: The input data (the dataset as well as the language model data) is tokenized and lowercased (if necessary)

# Prepare the resources

# Prepare LM

echo "Preparing source language model"

simpleTask "$SRCLMPATH" "$SRCLMCCMD"

echo "Preparing target language model"

simpleTask "$TGTLMPATH" "$TGTLMCCMD"

# Run tagger on LM data

echo "POS tagging the source language model data"

runTagger "$WORKDIR/$SRCLMDATA" "$SRCLMDATAPATH" "SRC"

echo "POS tagging the target language model data"

runTagger "$WORKDIR/$TGTLMDATA" "$TGTLMDATAPATH" "TGT"

# Prepare POS tagged LM

echo "Preparing POS tagged source language model"

simpleTask "$SRCPOSLMPATH" "$SRCPOSLMCCMD"

echo "Preparing POS tagged target language model"

simpleTask "$TGTPOSLMPATH" "$TGTPOSLMCCMD"

# Prepare POS tagged nolex LM

echo "Preparing POS tagged nolex source language model"

simpleTask "$SRCPOSLMNLPATH" "$SRCPOSLMNLCCMD"

echo "Preparing POS tagged nolex target language model"

simpleTask "$TGTPOSLMNLPATH" "$TGTPOSLMNLCCMD"

# Run tagger on dataset

echo "POS tagging the source file"

runTagger "$WORKDIR/$SRCNAME" "$SRCFILE" "SRC"

echo "POS tagging the target file"

runTagger "$WORKDIR/$TGTNAME" "$TGTFILE" "TGT"

# Extract features

# LM features

getLMFeaturesST "lex" "$FEATUREDIR/$SRCNAME.lm"

getLMFeaturesST "pos" "$FEATUREDIR/$SRCNAME.pos.lm"

getLMFeaturesST "nolex" "$FEATUREDIR/$SRCNAME.pos.nolex.lm"

getSOULFeatures

cd ..

# Surface features (including n-gram quartile features)
CMD="$JCMD org.limsi.mt.cm.features.impl.SurfaceFeatureExtractor -cs UTF-8 -o $FEATUREDIR/$SRCNAME.surface $SRCFILE $TGTFILE"

simpleTaskEx "$FEATUREDIR/$SRCNAME.surface" "$CMD"

# POSCOUNT features
if [ "$REVERSE" == "REV" ];
then
   CMD="$JCMD org.limsi.mt.cm.features.impl.POSCountsFeatureExtractor -cs UTF-8 -sts $TGTTAGSET -tts $SRCTAGSET -o $FEATUREDIR/$SRCNAME.poscount $WORKDIR/$SRCNAME.pos $WORKDIR/$TGTNAME.pos"
else
   CMD="$JCMD org.limsi.mt.cm.features.impl.POSCountsFeatureExtractor -cs UTF-8 -sts $SRCTAGSET -tts $TGTTAGSET -o $FEATUREDIR/$SRCNAME.poscount $WORKDIR/$SRCNAME.pos $WORKDIR/$TGTNAME.pos"
fi

simpleTaskEx "$FEATUREDIR/$SRCNAME.poscount" "$CMD"

# IBM1 features
if [ "$REVERSE" == "REV" ];
then
   CMD="$JCMD org.limsi.mt.cm.features.impl.WordTranslationFeatureExtractor -cs UTF-8 -o $FEATUREDIR/$SRCNAME.ibm1 $SRCFILE $TGTFILE $TSWDTRANSPATH $STWDTRANSPATH"
else
   CMD="$JCMD org.limsi.mt.cm.features.impl.WordTranslationFeatureExtractor -cs UTF-8 -o $FEATUREDIR/$SRCNAME.ibm1 $SRCFILE $TGTFILE $STWDTRANSPATH $TSWDTRANSPATH"
fi

simpleTaskEx "$FEATUREDIR/$SRCNAME.ibm1" "$CMD"

# hTER scores
CMD="$JCMD org.limsi.cm.conv.GenerateTercomFormat -cs UTF-8 -o $WORKDIR/$TGTNAME.tercom.input $TGTFILE"

simpleTask "$WORKDIR/$TGTNAME.tercom.input" "$CMD"

CMD="$JCMD org.limsi.cm.conv.GenerateTercomFormat -cs UTF-8 -o $WORKDIR/$PEDITNAME.tercom.input $PEDITFILE"

simpleTask "$WORKDIR/$PEDITNAME.tercom.input" "$CMD"

sed -i -e "s/($PEDITNAME-/($TGTNAME-/g" "$WORKDIR/$PEDITNAME.tercom.input"

cd -

cd "$WORKDIR"

CMD="$TERCOM -r $WORKDIR/$PEDITNAME.tercom.input -h $WORKDIR/$TGTNAME.tercom.input -s -N"

simpleTask "$WORKDIR/$TGTNAME.tercom.input.sys.sum" "$CMD"

sed -i '$d' "$WORKDIR/$TGTNAME.tercom.input.sys.sum"

cd -

cd ..

CMD="$JCMD org.limsi.cm.conv.ReadTercomFormat -cs UTF-8 -o $FEATUREDIR/$SRCNAME.detailed.hter $WORKDIR/$TGTNAME.tercom.input.sys.sum"

simpleTask "$FEATUREDIR/$SRCNAME.detailed.hter" "$CMD"

CMD="cut -d ' ' -f 9 $FEATUREDIR/$SRCNAME.detailed.hter > $FEATUREDIR/$SRCNAME.hter"

simpleTask "$FEATUREDIR/$SRCNAME.hter" "$CMD"

# Post-process

# Merge features

CMD="paste -d ' ' $FEATUREDIR/$SRCNAME.lm $FEATUREDIR/$SRCNAME.lm.norm $FEATUREDIR/$SRCNAME.lm.ratio $FEATUREDIR/$SRCNAME.pos.lm $FEATUREDIR/$SRCNAME.pos.lm.norm $FEATUREDIR/$SRCNAME.pos.lm.ratio $FEATUREDIR/$SRCNAME.pos.nolex.lm $FEATUREDIR/$SRCNAME.pos.nolex.lm.norm $FEATUREDIR/$SRCNAME.pos.nolex.lm.ratio $FEATUREDIR/$SRCNAME.soul $FEATUREDIR/$SRCNAME.soul.norm $FEATUREDIR/$SRCNAME.soul.ratio $FEATUREDIR/$SRCNAME.surface $FEATUREDIR/$SRCNAME.poscount $FEATUREDIR/$SRCNAME.ibm1 $FEATUREDIR/$SRCNAME.hter > $FEATUREDIR/$SRCNAME.$FEATURESET"

simpleTask "$FEATUREDIR/$SRCNAME.$FEATURESET" "$CMD"

sed -i -e "s/  / /g" "$FEATUREDIR/$SRCNAME.$FEATURESET"

# Convert to Arff format, if necessary
CMD="$JCMD org.limsi.mt.cm.features.impl.FeatureFormatConverter -cs UTF-8 -if svmlight -of arff -o $FEATUREDIR/$SRCNAME.$FEATURESET.arff $FEATUREDIR/$SRCNAME.$FEATURESET"

simpleTask "$FEATUREDIR/$SRCNAME.$FEATURESET.arff" "$CMD"

