#!/bin/sh

JCMD="java -Xmx10g -Djava.ext.dirs=lib:lib-nodistrib -cp .:dist/Questimate.jar"

FEATURESET="collection1"

MAINDIR="/people/anil/work/trace-qe/questimate"

WORKDIR="$MAINDIR/work"
FEATUREDIR="$MAINDIR/features"
RESULTSDIR="$MAINDIR/results"

#SRCFILE="/people/anil/work/trace-qe/questimate/en/sample.en"
#TGTFILE="/people/anil/work/trace-qe/questimate/fr/sample.fr"
#PEDITFILE="/people/anil/work/trace-qe/questimate/fr/sample.en.pedit"

SRCFILE="$1"
TGTFILE="$2"
PEDITFILE="$3"
REVERSE="$4"

SRCLMDATAPATH="/people/anil/work/wmt12-quality-estimation/resources/europarl-nc-sample.en"
TGTLMDATAPATH="/people/anil/work/wmt12-quality-estimation/resources/europarl-nc-sample.fr"

LMORDER=3
POSLMORDER=3

SRCTAGSET="PTB_POS"
TGTTAGSET="TT_POS_FR"

STWDTRANSPATH="/people/anil/work/wmt12-quality-estimation/resources/lex.e2f"
TSWDTRANSPATH="/people/anil/work/wmt12-quality-estimation/resources/lex.f2e"

SOULCMD="/people/lehaison/share/global/script/bin/compute_cible_soul.sh"
SRCSOULMODEL="/vol/corpora6/smt/lehaison/WMT/wmt12/soul/en.10gram.soul"
TGTSOULMODEL="/vol/corpora6/smt/lehaison/WMT/wmt12/soul/fr.10gram.soul"

TERCOM="/people/anil/work/tools/tercom_v6b/tercom_v6b.pl"

SRILM="/people/anil/work/tools/srilm"

SRCTAGGERCMD="/people/anil/work/tools/treetagger/cmd/tree-tagger-english"
TGTTAGGERCMD="/people/anil/work/tools/treetagger/cmd/tree-tagger-french-utf8"


