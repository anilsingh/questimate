
normalizeFST()
{
   if [ "$2" = "true" ];
   then
      echo "Compiling $1"
      CMD="fstcompile --isymbols=$1.input.syms --osymbols=$1.output.syms --keep_isymbols --keep_osymbols $1.fst.txt $1.fst"
      eval "$CMD"
   fi

   echo "Projecting $1"
   CMD="fstproject --project_output $1.fst $1.fst.po"
   eval "$CMD"

   echo "Removing epsilons $1"
   CMD="fstrmepsilon $1.fst.po $1.fst.po.re"
   eval "$CMD"

   CMD="rm $1.fst.po"
   eval "$CMD"

   CMD="fstdeterminize $1.fst.po.re $1.fst.po.re.det"
   eval "$CMD"

   CMD="rm $1.fst.po.re"
   eval "$CMD"

   CMD="fstminimize $1.fst.po.re.det $1.fst.po.re.det.min"
   eval "$CMD"

   CMD="rm $1.fst.po.re.det"
   eval "$CMD"

   CMD="fstpush --push_weights --remove_total_weight $1.fst.po.re.det.min $1.fst.po.re.det.min.pu"
   #CMD="fstpush --push_weights $1.fst.po.re.det.min $1.fst.po.re.det.min.pu"
   eval "$CMD"

   CMD="rm $1.fst.po.re.det.min"
   eval "$CMD"

   CMD="fsttopsort $1.fst.po.re.det.min.pu $1.fst.po.re.det.min.pu.ts"
   eval "$CMD"

   CMD="rm $1.fst.po.re.det.min.pu"
   eval "$CMD"

   CMD="mv $1.fst.po.re.det.min.pu.ts $1.fst.norm"
   eval "$CMD"

   CMD="fstprint $1.fst.norm $1.norm.fst.txt"
   eval "$CMD"

   #cut -f 4 $1.fin.fst.txt | grep "_|_" | sort -u > $1.$2.uniq

   CMD="fstsymbols --save_isymbols=$1.norm.input.syms  --save_osymbols=$1.norm.output.syms $1.fst.norm > tmp.fst"
   eval "$CMD"

   CMD="fstshortestdistance --reverse $1.fst.norm"
   #eval "$CMD"
}
