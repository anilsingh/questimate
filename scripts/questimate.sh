#!/bin/sh

source config-prediction.sh
source config-feature-extraction.sh
source lib.sh

# Questimate

# Run the Questimate tool to get sentence level quality estimates
cd ..

#CMD="$JCMD org.limsi.mt.cm.HypothesisConfidence -split -ml $WEKACLASSIFIER -fcs UTF-8 -m tmp/model -o $RESULTSDIR/$SRCNAME.$FEATURESET-$WEKACLS-split -p 80.0 $FEATUREDIR/$SRCNAME.$FEATURESET.arff"

CMD="$JCMD org.limsi.mt.cm.HypothesisConfidence -split -ml $WEKACLASSIFIER -fcs UTF-8 -m tmp/model -o $RESULTSDIR/$FFNAME-split -p 80.0 $FEATUREFILE"

#simpleTask "$RESULTSDIR/$SRCNAME.$FEATURESET-$WEKACLS-split" "$CMD"
simpleTask "$FFNAME-split" "$CMD"

cd -
