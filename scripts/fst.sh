#!/bin/sh

. ./compose.sh
. ./normalize.sh
. ./compose-normalize.sh

echo "Script to compose and/or normalize an FST [and its mapping ngram FST] by providing the basename as the argument"

if [ "$1" = "-c" ];
   then
   if [ "$#" -ne 3 ];
      then
         echo "Usage $0 -c fst1 fst2"
         exit 1
   else
      echo "Composing FSTs $2 and $3"
      composeFSTs "$2" "$3"
   fi
elif [ "$1" = "-n" ];
   then
   if [ "$#" -lt 2 ];
      then
         echo "Usage $0 -n fst"
         exit 1
   else
      echo "Normalizing FST $2"
      normalizeFST "$2" "$3"
   fi
elif [ "$1" = "-cn" ];
   then
   if [ "$#" -ne 3 ];
      then
         echo "Usage $0 -cn fst1 fst2"
         exit 1
   else
      echo "Composing and normalizing FSTs $2 and $3"
      composeNormalizeFST "$2" "$3"
   fi
else
   echo "Usage $0 -option arguments"
   echo "\t-c: Compose two FSTs: first arg is basename (path prefix before .fst.txt), second is ngram (bg, tg, fg)"
   echo "\tExample: $0 -c /some/path/foo-fst bg"
   echo "\t-n: Normalize an FST, whose basenames are is the only argument"
   echo "\tExample: $0 -n /some/path/foo-fst"
   echo "\t-cn: Compose and normalized two FSTs, arguments same as for -c"
   echo "\tExample: $0 -cn /some/path/foo-fst bg"
   echo "The naming convention is as follows:"
   echo "\tName of FST text file (OpenFst format): foo-fst.fst.txt"
   echo "\tName of the input symbols file: foo-fst.input.syms"
   echo "\tName of the input symbols file: foo-fst.output.syms"
   echo "\tName of the mapping ngram FST file: foo-fst.bg.fst.txt"
   echo "The output is saved in text format as well as binary format."
fi

