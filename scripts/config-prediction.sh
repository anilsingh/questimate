
# Weka classifier
#WEKACLASSIFIER="weka.classifiers.functions.LinearRegression"
#WEKACLASSIFIER="org.limsi.mt.cm.classifiers.LowerBound"
WEKACLASSIFIER="$1"

# Short name for the Weka classifier that will be used for naming the output files
#WEKACLS="LR"
#WEKACLS="LowerBound"
WEKACLS="$2"

FEATUREFILE="$3"

RESULTSDIR="$4"

FFNAME=`basename $FEATUREFILE .arff`

#CMD="$JCMD org.limsi.mt.cm.HypothesisConfidence -split -ml $WEKACLASSIFIER -fcs UTF-8 -m tmp/model -o $FFNAME-$WEKACLS-split -p 80.0 $FEATUREFILE"
