#!/bin/sh

source config-feature-extraction.sh
#source config-prediction.sh
source lib.sh

SRCLMDATA=`basename $SRCLMDATAPATH`
TGTLMDATA=`basename $TGTLMDATAPATH`

SRCLMPATH="$WORKDIR/$SRCLMDATA.lm"
TGTLMPATH="$WORKDIR/$TGTLMDATA.lm"

SRCPOSLMPATH="$WORKDIR/$SRCLMDATA.poslm"
TGTPOSLMPATH="$WORKDIR/$TGTLMDATA.poslm"

SRCPOSLMNLPATH="$WORKDIR/$SRCLMDATA.poslm.nolex"
TGTPOSLMNLPATH="$WORKDIR/$TGTLMDATA.poslm.nolex"

SRCLMCCMD="$SRILM/bin/*/ngram-count -unk -order $LMORDER -kndiscount -interpolate -text $SRCLMDATAPATH -lm $SRCLMPATH"
TGTLMCCMD="$SRILM/bin/*/ngram-count -unk -order $LMORDER -kndiscount -interpolate -text $TGTLMDATAPATH -lm $TGTLMPATH"

SRCLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $SRCLMPATH -debug 1 -ppl $SRCFILE > $WORKDIR/$SRCNAME.ppl"
TGTLMPCMD="$SRILM/bin/*/ngram -unk -order $LMORDER -lm $TGTLMPATH -debug 1 -ppl $TGTFILE > $WORKDIR/$TGTNAME.ppl"

SRCPOSLMCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$SRCLMDATA.pos -lm $SRCPOSLMPATH"
TGTPOSLMCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$TGTLMDATA.pos -lm $TGTPOSLMPATH"

SRCPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMPATH -debug 1 -ppl $SRCFILE > $WORKDIR/$SRCNAME.pos.ppl"
TGTPOSLMPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMPATH -debug 1 -ppl $TGTFILE > $WORKDIR/$TGTNAME.pos.ppl"

SRCPOSLMNLCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$SRCLMDATA.pos.nolex -lm $SRCPOSLMNLPATH"
TGTPOSLMNLCCMD="$SRILM/bin/*/ngram-count -unk -tagged -order $POSLMORDER -wbdiscount -interpolate -text $WORKDIR/$TGTLMDATA.pos.nolex -lm $TGTPOSLMNLPATH"

SRCPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $SRCPOSLMNLPATH -debug 1 -ppl $SRCFILE > $WORKDIR/$SRCNAME.pos.nolex.ppl"
TGTPOSLMNLPCMD="$SRILM/bin/*/ngram -unk -tagged -order $POSLMORDER -lm $TGTPOSLMNLPATH -debug 1 -ppl $TGTFILE > $WORKDIR/$TGTNAME.pos.nolex.ppl"

# Assumption: The input data (the dataset as well as the language model data) is tokenized and lowercased (if necessary)

# Prepare the resources

# Prepare LM

echo "Preparing source language model"

simpleTask "$SRCLMPATH" "$SRCLMCCMD"

echo "Preparing target language model"

simpleTask "$TGTLMPATH" "$TGTLMCCMD"

# Run tagger on LM data

echo "POS tagging the source language model data"

runTagger "$WORKDIR/$SRCLMDATA" "$SRCLMDATAPATH"

echo "POS tagging the target language model data"

runTagger "$WORKDIR/$TGTLMDATA" "$TGTLMDATAPATH"

# Prepare POS tagged LM

echo "Preparing POS tagged source language model"

simpleTask "$SRCPOSLMPATH" "$SRCPOSLMCCMD"

echo "Preparing POS tagged target language model"

simpleTask "$TGTPOSLMPATH" "$TGTPOSLMCCMD"

# Prepare POS tagged nolex LM

echo "Preparing POS tagged nolex source language model"

simpleTask "$SRCPOSLMNLPATH" "$SRCPOSLMNLCCMD"

echo "Preparing POS tagged nolex target language model"

simpleTask "$TGTPOSLMNLPATH" "$TGTPOSLMNLCCMD"

# Run tagger on dataset

echo "POS tagging the source file"

runTagger "$WORKDIR/$SRCNAME" "$SRCFILE"

echo "POS tagging the target file"

runTagger "$WORKDIR/$TGTNAME" "$TGTFILE"

# Extract features

# LM features

getLMFeaturesST "lex" "$FEATUREDIR/$SRCNAME.lm"

getLMFeaturesST "pos" "$FEATUREDIR/$SRCNAME.pos.lm"

getLMFeaturesST "nolex" "$FEATUREDIR/$SRCNAME.pos.nolex.lm"

getSOULFeatures

cd ..

# Surface features (including n-gram quartile features)
CMD="$JCMD org.limsi.mt.cm.features.impl.SurfaceFeatureExtractor -cs UTF-8 -o $FEATUREDIR/$SRCNAME.surface $SRCFILE $TGTFILE"

simpleTaskEx "$FEATUREDIR/$SRCNAME.surface" "$CMD"

# POSCOUNT features
CMD="$JCMD org.limsi.mt.cm.features.impl.POSCountsFeatureExtractor -cs UTF-8 -sts $SRCTAGSET -tts $TGTTAGSET -o $FEATUREDIR/$SRCNAME.poscount $WORKDIR/$SRCNAME.pos $WORKDIR/$TGTNAME.pos"

simpleTaskEx "$FEATUREDIR/$SRCNAME.poscount" "$CMD"

# IBM1 features
CMD="$JCMD org.limsi.mt.cm.features.impl.WordTranslationFeatureExtractor -cs UTF-8 -o $FEATUREDIR/$SRCNAME.ibm1 $SRCFILE $TGTFILE $STWDTRANSPATH $TSWDTRANSPATH"

simpleTaskEx "$FEATUREDIR/$SRCNAME.ibm1" "$CMD"

# hTER scores
CMD="$JCMD org.limsi.cm.conv.GenerateTercomFormat -cs UTF-8 -o $WORKDIR/$TGTNAME.tercom.input $TGTFILE"

simpleTask "$WORKDIR/$TGTNAME.tercom.input" "$CMD"

CMD="$JCMD org.limsi.cm.conv.GenerateTercomFormat -cs UTF-8 -o $WORKDIR/$PEDITNAME.tercom.input $PEDITFILE"

simpleTask "$WORKDIR/$PEDITNAME.tercom.input" "$CMD"

sed -i -e "s/($PEDITNAME-/($TGTNAME-/g" "$WORKDIR/$PEDITNAME.tercom.input"

cd -

cd "$WORKDIR"

CMD="$TERCOM -r $WORKDIR/$PEDITNAME.tercom.input -h $WORKDIR/$TGTNAME.tercom.input -s -N"

simpleTask "$WORKDIR/$TGTNAME.tercom.input.sys.sum" "$CMD"

sed -i '$d' "$WORKDIR/$TGTNAME.tercom.input.sys.sum"

cd -

cd ..

CMD="$JCMD org.limsi.cm.conv.ReadTercomFormat -cs UTF-8 -o $FEATUREDIR/$SRCNAME.detailed.hter $WORKDIR/$TGTNAME.tercom.input.sys.sum"

simpleTask "$FEATUREDIR/$SRCNAME.detailed.hter" "$CMD"

CMD="cut -d ' ' -f 9 $FEATUREDIR/$SRCNAME.detailed.hter > $FEATUREDIR/$SRCNAME.hter"

simpleTask "$FEATUREDIR/$SRCNAME.hter" "$CMD"

# Post-process

# Merge features

CMD="paste -d ' ' $FEATUREDIR/$SRCNAME.lm $FEATUREDIR/$SRCNAME.lm.norm $FEATUREDIR/$SRCNAME.lm.ratio $FEATUREDIR/$SRCNAME.pos.lm $FEATUREDIR/$SRCNAME.pos.lm.norm $FEATUREDIR/$SRCNAME.pos.lm.ratio $FEATUREDIR/$SRCNAME.pos.nolex.lm $FEATUREDIR/$SRCNAME.pos.nolex.lm.norm $FEATUREDIR/$SRCNAME.pos.nolex.lm.ratio $FEATUREDIR/$SRCNAME.soul $FEATUREDIR/$SRCNAME.soul.norm $FEATUREDIR/$SRCNAME.soul.ratio $FEATUREDIR/$SRCNAME.surface $FEATUREDIR/$SRCNAME.poscount $FEATUREDIR/$SRCNAME.ibm1 $FEATUREDIR/$SRCNAME.hter > $FEATUREDIR/$SRCNAME.$FEATURESET"

simpleTask "$FEATUREDIR/$SRCNAME.$FEATURESET" "$CMD"

sed -i -e "s/  / /g" "$FEATUREDIR/$SRCNAME.$FEATURESET"

# Convert to Arff format, if necessary
CMD="$JCMD org.limsi.mt.cm.features.impl.FeatureFormatConverter -cs UTF-8 -if svmlight -of arff -o $FEATUREDIR/$SRCNAME.$FEATURESET.arff $FEATUREDIR/$SRCNAME.$FEATURESET"

simpleTask "$FEATUREDIR/$SRCNAME.$FEATURESET.arff" "$CMD"

