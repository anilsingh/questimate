#!/bin/sh

#sh feature-extraction-trace.sh "/people/anil/work/trace-qe/questimate/en/sample.en" "/people/anil/work/trace-qe/questimate/fr/sample.fr" "/people/anil/work/trace-qe/questimate/fr/sample.en.pedit"

#sh feature-extraction-trace.sh "$1" "$2" "$3"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/en/en_first.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/en_first.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/en_first.data.pedit.fte.utf8.lc"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/en/en_second.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/en_second.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/en_second.data.pedit.fte.utf8.lc"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/fr/fr_first.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/fr_first.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/fr_first.data.pedit.fte.utf8.lc" "REV"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/fr/fr_second.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/fr_second.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/fr_second.data.pedit.fte.utf8.lc" "REV"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/en/corrections_en2fr.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/corrections_en2fr.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/fr/corrections_en2fr.data.pedit.fte.utf8.lc"

sh feature-extraction-trace.sh "/people/anil/work/trace-qe/tok-noalt-lc/fr/corrections_fr2en.data.src.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/corrections_fr2en.data.trans.fte.utf8.lc" "/people/anil/work/trace-qe/tok-noalt-lc/en/corrections_fr2en.data.pedit.fte.utf8.lc" "REV"

#SRCFILE="/people/anil/work/trace-qe/questimate/en/sample.en"
#TGTFILE="/people/anil/work/trace-qe/questimate/fr/sample.fr"
#PEDITFILE="/people/anil/work/trace-qe/questimate/fr/sample.en.pedit"
 
