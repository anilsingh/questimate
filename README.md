Questimate: A Tool for Machine Translation Quality Estimation

Includes scripts for feature extraction and prediction/classification.

Scripts can be customized, preferably by making a copy of them. More customization is possible from the Java API.

More details available here: [Questimate Wiki](https://bitbucket.org/anilsingh/questimate/wiki)
