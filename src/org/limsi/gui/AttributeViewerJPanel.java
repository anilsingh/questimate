/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.gui;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.HypothesisConfidence;
import org.limsi.types.FormatType;
import weka.core.*;
import weka.filters.Filter;
import weka.filters.SupervisedFilter;
import weka.filters.unsupervised.attribute.Copy;
import weka.filters.unsupervised.attribute.Remove;
import weka.gui.*;
import weka.gui.explorer.Explorer;
import weka.gui.explorer.ExplorerDefaults;

/**
 *
 * @author anil
 */
public class AttributeViewerJPanel extends javax.swing.JPanel implements PropertyChangeListener {

    private List _listeners = new ArrayList();
//    private AttributeManagerJPanel parent;
//    private String featureFilePath = "examples/features-baseline.arff";
    private String featureFilePath = "/people/anil/work/confidence-estimation/mtce-features/features-baseline.arff";
    private Instances instances;
    private Attribute currentAttribute;
    private AttributeSelectionPanel attribSelectionPanel;
    private AttributeSummaryPanel attribSummaryPanel;
    private AttributeVisualizationPanel attribVisPanel;
    private InstancesSummaryPanel instancesSummaryPanel;
    private JComboBox attributesJComboBox;
    /**
     * The frame containing the explorer interface
     */
    protected JFrame m_ExplorerFrame;
    /**
     * Manages sending notifications to people when we change the set of working
     * instances.
     */
    protected PropertyChangeSupport m_Support = new PropertyChangeSupport(this);
    /**
     * A thread for loading/saving instances from a file or URL
     */
    protected Thread m_IOThread;
    /**
     * The message logger
     */
    protected Logger m_Log = new SysErrLog();
    /**
     * Lets the user configure the filter
     */
    protected GenericObjectEditor m_FilterEditor =
            new GenericObjectEditor();

    /**
     * Creates new form AttributeViewerJPanel
     */
    public AttributeViewerJPanel() {
        initComponents();

        m_FilterEditor.setClassType(weka.filters.Filter.class);
        if (ExplorerDefaults.getFilter() != null) {
            m_FilterEditor.setValue(ExplorerDefaults.getFilter());
        }

        m_FilterEditor.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
//            m_ApplyFilterBut.setEnabled(getInstances() != null);
                Capabilities currentCapabilitiesFilter = m_FilterEditor.getCapabilitiesFilter();
                Filter filter = (Filter) m_FilterEditor.getValue();
                Capabilities currentFilterCapabilities = null;
                if (filter != null && currentCapabilitiesFilter != null
                        && (filter instanceof CapabilitiesHandler)) {
                    currentFilterCapabilities = ((CapabilitiesHandler) filter).getCapabilities();

                    if (!currentFilterCapabilities.supportsMaybe(currentCapabilitiesFilter)
                            && !currentFilterCapabilities.supports(currentCapabilitiesFilter)) {
                        try {
                            filter.setInputFormat(getInstances());
                        } catch (Exception ex) {
//                m_ApplyFilterBut.setEnabled(false);
                        }
                    }
                }
            }
        });

        initComboBox(instances);
    }

    private void initComboBox(Instances instances) {
        if (attributesJComboBox != null) {
            optionsJPanel.remove(attributesJComboBox);
        }

        attributesJComboBox = new JComboBox();

        String attributes[] = new String[10];

        if (instances != null) {
            int acount = instances.numAttributes();

            attributes = new String[acount];

            for (int i = 0; i < acount; i++) {
                Attribute a = instances.attribute(i);

                attributes[i] = a.name();
            }
        }

        AutoCompleteSupport support = AutoCompleteSupport.install(
                attributesJComboBox, GlazedLists.eventListOf(attributes));

        support.setStrict(true);

        attributesJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attributeSelectionActionPerformed(evt);
            }
        });

        optionsJPanel.add(attributesJComboBox, BorderLayout.WEST);
    }

    public void init(String featureFilePath) {
        if (instancesSummaryPanel != null) {
            instancesJPanel.remove(instancesSummaryPanel);
        }

        instancesSummaryPanel = new InstancesSummaryPanel();

//        instancesSummaryPanel.setInstances(instances);

        instancesJPanel.add(instancesSummaryPanel, BorderLayout.CENTER);

        instancesSummaryPanel.addPropertyChangeListener(this);

        attribDetailJPanel.setVisible(false);

        if (attribSelectionPanel != null) {
            remove(attribSelectionPanel);
        }

        attribSelectionPanel = new AttributeSelectionPanel();

//        attribSelectionPanel.setInstances(getInstances());

        add(attribSelectionPanel, BorderLayout.CENTER);

        if (attribSummaryPanel != null) {
            attribDetailJPanel.remove(attribSummaryPanel);
        }

        attribSummaryPanel = new AttributeSummaryPanel();

//        attribSummaryPanel.setInstances(getInstances());

        attribDetailJPanel.add(attribSummaryPanel);

        if (attribVisPanel != null) {
            attribDetailJPanel.remove(attribVisPanel);
        }

        attribVisPanel = new AttributeVisualizationPanel();

//        attribVisPanel.setInstances(getInstances());

        attribDetailJPanel.add(attribVisPanel);

//        if(featureFilePath != null)
        if (MiscellaneousUtils.fileExists(featureFilePath)) {
            setInstances(HypothesisConfidence.getInstances(featureFilePath, null));
        } else {
            setInstances(instances);
        }

        attribSelectionPanel.getSelectionModel()
                .addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    ListSelectionModel lm = (ListSelectionModel) e.getSource();
                    for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++) {
                        if (lm.isSelectedIndex(i)) {
                            attribSummaryPanel.setAttribute(i);
                            attribVisPanel.setAttribute(i);

                            if (instances != null) {
                                currentAttribute = instances.attribute(i);
                            }

                            fireAttributeSelectionEvent();
                            break;
                        }
                    }
                }
            }
        });

        attribSummaryPanel.setAttribute(0);

        attribVisPanel.setAttribute(0);

        initComboBox(instances);
    }

    /**
     * Passes the dataset through the filter that has been configured for use.
     *
     * @param filter	the filter to apply
     */
    protected void applyFilter(final Filter filter) {

        if (m_IOThread == null) {
            m_IOThread = new Thread() {
                public void run() {
                    try {

                        if (filter != null) {

                            if (m_Log instanceof TaskLogger) {
                                ((TaskLogger) m_Log).taskStarted();
                            }
                            m_Log.statusMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Log_StatusMessage_Text_First")
                                    + filter.getClass().getName());
                            String cmd = filter.getClass().getName();
                            if (filter instanceof OptionHandler) {
                                cmd += " " + Utils.joinOptions(((OptionHandler) filter).getOptions());
                            }
                            m_Log.logMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Log_LogMessage_Text_First") + cmd);
                            int classIndex = attribVisPanel.getColoringIndex();
                            if ((classIndex < 0) && (filter instanceof SupervisedFilter)) {
                                throw new IllegalArgumentException(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_IllegalArgumentException_Text"));
                            }
                            Instances copy = new Instances(instances);
                            copy.setClassIndex(classIndex);
                            filter.setInputFormat(copy);
                            Instances newInstances = Filter.useFilter(copy, filter);
                            if (newInstances == null || newInstances.numAttributes() < 1) {
                                throw new Exception(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Exception_Text"));
                            }
                            m_Log.statusMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Log_StatusMessage_Text_Second"));
//	      addUndoPoint();
                            attribVisPanel.setColoringIndex(copy.classIndex());
                            // if class was not set before, reset it again after use of filter
                            if (instances.classIndex() < 0) {
                                newInstances.setClassIndex(-1);
                            }
                            instances = newInstances;
                            setInstances(instances);
                            if (m_Log instanceof TaskLogger) {
                                ((TaskLogger) m_Log).taskFinished();
                            }
                        }

                    } catch (Exception ex) {

                        if (m_Log instanceof TaskLogger) {
                            ((TaskLogger) m_Log).taskFinished();
                        }
                        // Pop up an error optionpane
                        JOptionPane.showMessageDialog(null,
                                weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_JOptionPaneShowMessageDialog_Text_First")
                                + ex.getMessage(),
                                weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_JOptionPaneShowMessageDialog_Text_Second"),
                                JOptionPane.ERROR_MESSAGE);
                        m_Log.logMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Log_LogMessage_Text_Second") + ex.getMessage());
                        m_Log.statusMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_Log_StatusMessage_Text_Third"));
                    }
                    m_IOThread = null;
                }
            };
            m_IOThread.setPriority(Thread.MIN_PRIORITY); // UI has most priority
            m_IOThread.start();
        } else {
            JOptionPane.showMessageDialog(this,
                    weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_JOptionPaneShowMessageDialog_Text_Third"),
                    weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_ApplyFilter_Run_JOptionPaneShowMessageDialog_Text_Fourth"),
                    JOptionPane.WARNING_MESSAGE);
        }

        m_Support.firePropertyChange("", null, null);
    }

    private void loadInstances() {
        instancesJPanel.remove(instancesSummaryPanel);

//        instancesSummaryPanel = new SetInstancesPanel();

        instancesSummaryPanel.setInstances(instances);

        instancesJPanel.add(instancesSummaryPanel, BorderLayout.CENTER);

        instancesSummaryPanel.setVisible(false);
        instancesSummaryPanel.setVisible(true);

        attribSelectionPanel.setInstances(getInstances());

        attribSelectionPanel.setVisible(false);
        attribSelectionPanel.setVisible(true);

        attribSummaryPanel.setInstances(getInstances());
        attribSummaryPanel.setAttribute(0);

        attribSummaryPanel.setVisible(false);
        attribSummaryPanel.setVisible(true);

        attribVisPanel.setInstances(getInstances());
        attribVisPanel.setAttribute(0);

        attribVisPanel.setVisible(false);
        attribVisPanel.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        instancesJPanel = new javax.swing.JPanel();
        cmdJPanel = new javax.swing.JPanel();
        optionsJPanel = new javax.swing.JPanel();
        filterJToggleButton = new javax.swing.JToggleButton();
        filterJScrollPane = new javax.swing.JScrollPane();
        filterJTextArea = new javax.swing.JTextArea();
        detailsJCheckBox = new javax.swing.JCheckBox();
        buttonsJPanel = new javax.swing.JPanel();
        removeJButton = new javax.swing.JButton();
        renameJButton = new javax.swing.JButton();
        makeClassJButton = new javax.swing.JButton();
        resetJButton = new javax.swing.JButton();
        browseJButton = new javax.swing.JButton();
        saveAsJButton = new javax.swing.JButton();
        explorerJButton = new javax.swing.JButton();
        attribDetailJPanel = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());
        add(instancesJPanel, java.awt.BorderLayout.CENTER);

        cmdJPanel.setLayout(new java.awt.GridLayout(0, 1));

        optionsJPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        filterJToggleButton.setText("Include");
        filterJToggleButton.setToolTipText("Filter attributes with this list (include or exclude)");
        filterJToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterJToggleButtonActionPerformed(evt);
            }
        });
        optionsJPanel.add(filterJToggleButton);

        filterJTextArea.setColumns(20);
        filterJTextArea.setRows(3);
        filterJTextArea.setToolTipText("Enter the list of attributes to be filtered here ...");
        filterJScrollPane.setViewportView(filterJTextArea);

        optionsJPanel.add(filterJScrollPane);

        detailsJCheckBox.setText("Details");
        detailsJCheckBox.setToolTipText("See attribute details");
        detailsJCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailsJCheckBoxActionPerformed(evt);
            }
        });
        optionsJPanel.add(detailsJCheckBox);

        cmdJPanel.add(optionsJPanel);

        buttonsJPanel.setLayout(new java.awt.GridLayout(1, 0));

        removeJButton.setText("Remove");
        removeJButton.setToolTipText("Remove attributes");
        removeJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(removeJButton);

        renameJButton.setText("Rename");
        renameJButton.setToolTipText("Remove attributes");
        renameJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                renameJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(renameJButton);

        makeClassJButton.setText("Make Class");
        makeClassJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                makeClassJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(makeClassJButton);

        resetJButton.setText("Reset");
        resetJButton.setToolTipText("Reload the attributes from the current file");
        resetJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(resetJButton);
        resetJButton.getAccessibleContext().setAccessibleName("Reset");
        resetJButton.getAccessibleContext().setAccessibleDescription("");

        browseJButton.setText("Browse");
        browseJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(browseJButton);

        saveAsJButton.setText("Save As");
        saveAsJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(saveAsJButton);

        explorerJButton.setText("Classifier");
        explorerJButton.setToolTipText("Open Weka classifier panel");
        explorerJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                explorerJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(explorerJButton);

        cmdJPanel.add(buttonsJPanel);

        add(cmdJPanel, java.awt.BorderLayout.SOUTH);

        attribDetailJPanel.setLayout(new java.awt.GridLayout(0, 1));
        add(attribDetailJPanel, java.awt.BorderLayout.EAST);
    }// </editor-fold>//GEN-END:initComponents

    private void detailsJCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailsJCheckBoxActionPerformed
        // TODO add your handling code here:
        attribDetailJPanel.setVisible(!attribDetailJPanel.isVisible());
    }//GEN-LAST:event_detailsJCheckBoxActionPerformed

    private void browseJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseJButtonActionPerformed
        String path = "~/";

        if (featureFilePath != null) {
            File featureFile = new File(featureFilePath);

            if (featureFile.exists()) {
                path = featureFile.getParentFile().getAbsolutePath();
            }
        }

        JFileChooser chooser;

        if (path != null) {
            chooser = new JFileChooser(path);
        } else {
            chooser = new JFileChooser();
        }

        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            featureFilePath = chooser.getSelectedFile().getAbsolutePath();
            init(featureFilePath);

            fireInstancesEvent();
        }
    }//GEN-LAST:event_browseJButtonActionPerformed

    private void removeJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeJButtonActionPerformed
        // TODO add your handling code here:
        try {
            Remove r = new Remove();
            int[] selected = attribSelectionPanel.getSelectedAttributes();
            if (selected.length == 0) {
                return;
            }
            if (selected.length == instances.numAttributes()) {
                // Pop up an error optionpane
                JOptionPane.showMessageDialog(null,
                        weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_JOptionPaneShowMessageDialog_Text_First"),
                        weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_JOptionPaneShowMessageDialog_Text_Second"),
                        JOptionPane.ERROR_MESSAGE);
                m_Log.logMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_Log_LogMessage_Text_First"));
                m_Log.statusMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_Log_StatusMessage_Text_First"));
                return;
            }

            r.setAttributeIndicesArray(selected);
            
            attribSelectionPanel.removeAll();
            
            Instances newInstances = new Instances(instances);
            
            try {
                r.setInputFormat(newInstances);

                instances = Filter.useFilter(newInstances, r);        
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            fireInstancesEvent();

            init(null);

        } catch (Exception ex) {
            if (m_Log instanceof TaskLogger) {
                ((TaskLogger) m_Log).taskFinished();
            }
            // Pop up an error optionpane
            JOptionPane.showMessageDialog(null,
                    weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_JOptionPaneShowMessageDialog_Text_Third")
                    + ex.getMessage(),
                    weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_JOptionPaneShowMessageDialog_Text_Fourth"),
                    JOptionPane.ERROR_MESSAGE);
            m_Log.logMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_Log_LogMessage_Text_Second") + ex.getMessage());
            m_Log.statusMessage(weka.gui.explorer.Messages.getInstance().getString("PreprocessPanel_Log_StatusMessage_Text_Second"));
        }
    }//GEN-LAST:event_removeJButtonActionPerformed

    private void makeClassJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_makeClassJButtonActionPerformed

        int[] selected = attribSelectionPanel.getSelectedAttributes();

        if (selected.length == 0) {
            return;
        }

        Copy copy = new Copy();

        try {
            copy.setAttributeIndicesArray(new int[]{selected[0]});
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        attribSelectionPanel.removeAll();

        Instances newInstances = new Instances(instances);

        try {
            copy.setInputFormat(newInstances);

            instances = Filter.useFilter(newInstances, copy);        
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        Remove remove = new Remove();
        remove.setAttributeIndicesArray(new int[]{selected[0]});

        newInstances = new Instances(instances);

        try {
            remove.setInputFormat(newInstances);

            instances = Filter.useFilter(newInstances, remove);        
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        }

        instances.setClassIndex(instances.numAttributes() - 1);

        Attribute attrib = instances.attribute(instances.numAttributes() - 1);

        instances.renameAttribute(attrib, "class");

        fireInstancesEvent();

        init(null);

    }//GEN-LAST:event_makeClassJButtonActionPerformed

    private void saveAsJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsJButtonActionPerformed
        String path = "~/";

        if (featureFilePath != null) {
            File featureFile = new File(featureFilePath);

            if (featureFile.exists()) {
                path = featureFile.getParentFile().getAbsolutePath();
            }
        }

        JFileChooser chooser;

        if (path != null) {
            chooser = new JFileChooser(path);
        } else {
            chooser = new JFileChooser();
        }

        int returnVal = chooser.showSaveDialog(this);

        FormatType.FeatureCollectionFormat format = FormatType.FeatureCollectionFormat.ARFF_FORMAT;

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            featureFilePath = chooser.getSelectedFile().getAbsolutePath();
            try {
                HypothesisConfidence.saveInstances(instances, featureFilePath, format);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_saveAsJButtonActionPerformed

    private void renameJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_renameJButtonActionPerformed
        int[] selected = attribSelectionPanel.getSelectedAttributes();
        if (selected.length == 0) {
            return;
        }

        Attribute attrib = instances.attribute(selected[0]);

        String name = JOptionPane.showInputDialog(this, "Type the new name:", "Rename Attribute", JOptionPane.QUESTION_MESSAGE);

        instances.renameAttribute(attrib, name);

        init(null);
    }//GEN-LAST:event_renameJButtonActionPerformed

    private void explorerJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_explorerJButtonActionPerformed
        // TODO add your handling code here:

        showExplorer(featureFilePath);

    }//GEN-LAST:event_explorerJButtonActionPerformed

    private void filterJToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterJToggleButtonActionPerformed
        // TODO add your handling code here:
        filterAttributes(!filterJToggleButton.isSelected());
        
        if(!filterJToggleButton.isSelected())
        {
            filterJToggleButton.setText("Include");
        }
        else
        {
            filterJToggleButton.setText("Exclude");
        }
        
    }//GEN-LAST:event_filterJToggleButtonActionPerformed

    private void resetJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetJButtonActionPerformed
        // TODO add your handling code here:
        init(featureFilePath);

        fireInstancesEvent();        
    }//GEN-LAST:event_resetJButtonActionPerformed

    private void filterAttributes(boolean exclude)
    {
        int count = instances.numAttributes();
                        

        boolean selected[] = new boolean[count];
        
        for (int i = 0; i < count; i++) {

            if(exclude)
            {                        
                selected[i] = true;
            }
            else
            {                        
                selected[i] = false;
            }            
        }
        
        String filterString = filterJTextArea.getText();
        
        if(!filterString.equals(""))
        {
            String anames[] = filterString.split("[\n]");
            
            for (int i = 0; i < anames.length; i++) {
                String string = anames[i];
                
                Attribute a = null;
                
                try
                {
                    a = instances.attribute(string);
                }
                catch(Exception ex)
                {
                    continue;
                }
                
                if(a != null)
                {
                    if(exclude)
                    {                        
                        selected[a.index()] = false;
                    }
                    else
                    {                        
                        selected[a.index()] = true;
                    }
                }
            }
            
            try {
                attribSelectionPanel.setSelectedAttributes(selected);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    int[] getSelectedAttributes() {
        int[] selected = attribSelectionPanel.getSelectedAttributes();

        return selected;
    }
    
    void setSelectedAttributes(boolean[] selected, boolean giveNames) {
        try {
            attribSelectionPanel.setSelectedAttributes(selected);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(giveNames)
        {
            String names = "";
            
            for (int i = 0; i < selected.length; i++) {
                boolean b = selected[i];
                
                if(b)
                {   
                    names += instances.attribute(i).name() + "\n";
                }
            }
            
            filterJTextArea.setText(names.trim());
        }
    }

    private void attributeSelectionActionPerformed(java.awt.event.ActionEvent evt) {
        int i = attributesJComboBox.getSelectedIndex();

        if (i >= 0 && i < instances.numAttributes()) {
            currentAttribute = instances.attribute(i);

            int selected[] = attribSelectionPanel.getSelectedAttributes();

//            attribSelectionPanel.

            attribSummaryPanel.setAttribute(i);
            attribVisPanel.setAttribute(i);

            fireAttributeSelectionEvent();
        }
    }

    public void showExplorer(String fileToLoad) {
        Explorer expl = null;
        if (m_ExplorerFrame == null) {
            explorerJButton.setEnabled(false);
            m_ExplorerFrame = new JFrame("Weka Explorer");
//      m_ExplorerFrame.setIconImage(m_Icon);
            m_ExplorerFrame.getContentPane().setLayout(new BorderLayout());
            expl = new Explorer();

            m_ExplorerFrame.getContentPane().add(expl, BorderLayout.CENTER);
            m_ExplorerFrame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent w) {
                    m_ExplorerFrame.dispose();
                    m_ExplorerFrame = null;
                    explorerJButton.setEnabled(true);
//            checkExit();
                }
            });
            m_ExplorerFrame.pack();
            m_ExplorerFrame.setSize(800, 600);
            m_ExplorerFrame.setVisible(true);
        } else {
            Object o = m_ExplorerFrame.getContentPane().getComponent(0);
            if (o instanceof Explorer) {
                expl = (Explorer) o;
            }
        }

        if (fileToLoad != null) {
            try {

                expl.getPreprocessPanel().setInstances(instances);

//        weka.core.converters.AbstractFileLoader loader = 
//          weka.core.converters.ConverterUtils.getLoaderForFile(fileToLoad);
//        loader.setFile(new File(fileToLoad));
//        expl.getPreprocessPanel().setInstancesFromFile(loader);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel attribDetailJPanel;
    private javax.swing.JButton browseJButton;
    private javax.swing.JPanel buttonsJPanel;
    private javax.swing.JPanel cmdJPanel;
    private javax.swing.JCheckBox detailsJCheckBox;
    private javax.swing.JButton explorerJButton;
    private javax.swing.JScrollPane filterJScrollPane;
    private javax.swing.JTextArea filterJTextArea;
    private javax.swing.JToggleButton filterJToggleButton;
    private javax.swing.JPanel instancesJPanel;
    private javax.swing.JButton makeClassJButton;
    private javax.swing.JPanel optionsJPanel;
    private javax.swing.JButton removeJButton;
    private javax.swing.JButton renameJButton;
    private javax.swing.JButton resetJButton;
    private javax.swing.JButton saveAsJButton;
    // End of variables declaration//GEN-END:variables

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        //Create and set up the window.
        JFrame frame = new JFrame("Attribute Manager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        AttributeViewerJPanel panel = new AttributeViewerJPanel();

//        panel.init("examples/features-baseline.arff");
        panel.init("/people/anil/work/confidence-estimation/mtce-features/features-baseline.arff");

        panel.setOpaque(true); //content panes must be opaque
        frame.setContentPane(panel);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * @return the featureFilePath
     */
    public String getFeatureFilePath() {
        return featureFilePath;
    }

    /**
     * @param featureFilePath the featureFilePath to set
     */
    public void setFeatureFilePath(String featureFilePath) {
        this.featureFilePath = featureFilePath;
    }

    /**
     * @return the instances
     */
    public Instances getInstances() {
        return instances;
    }

    /**
     * @param instances the instances to set
     */
    public void setInstances(Instances instances) {

        if (instances == null) {
            // Declare two numeric attributes
            Attribute Attribute1 = new Attribute("dummyAttribute1");
            Attribute Attribute2 = new Attribute("dummyAttribute2");

            // Declare a nominal attribute along with its values
//            FastVector fvNominalVal = new FastVector(3);
//            fvNominalVal.addElement("blue");
//            fvNominalVal.addElement("gray");
//            fvNominalVal.addElement("black");
//            Attribute Attribute3 = new Attribute("aNominal", "fvNominalVal");

            // Declare the class attribute along with its values
            FastVector fvClassVal = new FastVector(2);
            fvClassVal.addElement("positive");
            fvClassVal.addElement("negative");
            Attribute ClassAttribute = new Attribute("browse-file-click-on-the-Browse-button", fvClassVal);

            // Declare the feature vector
            FastVector fvWekaAttributes = new FastVector(3);
            fvWekaAttributes.addElement(Attribute1);
            fvWekaAttributes.addElement(Attribute2);
//            fvWekaAttributes.addElement(Attribute3);    
            fvWekaAttributes.addElement(ClassAttribute);

            // Create an empty training set
            instances = new Instances("Rel", fvWekaAttributes, 10);
            // Set class index
            instances.setClassIndex(2);

            // Create the instance
            Instance iExample = new Instance(4);
            iExample.setValue((Attribute) fvWekaAttributes.elementAt(0), 1.0);
            iExample.setValue((Attribute) fvWekaAttributes.elementAt(1), 0.5);
//            iExample.setValue((Attribute)fvWekaAttributes.elementAt(2), "gray");
            iExample.setValue((Attribute) fvWekaAttributes.elementAt(2), "positive");

            // add the instance
            instances.add(iExample);
        }

        this.instances = instances;

        attribSelectionPanel.setInstances(instances);
        attribVisPanel.setInstances(instances);
        attribSummaryPanel.setInstances(instances);
        instancesSummaryPanel.setInstances(instances);

        initComboBox(instances);
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
//        Object oldProp = pce.getOldValue();
//        
//        if(oldProp instanceof Instances)
//        {
//            instances = (Instances) pce.getNewValue();
//        }
//        Object source = pce.getSource();
//        
//        if(source == instancesSummaryPanel)
//        {        
//            instances = ((SetInstancesPanel) source).getInstances();
//            
//            loadInstances();
//            
//            setVisible(false);
//            setVisible(true);
//        }
    }

    public synchronized void addEventListener(AttributeSelectionListener listener) {
        _listeners.add(listener);
    }

    public synchronized void removeEventListener(AttributeSelectionListener listener) {
        _listeners.remove(listener);
    }

    private synchronized void fireAttributeSelectionEvent() {
        AttributeSelectionEvent event = new AttributeSelectionEvent(this);

        Iterator i = _listeners.iterator();

        while (i.hasNext()) {
            ((AttributeSelectionListener) i.next()).attributeSelectionChanged(event);
        }
    }

    private synchronized void fireInstancesEvent() {
        InstancesEvent event = new InstancesEvent(this);

        Iterator i = _listeners.iterator();

        while (i.hasNext()) {
            ((InstancesListener) i.next()).newSetOfInstances(event);
        }
    }
}
