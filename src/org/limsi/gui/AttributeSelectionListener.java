/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.gui;

import java.util.EventObject;

/**
 *
 * @author anil
 */
public interface AttributeSelectionListener {
    public void attributeSelectionChanged(EventObject source);
}
