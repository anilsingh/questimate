/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.gui;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import edu.uci.lasso.LassoFit;
import java.awt.BorderLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javastat.inference.onesample.OneSampMeanTTest;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.editor.ChartEditor;
import org.limsi.cm.util.InstancesUtils;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.HypothesisConfidence;
import org.limsi.mt.cm.stats.BasicStatistics;
import org.limsi.mt.cm.stats.StatisticalTests;
import sanchay.table.SanchayTableModel;
import sanchay.table.gui.SanchayTableJPanel;
import sanchay.text.editor.gui.TextEditorJPanel;
import weka.core.Attribute;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class AttributesStatisticsJPanel extends javax.swing.JPanel {

    private Instances instances;
    private String featureFilePath = "/people/anil/work/confidence-estimation/mtce-features/features-baseline.arff";
    
    private Attribute xAttribute;
    private Attribute yAttribute;
    
    private ChartPanel chartPanel;
    private ChartEditor chartEditor;
    private JFreeChart plot;
    
    private String langEnc = "eng::utf8";
    private String charset = "UTF-8";
    
    private TextEditorJPanel textEditorJPanel;
    private SanchayTableJPanel tableJPanel;
    private SanchayTableModel tableModel;
    
    private BasicStatistics basicStatistics;
    
    final static String LOOKANDFEEL = null;
    
    public final static int XCOMBOBOX = 0;
    public final static int YCOMBOBOX = 1;
    public final static int CONDITION_COMBOBOX = 2;

    /**
     * Creates new form AttributesPlotJPanel
     */
    public AttributesStatisticsJPanel() {
        initComponents();
        
        textEditorJPanel = new TextEditorJPanel(langEnc, charset, null, null, TextEditorJPanel.MINIMAL_MODE);

        testJPanel.add(textEditorJPanel, BorderLayout.CENTER);
        
        tableModel = new SanchayTableModel(BasicStatistics.STATISTICS, 2);
        tableJPanel = SanchayTableJPanel.createFeatureTableJPanel(tableModel, langEnc);
        
        tableJPanel.showCommandButtons(false);

        statsJPanel.add(tableJPanel, BorderLayout.CENTER);
    }

    public void init(String featureFilePath)
    {
        if(MiscellaneousUtils.fileExists(featureFilePath))
        {
            instances = HypothesisConfidence.getInstances(featureFilePath, null);
            
            setInstances(instances);
        }        
    }   
    
    public void setInstances(Instances instances)
    {            
        this.instances = instances;
        
        if(instances != null)
        {
//            xJComboBox.removeAllItems();
//            yJComboBox.removeAllItems();
//
//            xConditionJComboBox.removeAllItems();
//
//            int acount = instances.numAttributes();
//
//            for (int i = 0; i < acount; i++) {
//                Attribute a = instances.attribute(i);
//
//                xJComboBox.addItem(a);
//                xConditionJComboBox.addItem(a);
//                yJComboBox.addItem(a);
//            }
            
            initComboBox(instances, XCOMBOBOX);
            initComboBox(instances, YCOMBOBOX);
            initComboBox(instances, CONDITION_COMBOBOX);            
        }      
        
        updateUI();
    }

    private void initComboBox(Instances instances, int type) {
        
        JComboBox comboBox = null;
        
        if(type == XCOMBOBOX)
        {
            comboBox = xJComboBox;
        }
        else if(type == YCOMBOBOX)
        {
            comboBox = yJComboBox;
        }
        else if(type == CONDITION_COMBOBOX)
        {
            comboBox = xConditionJComboBox;
        }

        String attributes[] = new String[10];

        if (instances != null) {
            int acount = instances.numAttributes();

            attributes = new String[acount];

            for (int i = 0; i < acount; i++) {
                Attribute a = instances.attribute(i);

                attributes[i] = a.name();
            }
        }

        AutoCompleteSupport support = AutoCompleteSupport.install(
                comboBox, GlazedLists.eventListOf(attributes));

        support.setStrict(true);

        comboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attributeSelectionActionPerformed(evt);
            }
        });
    }

    private void attributeSelectionActionPerformed(java.awt.event.ActionEvent evt) {
//        int i = attributesJComboBox.getSelectedIndex();
//
//        if (i >= 0 && i < instances.numAttributes()) {
//            currentAttribute = instances.attribute(i);
//
//            int selected[] = attribSelectionPanel.getSelectedAttributes();
//
////            attribSelectionPanel.
//
//            attribSummaryPanel.setAttribute(i);
//            attribVisPanel.setAttribute(i);
//
//            fireAttributeSelectionEvent();
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        testJPanel = new javax.swing.JPanel();
        statsJPanel = new javax.swing.JPanel();
        attribsJPanel = new javax.swing.JPanel();
        xJPanel = new javax.swing.JPanel();
        xJLabel = new javax.swing.JLabel();
        xJComboBox = new javax.swing.JComboBox();
        xJCheckBox = new javax.swing.JCheckBox();
        yJPanel = new javax.swing.JPanel();
        yJLabel = new javax.swing.JLabel();
        yJComboBox = new javax.swing.JComboBox();
        yJCheckBox = new javax.swing.JCheckBox();
        xConditionJPanel = new javax.swing.JPanel();
        xConditionJLabel = new javax.swing.JLabel();
        xConditionJComboBox = new javax.swing.JComboBox();
        xConditionJTextField = new javax.swing.JTextField();
        typeJPanel = new javax.swing.JPanel();
        typeJLabel = new javax.swing.JLabel();
        typeJComboBox = new javax.swing.JComboBox();
        commandsJPanel = new javax.swing.JPanel();
        optionsJPanel = new javax.swing.JPanel();
        detailsJCheckBox = new javax.swing.JCheckBox();
        reverseJCheckBox = new javax.swing.JCheckBox();
        buttonsJPanel = new javax.swing.JPanel();
        updateJButton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        testJPanel.setLayout(new java.awt.BorderLayout());
        add(testJPanel, java.awt.BorderLayout.CENTER);

        statsJPanel.setLayout(new java.awt.BorderLayout());

        attribsJPanel.setLayout(new java.awt.GridLayout(0, 1, 0, 2));

        xJPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        xJLabel.setLabelFor(xJComboBox);
        xJLabel.setText("x: ");
        xJPanel.add(xJLabel);

        xJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xJComboBoxActionPerformed(evt);
            }
        });
        xJPanel.add(xJComboBox);

        xJCheckBox.setSelected(true);
        xJCheckBox.setText("On");
        xJCheckBox.setToolTipText("Select this attribute for the plot");
        xJCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xJCheckBoxActionPerformed(evt);
            }
        });
        xJPanel.add(xJCheckBox);

        attribsJPanel.add(xJPanel);

        yJPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        yJLabel.setLabelFor(yJComboBox);
        yJLabel.setText("y: ");
        yJPanel.add(yJLabel);

        yJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yJComboBoxActionPerformed(evt);
            }
        });
        yJPanel.add(yJComboBox);

        yJCheckBox.setSelected(true);
        yJCheckBox.setText("On");
        yJCheckBox.setToolTipText("Select this attribute for the plot");
        yJCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yJCheckBoxActionPerformed(evt);
            }
        });
        yJPanel.add(yJCheckBox);

        attribsJPanel.add(yJPanel);

        xConditionJPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        xConditionJLabel.setText("x = ");
        xConditionJLabel.setToolTipText("Select instances based on value of x attribute");
        xConditionJPanel.add(xConditionJLabel);

        xConditionJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xConditionJComboBoxActionPerformed(evt);
            }
        });
        xConditionJPanel.add(xConditionJComboBox);

        xConditionJTextField.setToolTipText("Type the value of the x attribute to take subset of instances with that value");
        xConditionJTextField.setPreferredSize(new java.awt.Dimension(100, 31));
        xConditionJPanel.add(xConditionJTextField);

        attribsJPanel.add(xConditionJPanel);

        statsJPanel.add(attribsJPanel, java.awt.BorderLayout.NORTH);

        typeJPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        typeJLabel.setLabelFor(typeJComboBox);
        typeJLabel.setText("Test:  ");
        typeJPanel.add(typeJLabel);

        typeJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "One Sample Mean T-Test", "Lasso Feature Selection" }));
        typeJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeJComboBoxActionPerformed(evt);
            }
        });
        typeJPanel.add(typeJComboBox);

        statsJPanel.add(typeJPanel, java.awt.BorderLayout.SOUTH);

        add(statsJPanel, java.awt.BorderLayout.EAST);

        commandsJPanel.setLayout(new java.awt.GridLayout(0, 1));

        detailsJCheckBox.setSelected(true);
        detailsJCheckBox.setText("Details");
        detailsJCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailsJCheckBoxActionPerformed(evt);
            }
        });
        optionsJPanel.add(detailsJCheckBox);

        reverseJCheckBox.setText("Reverse Axes");
        reverseJCheckBox.setToolTipText("Plot y vs. x");
        reverseJCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reverseJCheckBoxActionPerformed(evt);
            }
        });
        optionsJPanel.add(reverseJCheckBox);

        commandsJPanel.add(optionsJPanel);

        updateJButton.setText("Update");
        updateJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateJButtonActionPerformed(evt);
            }
        });
        buttonsJPanel.add(updateJButton);

        commandsJPanel.add(buttonsJPanel);

        add(commandsJPanel, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void xJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xJComboBoxActionPerformed
        // TODO add your handling code here:
        if(instances == null)
        {
            return;
        }
        
        String aname = (String) xJComboBox.getSelectedItem();
        
        xAttribute = instances.attribute(aname);
        
        calculateStatistics(xAttribute, yAttribute);
    }//GEN-LAST:event_xJComboBoxActionPerformed

    private void yJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yJComboBoxActionPerformed
        // TODO add your handling code here:
        if(instances == null)
        {
            return;
        }

        String aname = (String) yJComboBox.getSelectedItem();
        
        yAttribute = instances.attribute(aname);
        
        calculateStatistics(xAttribute, yAttribute);        
    }//GEN-LAST:event_yJComboBoxActionPerformed

    private void typeJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeJComboBoxActionPerformed
        // TODO add your handling code here:
        performTest(xAttribute, yAttribute);                
    }//GEN-LAST:event_typeJComboBoxActionPerformed

    private void updateJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateJButtonActionPerformed
        // TODO add your handling code here:
        calculateStatistics(xAttribute, yAttribute);
    }//GEN-LAST:event_updateJButtonActionPerformed

    private void detailsJCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailsJCheckBoxActionPerformed
        // TODO add your handling code here:
        if(detailsJCheckBox.isSelected())
        {
            statsJPanel.setVisible(true);
        }
        else
        {
            statsJPanel.setVisible(false);            
        }
    }//GEN-LAST:event_detailsJCheckBoxActionPerformed

    private void reverseJCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reverseJCheckBoxActionPerformed
        // TODO add your handling code here:
        Attribute a = xAttribute;
        xAttribute = yAttribute;
        yAttribute = a;
        
        calculateStatistics(xAttribute, yAttribute);                        
    }//GEN-LAST:event_reverseJCheckBoxActionPerformed

    private void xJCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xJCheckBoxActionPerformed
        // TODO add your handling code here:
        calculateStatistics(xAttribute, yAttribute);                                
    }//GEN-LAST:event_xJCheckBoxActionPerformed

    private void yJCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yJCheckBoxActionPerformed
        // TODO add your handling code here:
        calculateStatistics(xAttribute, yAttribute);                                
    }//GEN-LAST:event_yJCheckBoxActionPerformed

    private void xConditionJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xConditionJComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_xConditionJComboBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel attribsJPanel;
    private javax.swing.JPanel buttonsJPanel;
    private javax.swing.JPanel commandsJPanel;
    private javax.swing.JCheckBox detailsJCheckBox;
    private javax.swing.JPanel optionsJPanel;
    private javax.swing.JCheckBox reverseJCheckBox;
    private javax.swing.JPanel statsJPanel;
    private javax.swing.JPanel testJPanel;
    private javax.swing.JComboBox typeJComboBox;
    private javax.swing.JLabel typeJLabel;
    private javax.swing.JPanel typeJPanel;
    private javax.swing.JButton updateJButton;
    private javax.swing.JComboBox xConditionJComboBox;
    private javax.swing.JLabel xConditionJLabel;
    private javax.swing.JPanel xConditionJPanel;
    private javax.swing.JTextField xConditionJTextField;
    private javax.swing.JCheckBox xJCheckBox;
    private javax.swing.JComboBox xJComboBox;
    private javax.swing.JLabel xJLabel;
    private javax.swing.JPanel xJPanel;
    private javax.swing.JCheckBox yJCheckBox;
    private javax.swing.JComboBox yJComboBox;
    private javax.swing.JLabel yJLabel;
    private javax.swing.JPanel yJPanel;
    // End of variables declaration//GEN-END:variables

    private void calculateStatistics(Attribute x, Attribute y)
    {
        if(x == null || y == null || instances == null)
        {
            return;
        }
        
        double[] xValues;
        double[] yValues;        
        
        String aname =  (String) xConditionJComboBox.getSelectedItem();

        Attribute xConditionAttribute = instances.attribute(aname);
        
        String conditionValue = xConditionJTextField.getText();

        if(xConditionAttribute == null || conditionValue == null || conditionValue.equals(""))
        {
            xValues = InstancesUtils.getValues(instances, x);
            yValues = InstancesUtils.getValues(instances, y);
        }
        else
        {
            xValues = InstancesUtils.getValues(instances, x, xConditionAttribute, conditionValue);
            yValues = InstancesUtils.getValues(instances, y, xConditionAttribute, conditionValue);
        }
       
        if(xValues != null && xValues.length > 0)
        {
            basicStatistics = new BasicStatistics(xValues);

            tableModel = basicStatistics.getStatisticsTable();

            tableJPanel.setModel(tableModel);

            String type = (String) typeJComboBox.getSelectedItem();

    //        if(type.equalsIgnoreCase("Scatter Plot"))
    //        {
    //            plot = PlotUtils.createScatterPlot(instances, x, y);
    //        }

            performTest(x, y);
        }
        else
        {
            tableModel = new SanchayTableModel(2, 2);

            tableJPanel.setModel(tableModel);            

            String result = "No matching instance found\n";
            
            textEditorJPanel.setText(result);
        }

        updateUI();
    }

    private void performTest(Attribute x, Attribute y)
    {
        if(x == null || y == null || instances == null)
        {
            return;
        }

        String aname =  (String) xConditionJComboBox.getSelectedItem();

        Attribute xConditionAttribute = instances.attribute(aname);
        
        String conditionValue = xConditionJTextField.getText();
       
        String test = (String) typeJComboBox.getSelectedItem();
        
        if(test.equalsIgnoreCase("One Sample Mean T-Test"))
        {
            OneSampMeanTTest ttest;

            if(xConditionAttribute == null || conditionValue == null || conditionValue.equals(""))
            {
                ttest = StatisticalTests.oneSampleTTest(instances, xAttribute);
            }
            else
            {
                ttest = StatisticalTests.oneSampleTTest(instances, xAttribute, xConditionAttribute, conditionValue);
            }
            
            String result = "";
            
            result += "Test statistic: " + ttest.testStatistic + "\n\n";
            result += "p-value: " + ttest.pValue + "\n\n";
            result += "Confidence interval: " + ttest.confidenceInterval[0] + " - " + ttest.confidenceInterval[1] + "\n\n";
            result += "u0: " + ttest.u0 + "\n\n";
            result += "Point estimate: " + ttest.pointEstimate + "\n\n";
            result += "Sample size: " + ttest.data.length + "\n\n";
            
            textEditorJPanel.setText(result);
        }
        else if(test.equalsIgnoreCase("Lasso Feature Selection"))
        {
            try {
                LassoFit lassoFit = StatisticalTests.lassoFeatureSelection(instances, yAttribute);
                
                String result = "Feature weights:\n\n";
                
                double weights[] = lassoFit.getWeights(lassoFit.numberOfLambdas - 1);
                
                for (int i = 0; i < weights.length; i++) {
                    double d = weights[i];

                    result += instances.attribute(i).name()
                            + "\t" + d + "\n";
                }

                result += "\n\n\n" + lassoFit.toString();
                
                textEditorJPanel.setText(result);
                
            } catch (Exception ex) {
                Logger.getLogger(AttributesStatisticsJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
        updateUI();
    }
    
    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        //Create and set up the window.
        JFrame frame = new JFrame("Attributes Statistics");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        AttributesStatisticsJPanel panel = new AttributesStatisticsJPanel();
        
        panel.init("examples/features-baseline.arff");
//        panel.init("/extra/work/mtce-features/features-BL-e1e2e3.arff");
//        panel.init("/people/anil/work/confidence-estimation/mtce-features/features-baseline.arff");
        
        panel.setOpaque(true); //content panes must be opaque
        frame.setContentPane(panel);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
    }
}
