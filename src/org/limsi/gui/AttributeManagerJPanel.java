/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.gui;

import java.awt.BorderLayout;
import java.util.EventObject;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.limsi.cm.util.InstancesUtils;
import org.limsi.mt.cm.HypothesisConfidence;
import sanchay.util.Pair;
import weka.core.Attribute;
import weka.core.Instances;
import weka.gui.GenericObjectEditor;
import weka.gui.arffviewer.ArffPanel;
import weka.gui.visualize.VisualizePanel;

/**
 *
 * @author anil
 */
public class AttributeManagerJPanel extends javax.swing.JPanel
        implements AttributeSelectionListener, InstancesListener {
    
    protected String leftFeatureFilePath = "tmp/features-BL-IBM1.arff";
    protected String rightFeatureFilePath = "tmp/features-BL-POSLM.arff";

    //Specify the look and feel to use.  Valid values:
    //null (use the default), "Metal", "System", "Motif", "GTK+"
    final static String LOOKANDFEEL = null;

    private AttributeViewerJPanel attributeViewerJPanel;
    
    private AttributeViewerJPanel leftAttributeViewerJPanel;
    private AttributeViewerJPanel rightAttributeViewerJPanel;

    private AttributesPlotJPanel plotterJPanel;
    private AttributesStatisticsJPanel statisticsJPanel;
    private VisualizePanel visualizerPanel;
    
    private weka.gui.explorer.VisualizePanel matrixVisualizerPanel;
    
    private ArffPanel dataViewerPanel;

    /**
     * Creates new form AttributeManagerJPanel
     */
    public AttributeManagerJPanel() {
        initComponents();
        
        init();
    }
    
    private void init()
    {   
        GenericObjectEditor.registerEditors();
        
        attributeViewerJPanel = new AttributeViewerJPanel();     
        attributeViewerJPanel.addEventListener(this);
        
        leftAttributeViewerJPanel = new AttributeViewerJPanel();
        rightAttributeViewerJPanel = new AttributeViewerJPanel();

        attributeViewerJPanel.init(leftFeatureFilePath);

        leftAttributeViewerJPanel.init(leftFeatureFilePath);
        rightAttributeViewerJPanel.init(rightFeatureFilePath);
        
        attributeDetailsJPanel.add(attributeViewerJPanel, BorderLayout.CENTER);
        
        leftMergeJPanel.add(leftAttributeViewerJPanel, BorderLayout.CENTER);
        rightMergeJPanel.add(rightAttributeViewerJPanel, BorderLayout.CENTER);
        
        plotterJPanel = new AttributesPlotJPanel();
        plotterJPanel.init(leftFeatureFilePath);
        
        statisticsJPanel = new AttributesStatisticsJPanel();
        statisticsJPanel.init(leftFeatureFilePath);
        
        visualizerPanel = new VisualizePanel();
        visualizerPanel.setInstances(leftAttributeViewerJPanel.getInstances());
        
        visualizeJPanel.add(visualizerPanel, BorderLayout.CENTER);
        
        matrixVisualizerPanel = new weka.gui.explorer.VisualizePanel();
        
        matrixVisualizerPanel.setInstances(leftAttributeViewerJPanel.getInstances());
        
        matrixVisualizeJPanel.add(matrixVisualizerPanel, BorderLayout.CENTER);
        
        dataViewerPanel = new ArffPanel();
        
        dataViewerPanel.setInstances(leftAttributeViewerJPanel.getInstances());
        
        dataJPanel.add(dataViewerPanel, BorderLayout.CENTER);
        
    }

    @Override
    public void attributeSelectionChanged(EventObject source) {
    }

    @Override
    public void newSetOfInstances(EventObject source) {

        if(plotterJPanel != null)
        {
            plotJPanel.remove(plotterJPanel);
    
            plotterJPanel = new AttributesPlotJPanel();
            
            plotterJPanel.setInstances(attributeViewerJPanel.getInstances());

            plotJPanel.add(plotterJPanel, BorderLayout.CENTER);
        }

        if(statisticsJPanel != null)
        {
            analysisJPanel.remove(statisticsJPanel);
    
            statisticsJPanel = new AttributesStatisticsJPanel();
            
            statisticsJPanel.setInstances(attributeViewerJPanel.getInstances());

            analysisJPanel.add(statisticsJPanel, BorderLayout.CENTER);
        }

        if(visualizerPanel != null)
        {
            visualizeJPanel.remove(visualizerPanel);
    
            visualizerPanel = new VisualizePanel();
            
            visualizerPanel.setInstances(attributeViewerJPanel.getInstances());

            visualizeJPanel.add(visualizerPanel, BorderLayout.CENTER);
        }
        
        if(matrixVisualizerPanel != null)
        {
            matrixVisualizeJPanel.remove(matrixVisualizerPanel);
    
            matrixVisualizerPanel = new weka.gui.explorer.VisualizePanel();
            
            matrixVisualizerPanel.setInstances(attributeViewerJPanel.getInstances());

            matrixVisualizeJPanel.add(matrixVisualizerPanel, BorderLayout.CENTER);
        }

        if(dataViewerPanel != null)
        {
            dataJPanel.remove(dataViewerPanel);
    
            dataViewerPanel = new ArffPanel();
            
            dataViewerPanel.setInstances(attributeViewerJPanel.getInstances());

            dataJPanel.add(dataViewerPanel, BorderLayout.CENTER);
        }
        
        updateUI();        
    }
        

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainJTabbedPane = new javax.swing.JTabbedPane();
        attributeDetailsJPanel = new javax.swing.JPanel();
        plotJPanel = new javax.swing.JPanel();
        analysisJPanel = new javax.swing.JPanel();
        mergeAttributesJPanel = new javax.swing.JPanel();
        mergeAttributesJSplitPane = new javax.swing.JSplitPane();
        leftMergeJPanel = new javax.swing.JPanel();
        rightMergeJPanel = new javax.swing.JPanel();
        mergeCmdJPanel = new javax.swing.JPanel();
        copyLeftJButton = new javax.swing.JButton();
        diffJButton = new javax.swing.JButton();
        copyRightJButton = new javax.swing.JButton();
        visualizeJPanel = new javax.swing.JPanel();
        matrixVisualizeJPanel = new javax.swing.JPanel();
        dataJPanel = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        attributeDetailsJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("View", null, attributeDetailsJPanel, "View attributes and their details");

        plotJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("Plots", plotJPanel);

        analysisJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("Statistics", analysisJPanel);

        mergeAttributesJPanel.setLayout(new java.awt.BorderLayout());

        mergeAttributesJSplitPane.setDividerSize(4);

        leftMergeJPanel.setLayout(new java.awt.BorderLayout());
        mergeAttributesJSplitPane.setLeftComponent(leftMergeJPanel);

        rightMergeJPanel.setLayout(new java.awt.BorderLayout());
        mergeAttributesJSplitPane.setRightComponent(rightMergeJPanel);

        mergeAttributesJPanel.add(mergeAttributesJSplitPane, java.awt.BorderLayout.CENTER);

        mergeCmdJPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        mergeCmdJPanel.setLayout(new java.awt.GridLayout(1, 0));

        copyLeftJButton.setText("Copy Left");
        copyLeftJButton.setToolTipText("Copy attributes from the left pane to the right");
        copyLeftJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyLeftJButtonActionPerformed(evt);
            }
        });
        mergeCmdJPanel.add(copyLeftJButton);

        diffJButton.setText("Diff");
        diffJButton.setToolTipText("Select attributes which are not common");
        diffJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diffJButtonActionPerformed(evt);
            }
        });
        mergeCmdJPanel.add(diffJButton);

        copyRightJButton.setText("Copy Right");
        copyRightJButton.setToolTipText("Copy attributes from the right pane to the left");
        copyRightJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyRightJButtonActionPerformed(evt);
            }
        });
        mergeCmdJPanel.add(copyRightJButton);

        mergeAttributesJPanel.add(mergeCmdJPanel, java.awt.BorderLayout.SOUTH);

        mainJTabbedPane.addTab("Merge", null, mergeAttributesJPanel, "Merge attributes");

        visualizeJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("Visualize", null, visualizeJPanel, "Visualize attribute relations");

        matrixVisualizeJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("Matrix Visualize", matrixVisualizeJPanel);

        dataJPanel.setLayout(new java.awt.BorderLayout());
        mainJTabbedPane.addTab("Data", null, dataJPanel, "View the instances data");

        add(mainJTabbedPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void copyLeftJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyLeftJButtonActionPerformed
        int [] selected = rightAttributeViewerJPanel.getSelectedAttributes();

        if (selected.length == 0) {
          return;
        }

        Instances fromInstances = rightAttributeViewerJPanel.getInstances();
        Instances toInstances = leftAttributeViewerJPanel.getInstances();

        HypothesisConfidence.copyAttributes(selected, fromInstances, toInstances);        
        
        leftAttributeViewerJPanel.init(null);
    }//GEN-LAST:event_copyLeftJButtonActionPerformed

    private void copyRightJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyRightJButtonActionPerformed
        int [] selected = leftAttributeViewerJPanel.getSelectedAttributes();

        if (selected.length == 0) {
          return;
        }

        Instances fromInstances = leftAttributeViewerJPanel.getInstances();
        Instances toInstances = rightAttributeViewerJPanel.getInstances();

        HypothesisConfidence.copyAttributes(selected, fromInstances, toInstances);        
        
        rightAttributeViewerJPanel.init(null);
    }//GEN-LAST:event_copyRightJButtonActionPerformed

    private void diffJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diffJButtonActionPerformed
        // TODO add your handling code here:
        Instances instancesLeft = leftAttributeViewerJPanel.getInstances();
        Instances instancesRight = rightAttributeViewerJPanel.getInstances();
        
        Pair<List<Attribute>, List<Attribute>> attributeDiff = InstancesUtils.getAttributeDiff(instancesLeft,
                instancesRight);
        
        leftAttributeViewerJPanel.setSelectedAttributes(InstancesUtils.getSelectedList(attributeDiff.first, instancesLeft), true);
        rightAttributeViewerJPanel.setSelectedAttributes(InstancesUtils.getSelectedList(attributeDiff.second, instancesRight), true);
        
    }//GEN-LAST:event_diffJButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel analysisJPanel;
    private javax.swing.JPanel attributeDetailsJPanel;
    private javax.swing.JButton copyLeftJButton;
    private javax.swing.JButton copyRightJButton;
    private javax.swing.JPanel dataJPanel;
    private javax.swing.JButton diffJButton;
    private javax.swing.JPanel leftMergeJPanel;
    private javax.swing.JTabbedPane mainJTabbedPane;
    private javax.swing.JPanel matrixVisualizeJPanel;
    private javax.swing.JPanel mergeAttributesJPanel;
    private javax.swing.JSplitPane mergeAttributesJSplitPane;
    private javax.swing.JPanel mergeCmdJPanel;
    private javax.swing.JPanel plotJPanel;
    private javax.swing.JPanel rightMergeJPanel;
    private javax.swing.JPanel visualizeJPanel;
    // End of variables declaration//GEN-END:variables

    private static void initLookAndFeel() {
        String lookAndFeel = null;
 
        if (LOOKANDFEEL != null) {
            if (LOOKANDFEEL.equals("Metal")) {
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            } else if (LOOKANDFEEL.equals("System")) {
                lookAndFeel = UIManager.getSystemLookAndFeelClassName();
            } else if (LOOKANDFEEL.equals("Motif")) {
                lookAndFeel = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
            } else if (LOOKANDFEEL.equals("GTK+")) { //new in 1.4.2
                lookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
            } else {
                System.err.println("Unexpected value of LOOKANDFEEL specified: "
                                   + LOOKANDFEEL);
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            }
 
            try {
                UIManager.setLookAndFeel(lookAndFeel);
            } catch (ClassNotFoundException e) {
                System.err.println("Couldn't find class for specified look and feel:"
                                   + lookAndFeel);
                System.err.println("Did you include the L&F library in the class path?");
                System.err.println("Using the default look and feel.");
            } catch (UnsupportedLookAndFeelException e) {
                System.err.println("Can't use the specified look and feel ("
                                   + lookAndFeel
                                   + ") on this platform.");
                System.err.println("Using the default look and feel.");
            } catch (Exception e) {
                System.err.println("Couldn't get specified look and feel ("
                                   + lookAndFeel
                                   + "), for some reason.");
                System.err.println("Using the default look and feel.");
                e.printStackTrace();
            }
        }
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Set the look and feel.
        initLookAndFeel();

        //Create and set up the window.
        JFrame frame = new JFrame("Attribute Manager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        AttributeManagerJPanel panel = new AttributeManagerJPanel();
        panel.setOpaque(true); //content panes must be opaque
        frame.setContentPane(panel);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
    }
}
