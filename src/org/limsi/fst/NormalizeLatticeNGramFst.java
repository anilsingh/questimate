/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.fst;

import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.openfst.Convert;
import edu.cmu.sphinx.fst.semiring.LogSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import jopenfst.ds.Push;
import jopenfst.ds.Reweight;

/**
 *
 * @author anil
 */
public class NormalizeLatticeNGramFst {

    private Fst fst;
    private Semiring semiring;
    
    public NormalizeLatticeNGramFst() {
        
    }
    
    public void readFST(String fstBasename, Semiring sr)
    {
        semiring = sr;
        fst = Convert.importFst(fstBasename, semiring);
    }

    public void writeFST(String fstBasename)
    {
        Convert.export(fst, fstBasename);
    }
    
    public void normalize()
    {
        Push.push(fst, Reweight.ReweightType.REWEIGHT_TO_INITIAL, Semiring.kDelta, true);
//        Push.push(fst, Reweight.ReweightType.REWEIGHT_TO_INITIAL, Semiring.kDelta, false);
    }
    
    public static void main(String args[])
    {
        NormalizeLatticeNGramFst normalizeLatticeNGramFst = new NormalizeLatticeNGramFst();
        
        normalizeLatticeNGramFst.readFST("/extra/work/fst/fst-test/test10/ft", new LogSemiring());

        normalizeLatticeNGramFst.normalize();
        
        normalizeLatticeNGramFst.writeFST("/extra/work/fst/fst-test/test10/ft.pushed");        
    }
    
}
