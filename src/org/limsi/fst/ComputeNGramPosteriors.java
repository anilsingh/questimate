/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.fst;

import edu.cmu.sphinx.fst.Arc;
import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.State;
import edu.cmu.sphinx.fst.openfst.Convert;
import edu.cmu.sphinx.fst.semiring.RealSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.limsi.cm.util.MiscellaneousUtils;

/**
 *
 * @author anil
 */
public class ComputeNGramPosteriors {
    
    private File inDirectory;
    private File outDirectory;
    private String charset;

    private Fst ngramFst;
    private Fst normalizedFst;
    
    private String normalizedFstPathPrefix;
    private Semiring semiring;

    private FilenameFilter fileFilter;
    
    LinkedHashMap<Integer, Float> alphas;
    LinkedHashMap<Integer, Float> alphaSums;
    LinkedHashMap<Integer, LinkedHashMap<Integer, Float>> ngramAlphas;
    LinkedHashMap<Integer, Float> posteriors;

    public ComputeNGramPosteriors() {
    }
    
    public void readFST(String normalizedFstBasename, Semiring sr)
    {
        this.normalizedFstPathPrefix = normalizedFstBasename;
        semiring = sr;
        normalizedFst = Convert.importFst(normalizedFstBasename, semiring);
        
//        Push.push(normalizedFst, Reweight.ReweightType.REWEIGHT_TO_INITIAL, Semiring.kDelta, true);        
    }
    
    public void readFSTs(String ngramFstBasename, String normalizedFstBasename, Semiring sr)
    {
        semiring = sr;
        ngramFst = Convert.importFst(ngramFstBasename, semiring);
        normalizedFst = Convert.importFst(normalizedFstBasename, semiring);
    }
    
    public static LinkedHashMap<Integer, Float> getNormalizedValues(LinkedHashMap<Integer, Float> values)
    {
        LinkedHashMap<Integer, Float> delogValues = getDeLogValues(values);
        LinkedHashMap<Integer, Float> normValues = new LinkedHashMap<Integer, Float>();

        double normValue = 0.0;
        
        Iterator<Integer> itr = delogValues.keySet().iterator();
        
        double min = getMin(delogValues);
        double range = getRange(delogValues);

        System.out.format("\tMin: %f, Range: %f\n", min, range);

        while(itr.hasNext())
        {
            int key = itr.next();
            
            double value = delogValues.get(key);

            normValue = (value - min) / range;
            
            normValues.put(key, (float) normValue);
        }        
        
        return normValues;
    }
    
    public static LinkedHashMap<Integer, Float> getNormalizedValues1(LinkedHashMap<Integer, Float> values)
    {
        LinkedHashMap<Integer, Float> delogValues = getDeLogValues(values);
        LinkedHashMap<Integer, Float> normValues = new LinkedHashMap<Integer, Float>();

        double normValue = 0.0;
        
        Iterator<Integer> itr = delogValues.keySet().iterator();
        
        double sum = getSum(delogValues);

        System.out.format("\tSum: %f\n", sum);

        while(itr.hasNext())
        {
            int key = itr.next();
            
            double value = delogValues.get(key);

            normValue = value / sum;
            
            normValues.put(key, (float) normValue);
        }        
        
        return normValues;
    }
    
    public static LinkedHashMap<Integer, Float> getDeLogValues(LinkedHashMap<Integer, Float> values)
    {
        LinkedHashMap<Integer, Float> delogValues = new LinkedHashMap<Integer, Float>();
        
        Iterator<Integer> itr = values.keySet().iterator();

        while(itr.hasNext())
        {
            int key = itr.next();
            
            double value = Math.exp(-values.get(key));
            
            delogValues.put(key, (float) value);
        }        
        
        return delogValues;
    }
    
    private static double getSum(LinkedHashMap<Integer, Float> values)
    {
        double sum = 0.0f;
        
        Iterator<Integer> itr = values.keySet().iterator();

        while(itr.hasNext())
        {
            int key = itr.next();

            sum += values.get(key);
        }        
        
        return sum;
    }
    
    private static double getMax(LinkedHashMap<Integer, Float> values)
    {
        double max = Double.MIN_VALUE;
        
        Iterator<Integer> itr = values.keySet().iterator();

        while(itr.hasNext())
        {
            int key = itr.next();

            if(max < values.get(key))
            {
                max = values.get(key);
            }
        }        
        
        return max;
    }
    
    private static double getMin(LinkedHashMap<Integer, Float> values)
    {
        double min = Double.MAX_VALUE;
        
        Iterator<Integer> itr = values.keySet().iterator();

        while(itr.hasNext())
        {
            int key = itr.next();

            if(min > values.get(key))
            {
                min = values.get(key);
            }
        }        
        
        return min;
    }
    
    private static double getRange(LinkedHashMap<Integer, Float> values)
    {
        return getMax(values) - getMin(values);
    }

    public static void saveNGramAlphas(Fst fst, LinkedHashMap<Integer, LinkedHashMap<Integer, Float>> ngramAlphas, String path, String charset) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, charset);
        
        printNGramAlphas(fst, ngramAlphas, ps);
        
        ps.close();
    }

    public static void saveAlphas(Fst fst, LinkedHashMap<Integer, Float> alphas, String path, String charset) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, charset);
        
        printAlphas(fst, alphas, ps);
        
        ps.close();
    }
    
    public static void printAlphas(Fst fst, LinkedHashMap<Integer, Float> alphas, PrintStream ps)
    {
        Iterator<Integer> itr = alphas.keySet().iterator();
        
        while(itr.hasNext())
        {
            int stateId = itr.next();

            double pv = alphas.get(stateId);

            ps.println(stateId + "\t" + pv);
        }
    }

    public static void printNGramAlphas(Fst fst, LinkedHashMap<Integer, LinkedHashMap<Integer, Float>> ngramAlphas, PrintStream ps)
    {
        Iterator<Integer> itr = ngramAlphas.keySet().iterator();
        
        while(itr.hasNext())
        {        
            int stateId = itr.next();
            
            LinkedHashMap<Integer, Float> values = ngramAlphas.get(stateId);
            
            Iterator<Integer> itr1 = values.keySet().iterator();

            while(itr1.hasNext())
            {
                int arcILabel = itr1.next();

                String input = fst.getIsyms()[arcILabel];

                double pv = values.get(arcILabel);

//                double value = Math.exp(-pv);

//                ps.println(input + "\t" + pv + "\t" + value);
                ps.println(stateId + "\t" + input + "\t" + pv);
            }
        }
    }

    public static void savePosteriors(Fst fst, LinkedHashMap<Integer, Float> posteriors, LinkedHashMap<Integer, Float> alphaSums,
            String path, String charset) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, charset);
        
        printPosteriors(fst, posteriors, alphaSums, ps);
        
        ps.close();
    }
    
    public static void printPosteriors(Fst fst, LinkedHashMap<Integer, Float> posteriors,
            LinkedHashMap<Integer, Float> alphaSums, PrintStream ps)
    {
        int numStates = fst.getNumStates();
//        double maxp = getMax(posteriors);
//        double maxp = getMax(alphaSums);
        
        Iterator<Integer> itr = posteriors.keySet().iterator();
        
        while(itr.hasNext())
        {
            int arcILabel = itr.next();
            
            String input = fst.getIsyms()[arcILabel];

//            double pv = posteriors.get(arcILabel) / (numStates - 1);
            double pv = posteriors.get(arcILabel);
//            double pvnorm = pv / maxp;
            double pvnorm = pv / alphaSums.get(arcILabel);

            ps.println(input + "\t" + pv + "\t" + pvnorm);
        }
    }
    
    public void computePosteriors()
    {
        if(!normalizedFst.isNormalized())
        {
            System.out.println("The FST is not normalized.");
        }
        else
        {
            System.out.println("The FST is found to be normalized.");
        }
        
        int numStates = normalizedFst.getNumStates();

        // One for each state: state id as key and weight as value
        alphas = new LinkedHashMap<Integer, Float>(numStates);
        alphaSums = new LinkedHashMap<Integer, Float>(numStates);

        // One for each ngram: state id as first key, arc ilabel as second key and and weight as value
        ngramAlphas = new LinkedHashMap<Integer, LinkedHashMap<Integer, Float>>(numStates);

        // One for each ngram: ilabel as key and weight as value
//        LinkedHashMap<Integer, Float> posteriors = new LinkedHashMap<Integer, Float>(ngramFst.getNumStates());
        posteriors = new LinkedHashMap<Integer, Float>(numStates - 1);

        System.out.println("Initializing alphas and ngram alphas...");
        
        int start = normalizedFst.getStart().getId();
        
//        LinkedList<State> topSortedStates = TopSort.getTopSortedStates(normalizedFst);
//        
//        Iterator<State> stateItr = topSortedStates.listIterator();
        DirectedGraph<State, DefaultEdge> directedGraph = normalizedFst.getDirectedGraph();

        TopologicalOrderIterator<State, DefaultEdge> topSortItr = new TopologicalOrderIterator<State, DefaultEdge>(directedGraph);

        while(topSortItr.hasNext())
        {
//        for (int i = 0; i < numStates; i++) {
//            State state = normalizedFst.getState(i);
            State state = topSortItr.next();
            
            int numArcs = state.getNumArcs();

            if(state.getId() == start)
            {
                alphas.put(state.getId(), semiring.one());
            }
            else
            {
                alphas.put(state.getId(), semiring.zero());                
            }
            
            ngramAlphas.put(state.getId(), new LinkedHashMap<Integer, Float>(numArcs));
            
            for (int j = 0; j < numArcs; j++) {               
                Arc arc = state.getArc(j);

                alphaSums.put(arc.getIlabel(), semiring.zero());
                
                posteriors.put(arc.getIlabel(), semiring.zero());                
            }
        }

        System.out.println("Starting to calculate posterior probabilities...");

        topSortItr = new TopologicalOrderIterator<State, DefaultEdge>(directedGraph);

//        for (int i = 0; i < numStates; i++) {
//            State state = normalizedFst.getState(i);
        while(topSortItr.hasNext())
        {
            State state = topSortItr.next();
            
            int stateId = state.getId();

            float fw = state.getFinalWeight();

            float fwi = (float) Math.exp(-fw);

            if(fwi == semiring.zero())
            {
                fwi = semiring.one();
            }
            
//            System.out.format("\tat the state %s...\n", i);
    
            int numArcs = state.getNumArcs();
            
            for (int j = 0; j < numArcs; j++) {
                Arc arc = state.getArc(j);
                
                int nextStateId = arc.getNextState().getId();                
                
                float w = (float) Math.exp(-arc.getWeight());
                
                if(w == semiring.zero())
                {
                    w = semiring.one();
                }
                
//                if(alphas.get(nextStateId) == null)
//                {
//                    alphas.put(nextStateId, semiring.zero());
//                }

                float a = alphas.get(state.getId());
                float an = alphas.get(nextStateId);
                float aw = semiring.times(a, w);
                an = semiring.plus(an, aw);
                
                alphas.put(nextStateId, an);

//                System.out.format("\t\tupdating alphas at arc ilabel %s to %f...\n", arc.getIlabel(), alphas.get(arc.getNextState().getId()));
                                
//                System.out.format("\t\t...processing current label...\n");
                
//                if(ngramAlphas.get(nextStateId) == null)
//                {
//                    continue;
////                    ngramAlphas.put(nextStateId, new LinkedHashMap<Integer, Float>(numArcs));            
//                }
                
                if(ngramAlphas.get(nextStateId).get(arc.getIlabel()) == null)
                {
                    ngramAlphas.get(nextStateId).put(arc.getIlabel(), semiring.zero());
                }
                
//                if(ngramAlphas.get(nextStateId) == null)
//                {
//                    ngramAlphas.put(nextStateId, new LinkedHashMap<Integer, Float>(numArcs));                    
//                }

//                ngramAlphas.get(nextStateId).put(arc.getIlabel(), semiring.zero());

                float ngan = ngramAlphas.get(nextStateId).get(arc.getIlabel());
                ngan = semiring.plus(ngan, aw);
                
                ngramAlphas.get(nextStateId).put(arc.getIlabel(), ngan);

//                System.out.format("\t\tupdating ngram-alphas at arc ilabel %s to %f...\n", arc.getIlabel(), ngramAlphas.get(nextStateId).get(arc.getIlabel()));

//                System.out.format("\t\t...processing the next labels...\n");
                
                Iterator<Integer> itr = ngramAlphas.get(stateId).keySet().iterator();
                
                while(itr.hasNext())
                {
                    int arcILabel = itr.next();
                    
                    if(arcILabel == arc.getIlabel())
                    {
                        continue;
                    }

                    if(ngramAlphas.get(nextStateId).get(arcILabel) == null)
                    {
                        ngramAlphas.get(nextStateId).put(arcILabel, semiring.zero());
                    }

//                    System.out.format("\t\tupdating ngram-alphas at arc ilabel %s to %f...\n", arcILabel, ngramAlphas.get(nextStateId).get(arcILabel));
                    
                    float nga = ngramAlphas.get(stateId).get(arcILabel);
                    
//                    if(nga == semiring.zero())
//                    {
//                        nga = semiring.one();
//                    }
                    
                    float nganp = ngramAlphas.get(nextStateId).get(arcILabel);
                    float ngapw = semiring.times(nga, w);
                    nganp = semiring.plus(nganp, ngapw);
//                    nganp = semiring.plus(nganp, aw);
                    
                    ngramAlphas.get(nextStateId).put(arcILabel, nganp);
                }

//                System.out.format("\t...updating posteriors for final states...\n");
                
//                if(numArcs == 0 || fw != semiring.zero())
//                {
//                    itr = ngramAlphas.get(stateId).keySet().iterator();
//
//                    while(itr.hasNext())
//                    {                        
//                        int arcILabel = itr.next();
//
//    //                    System.out.format("\t\t...updating posteriors for state %d from %f\n", stateId, posteriors.get(arcILabel));
//
//                        float fwi = fw;
//
//                        if(fwi == semiring.zero())
//                        {
//                            fwi = semiring.one();
//                        }
//
//                        float ngaf = ngramAlphas.get(stateId).get(arcILabel);
//                        float pw = semiring.times(ngaf, fwi);
//                        float pnew = semiring.plus(posteriors.get(arcILabel), pw);
//
//                        posteriors.put(arcILabel, pnew);
//
//    //                    System.out.format("\t\t...updated posteriors for %d to %f\n",
//    //                            arcILabel, posteriors.get(arcILabel));
//                    }                    
//
////                    ngramAlphas.remove(stateId);
//    //                alphas.remove(stateId);
//    //                posteriors.remove(itr);
//                }
            }

//            if(state.getFinalWeight() != semiring.zero())
            if(numArcs == 0 || fw != semiring.zero())
//            if(stateId != start && (numArcs == 0 || state.getFinalWeight() != semiring.zero()))
            {
                Iterator<Integer> itr = ngramAlphas.get(stateId).keySet().iterator();

                while(itr.hasNext())
                {                        
                    int arcILabel = itr.next();

//                    System.out.format("\t\t...updating posteriors for state %d from %f\n", stateId, posteriors.get(arcILabel));

                    float p = posteriors.get(arcILabel);
                    float ngaf = ngramAlphas.get(stateId).get(arcILabel);
                    float pw = semiring.times(ngaf, fwi);
                    p = semiring.plus(p, pw);

                    posteriors.put(arcILabel, p);

                    float a = alphas.get(stateId);
                    float asum = alphaSums.get(arcILabel);
                    asum = semiring.plus(asum, a);
                    
                    alphaSums.put(arcILabel, asum);

//                    System.out.format("\t\t...updated posteriors for %d to %f\n",
//                            arcILabel, posteriors.get(arcILabel));
                }                    

//                ngramAlphas.remove(stateId);
//                alphas.remove(stateId);
//                posteriors.remove(itr);
            }
        }

//        System.out.println("\nFinal alphas...\n");
//
//        printPosteriors(normalizedFst, alphas, System.out);

//        System.out.println("\nDe-log alphas...\n");

//        LinkedHashMap<Integer, Float> delogAlphas = getDeLogValues(alphas);

//        LinkedHashMap<Integer, Float> delogPosteriors = getDeLogValues(posteriors);
        
        ///
//        System.out.println("\nFinal ngram alphas...\n");

        ///
        
//        System.out.println("\nFinal posteriors...\n");
//
//        printPosteriors(normalizedFst, posteriors, System.out);

//        System.out.println("\nDe-log posteriors...\n");
        
//        LinkedHashMap<Integer, Float> normPosteriors = getNormalizedValues(posteriors);
//
//        System.out.println("\nNormalized posteriors...\n");
//
//        printPosteriors(normalizedFst, normPosteriors, System.out);
//        
//        LinkedHashMap<Integer, Float> normPosteriors1 = getNormalizedValues1(posteriors);
//
//        System.out.println("\nNormalized posteriors (to sum to one)...\n");
//
//        printPosteriors(normalizedFst, normPosteriors1, System.out);

        System.out.println("Finished calculating posterior probabilities...");
    }
    
    public void saveResults(String outDir)
    {        
        File alphaFile = new File(normalizedFstPathPrefix + ".alphas");
        File ngramAlphaFile = new File(normalizedFstPathPrefix + ".ngram.alphas");
        File posteriorFile = new File(normalizedFstPathPrefix + ".post");

        File nfile = new File(normalizedFstPathPrefix + ".fst.txt");

        if(outDir != null)
        {
            String nbasename = nfile.getName();

            nbasename = MiscellaneousUtils.replaceLast(nbasename, ".fst.txt", "");
            
            File odir = new File(outDir);

            alphaFile = new File(odir, nbasename + ".alphas");
            ngramAlphaFile = new File(odir, nbasename + ".ngram.alphas");
            posteriorFile = new File(odir, nbasename + ".post");
        }        
        
        try {
//            savePosteriors(normalizedFst, delogAlphas, normalizedFstBasename + ".alphas", "UTF-8");
            saveAlphas(normalizedFst, alphas, alphaFile.getAbsolutePath(), "UTF-8");
            saveNGramAlphas(normalizedFst, ngramAlphas, ngramAlphaFile.getAbsolutePath(), "UTF-8");
//            savePosteriors(normalizedFst, delogPosteriors, normalizedFstBasename + ".post", "UTF-8");
            savePosteriors(normalizedFst, posteriors, alphaSums, posteriorFile.getAbsolutePath(), "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public void computePosteriorsBatch(String inDir, String basename,
            String outDir, String cs, boolean bm) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        inDirectory = new File(inDir);
        outDirectory = new File(outDir);
        
        if(bm == false)
        {
            basename = MiscellaneousUtils.replaceLast(basename, ".fst.txt", "");
            readFST(basename, new RealSemiring());        
        }
        
        this.normalizedFstPathPrefix = basename;
        charset = cs;
        
        final String bn = basename;
        
        if(bm)
        {
            String[] inputFiles;
            
            fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File directory, String fileName) {
                    return fileName.endsWith(bn);
                }
            };

            inputFiles = inDirectory.list(fileFilter);
            
            for (int i = 0; i < inputFiles.length; i++) {
                String inputFile = inputFiles[i];
                
                System.out.println("Processing file: " + inputFile);

                ComputeNGramPosteriors cngp = new ComputeNGramPosteriors();
                
                cngp.normalizedFstPathPrefix = MiscellaneousUtils.replaceLast(inputFile, ".fst.txt", "");
                
                File ifile = new File(inDir, cngp.normalizedFstPathPrefix);
                
                cngp.readFST(ifile.getAbsolutePath(), new RealSemiring());        

                cngp.computePosteriors();
                
                cngp.saveResults(outDir);
            }
        }
        else
        {
            System.out.println("Processing file: " + normalizedFstPathPrefix + ".norm.fst.txt");
    
            computePosteriors();
                
            saveResults(outDir);            
        }        
    }
    
    public static void main(String args[])
    {
//        ComputeNGramPosteriors cngp = new ComputeNGramPosteriors();
        
//        String ngramFstBasename = "/extra/work/fst/fst-test/clean/wmtbg";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/clean/wmtgraph-sorted.bg.fst.fst.norm";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/test10/ft.norm";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/ncode-test/output/doc147.GRAPH.small-1.bg.log.composed.fin";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/ncode-sample/bbc.2008.09.29.126145.en.fte.utf8.GRAPH-11.norm";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/ncode-sample/bbc.2008.09.29.126145.en.fte.utf8.GRAPH-11.bg.composed.norm";
//        String normalizedFstBasename = "/extra/work/fst/fst-test/wgraph.norm";
        
//        cngp.readFSTs(ngramFstBasename, normalizedFstBasename, new LogSemiring());
//        cngp.readFST(normalizedFstBasename, new LogSemiring());
//        cngp.readFST(normalizedFstBasename, new LogSemiring());
//        cngp.readFST(normalizedFstBasename, new RealSemiring());        
//        cngp.computePosteriors();
        
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("f", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("g", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("n", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String indir = "";
        String basename = "";
        String outdir = "";
        boolean batchMode = false;

        if (!opt.check()) {
            System.out.println("ComputeNGramPosteriors [-cs charset] [-b batchMode] -o outdir -b basename indir");
            System.out.println("In case of batch mode, the basename will be interpreted as a Java file filter expression.");
            System.out.println("(Only the filename suffix implemented at present, e.g. .norm.fst.txt)");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("b")) {
            String bmStr = opt.getSet().getOption("b").getResultValue(0);  
            batchMode = Boolean.parseBoolean(bmStr);
            System.out.println("Batch mode: " + batchMode);
        }
        
        if (opt.getSet().isSet("o")) {
            outdir = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output path " + outdir);
        }
        
        if (opt.getSet().isSet("n")) {
            basename = opt.getSet().getOption("n").getResultValue(0);  
            System.out.println("Basename " + basename);
        }

        indir = opt.getSet().getData().get(0);

        System.out.println("Input path " + indir);
        
        try {            

            ComputeNGramPosteriors cngp = new ComputeNGramPosteriors();            
            
            cngp.computePosteriorsBatch(indir, basename, outdir, charset, batchMode);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ComputeNGramPosteriors.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
