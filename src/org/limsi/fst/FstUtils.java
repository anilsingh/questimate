/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.fst;

import edu.cmu.sphinx.fst.Arc;
import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.State;
import edu.cmu.sphinx.fst.openfst.Convert;
import edu.cmu.sphinx.fst.operations.Compose;
import edu.cmu.sphinx.fst.operations.Connect;
import edu.cmu.sphinx.fst.operations.Determinize;
import edu.cmu.sphinx.fst.operations.NShortestPaths;
import edu.cmu.sphinx.fst.operations.Project;
import edu.cmu.sphinx.fst.operations.ProjectType;
import edu.cmu.sphinx.fst.operations.Reverse;
import edu.cmu.sphinx.fst.operations.RmEpsilon;
import edu.cmu.sphinx.fst.semiring.Semiring;
import edu.cmu.sphinx.fst.utils.Utils;
import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.cm.wg.WordGraph;
import static org.limsi.cm.wg.WordGraph.WORD_SEPERATOR;

/**
 *
 * @author anil
 */
public class FstUtils {
    public static Fst wordGraph2Fst(String graphPath, String cs, String basename, Semiring semiring)
            throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        WordGraph wg = new WordGraph();
        
        wg.readWordGraphNCODE(graphPath, cs, semiring);
        
        Fst fst = WordGraph.wordGraph2Fst(wg, basename, cs, semiring);
        
        return fst;
    }
    
    public static Fst compose(String basename1, String basename2, Semiring sr)
    {
        Fst fst1 = Convert.importFst(basename1, sr);
        Fst fst2 = Convert.importFst(basename2, sr);
        
        Fst fst = Compose.compose(fst1, fst2, sr, true);
        
        return fst;
    }
    
    public static Fst deteminize(String basename, Semiring sr)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Fst detFst = Determinize.get(fst);
        
        return detFst;
    }
    
    public static Fst project(String basename, Semiring sr, ProjectType pt)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Project.apply(fst, pt);
        
        return fst;
    }
    
    public static Fst rmEpsilon(String basename, Semiring sr)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Fst rmEpsFst = RmEpsilon.get(fst);
        
        return rmEpsFst;
    }
    
    public static Fst reverse(String basename, Semiring sr)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Fst rmEpsFst = Reverse.get(fst);
        
        return rmEpsFst;
    }
    
    public static Fst connect(String basename, Semiring sr)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Connect.apply(fst);
        
        return fst;
    }
    
    public static Fst nShortestPaths(String basename, Semiring sr, int n, boolean determinize)
    {
        Fst fst = Convert.importFst(basename, sr);
        
        Fst nShortestPathFst = NShortestPaths.get(fst, n, determinize);
        
        return nShortestPathFst;
    }
    
    public static Index<String> getUniqueOutputLabels(Fst fst)
    {
        Index<String> index = new HashIndex<String>();
        
        String labels[] = fst.getOsyms();

        int numStates = fst.getNumStates();
        
        for (int i = 0; i < numStates; i++) {
            State state = fst.getState(i);
            
            int numArcs = state.getNumArcs();
            
            for (int j = 0; j < numArcs; j++) {               
                Arc arc = state.getArc(j);
     
                index.add(labels[arc.getOlabel()]);
            }
        }
        
        return index;
    }
    
    public static State addArc(Fst fst, int fromStateId, int toStateId, String input, String output, Index<String> ioIndex, 
            HashMap<String, Integer> isyms, HashMap<String, Integer> osyms, HashMap<Integer, State> stateMap, float fromWeight, float toWeight)
    {
        String ioKey = fromStateId + ":" + toStateId;
        
        if(ioIndex.indexOf(ioKey) == -1)
        {
            ioIndex.add(ioKey);
        }
        else
        {
            return null;
        }
        
        State fromState = stateMap.get(fromStateId);
        if (fromState == null) {
            fromState = new State(fromWeight);
            fst.addState(fromState);
            stateMap.put(fromStateId, fromState);
        }

//        if(fromState.getId() == 0)
//        {
//            fst.setStart(fromState);
//        }

        State toState = stateMap.get(toStateId);
        if (toState == null) {
            toState = new State(toWeight);
            fst.addState(toState);
            stateMap.put(toStateId, toState);
        }

        Integer ilabel = isyms.get(input);
        
        if(ilabel == null)
        {
            isyms.put(input, isyms.size());
            
            ilabel = isyms.get(input);
        }
        
        Integer olabel = osyms.get(output);
        
        if(olabel == null)
        {
            osyms.put(output, osyms.size());

            olabel = osyms.get(output);
        }

        Arc arc = new Arc(ilabel, olabel, fst.getSemiring().one(), toState);

        fromState.addArc(arc);
        
        return fromState;
    }
    
    public static void markStartState(Fst fst)
    {
        DirectedGraph<State, DefaultEdge> directedGraph = fst.getDirectedGraph();

        TopologicalOrderIterator<State, DefaultEdge> topSortItr = new TopologicalOrderIterator<State, DefaultEdge>(directedGraph);

        if(topSortItr.hasNext())
        {
            State state = topSortItr.next();
            
            fst.setStart(state);
        }        
    }
    
    public static boolean addBigram(Fst fst, String ug1, String ug2, Map<String, Integer> filterMap, Index<String> ioIndex,
            HashMap<String, Integer> isyms, HashMap<String, Integer> osyms, HashMap<Integer, State> stateMap)
    {
        int i1 = filterMap.get(ug1);
        int i2 = filterMap.get(ug2);
            
        if(i1 == 0 || i2 == 0)
        {
            return false;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());

        String ng = ug1 + WORD_SEPERATOR + ug2;

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = ng;

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().one());

//        addArc(toState, -1, null, null, null, fst.getSemiring().one());        
        
        return true;
    }
    
    public static boolean addTrigram(Fst fst, String ug1, String ug2, String ug3, Map<String, Integer> filterMap, Index<String> ioIndex, Index memIndex,
            HashMap<String, Integer> isyms, HashMap<String, Integer> osyms, HashMap<Integer, State> stateMap)
    {
//        int i1 = filterMap.get(ug1);
//        int i2 = filterMap.get(ug2);
//        int i3 = filterMap.get(ug3);
        int i1 = memIndex.indexOf(ug1);
        int i2 = memIndex.indexOf(ug2);
        int i3 = memIndex.indexOf(ug3);
            
        if(i1 == 0 || i2 == 0 || i3 == 0)
        {
            return false;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());
        
        i2 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2, true);

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());

        String ng = ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3;

        i3 = memIndex.indexOf(ug2 + WORD_SEPERATOR + ug3, true);

        fromState = toState;
        toState = i3;
        
        input = ug3;
        output = ng;

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().one());

//        addArc(toState, -1, null, null, null, semiring.one());        
        
        return true;
    }
    
    public static boolean addFourGram(Fst fst, String ug1, String ug2, String ug3, String ug4, Map<String, Integer> filterMap, Index<String> ioIndex, Index memIndex,
            HashMap<String, Integer> isyms, HashMap<String, Integer> osyms, HashMap<Integer, State> stateMap)
    {
//        int i1 = filterMap.get(ug1);
//        int i2 = filterMap.get(ug2);
//        int i3 = filterMap.get(ug3);
//        int i4 = filterMap.get(ug4);
        int i1 = memIndex.indexOf(ug1);
        int i2 = memIndex.indexOf(ug2);
        int i3 = memIndex.indexOf(ug3);
        int i4 = memIndex.indexOf(ug4);
            
        if(i1 == 0 || i2 == 0 || i3 == 0 || i4 == 0)
        {
            return false;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());
        
        i2 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2, true);

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());
        
        i3 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3, true);

        fromState = toState;
        toState = i3;
        
        input = ug3;
        output = "<eps>";

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().zero());
        
        i4 = memIndex.indexOf(ug2 + WORD_SEPERATOR + ug3 + WORD_SEPERATOR + ug4, true);

        String ng = ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3 + WORD_SEPERATOR + ug4;

        fromState = toState;
        toState = i4;
        
        input = ug4;
        output = ng;

        addArc(fst, fromState, toState, input, output, ioIndex, isyms, osyms, stateMap,
                fst.getSemiring().zero(), fst.getSemiring().one());

//        addArc(toState, -1, null, null, null, semiring.one());        
        
        return true;
    }
    
    public static List<String> getInputList(Fst fst)
    {
        String[] inputs = fst.getIsyms();
        
        List<String> list= new ArrayList<String>();
        
        Collections.addAll(list, inputs);

        return list;
    }
    
    public static Map<String, Integer> getInputMap(Fst fst)
    {
        Map<String, Integer> map = new LinkedHashMap<String, Integer>();
        
        DirectedGraph<State, DefaultEdge> directedGraph = fst.getDirectedGraph();

        TopologicalOrderIterator<State, DefaultEdge> topSortItr = new TopologicalOrderIterator<State, DefaultEdge>(directedGraph);

        while(topSortItr.hasNext())
        {
            State state = topSortItr.next();
            
            int numArcs = state.getNumArcs();
            
            for (int j = 0; j < numArcs; j++) {               
                Arc arc = state.getArc(j);
                
                int label = arc.getIlabel();
                
                String input = fst.getIsyms()[label];
                
                map.put(input, label);
            }
        }
        
        return map ;
    }
    
    public static Map<String, Integer> getOutputMap(Fst fst)
    {
        Map<String, Integer> map = new LinkedHashMap<String, Integer>();
        
        String[] inputs = fst.getOsyms();
        
        for (int i = 0; i < inputs.length; i++) {
            String string = inputs[i];
            map.put(string, i);
        }
        
        return map ;
    }
    
    public static List<String> getOutputList(Fst fst)
    {
        String[] inputs = fst.getOsyms();
        
        List<String> list= new ArrayList<String>();
        
        Collections.addAll(list, inputs);

        return list;
    }
    
    public static Index<String> getOutputIndex(Fst fst)
    {
        List<String> list = getOutputList(fst);
        
        Index<String> index = new HashIndex<String>();
        
        index.add("<eps>");
        
        Index<String> index1 = MiscellaneousUtils.listToIndex(list);
        
        Iterator<String> itr = index1.iterator();
        
        while(itr.hasNext())
        {
            String word = itr.next();
            
            if(!word.equals("<eps>"))
            {
                index.add(word);
            }
        }
        
        return index;
    }
    
    public static Fst generateBigramFst(Fst ugFst)
    {
        Map<String, Integer> inputMap = FstUtils.getOutputMap(ugFst);
         
        Fst bgFst = new Fst(ugFst.getSemiring());
        
        HashMap<String, Integer> isyms = new LinkedHashMap<String, Integer>();
        HashMap<String, Integer> osyms = new LinkedHashMap<String, Integer>();

        HashMap<Integer, State> stateMap = new LinkedHashMap<Integer, State>();
        
        Index<String> ioMap = new HashIndex<String>();
        
        Iterator<String> itr1 = inputMap.keySet().iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();            
            
//            if(ug1.contains(WORD_SEPERATOR))
//            {
//                continue;
//            }
                        
            Iterator<String> itr2 = inputMap.keySet().iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();
            
//                if(ug2.contains(WORD_SEPERATOR))
//                {
//                    continue;
//                }

                addBigram(bgFst, ug1, ug2, inputMap, ioMap, isyms, osyms, stateMap);
            }
        }

        bgFst.setIsyms(Utils.toStringArray(isyms));
        bgFst.setOsyms(Utils.toStringArray(osyms));
        
        markStartState(bgFst);
        
        return bgFst;        
    }
    
    public static Fst generateTrigramFst(Fst bgFst, Fst ugFst)
    {
        Map<String, Integer> inputMap = FstUtils.getOutputMap(bgFst);
         
        Fst tgFst = new Fst(bgFst.getSemiring());
        
        HashMap<String, Integer> isyms = new LinkedHashMap<String, Integer>();
        HashMap<String, Integer> osyms = new LinkedHashMap<String, Integer>();

        Index<String> ioMap = new HashIndex<String>();

        HashMap<Integer, State> stateMap = new LinkedHashMap<Integer, State>();
        
        Index<String> inputIndex = getOutputIndex(ugFst);
        Index<String> memIndex = getOutputIndex(ugFst);
        
        Iterator<String> itr1 = inputIndex.iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();
                                    
            Iterator<String> itr2 = inputIndex.iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();
                
                if(inputMap.get(ug1 + WORD_SEPERATOR + ug2) == null)
                {
                    continue;
                }

                Iterator<String> itr3 = inputIndex.iterator();

                while(itr3.hasNext())
                {
                    String ug3 = itr3.next();

                    addTrigram(tgFst, ug1, ug2, ug3, inputMap, ioMap, memIndex, isyms, osyms, stateMap);
                }
            }
        }

        tgFst.setIsyms(Utils.toStringArray(isyms));
        tgFst.setOsyms(Utils.toStringArray(osyms));

        markStartState(tgFst);
        
        return tgFst;        
    }
    
    public static Fst generateFourGramFst(Fst tgFst, Fst ugFst)
    {
        Map<String, Integer> inputMap = FstUtils.getOutputMap(tgFst);
         
        Fst fgFst = new Fst(tgFst.getSemiring());
        
        HashMap<String, Integer> isyms = new LinkedHashMap<String, Integer>();
        HashMap<String, Integer> osyms = new LinkedHashMap<String, Integer>();

        Index<String> ioMap = new HashIndex<String>();

        HashMap<Integer, State> stateMap = new LinkedHashMap<Integer, State>();

        Index<String> inputIndex = getOutputIndex(ugFst);
        Index<String> memIndex = getOutputIndex(ugFst);
        
        Iterator<String> itr1 = inputIndex.iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();
                                    
            Iterator<String> itr2 = inputIndex.iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();

                Iterator<String> itr3 = inputIndex.iterator();

                while(itr3.hasNext())
                {
                    String ug3 = itr3.next();
                                    
                    if(inputMap.get(ug1 + WORD_SEPERATOR + ug2
                            + WORD_SEPERATOR + ug3) == null)
                    {
                        continue;
                    }

                    Iterator<String> itr4 = inputIndex.iterator();

                    while(itr4.hasNext())
                    {
                        String ug4 = itr4.next();

                        addFourGram(fgFst, ug1, ug2, ug3, ug4, inputMap, ioMap, memIndex, isyms, osyms, stateMap);
                    }
                }
            }
        }

        fgFst.setIsyms(Utils.toStringArray(isyms));
        fgFst.setOsyms(Utils.toStringArray(osyms));

        markStartState(fgFst);
        
        return fgFst;        
    }
    
    public static Fst wordGraph2Fst(WordGraph wg)
    {
        Fst fst = new Fst();
        
        return fst;
    }
}
