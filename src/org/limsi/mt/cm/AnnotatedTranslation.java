/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import org.limsi.cm.wg.WordGraph;
import edu.stanford.nlp.mt.base.FeatureValueCollection;
import edu.stanford.nlp.mt.base.Sequence;
import java.io.PrintStream;
import java.util.regex.Pattern;

/**
 *
 * @author anil
 */
public class AnnotatedTranslation<TK extends Object, FV extends Object>
    extends TranslationHypothesis<TK, FV>
{
    protected int id;
    
    protected Sequence<TK> source;

    protected Sequence<TK> translationDetrueCased;

    protected Sequence<TK> reference;

    protected Sequence<TK> postEdited;
    
    protected double weightedAvgEffort;
    
    public int NUM_EFFORTS;
    
    protected int effortScores[];
    
    protected double effortWeights[];

    protected WordGraph wordGraph;

    public AnnotatedTranslation() {
        NUM_EFFORTS = 3;
        effortScores = new int[NUM_EFFORTS];
        effortWeights = new double[NUM_EFFORTS];
    }
    
    public AnnotatedTranslation(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score, double latticeId) {
        super(sequenceStored, featureValues, score, latticeId);

        NUM_EFFORTS = 3;
        effortScores = new int[NUM_EFFORTS];
        effortWeights = new double[NUM_EFFORTS];
    }

    public AnnotatedTranslation(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score, PhraseAlignment palignment, WordAlignment walignment) {
        super(sequenceStored, featureValues, score, palignment, walignment);

        NUM_EFFORTS = 3;
        effortScores = new int[NUM_EFFORTS];
        effortWeights = new double[NUM_EFFORTS];
    }

    public AnnotatedTranslation(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score) {
        super(sequenceStored, featureValues, score);

        NUM_EFFORTS = 3;
        effortScores = new int[NUM_EFFORTS];
        effortWeights = new double[NUM_EFFORTS];
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the source
     */
    public Sequence<TK> getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(Sequence<TK> source) {
        this.source = source;
    }

    /**
     * @return the translationDetrueCased
     */
    public Sequence<TK> getTranslationDetrueCased() {
        return translationDetrueCased;
    }

    /**
     * @param translationDetrueCased the translationDetrueCased to set
     */
    public void setTranslationDetrueCased(Sequence<TK> translationDetrueCased) {
        this.translationDetrueCased = translationDetrueCased;
    }

    /**
     * @return the reference
     */
    public Sequence<TK> getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(Sequence<TK> reference) {
        this.reference = reference;
    }

    /**
     * @return the postEdited
     */
    public Sequence<TK> getPostEdited() {
        return postEdited;
    }

    /**
     * @param postEdited the postEdited to set
     */
    public void setPostEdited(Sequence<TK> postEdited) {
        this.postEdited = postEdited;
    }

    /**
     * @return the weightedAvgEffort
     */
    public double getWeightedAvgEffort() {
        return weightedAvgEffort;
    }

    /**
     * @param weightedAvgEffort the weightedAvgEffort to set
     */
    public void setWeightedAvgEffort(double weightedAvgEffort) {
        this.weightedAvgEffort = weightedAvgEffort;
    }

    /**
     * @return the effortScores
     */
    public int[] getEffortScores() {
        return effortScores;
    }

    /**
     * @return the effortScores as a String
     */
    public String getEffortScoresStr() {
        StringBuilder effortScoresStr = new StringBuilder();
        
        for (int i = 0; i < effortScores.length; i++) {            
            
            effortScoresStr.append(effortScores[i]);

            if(i < effortScores.length - 1)
                effortScoresStr.append(" ");
        }
        
        return effortScoresStr.toString();
    }

    /**
     * @param efforts the effortScores to set
     */
    public void setEffortScores(int[] efforts) {
        this.effortScores = efforts;
    }

    /**
     * @param efforts the effortScores to set
     */
    public void setEffortScores(String efforts) {
        Pattern p = Pattern.compile("\\s+");
        
        String e[] = p.split(efforts);
        
        for (int i = 0; i < e.length && i < effortScores.length; i++) {
            effortScores[i] = Integer.parseInt(e[i]);
        }
    }

    /**
     * @return the effortWeights
     */
    public double[] getEffortWeights() {
        return effortWeights;
    }

    /**
     * @return the effortWeights as a String
     */
    public String getEffortWeightsStr() {
        StringBuilder effortWeightsStr = new StringBuilder();
        
        for (int i = 0; i < effortWeights.length; i++) {
            
            effortWeightsStr.append(effortWeights[i]);
            
            if(i < effortWeights.length - 1)
                effortWeightsStr.append(" ");
        }
        
        return effortWeightsStr.toString();
    }

    /**
     * @param effortWeights the effortWeights to set
     */
    public void setEffortWeights(double[] effortWeights) {
        this.effortWeights = effortWeights;
    }

    /**
     * @param effortWeights the effortWeights to set
     */
    public void setEffortWeights(String efforts) {
        Pattern p = Pattern.compile("\\s+");
        
        String e[] = p.split(efforts);
        
        for (int i = 0; i < e.length && i < effortWeights.length; i++) {
            effortWeights[i] = Double.parseDouble(e[i]);
        }
    }

    /**
     * @return the wordGraph
     */
    public WordGraph getWordGraph() {
        return wordGraph;
    }

    /**
     * @param wordGraph the wordGraph to set
     */
    public void setWordGraph(WordGraph wordGraph) {
        this.wordGraph = wordGraph;
    }
    
    @Override
    public void print(PrintStream ps)
    {
        ps.println("ID>> " + id);
        ps.println("Source>> " + source);
        ps.println("Target>>" + translation);
        ps.println("Reference>>" + reference);
        ps.println("Post-Edited>>" + postEdited);
        ps.println("weightedAvgEffort>>" + weightedAvgEffort);
        ps.println("effortScores>>" + getEffortScoresStr());
        ps.println("effortWeights>>" + getEffortWeightsStr());
    }
}
