/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.props;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.properties.KeyValueProperties;
import sanchay.properties.PropertyTokens;

/**
 *
 * @author anil
 */
public class CEProperties {
    
    protected static KeyValueProperties propertiesPaths;
    
    protected static KeyValueProperties coreDataPaths;
    protected static KeyValueProperties resourcesPaths;
    
    protected static KeyValueProperties tagsetPaths;
    protected static LinkedHashMap<String, PropertyTokens> tagsets;

    protected static KeyValueProperties featureTypePaths;
    protected static LinkedHashMap<String, PropertyTokens> featureTypes;

    protected static KeyValueProperties featureExtractionOptions;

    private static KeyValueProperties toolCommands;
    
    public static final int CORE_DATA_PATHS = 0;
    public static final int RESOURCES_PATHS = 1;
    public static final int TAGSET_PATHS = 2;
    public static final int FEATURE_TYPE_PATHS = 3;
    public static final int TOOL_COMMANDS = 4;

    public static final int FEATURE_EXTRACTION_OPTIONS = 51;
        
    public CEProperties() {
        loadProperties(true);
    }
    
    private static void loadProperties(boolean forceLoad)
    {
        if(propertiesPaths != null && !forceLoad)
        {
            return;
        }
        
        try {
            propertiesPaths = new KeyValueProperties("props/properties-paths.txt", "UTF-8");
            
            String path = propertiesPaths.getPropertyValue("CoreDataPaths");
            coreDataPaths = new KeyValueProperties(path, "UTF-8");
            
            path = propertiesPaths.getPropertyValue("ResourcesPaths");
            resourcesPaths = new KeyValueProperties(path, "UTF-8");
            
            loadTagSets();
            
            loadFeatureTypes();
            
            path = propertiesPaths.getPropertyValue("FeatureExtractionOptions");
            featureExtractionOptions = new KeyValueProperties(path, "UTF-8");

            path = propertiesPaths.getPropertyValue("ToolCommands");
            setToolCommands(new KeyValueProperties(path, "UTF-8"));
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CEProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CEProperties.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private static void loadTagSets() throws FileNotFoundException, IOException
    {            
        String path = propertiesPaths.getPropertyValue("TagSetPaths");
        tagsetPaths = new KeyValueProperties(path, "UTF-8");

        tagsets = new LinkedHashMap<String, PropertyTokens>();        
                
        Iterator<String> itr = tagsetPaths.getPropertyKeys();
        
        while(itr.hasNext())
        {
            String tsName = itr.next();
            
            String tsPath = tagsetPaths.getPropertyValue(tsName);
            
            PropertyTokens tagset = new PropertyTokens(tsPath, "UTF-8");
            
            tagsets.put(tsName, tagset);
        }
    }
    
    private static void loadFeatureTypes() throws FileNotFoundException, IOException
    {
        String path = propertiesPaths.getPropertyValue("FeatureTypePaths");
        featureTypePaths = new KeyValueProperties(path, "UTF-8");

        featureTypes = new LinkedHashMap<String, PropertyTokens>();        
                
        Iterator<String> itr = featureTypePaths.getPropertyKeys();
        
        while(itr.hasNext())
        {
            String ftName = itr.next();
            
            String ftPath = featureTypePaths.getPropertyValue(ftName);
            
            PropertyTokens featureType = new PropertyTokens(ftPath, "UTF-8");
            
            featureTypes.put(ftName, featureType);
        }        
    }

    /**
     * @return the propertiesPaths
     */
    public static KeyValueProperties getPropertiesPaths() {
        loadProperties(false);
        
        return propertiesPaths;
    }

    /**
     * @param static propertiesPaths the propertiesPaths to set
     */
    public static void setPropertiesPaths(KeyValueProperties propsPaths) {
        propertiesPaths = propsPaths;
    }

    /**
     * @return the coreDataPaths
     */
    public static KeyValueProperties getCoreDataPaths() {
        loadProperties(false);
        
        return coreDataPaths;
    }

    /**
     * @param coreDataPaths the coreDataPaths to set
     */
    public static void setCoreDataPaths(KeyValueProperties cdPaths) {
        coreDataPaths = cdPaths;
    }

    /**
     * @return the resourcesPaths
     */
    public static KeyValueProperties getResourcesPaths() {
        loadProperties(false);
        
        return resourcesPaths;
    }

    /**
     * @param resourcesPaths the resourcesPaths to set
     */
    public static void setResourcesPaths(KeyValueProperties resPaths) {
        resourcesPaths = resPaths;
    }

    /**
     * @return the tagsetPaths
     */
    public static KeyValueProperties getTagsetPaths() {
        loadProperties(false);
        
        return tagsetPaths;
    }

    /**
     * @param tagsetPaths the tagsetPaths to set
     */
    public static void setTagsetPaths(KeyValueProperties tsPaths) {
        tagsetPaths = tsPaths;
    }

    /**
     * @return the tagsets
     */
    public static LinkedHashMap<String, PropertyTokens> getTagsets() {
        loadProperties(false);
        
        return tagsets;
    }

    /**
     * @param tagsets the tagsets to set
     */
    public static void setTagsets(LinkedHashMap<String, PropertyTokens> tsets) {
        tagsets = tsets;
    }

    /**
     * @return the featureExtractionOptions
     */
    public static KeyValueProperties getFeatureExtractionOptions() {
        loadProperties(false);
        
        return featureExtractionOptions;
    }

    /**
     * @param featureExtractionOptions the featureExtractionOptions to set
     */
    public static void setFeatureExtractionOptions(KeyValueProperties extractionOptions) {
        featureExtractionOptions = extractionOptions;
    }

    /**
     * @return the toolCommands
     */
    public static KeyValueProperties getToolCommands() {
        loadProperties(false);
        
        return toolCommands;
    }

    /**
     * @param aToolCommands the toolCommands to set
     */
    public static void setToolCommands(KeyValueProperties aToolCommands) {
        toolCommands = aToolCommands;
    }

    public static String getProperty(String name, int type)
    {
        loadProperties(false);

        String prop = null;
        
        switch(type)
        {
            case CORE_DATA_PATHS:
            {
                prop = coreDataPaths.getPropertyValue(name);
            }
            break;

            case RESOURCES_PATHS:
            {
                prop = resourcesPaths.getPropertyValue(name);
            }
            break;

            case TAGSET_PATHS:
            {
                prop = tagsetPaths.getPropertyValue(name);
            }
            break;

            case FEATURE_TYPE_PATHS:
            {
                prop = featureTypePaths.getPropertyValue(name);
            }
            break;

            case FEATURE_EXTRACTION_OPTIONS:
            {
                prop = featureExtractionOptions.getPropertyValue(name);
            }
            break;

            case TOOL_COMMANDS:
            {
                prop = toolCommands.getPropertyValue(name);
            }
            break;
        }
        
        return prop;
    }
    
    public static PropertyTokens getTagSet(String tsName)
    {
        loadProperties(false);

        return tagsets.get(tsName);
    }
    
    public static PropertyTokens getFeatures(String ftype)
    {
        return featureTypes.get(ftype);
    }
}
