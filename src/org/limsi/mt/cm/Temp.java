/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.avro.Schema;
import org.apache.avro.reflect.ReflectData;
import org.limsi.cm.resources.CEResources;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.features.impl.FeatureStructureImpl;
import sanchay.corpus.ssf.impl.SSFStoryImpl;

/**
 *
 * @author anil
 */
public class Temp {
    
    public static void main(String args[])
    {
        CEResources.setSSFProps();
        
//        WordAlignment pal = new WordAlignment("0-0 1-1 2-2 3-3 4-4 5-5 6-7 7-8 8-9 9-10 10-11 12-12 11-13 11-14", Formats.PHRASAL_NBEST_FORMAT);
//        PhraseAlignment pal = new PhraseAlignment("0=0-2 3-4=4 1=5 2=7 5-6=7-9", Formats.PHRASAL_NBEST_FORMAT);
//        System.out.println(pal.s2tStr());
        
//        WordNet wn = new WordNet("/people/anil/work/wn", true);
//        
//        WNIndexEntry indexEntry = wn.getIndexEntry("book", "v");
//
//        int synsets[] = indexEntry.getSynsets();
//        
//        for (int i = 0; i < synsets.length; i++) {
//            WNWord word = wn.getSynsetName("v", synsets[i]);
//
////            WNWord words[] = entry.getWords();
//
//            System.out.print("Entry for book_NN:");
//
////            for (int j = 0; j < words.length; j++) {
////                String wordStr = words[j].getWord();
//                System.out.print("\t" + word.getWord() + ";");
////            }                    
//        }        
        
//        System.setProperty("wordnet.database.dir", "/people/anil/work/wn/WordNet-3.0/dict");
//        
//        WordNetDatabase database = WordNetDatabase.getFileInstance();
//        
//        NounSynset nounSynset; 
//        NounSynset[] hyponyms; 
//
//        Synset[] synsets = database.getSynsets("book", SynsetType.NOUN); 
//
//        for (int i = 0; i < synsets.length; i++) { 
//            nounSynset = (NounSynset)(synsets[i]); 
//            hyponyms = nounSynset.getHyponyms(); 
//            System.err.println(nounSynset.getWordForms()[0] + 
//                    ": " + nounSynset.getDefinition() + ") has " + hyponyms.length + " hyponyms"); 
//        }        

//        String settingsFile = "/people/anil/work/dbparser/settings/collins.properties";
//
//        try {
//            Settings.load(settingsFile);
//            Parser parser = null;
//            
//            try {
//                parser = new Parser("/people/anil/work/dbparser/wsj-02-21.obj.gz");
//            } catch (RemoteException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (ClassNotFoundException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (NoSuchMethodException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvocationTargetException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IllegalAccessException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InstantiationException ex) {
//                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
//            SexpList list = Sexp.read("(This is a funny world)").list();
//            
////            SexpList list = Sexp.read("(S (NP (DT The) (NN cat)) (VP (VBD sat) (PP (IN on) (NP (DT the) (NN mat)))) (. .))").list();
//            
//            Sexp result = parser.parse(list);        
//            
//            System.out.println("Parsed output: ");
//            
//            SyntacticFeatureExtractor.printSentence(result, System.out, 0, null);
//
////            printSentence(list, System.out, 0, null);
//            
////            SSFNode root = lisp2SSF(list, null);            
//            SSFNode root = SyntacticFeatureExtractor.lisp2SSF((SexpList) result, null);            
//            
//            root.print(System.out);
//            
//        } catch (IOException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
//        try {
//            SyntacticFeatureExtractor.treetaggerPOS2POSTagged("/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa.pos-treetagger",
//                            "/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa.postagged", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSNoLex("/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa.pos-treetagger",
//                            "/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa.pos.nolex", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSNoLex("/people/anil/work/wmt12-quality-estimation/test_set/source.eng.pos-treetagger",
//                            "/people/anil/work/wmt12-quality-estimation/test_set/source.eng.pos.nolex", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSTagged("/people/anil/work/wmt12-quality-estimation/training_set/source.eng.pos-treetagger",
//                            "/people/anil/work/wmt12-quality-estimation/training_set/source.eng.postagged", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSTagged("/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.en.pos-treetagger-new",
//                            "/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.en.postagged", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSNoLex("/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.en.pos-treetagger-new",
//                            "/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.en.postagged.nolex", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSTagged("/people/anil/work/wmt12-quality-estimation/test_set/target_system.spa.pos-treetagger",
//                            "/people/anil/work/wmt12-quality-estimation/test_set/target_system.spa.postagged", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSTagged("/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.es.pos-tagger",
//                    "/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.es.pos-tagged", "UTF-8");
//            SyntacticFeatureExtractor.treetaggerPOS2POSNoLex("/people/anil/work/wmt12-quality-estimation/resources/tmp.txt",
//                    "/people/anil/work/wmt12-quality-estimation/resources/tmp.nolex", "UTF-8");
            
//            SSFStory story = SyntacticFeatureExtractor.readTreetaggerPOSFile("/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.es.pos-tagger.tar.gz", "UTF-8");
////            SSFStory story = SyntacticFeatureExtractor.readLispFile("/people/anil/work/wmt12-quality-estimation/test_set/source.eng.pre1.parsed", "UTF-8");
//            
//            System.out.println("Number of sentences: " + story.countSentences());
//
////            story.savePOSTagged("/people/anil/work/wmt12-quality-estimation/test_set/target_system.spa.pos.ssf", "UTF-8");
//
//            SyntacticFeatureUtils.ssf2POSNoLex(story, "/people/anil/work/wmt12-quality-estimation/resources/europarl-nc.es.pos-tagger.nolex", "UTF-8");
            
//            story.save("/people/anil/work/wmt12-quality-estimation/test_set/target_system.spa.pos-treetagger.ssf", "UTF-8");
            
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        NGramLM senLM = new NGramLMImpl(null, "word", 3);
        
//        NGramLM nGramLM = new NGramLMImpl(new File("/people/anil/work/wmt12-quality-estimation/test_set/source.eng.pos.nolex.lm.sanchay"), "word", 3);
        SSFStory story = new SSFStoryImpl();
        try {
//            Classifier vote = Classifier.forName("weka.classifiers.trees.J48", new String[]{""});
//            Classifier vote = Classifier.forName("weka.classifiers.trees.M5P", new String[]{""});
//            Classifier vote = Classifier.forName("weka.classifiers.trees.LADTree", new String[]{""});
//            Classifier vote = new LowerBoundClassification();
//            Classifier vote = ClassVote.getInstance("weka.classifiers.trees.LADTree",
//                    new String[]{"effort1", "effort2", "effort3"},
//                    new String[]{""},
//                    new double[]{0.33, 0.17, 0.50});
//            Classifier vote = ClassVote.getInstance("weka.classifiers.trees.J48",
//                    new String[]{"effort1", "effort2", "effort3"},
//                    new String[]{""},
//                    new double[]{0.33, 0.17, 0.50});
//            Classifier vote = ClassVote.getInstance("weka.classifiers.trees.M5P",
//                    new String[]{"effort1", "effort2", "effort3"},
//                    new String[]{""},
//                    new double[]{0.33, 0.17, 0.50});
//                    new double[]{0.33, 0.33, 0.34});
//            Classifier vote = ClassVote.getInstance("weka.classifiers.functions.LinearRegression",
//                    new String[]{"effort1", "effort2", "effort3"},
//                    new String[]{""},
//                    new double[]{0.33, 0.17, 0.50});
            
//            int runs = 100; int folds = 0;
//            double percentageSplit = 81.28;
//            System.out.format("Cross validation with %d runs, %d folds and %f split\n", runs, folds, percentageSplit);
//            HypothesisConfidence hypothesisConfidence = new HypothesisConfidence(Classifier.forName("weka.classifiers.trees.M5P", new String[]{""}));
//            HypothesisConfidence hypothesisConfidence = new HypothesisConfidence(vote);

//            hypothesisConfidence.crossValidation("/extra/work/mtce-features/features-BL-e1e2e3.arff",
//                    "UTF-8",
//                    "tmp/model",
//                    "/extra/work/mtce-features/results/features-BL-e1e2e3-cls-LADTree-cv",
//                    1, folds, runs, percentageSplit);
            
//            hypothesisConfidence.crossValidation("/extra/work/mtce-features/effort/features-e3-BL.arff",
//                    "UTF-8",
//                    "tmp/model",
//                    "/extra/work/mtce-features/results/features-BL-e3-LADTree-cv",
//                    1, folds, runs, percentageSplit);

//            System.out.format("Train/test split with %f split\n", percentageSplit);

//            hypothesisConfidence.trainTestSplit("/people/anil/work/confidence-estimation/mtce-features/features-BL-e1e2e3.arff",
//                    "UTF-8",
//                    "tmp/model",
//                    "/people/anil/work/confidence-estimation/mtce-features/results/features-BL-e1e2e3-cls-LADTree-split",
//                    percentageSplit);

//            hypothesisConfidence.trainTestSplit("/extra/work/mtce-features/features-BL-e1e2e3.arff",
//                    "UTF-8",
//                    "tmp/model",
//                    "/extra/work/mtce-features/results/features-BL-e1e2e3-cls-LADTree-split",
//                    percentageSplit);
            
//            hypothesisConfidence.trainTestSplit("/extra/work/mtce-features/effort/features-e1-BL.arff",
//                    "UTF-8",
//                    "tmp/model",
//                    "/extra/work/mtce-features/results/features-BL-e1-LADTree-split",
//                    percentageSplit);
            
//            senLM.makeNGramLM("i'd like to share with you a discovery that i made a few months ago while writing an article for italian wired .");
//            
//            senLM.writeNGramLM(System.out, true);
//            story.readFile("/people/anil/work/questimate/tmp/source.eng", "UTF-8", CorpusType.RAW);
            
            ReflectData reflectData = ReflectData.get();

//            Schema schema = reflectData.getSchema(NBestListDTO.class);
            
//            System.out.println(schema.toString(true));
            
//            nGramLM.readNGramLM(new File("/people/anil/work/wmt12-quality-estimation/test_set/source.eng.pos.nolex.lm.sanchay"));
//            
//            PrintStream ps = new PrintStream("/people/anil/work/wmt12-quality-estimation/test_set/source.eng.lm", "UTF-8");
//            
//            nGramLM.writeNGramLM(ps, true);
//            story.print(System.out);
            
            
            
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        WordTranslationScores wordTranslationScores = new WordTranslationScores("/people/anil/work/wmt12-quality-estimation/resources/lex.e2s", "UTF-8");
//        
//        wordTranslationScores.printScores(System.out);
        
//        WordGraph wordGraph = new WordGraph();
//        
//        try {
//            wordGraph.readWordGraph("/people/anil/work/wmt12-quality-estimation/word-graph-sample.txt", "UTF-8");
//            
//            wordGraph.print(System.out);
//            
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
