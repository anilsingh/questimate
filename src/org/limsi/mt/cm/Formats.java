/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

/**
 *
 * @author anil
 */
public class Formats {
    
    public static int UNKNOWN_FORMAT = -1;
    
    public static int GIZA_FORMAT = 0;

    public static int MOSES_NBEST_FORMAT = 1;
    public static int BINCODER_NBEST_FORMAT = 2;
    public static int PHRASAL_NBEST_FORMAT = 3;
    
    public static int MOSES_PHRASE_TABLE_FORMAT = 11;
    public static int BINCODER_PHRASE_TABLE_FORMAT = 12;
    public static int PHRASAL_PHRASE_TABLE_FORMAT = 13;
}
