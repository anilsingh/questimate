/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.stats;

import edu.uci.lasso.LassoFit;
import edu.uci.lasso.LassoFitGenerator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javastat.inference.onesample.OneSampMeanTTest;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.limsi.cm.util.InstancesUtils;
import weka.core.Attribute;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class StatisticalTests {

    public static Map<String, OneSampMeanTTest> oneSampleTTest(Instances instances) {
        LinkedHashMap<String, OneSampMeanTTest> resultMap = new LinkedHashMap<String, OneSampMeanTTest>();

        int acount = instances.numAttributes();

        for (int i = 0; i < acount; i++) {
            Attribute a = instances.attribute(i);

            resultMap.put(a.name(), oneSampleTTest(instances, a));
        }

        return resultMap;
    }

    public static OneSampMeanTTest oneSampleTTest(Instances instances, Attribute attribute) {
        double[] values = InstancesUtils.getValues(instances, attribute);

        DescriptiveStatistics dstat = new DescriptiveStatistics(values);

        double mean = dstat.getMean();

        OneSampMeanTTest test = new OneSampMeanTTest(0.95, mean, "greater", values);

        return test;
    }

    public static OneSampMeanTTest oneSampleTTest(Instances instances, Attribute attribute,
            Attribute condition, String value) {
        double[] values = InstancesUtils.getValues(instances, attribute, condition, value);

        DescriptiveStatistics dstat = new DescriptiveStatistics(values);

        double mean = dstat.getMean();

        OneSampMeanTTest test = new OneSampMeanTTest(0.95, mean, "greater", values);

        return test;
    }

    public static LassoFit lassoFeatureSelection(Instances instances, Attribute classAttribute) throws Exception {
        /*
         * LassoFitGenerator is initialized
         */
        LassoFitGenerator fitGenerator = new LassoFitGenerator();
        int numObservations = instances.numInstances();
        int featuresCount = instances.numAttributes() - 1;
        
        fitGenerator.init(featuresCount, numObservations);
        
        float[] observation;

        for (int i = 0; i < numObservations; i++) {
            observation = InstancesUtils.getFeatureValues(instances.instance(i), classAttribute);
            
            fitGenerator.setObservationValues(i, observation);
            fitGenerator.setTarget(i, instances.instance(i).value(classAttribute));
        }

        /*
         * Generate the Lasso fit. The -1 arguments means that
         * there would be no limit on the maximum number of 
         * features per model
         */
        LassoFit fit = fitGenerator.fit(-1);

        return fit;
    }
//    public static Object performTest(Instances instances, Attribute attribute, String test)
//    {
//        if(test == null)
//        {
//            return null;
//        }
//        
//        if(test.equalsIgnoreCase("One Sample Mean T-Test"))
//    }
}
