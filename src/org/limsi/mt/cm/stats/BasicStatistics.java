/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.stats;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import sanchay.table.SanchayTableModel;
import umontreal.iro.lecuyer.stat.Tally;

/**
 *
 * @author anil
 */
public class BasicStatistics {
    
    private int accuracy = -1;
    private ArrayList<Double> modes;
    private Map<Double, Integer> uniqueValues;
    
    private DescriptiveStatistics dstat;
    
    private TDistribution tdistribution;
    
    public final static String[] STATISTICS = {
        "Mean", "Mode", "Median", "Standard Deviation", "Confidence Interval (Normal)", "Confidence Interval (Student)",
         "Confidence Interval (Variance Chi2)", "Variance", "Population Variance", "Covariance", "Kurtosis", "Skewness",
        "Minimum", "Maximum", "Sum", "Sum Square",
        "Geometric Mean", "Sample Size"
    };
    
    public BasicStatistics(double[] values) {
        dstat = new DescriptiveStatistics(values);
        modes = new ArrayList<Double>();
        uniqueValues = new LinkedHashMap<Double, Integer>();
    }
    
    public double getMean() {
        return dstat.getMean();
    }
    
    public double getMedian() {
        return dstat.getPercentile(50.0);
    }
    
    public double getMode() {
        tallyFrequencyModes();
        
        return modes.get(0);
    }
    
    public double getStandardDeviation() {
        return dstat.getStandardDeviation();
    }
    
    public double getSkewness() {
        return dstat.getSkewness();
    }
    
    public double getVariance() {
        return dstat.getVariance();
    }
    
    public double getKurtosis() {
        return dstat.getKurtosis();
    }
    
    public double getCovariance() {
        return dstat.getMean();
    }
    
    public double getMinimum() {
        return dstat.getMin();
    }
    
    public double getMaximum() {
        return dstat.getMax();
    }
    
    public double getSum() {
        return dstat.getSum();
    }
    
    public double getSumSq() {
        return dstat.getSumsq();
    }
    
    public double getN() {
        return dstat.getN();
    }
    
    public double getGeometricMean() {
        return dstat.getGeometricMean();
    }
    
    public double getPopulationVariance() {
        return dstat.getPopulationVariance();
    }
    
    public SanchayTableModel getStatisticsTable() {
        SanchayTableModel statsTable = new SanchayTableModel(new String[]{"Statistic", "Value"},
                STATISTICS.length);
        
        for (int i = 0; i < STATISTICS.length; i++) {
            String string = STATISTICS[i];
            
            statsTable.setValueAt(string, i, 0);
            statsTable.setValueAt(getStatistic(string), i, 1);
        }
        
        return statsTable;
    }
    
    public double[] getConfidenceIntervalStudent(double confidenceLevel)
//    public String getConfidenceIntervalNormal(double confidenceLevel)
    {
        double[] ci = new double[2];
        
//        tdistribution = new TDistribution(dstat.getN() - 1);
//                
//        double mu = dstat.getMean();
//        double s = dstat.getVariance();
//        double c = tdistribution.inverseCumulativeProbability(1D - (1.0 - confidenceLevel)/2D);
//        
//        double interval = c * Math.sqrt(s) / Math.sqrt(dstat.getN());
//        
//        ci[0] = mu - interval;
//        ci[1] = mu + interval;
        
        Tally tally = new Tally();
        
        double values[] = dstat.getValues();
        
        for (int i = 0; i < values.length; i++) {
            double d = values[i];
            
            tally.add(d);
        }

        tally.confidenceIntervalStudent(confidenceLevel, ci);

        double low = ci[0] - ci[1];
        double high = ci[0] + ci[1];
        
        ci[0] = low;
        ci[1] = high;
        
        return ci;
    }
    
    public double[] getConfidenceIntervalNormal(double confidenceLevel)
    {
        double[] ci = new double[2];
                
        Tally tally = new Tally();
        
        double values[] = dstat.getValues();
        
        for (int i = 0; i < values.length; i++) {
            double d = values[i];
            
            tally.add(d);
        }

        tally.confidenceIntervalNormal(confidenceLevel, ci);

        double low = ci[0] - ci[1];
        double high = ci[0] + ci[1];
        
        ci[0] = low;
        ci[1] = high;
        
        return ci;
    }
    
    public double[] getConfidenceIntervalChi2(double confidenceLevel)
    {
        double[] ci = new double[2];
                
        Tally tally = new Tally();
        
        double values[] = dstat.getValues();
        
        for (int i = 0; i < values.length; i++) {
            double d = values[i];
            
            tally.add(d);
        }

        tally.confidenceIntervalVarianceChi2(confidenceLevel, ci);

        double low = ci[0] - ci[1];
        double high = ci[0] + ci[1];
        
        ci[0] = low;
        ci[1] = high;
        
        return ci;
    }
    
    public Object getStatistic(String statisticType) {
        if (statisticType.equalsIgnoreCase("Mean")) {
            return "" + getMean();
        } else if (statisticType.equalsIgnoreCase("Median")) {
            return "" + getMedian();
        } else if (statisticType.equalsIgnoreCase("Mode")) {
            return "" + getMode();
        } else if (statisticType.equalsIgnoreCase("Standard Deviation")) {
            return "" + getStandardDeviation();
        } else if (statisticType.equalsIgnoreCase("Variance")) {
            return "" + getVariance();
        } else if (statisticType.equalsIgnoreCase("Skewness")) {
            return "" + getSkewness();
        } else if (statisticType.equalsIgnoreCase("Covariance")) {
            return "" + getCovariance();
        } else if (statisticType.equalsIgnoreCase("Population Variance")) {
            return "" + getPopulationVariance();
        } else if (statisticType.equalsIgnoreCase("Kurtosis")) {
            return "" + getKurtosis();
        } else if (statisticType.equalsIgnoreCase("Geometric Mean")) {
            return "" + getGeometricMean();
        } else if (statisticType.equalsIgnoreCase("Minimum")) {
            return "" + getMinimum();
        } else if (statisticType.equalsIgnoreCase("Maximum")) {
            return "" + getMaximum();
        } else if (statisticType.equalsIgnoreCase("Sum")) {
            return "" + getSum();
        } else if (statisticType.equalsIgnoreCase("Sum Square")) {
            return "" + getSumSq();
        } else if (statisticType.equalsIgnoreCase("Sample Size")) {
            return "" + getN();
        } else if (statisticType.equalsIgnoreCase("Confidence Interval (Normal)")) {
            double ci[] = getConfidenceIntervalNormal(0.95);
            
            DecimalFormat dc = new DecimalFormat("0.000");
        
            return dc.format(ci[0]) + " - " + dc.format(ci[1]);
        } else if (statisticType.equalsIgnoreCase("Confidence Interval (Student)")) {
            double ci[] = getConfidenceIntervalStudent(0.95);
            
            DecimalFormat dc = new DecimalFormat("0.000");
        
            return dc.format(ci[0]) + " - " + dc.format(ci[1]);
        } else if (statisticType.equalsIgnoreCase("Confidence Interval (Variance Chi2)")) {
            double ci[] = getConfidenceIntervalChi2(0.95);
            
            DecimalFormat dc = new DecimalFormat("0.000");
        
            return dc.format(ci[0]) + " - " + dc.format(ci[1]);
        }
        
        return Double.NaN;
    }
    
    public void tallyFrequencyModes() {
        Integer highestFrequency = 0;
        
        long count = dstat.getN();
        
        for (int i = 0; i < count; i++) {
            
            Double dataPoint = dstat.getElement(i);

            // Round data based on rounding
            double effectiveValue = roundData(dataPoint);
            Integer frequency = uniqueValues.get(effectiveValue);
            frequency = (frequency == null) ? 1 : frequency + 1;
            uniqueValues.put(effectiveValue, frequency);
            if (frequency > highestFrequency) {
                // Set the max frequency to the new max
                highestFrequency = frequency;
            }
        }

        for (Double key : uniqueValues.keySet()) {
            // Important note: there can be more than one mode
            // So we are storing the modal values in an array
            if (uniqueValues.get(key) == highestFrequency) {
                modes.add(key);
            }
        }
    }
    
    double roundData(double d) {
        if (getAccuracy() != -1) {
            BigDecimal bigDecimal = new BigDecimal(Double
                    .toString(d));
            bigDecimal = bigDecimal.setScale(this.accuracy,
                    BigDecimal.ROUND_HALF_UP);
            return bigDecimal.doubleValue();
        } else {
            return d;
        }
    }
    // if accuracy has been set to -1 then no rounding occurs
    // if accuracy has been set > -1 then number of digits to round

    public int getAccuracy() {
        return this.accuracy;
    }
    
    public void setAccuracy(int a) {
        this.accuracy = a;
    }
}
