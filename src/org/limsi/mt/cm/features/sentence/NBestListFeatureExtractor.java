/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.cm.resources.CEResources;
import edu.stanford.nlp.mt.base.IString;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;

/**
 *
 * @author anil
 */
public class NBestListFeatureExtractor extends SentenceLevelFeatureExtractor {
    
    public NBestListFeatureExtractor (boolean reverse) {
        super();
        
        this.reverse = reverse;
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        if(!reverse)
        {
            return intoThisHF;
        }
        
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();
        
        List<TranslationHypothesis<IString,String>> translations = featureCollector.getFeatureExtractionCorpus().getNbestList().getNbestLists().get(srcIndex);

        TranslationHypothesis<IString,String> translation = translations.get(hypIndex);

        intoThisHF.setTranslation(translation);
               
        intoThisHF.extractFeatures();
        
        return intoThisHF;
    }
    
//
//    protected void loadNBestList()
//    {
////        String nbestListFilename = "/vol/corpora6/smt/lehaison/WMT/wmt12/en2fr/bincode/optim/09.minfr1.tnb0/mert.bWW.tW.CFB.3/run5.out.NBEST.gz";
////        String nbestListFilename = "/vol/corpora6/smt/lehaison/WMT/wmt12/en2fr/moses/newsco+epps/optim/09/1/run.best300.out";
//        
//        try {
//            nbestList = new NBestListEx(nbestListPath, null, false, NBestListEx.PHRASAL_FORMAT);
//        } catch (IOException ex) {
//            Logger.getLogger(NBestListFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
////        System.out.print(nbestList.printMosesFormat());        
//    }
    
    public static void main(String args[])
    {
        CEAnnotatedCorpusWMT12.setSourcePath("/people/anil/work/wmt12-quality-estimation/training_set/source.eng");
        CEAnnotatedCorpusWMT12.setTargetPath("/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa");

        CEResources.setNbestListPath("/people/anil/work/wmt12-quality-estimation/wmt12qe.training.output.1000-best");
        
        NBestListFeatureExtractor nbestListFeatureExtractor = new NBestListFeatureExtractor(true);
        
//        nbestListFeatureExtractor.loadCorpus();
//        
//        nbestListFeatureExtractor.getSourceText().print(System.out);
        
//        nbestListFeatureExtractor.loadNBestList();
        
//        nbestListFeatureExtractor.testFeatureExtraction();
        
        try {
            nbestListFeatureExtractor.saveFeatures("/people/anil/work/wmt12-quality-estimation/training_set/nbest_features.txt", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NBestListFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NBestListFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
