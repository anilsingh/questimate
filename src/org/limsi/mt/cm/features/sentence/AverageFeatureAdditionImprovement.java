/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.*;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author anil
 */
public class AverageFeatureAdditionImprovement {
    
    public static boolean isSubset(String ofFeature, String feature)
    {
        String ff1[] = ofFeature.split("\\-");
        
        ArrayList<String> ff1List = new ArrayList<String>();
        ff1List.addAll(Arrays.asList(ff1));
        
        String ff2[] = feature.split("\\-");
        
        for (int i = 0; i < ff2.length; i++) {
            if(ff1List.indexOf(ff2[i]) == -1)
                return false;
        }
        
        return true;
    }
    
    public static void main(String args[]) throws IOException
    {
        String path = "/people/anil/work/confidence-estimation/amta12_pe_eval/results/results-summary-3.txt";
        String encoding = "UTF-8";
        
        String featureFilePath = "/people/anil/work/confidence-estimation/amta12_pe_eval/results/results-summary-3-afai.txt";

        List<String> featureList = new LinkedList<String>();
        List<Double[]> featureVals = new LinkedList<Double[]>();
        Map<String, Double[]> valMap = new LinkedHashMap<String, Double[]>();        
        
        LineNumberReader reader;

        if (path.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(path)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    path), encoding));
        }
        
        for (String ttLine; (ttLine = reader.readLine()) != null;)
        {
            ttLine = ttLine.trim();
            
            if(ttLine.startsWith("RR-"))
            {
                String fields[] = ttLine.split("[\\s]+");
                
                featureList.add(fields[0]);
                
                featureVals.add(new Double[] {Double.parseDouble(fields[1]),
                        Double.parseDouble(fields[2]),
                        Double.parseDouble(fields[3]),
                        Double.parseDouble(fields[4])});
            }
        }
        
        reader.close();
        
        int count = featureList.size();
        
        for (int i = 0; i < count; i++) {
            
            String feature1 = featureList.get(i);

            for (int j = 0; j < count; j++) {
                String feature2 = featureList.get(j);
                
                if(isSubset(feature1, feature2) && !feature1.equals(feature2))
                {
                    valMap.put(feature1 + "::" + feature2,
                            new Double[]{featureVals.get(i)[0] - featureVals.get(j)[0],
                                featureVals.get(i)[1] - featureVals.get(j)[1],
                                featureVals.get(i)[2] - featureVals.get(j)[2],
                                featureVals.get(i)[3] - featureVals.get(j)[3]});
                }
            }            
        }

        PrintStream ps = new PrintStream(featureFilePath, encoding);

        DecimalFormat df = new DecimalFormat("0.00");
        
        Iterator<String> itr = valMap.keySet().iterator();
        
        Double avgVals[] = {0.0, 0.0, 0.0, 0.0};
        Integer valDirsPos[] = {0, 0, 0, 0};
        Integer valDirsNeg[] = {0, 0, 0, 0};
        Integer valDirsSame[] = {0, 0, 0, 0};
        
        while(itr.hasNext())
        {
            String feature = itr.next();
            
            Double[] vals = valMap.get(feature);
            
            ps.println(feature + "\t" + df.format(vals[0]) + "\t" + df.format(vals[1])
                     + "\t" + df.format(vals[2]) + "\t" + df.format(vals[3]));
            
            avgVals[0] += vals[0];
            avgVals[1] += vals[1];
            avgVals[2] += vals[2];
            avgVals[3] += vals[3];
            
            if(vals[0] > 0.0)
                valDirsNeg[0]++;
            else if(vals[0] < 0.0)
                valDirsPos[0]++;
            else if(vals[0] == 0.0)
                valDirsSame[0]++;

            if(vals[1] > 0.0)
                valDirsNeg[1]++;
            else if(vals[1] < 0.0)
                valDirsPos[1]++;
            else if(vals[1] == 0.0)
                valDirsSame[1]++;

            if(vals[2] > 0.0)
                valDirsNeg[2]++;
            else if(vals[2] < 0.0)
                valDirsPos[2]++;
            else if(vals[2] == 0.0)
                valDirsSame[2]++;

            if(vals[3] > 0.0)
                valDirsNeg[3]++;
            else if(vals[3] < 0.0)
                valDirsPos[3]++;
            else if(vals[3] == 0.0)
                valDirsSame[3]++;
        }
            
        ps.println("************************************");

        ps.println(df.format(-1 * avgVals[0]) + "\t" + df.format(-1 * avgVals[1])
                     + "\t" + df.format(-1 * avgVals[2]) + "\t" + df.format(-1 * avgVals[3]));

        avgVals[0] = avgVals[0] / featureVals.size();
        avgVals[1] = avgVals[1] / featureVals.size();
        avgVals[2] = avgVals[2] / featureVals.size();
        avgVals[3] = avgVals[3] / featureVals.size();
            
        ps.println("************************************");

        ps.println(df.format(-1 * avgVals[0]) + "\t" + df.format(-1 * avgVals[1])
                     + "\t" + df.format(-1 * avgVals[2]) + "\t" + df.format(-1 * avgVals[3]));
            
        ps.println("************************************");

        ps.println("Increase: " + valDirsPos[0] + "\t" + valDirsPos[1] + "\t" + valDirsPos[2] + "\t" + valDirsPos[3]);

        ps.println("Decrease: " + valDirsNeg[0] + "\t" + valDirsNeg[1] + "\t" + valDirsNeg[2] + "\t" + valDirsNeg[3]);

        ps.println("Same: " + valDirsSame[0] + "\t" + valDirsSame[1] + "\t" + valDirsSame[2] + "\t" + valDirsNeg[3]);

        ps.close();
    }
    
}
