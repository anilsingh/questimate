/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.semiring.LogSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.wg.GenerateNGramFstsWG;
import org.limsi.cm.wg.WordGraph;
import org.limsi.cm.wg.WordGraphReader;
import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FormatType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.mlearning.lm.ngram.NGramLM;
import sanchay.mlearning.lm.ngram.impl.NGramLMImpl;

/**
 *
 * @author anil
 */
public class LatticeFeatureExtractor extends SentenceLevelFeatureExtractor {
    
    protected WordGraphReader wordGraphReader;
    
    protected GenerateNGramFstsWG generateNGramFsts;
//    protected GenerateNGramFsts generateNGramFsts;
    
    protected WordGraph wordGraph;
    
    protected NGramLM ngramPosteriorProbs;
    
    protected Fst latticFst;
    
    protected String wordGraphPath;
    protected String wordGraphSuffix;
    protected String basename;

    protected String fstDirPath;
    
    protected FormatType.WordGraphFormat wordGraphFormat = FormatType.WordGraphFormat.NCODE_FORMAT;

    public LatticeFeatureExtractor (String wordGraphPath, String wordGraphSuffix,
            String fstDirPath, FormatType.WordGraphFormat format, boolean reverse) {
        super();

        this.wordGraphPath = wordGraphPath;
        this.wordGraphSuffix = wordGraphSuffix;
        this.fstDirPath = fstDirPath;
        this.wordGraphFormat = format;
        this.reverse = reverse;
          
//        CEResources.loadNBestList(false);
//
//        nbestList = CEResources.getNbestList();
   }

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(!reverse)
        {
            return;
        }
        
        File f = new File(wordGraphPath);
        
        if(!wordGraphFormat.equals(FormatType.WordGraphFormat.OPEN_FST_FORMAT))
        {
            basename = f.getName().substring(0, f.getName().lastIndexOf(wordGraphSuffix));
        }
        else
        {
            String parts[] = wordGraphPath.split("/");
            
            basename = parts[parts.length - 1];
        }
        
        wordGraphReader = new WordGraphReader(f.getParentFile().getAbsolutePath(), basename, featureType.getTgtCharset());
        
        generateNGramFsts = new GenerateNGramFstsWG(wordGraphFormat, "log",
                f.getParentFile().getAbsolutePath(), basename, fstDirPath,
                featureType.getTgtCharset(), wordGraphReader);
//        generateNGramFsts = new GenerateNGramFsts(wordGraphFormat, "log",
//                f.getParentFile().getAbsolutePath(), basename, fstDirPath,
//                featureType.getTgtCharset(), wordGraphReader);
    }

    @Override
    public List<HypothesisFeatures> extractAllFeatures(int srcIndex, List<HypothesisFeatures> intoList)
    {
        try {
            extractAllFeaturesPrelim(srcIndex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SoulLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SoulLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return super.extractAllFeatures(srcIndex, intoList);
    }

    @Override
    public void extractAllFeaturesPrelim(int srcIndex) throws FileNotFoundException, IOException
    {
        if(!reverse)
        {
            return;
        }
        
        if(wordGraphFormat.equals(FormatType.WordGraphFormat.NCODE_FORMAT))
        {
            wordGraph = wordGraphReader.readWordGraphNCODE(wordGraphPath, featureType.getTgtCharset(), new LogSemiring());
        }
        else if(wordGraphFormat.equals(FormatType.WordGraphFormat.WMT12_FORMAT))
        {
            wordGraph = wordGraphReader.readWordGraphWMT12(wordGraphPath, featureType.getTgtCharset(), new LogSemiring());
        }
        else if(wordGraphFormat.equals(FormatType.WordGraphFormat.WMT13_FORMAT))
        {
            wordGraph = wordGraphReader.readWordGraphWMT13(wordGraphPath, featureType.getTgtCharset(), new LogSemiring());
        }
        else if(wordGraphFormat.equals(FormatType.WordGraphFormat.OPEN_FST_FORMAT))
        {
            wordGraph = wordGraphReader.readWordGraphFST(wordGraphPath, featureType.getTgtCharset(), new LogSemiring());
        }
        
        ngramPosteriorProbs = new NGramLMImpl(null, "word", 4);
        
        latticFst = generateNGramFsts.generateNGramFsts(wordGraph, basename, 4, true, ngramPosteriorProbs, true);
	generateNGramFsts.readPosteriorProbabilities(basename, 4, ngramPosteriorProbs);        

        ngramPosteriorProbs.calcTokenCount();
            
//        System.out.println("Soul probs file: " + soulPath);
    }

    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        if(!reverse)
        {
            return intoThisHF;
        }

        SSFSentence ssfSrc = getCopy(srcIndex, reverse);
        
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//        ssfSrc.convertToLowerCase();

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();        
        
        int len = ssfSrc.getRoot().getAllLeaves().size();
        
        String sentenceString = ssfSrc.convertToRawText();
        
        double value = 0.0;
 
        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {

            if(featureCollector.getFeatureExtractionCorpus().getNbestList() == null)
            {
//                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_STATES))
//                {
//                    value = latticFst.getNumStates();
                    
//                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
//                            value, len, true, false, reverse);
//                }

//                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_ARCS))
//                {
//                    value = latticFst.getDirectedGraph().edgeSet().size();
                    
//                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
//                            value, len, true, false, reverse);
//                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_UNIGRAMS))
                {
                    value = ngramPosteriorProbs.countTokens(1);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_BIGRAMS))
                {
                    value = ngramPosteriorProbs.countTokens(2);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_TRIGRAMS))
                {
                    value = ngramPosteriorProbs.countTokens(3);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_FOUR_GRAMS))
                {
                    value = ngramPosteriorProbs.countTokens(4);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_UNIGRAMS1))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.1, 1);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_BIGRAMS1))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.1, 2);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_TRIGRAMS1))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.1, 3);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_FOUR_GRAMS1))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.1, 4);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_UNIGRAMS4))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.4, 1);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_BIGRAMS4))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.4, 2);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_TRIGRAMS4))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.4, 3);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }

                if(featureSubType.equals(FeatureSubTypesEnum.LATTICE_NUM_FOUR_GRAMS4))
                {
                    value = NGramLMImpl.getNumNGrams(ngramPosteriorProbs, 0.4, 4);
                    
                    FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                            value, len, true, false, reverse);
                }
            }
            
            Semiring sr = new LogSemiring();
                
            if(featureSubType.equals(FeatureSubTypesEnum.UG_POSTERIOR_PROB))
            {
                value = ngramPosteriorProbs.getSentencePosteriorProb(sentenceString, sr, 1);
                                    
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, len, true, false, reverse);
            }
                
            if(featureSubType.equals(FeatureSubTypesEnum.BG_POSTERIOR_PROB))
            {
                value = ngramPosteriorProbs.getSentencePosteriorProb(sentenceString, sr, 2);
                                    
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, len, true, false, reverse);
            }
                
            if(featureSubType.equals(FeatureSubTypesEnum.TG_POSTERIOR_PROB))
            {
                value = ngramPosteriorProbs.getSentencePosteriorProb(sentenceString, sr, 3);
                                    
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, len, true, false, reverse);
            }
                
            if(featureSubType.equals(FeatureSubTypesEnum.FG_POSTERIOR_PROB))
            {
                value = ngramPosteriorProbs.getSentencePosteriorProb(sentenceString, sr, 4);
                                    
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, len, true, false, reverse);
            }
        }

        return intoThisHF;
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 2);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String srcInfile = "";
        String tgtInfile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("LatticeFeatureExtractor [-cs charset] [-o outfile] [-b batchPath] [infilesrc infiletgt]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            srcInfile = opt.getSet().getData().get(0);
            tgtInfile = opt.getSet().getData().get(1);

            System.out.println("Source input file " + srcInfile);
            System.out.println("Target input file " + tgtInfile);

            CEAnnotatedCorpusWMT12.setSourcePath(srcInfile);
            CEAnnotatedCorpusWMT12.setTargetPath(tgtInfile);
        }
        
        try {            
            LatticeFeatureExtractor featureExtractor = new LatticeFeatureExtractor(batchPath, outfile, batchPath,
                    FormatType.WordGraphFormat.NCODE_FORMAT, batchMode);            
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData();
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
