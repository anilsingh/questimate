/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.String;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.limsi.types.FormatType;

/**
 *
 * @author anil
 */
public class MergeFeatures<T> {

    protected HypothesisFeatures<T> hypothesisFeatures;
    
    protected ArrayList<String> inFiles;
    
    protected String outFile;
    protected String charset;
    
    public MergeFeatures(ArrayList<String> inFiles, String outFile, String cs)
    {
        this.inFiles = inFiles;
        this.outFile = outFile;
        this.charset = cs;
    }
    
    public void mergeFeatures() throws UnsupportedEncodingException, IOException
    {
        ArrayList<LineNumberReader> readers = new ArrayList<LineNumberReader>(inFiles.size());
        
        for (String inFile : inFiles)
        {       
            LineNumberReader reader = null;
            
            if (inFile.endsWith(".gz")) {
                reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                        new FileInputStream(inFile)), charset));
            } else {
                reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                        inFile), charset));
            }
            
            readers.add(reader);
        }
        
        PrintStream ps = new PrintStream(outFile, charset);
        
        int count = inFiles.size();
        
        for (String inline; (inline = readers.get(0).readLine()) != null;)
        {
            hypothesisFeatures = new HypothesisFeatures<T>();
            
            hypothesisFeatures.readFeatures(inline);
            
            String inline1;
            
            for (int i = 1; i < count && (inline1 = readers.get(i).readLine()) != null; i++)
            {
                hypothesisFeatures.readFeatures(inline1);
            }
            
            hypothesisFeatures.printFeatures(ps, FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT);
        }
        
        for (LineNumberReader reader : readers)
        {
            reader.close();
        }       
        
        ps.close();
    }

    public static void main(String args[])
    {
        Options opt = new Options(args, 1, 1000);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        
        String charset = "UTF-8";
        String outfile = "";

        if (!opt.check()) {
            System.out.println("MergeFeatures [-cs charset] -o outfile infiles...");

            System.out.println(opt.getCheckErrors());
            
            System.exit(1);
        }

        // Normal processing

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        ArrayList<String> inFiles = opt.getSet().getData();
        
        try {            
            MergeFeatures<String> mergeFeatures = new MergeFeatures<String>(inFiles, outfile, charset);
            
            mergeFeatures.mergeFeatures();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
}
