/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.conv.POSTaggedFormatConvertor;
import org.limsi.types.FormatType;

/**
 *
 * @author anil
 */
public class FeatureFormatConverter {
    
    protected String charset = "UTF-8";
    protected String inFormat = "svmlight";
    protected String outFormat = "arff";
    protected String infile = "";
    protected String outfile = "";
    
    public static String SVMLIGHT = "svmlight";
    public static String ARFF = "arff";
    
    public FeatureFormatConverter(String inFormat, String outFormat,
            String infile, String outfile, String charset)
    {
        this.inFormat = inFormat;
        this.outFormat = outFormat;
        this.infile = infile;
        this.outfile = outfile;
        this.charset = charset;
    }
    
    public void convert() throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        if(inFormat.equalsIgnoreCase(SVMLIGHT) && outFormat.equalsIgnoreCase(ARFF))
        {
            FeatureFormatConverter.convertToArff(infile, outfile, charset);
        }
    }
  
    public static void convertToArff(String svmlighPath, String arffPath, String cs) throws UnsupportedEncodingException, IOException
    {
        System.out.printf("Loading features from file %s\n", svmlighPath);
        
        LineNumberReader reader;
        
        if (svmlighPath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(svmlighPath)), CEAnnotatedCorpusWMT12.getEncoding()));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    svmlighPath), CEAnnotatedCorpusWMT12.getEncoding()));
        }
        
        PrintStream ps = new PrintStream(arffPath, cs);

        int i = 0;
        
        for (String inline; (inline = reader.readLine()) != null;)
        {
            if(i++ > 0 && i % 100 == 0)
            {
               System.out.printf("Processed %d lines\r", i);            
            }

            HypothesisFeatures<String> hf = new HypothesisFeatures<String>();
            
            hf.readSVMLight(inline);
            
            if(i == 1)
                hf.printHeader(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);
            
            hf.printFeatures(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);
        }

        System.out.printf("Total lines found %d\n", i);
        
        reader.close();
        ps.close();
    }
    
    public static void main(String args[])
    {
        Options opt = new Options(args, 1);

//        opt.getSet().addOption("cs", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("if", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("of", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        
        String charset = "UTF-8";
        String inFormat = "svmlight";
        String outFormat = "arff";
        String infile = "";
        String outfile = "";

        if (!opt.check()) {
        // Print usage hints
            System.out.println("FeatureFormatConverter [-cs charset] [-if input format] [-of output format] -o outfile infile");
            System.exit(1);
        }

        // Normal processing

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("if")) {
            inFormat = opt.getSet().getOption("if").getResultValue(0);  
            System.out.println("Input format " + inFormat);
        }
        
        if (opt.getSet().isSet("of")) {
            outFormat = opt.getSet().getOption("of").getResultValue(0);  
            System.out.println("Output format " + outFormat);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }

        infile = opt.getSet().getData().get(0);
        
        System.out.println("Input file " + infile);
        
        try {
                    
            CEResources.setSSFProps();
            
            FeatureFormatConverter formatConvertor = new FeatureFormatConverter(inFormat,
                    outFormat, infile, outfile, charset);
            
            formatConvertor.convert();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
