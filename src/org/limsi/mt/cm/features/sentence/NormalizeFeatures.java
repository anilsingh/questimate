/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import ml.options.Options;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import org.limsi.cm.resources.CEResources;
import org.limsi.types.FormatType;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class NormalizeFeatures {
    protected SSFStory text;
        
    protected String textPath;
    protected String featurePath;
    protected String charset;

    protected boolean batchMode;
    protected ArrayList<String> featureNames;

    protected SanchayTableModel batchPaths;
    
    public NormalizeFeatures(boolean bm, String fns, String cs)
    {
        charset = cs;
        
        CEResources.setSSFProps();
        
        batchMode = bm;
        if(fns != null)
        {
            featureNames = new ArrayList<String>();

            String fn[] = fns.split(",");

            featureNames.addAll(Arrays.asList(fn));        
        }
    }
    
    public void loadData(String textpath, String featureFile,
            String cs) throws FileNotFoundException, IOException, Exception
    {
        charset = cs;
        
        featurePath = featureFile;
        
        text = new SSFStoryImpl();
        
        text.readFile(textpath, cs);
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void loadBatchPaths(String batchPaths) throws FileNotFoundException, IOException {
        this.batchPaths = new SanchayTableModel(batchPaths, "UTF-8");
        
        this.batchPaths.read(batchPaths, "UTF-8");
        
//        this.batchPaths.print(System.out);
    }
    
    public void normalizeFeatures(String outpath,
            String cs) throws UnsupportedEncodingException, IOException
    {
        charset = cs;
        
        LineNumberReader reader;
        PrintStream ps;

        if (featurePath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(featurePath)), charset));

            ps = new PrintStream(new GZIPOutputStream(
                    new FileOutputStream(outpath)), true, charset);
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    featurePath), charset));

            ps = new PrintStream(outpath, charset);
        }
        
        int s = 0;
       
        for (String inLine; (inLine = reader.readLine()) != null;)
        {                
            System.err.printf("Processing sentence: " + ++s + "\r");
            
            inLine = inLine.trim();

            SSFSentence sentence = text.getSentence(s - 1);
            
            HypothesisFeatures<String> hf = new HypothesisFeatures<String>(); 
            
            hf.readFeatures(inLine);
            
            HypothesisFeatures<String> hfNorm = hf.normalizeFeatures(sentence, featureNames);
            
            hfNorm.printFeatures(ps, FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT);
       }
        
        reader.close();
        ps.close();
    }
    
    public void normalizeFeaturesBatch(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception
    {
        if(batchMode == false)
        {
            normalizeFeatures(outFilePath, encoding);
            return;
        }

        if(batchPaths == null)
        {
            System.err.println("Batch paths not loaded.");
            return;
        }
        
        int rcount = batchPaths.getRowCount();
        
        for (int i = 0; i < rcount; i++)
        {
            featureNames = new ArrayList<String>();
    
            String featureNamesString = (String) batchPaths.getValueAt(i, 0);

            String fn[] = featureNamesString.split(",");
            
            featureNames.addAll(Arrays.asList(fn));

            textPath = (String) batchPaths.getValueAt(i, 1);
            featurePath = (String) batchPaths.getValueAt(i, 2);
            String outpath = (String) batchPaths.getValueAt(i, 3);
            charset = (String) batchPaths.getValueAt(i, 4);

            System.out.printf("Processing files %s\n", featurePath);
            
            loadData(textPath, featurePath, charset);
            
            normalizeFeatures(outpath, charset);
        }
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 2);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("fn", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String featureNames = "";
        String textFile = "";
        String featureFile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("NormalizeFeatures [-cs charset] [-fn featureNames] [-b batchPath] [-o outfile] [textpath] [featurepath]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }

        if (opt.getSet().isSet("fn")) {
            featureNames = opt.getSet().getOption("fn").getResultValue(0);
            System.out.println("Feature names: " + featureNames);
        }

        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }

        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            textFile = opt.getSet().getData().get(0);

            System.out.println("Text file " + textFile);

            featureFile = opt.getSet().getData().get(1);

            System.out.println("Feature file " + featureFile);
        }
        
        try {            
            NormalizeFeatures normalizeFeatures = new NormalizeFeatures(batchMode, featureNames, charset);
            
            if(batchMode)
            {
                normalizeFeatures.loadBatchPaths(batchPath);
            }
            else
            {
                normalizeFeatures.loadData(textFile, featureFile, charset);
            }
            
            normalizeFeatures.normalizeFeaturesBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NormalizeFeatures.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NormalizeFeatures.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NormalizeFeatures.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NormalizeFeatures.class.getName()).log(Level.SEVERE, null, ex);
        }
    }            
}
