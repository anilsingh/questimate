/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.WordIndexer;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.cm.util.NGramLMUtils;
import org.limsi.cm.workflow.ResourceFlow;
import org.limsi.cm.workflow.ToolFlow;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import org.limsi.types.LMType;
import org.limsi.types.POSTaggerType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.mlearning.lm.ngram.NGramLM;
import sanchay.mlearning.lm.ngram.impl.NGramLMImpl;

/**
 *
 * @author anil
 */
public class NGramLMFeatureExtractor extends SentenceLevelFeatureExtractor {

    protected LMType type;
    
    protected NgramLanguageModel<String> berkeleyLM;
    protected WordIndexer<String> wordIndexer;

    private POSTaggerType taggerType = POSTaggerType.TREE_TAGGER;
    
    public NGramLMFeatureExtractor(LMType type, boolean rev, boolean bm)
    {
        super();
        
        this.type = type;
        this.reverse = rev;
        batchMode = bm;        
    }

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            return;
        }
        
        if(featureType.equals(FeatureType.NGRAM_LM_POS) || featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
        {
            if(reverse)
            {
                ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getTargetText());
            }
            else
            {
                ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getSourceText());                
            }
            
            ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).runTagger();
        }
    }

    /**
     * @return the taggerType
     */
    public LMType getType() {
        return type;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setType(LMType type) {
        this.type = type;
    }

    /**
     * @return the berkeleyLM
     */
    public NgramLanguageModel<String> getBerkeleyLM() {
        return berkeleyLM;
    }

    /**
     * @param berkeleyLM the berkeleyLM to set
     */
    public void setBerkeleyLM(NgramLanguageModel<String> berkeleyLM) {
        this.berkeleyLM = berkeleyLM;
    }

    /**
     * @return the wordIndexer
     */
    public WordIndexer<String> getWordIndexer() {
        return wordIndexer;
    }

    /**
     * @param wordIndexer the wordIndexer to set
     */
    public void setWordIndexer(WordIndexer<String> wordIndexer) {
        this.wordIndexer = wordIndexer;
    }

    /**
     * @return the taggerType
     */
    public POSTaggerType getTaggerType() {
        return taggerType;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setTaggerType(POSTaggerType taggerType) {
        this.taggerType = taggerType;
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(featureType.equals(FeatureType.NGRAM_LM_POS) || featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
            {
                try {            
                    ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).tagSentence(ssfSrc);
                } catch (IOException ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if(featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
        {
            ssfSrc.convertToPOSNolex();
        }
        else
        {
            if(type.isLowercase())
            {
                ssfSrc.convertToLowerCase();
            }
        }
        
        String sentence = ssfSrc.convertToRawText().trim();
        
        NGramLM senLM = new NGramLMImpl(null, "word", getType().getOrder().order());
        
        senLM.makeNGramLM(sentence);
        
        if(intoThisHF == null) {
            intoThisHF = new HypothesisFeatures<String>();
        }
        
        List<String> wordList = MiscellaneousUtils.getWordList(ssfSrc);
        
        double len = wordList.size();
        
        int order = getBerkeleyLM().getLmOrder();
        
        order = Math.min(order, senLM.getNGramOrder());

        double score = getBerkeleyLM().scoreSentence(wordList);                
        double ppl = Math.pow(2D, -score/len);
        double bcount = NGramLMUtils.countNumberOfBackoffs(senLM, order, getBerkeleyLM(), getWordIndexer());
 
        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            if(featureSubType.equals(FeatureSubTypesEnum.LM_LOGPROB)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_LOGPROB_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_LOGPROB_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName, score, len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.LM_PERPLEXITY)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_PERPLEXITY_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_PERPLEXITY_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName, ppl, len, false, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.LM_BACKOFF_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_BACKOFF_COUNT_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.LM_BACKOFF_COUNT_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName, bcount, len, true, true, reverse);
            }
        }
        
        return intoThisHF;
    }    
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("lmPath", "lm", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("binary", "bin", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("text", "t", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("type", "y", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("order", "n", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("smooth", "s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tool", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        boolean reverse = false;
        String tool = LMType.LMTool.BERKELEY.name();
        String order = LMType.LMOrder.THREE.name();
        String type = LMType.TEXT_LM.name();
        String smoothing = LMType.LMSmoothing.WITTEN_BELL.name();
        String infile = "";
        String outfile = "";
        String lmPath = "";
        String binaryLMPath = "";
        String textPath = "";
        boolean batchMode = false;
        String batchPath = "";
        LMType lmtype = LMType.TEXT_LM;

        if (!opt.check()) {
            System.out.println("NGramLMFeatureExtractor [-cs charset] [[-lm lmPath -binary binaryLMPath] -text textPath]\n"
                   + "[-order order] [-type type] [-smooth smoothing] [-tool lmtool]\n"
                    + "[-o outfile] [-r <ifreverse>] [-b batchPath] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse: " + reverse);
        }

        if (opt.getSet().isSet("lm")) {
            lmPath = opt.getSet().getOption("lm").getResultValue(0);
            System.out.println("lmPath: " + lmPath);
        }

        if (opt.getSet().isSet("binary")) {
            binaryLMPath = opt.getSet().getOption("binary").getResultValue(0);
            System.out.println("binaryLMPath: " + binaryLMPath);
        }

        if (opt.getSet().isSet("text")) {
            textPath = opt.getSet().getOption("text").getResultValue(0);
            System.out.println("textPath: " + textPath);
        }

        if (opt.getSet().isSet("type")) {
            type = opt.getSet().getOption("type").getResultValue(0);
            System.out.println("type: " + type);
        }

        if (opt.getSet().isSet("order")) {
            order = opt.getSet().getOption("order").getResultValue(0);
            System.out.println("order: " + order);
        }

        if (opt.getSet().isSet("smooth")) {
            smoothing = opt.getSet().getOption("smooth").getResultValue(0);
            System.out.println("smoothing: " + smoothing);
        }

        if (opt.getSet().isSet("tool")) {
            tool = opt.getSet().getOption("tool").getResultValue(0);
            System.out.println("lmtool: " + tool);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }
        
        try {
            lmtype = LMType.valueOf(type);

            lmtype.setLMTool(LMType.LMTool.valueOf(tool));

            lmtype.setOrder(LMType.LMOrder.valueOf(order));

            lmtype.setSmoothing(LMType.LMSmoothing.valueOf(smoothing));
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file: " + infile);
        }

//        CEAnnotatedCorpusWMT12.setSourcePath("tmp/source.eng");
//        
//        boolean batchMode = false;
//        String binaryLMPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc.lm.bin";
////        String lmPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc.lm";
//        String lmPath = null;
////        String textPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc";
//        String textPath = null;
//        String outfile = "tmp/lm-features.lm";
//        String batchPath = "";
//        String charset = "UTF-8";
//        LMType taggerType = LMType.TEXT_LM;
        
        try {            
            NGramLMFeatureExtractor featureExtractor = new NGramLMFeatureExtractor(lmtype, reverse, batchMode);            
//            NGramLMFeatureExtractor featureExtractor = new NGramLMFeatureExtractor(taggerType, true, batchMode);            

            if(lmtype.equals(LMType.TEXT_LM))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_LM);
            }
            else if(lmtype.equals(LMType.POS_TAGGED_LM))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_LM_POS);
            }
            else if(lmtype.equals(LMType.POS_NOLEX_LM))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_LM_POS_NOLEX);
            }

            ResourceFlow.setCustomNGramLMPath(true);
            ResourceFlow.setCustomNGramCountsPath(true);
            
            ResourceFlow.setNgramLMTextPath(textPath);
            ResourceFlow.setNgramLMPath(lmPath);
            ResourceFlow.setNgramLMBinaryPath(binaryLMPath);
            ResourceFlow.setNgramCountsPath(lmPath);

            if(lmtype.equals(LMType.TEXT_LM)
                    || lmtype.equals(LMType.POS_TAGGED_LM)
                    || lmtype.equals(LMType.POS_NOLEX_LM))
            {
                ResourceFlow.loadOrMakeLM(lmtype, Language.ENGLISH, true);
                featureExtractor.setBerkeleyLM(ResourceFlow.getBerkeleyLM());
                featureExtractor.setWordIndexer(ResourceFlow.getWordIndexer());
            }
            else
            {
                System.err.println("Wrong ngram LM type: " + lmtype.name());
                System.exit(1);
            }
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(infile, null, null, null, null);
                featureExtractor.preprocessData();
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
