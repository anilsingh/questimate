/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import edu.stanford.nlp.mt.base.IString;
import java.util.List;
import sanchay.corpus.ssf.SSFSentence;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.resources.NBestList;
import org.limsi.mt.cm.PhraseAlignment;
import org.limsi.mt.cm.TranslationHypothesis;

/**
 *
 * @author anil
 */
public class PhraseAlignmentFeatureExtractor extends SentenceLevelFeatureExtractor {
       
    protected NBestList nbestList;

    public PhraseAlignmentFeatureExtractor () {
        super();
         
        CEResources.loadNBestList(false);

        nbestList = CEResources.getNbestList();
    }
 
    /**
     * @return the nbestList
     */
    public NBestList getNbestList() {
        return nbestList;
    }

    /**
     * @param nbestList the nbestList to set
     */
    public void setNbestList(NBestList nbestList) {
        this.nbestList = nbestList;
    }
   
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);
        
//        ssfSrc.print(System.out);

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();
        
        List<TranslationHypothesis<IString,String>> translations = nbestList.getNbestLists().get(srcIndex);

//        System.out.println("Number of translations: " + translations.size());

        TranslationHypothesis<IString,String> translation = translations.get(hypIndex);
        
        PhraseAlignment phraseAlignment = translation.getPhraseAlignment();
        
        return intoThisHF;
    }
    
    public static void main(String args[])
    {
        PhraseAlignmentFeatureExtractor featureExtractor = new PhraseAlignmentFeatureExtractor();
        
        featureExtractor.testFeatureExtraction();
    }
}
