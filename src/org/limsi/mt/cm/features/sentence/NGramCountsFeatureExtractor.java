/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import edu.stanford.nlp.util.Index;
import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import java.beans.PropertyVetoException;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import sanchay.corpus.ssf.SSFSentence;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.workflow.ResourceFlow;
import org.limsi.cm.workflow.ToolFlow;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import org.limsi.types.LMType;
import org.limsi.types.POSTaggerType;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.mlearning.lm.ngram.NGramCountsImpl;
import sanchay.mlearning.lm.ngram.impl.NGramCounts;

/**
 *
 * @author anil
 */
public class NGramCountsFeatureExtractor extends SentenceLevelFeatureExtractor {

    protected LMType type;
    
    protected List<LinkedHashMap<List<Integer>, Long>> cumFreqsList;
    protected Index<String> ngramCountsIndex;    
    protected List<Long> topCumFreqList;

    private POSTaggerType taggerType = POSTaggerType.TREE_TAGGER;
    
    public NGramCountsFeatureExtractor (LMType type, boolean rev, boolean bm) {
        super();

        this.type = type;
        reverse = rev;
        batchMode = bm;
    } 

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            return;
        }
        
        if(featureType.equals(FeatureType.NGRAM_COUNTS_POS) || featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
        {
            if(reverse)
            {
                ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getTargetText());
            }
            else
            {
                ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getSourceText());                
            }

            ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).runTagger();
        }
    }

    /**
     * @return the taggerType
     */
    public LMType getType() {
        return type;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setType(LMType type) {
        this.type = type;
    }

    /**
     * @return the cumFreqsList
     */
    public List<LinkedHashMap<List<Integer>, Long>> getCumFreqsList() {
        return cumFreqsList;
    }

    /**
     * @param cumFreqsList the cumFreqsList to set
     */
    public void setCumFreqsList(List<LinkedHashMap<List<Integer>, Long>> cumFreqsList) {
        this.cumFreqsList = cumFreqsList;
    }

    /**
     * @return the ngramCountsIndex
     */
    public Index<String> getNgramCountsIndex() {
        return ngramCountsIndex;
    }

    /**
     * @param ngramCountsIndex the ngramCountsIndex to set
     */
    public void setNgramCountsIndex(Index<String> ngramCountsIndex) {
        this.ngramCountsIndex = ngramCountsIndex;
    }

    /**
     * @return the topCumFreqList
     */
    public List<Long> getTopCumFreqList() {
        return topCumFreqList;
    }

    /**
     * @param topCumFreqList the topCumFreqList to set
     */
    public void setTopCumFreqList(List<Long> topCumFreqList) {
        this.topCumFreqList = topCumFreqList;
    }

    /**
     * @return the taggerType
     */
    public POSTaggerType getTaggerType() {
        return taggerType;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setTaggerType(POSTaggerType taggerType) {
        this.taggerType = taggerType;
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(featureType.equals(FeatureType.NGRAM_COUNTS_POS) || featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
            {
                try {            
                    ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).tagSentence(ssfSrc);
                } catch (IOException ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if(featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
        {
            ssfSrc.convertToPOSNolex();
        }
        else
        {
            if(type.isLowercase())
            {
                ssfSrc.convertToLowerCase();
            }
        }

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();

        NGramCounts senLM = new NGramCountsImpl(null, "word", getType().getOrder().order(), getNgramCountsIndex());

        if(featureType.equals(FeatureType.NGRAM_COUNTS_POS))
        {
            senLM.makeNGramLM(ssfSrc.convertToPOSTagged());
        }
        else
        {
            senLM.makeNGramLM(ssfSrc.convertToRawText().trim());            
        }
        
        double len = ssfSrc.getRoot().getAllLeaves().size();

        List<List<Double>> pcGrams = NGramCountsImpl.percentNGramsInQuartile(senLM, getCumFreqsList(), getTopCumFreqList());

        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            // Unigrams
            if(featureSubType.equals(FeatureSubTypesEnum.OOV_UNIGRAM)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_UNIGRAM_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_UNIGRAM_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(0).get(0), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(0).get(1), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(0).get(2), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(0).get(3), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(0).get(4), len, true, true, reverse);
            }
            // Bigrams
            else if(featureSubType.equals(FeatureSubTypesEnum.OOV_BIGRAM)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_BIGRAM_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_BIGRAM_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(1).get(0), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(1).get(1), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(1).get(2), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(1).get(3), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(1).get(4), len, true, true, reverse);
            }
            // Trigrams
            else if(featureSubType.equals(FeatureSubTypesEnum.OOV_TRIGRAM)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_TRIGRAM_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.OOV_TRIGRAM_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(2).get(0), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(2).get(1), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE2)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE2_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE2_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(2).get(2), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE3)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE3_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE3_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(2).get(3), len, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4_POS)
                    || featureSubType.equals(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4_POS_NOLEX))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        pcGrams.get(2).get(4), len, true, true, reverse);
            }
        }
        
        return intoThisHF;
    }
    
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("lmPath", "lm", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("binary", "bin", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("text", "t", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("type", "y", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("order", "n", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tool", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        boolean reverse = false;
        String tool = LMType.LMTool.SANCHAY.name();
        String order = LMType.LMOrder.THREE.name();
        String type = LMType.TEXT_LM.name();
        String infile = "";
        String outfile = "";
        String lmPath = "";
        String binaryLMPath = "";
        String textPath = "";
        boolean batchMode = false;
        String batchPath = "";
        LMType lmtype = LMType.TEXT_LM;

        if (!opt.check()) {
            System.out.println("NGramCountsFeatureExtractor [-cs charset] [[-lm lmPath -binary binaryLMPath] -text textPath]\n"
                   + "[-order order] [-type type] [-tool lmtool]\n"
                    + "[-o outfile] [-b batchPath] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse: " + reverse);
        }

        if (opt.getSet().isSet("lm")) {
            lmPath = opt.getSet().getOption("lm").getResultValue(0);
            System.out.println("lmPath: " + lmPath);
        }

        if (opt.getSet().isSet("binary")) {
            binaryLMPath = opt.getSet().getOption("binary").getResultValue(0);
            System.out.println("binaryLMPath: " + binaryLMPath);
        }

        if (opt.getSet().isSet("text")) {
            textPath = opt.getSet().getOption("text").getResultValue(0);
            System.out.println("textPath: " + textPath);
        }

        if (opt.getSet().isSet("type")) {
            type = opt.getSet().getOption("type").getResultValue(0);
            System.out.println("type: " + type);
        }

        if (opt.getSet().isSet("order")) {
            order = opt.getSet().getOption("order").getResultValue(0);
            System.out.println("order: " + order);
        }

        if (opt.getSet().isSet("tool")) {
            tool = opt.getSet().getOption("tool").getResultValue(0);
            System.out.println("lmtool: " + tool);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }
        
        try {
            lmtype = LMType.valueOf(type);

            System.out.println("NGram LM type: " + lmtype.name());

            lmtype.setLMTool(LMType.LMTool.valueOf(tool));

            lmtype.setOrder(LMType.LMOrder.valueOf(order));

        } catch (PropertyVetoException ex) {
            Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file: " + infile);
        }

//        CEAnnotatedCorpusWMT12.setSourcePath("tmp/source.eng");
//        
//        boolean batchMode = false;
//        String binaryLMPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc.lm.bin";
////        String lmPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc.lm";
//        String lmPath = null;
////        String textPath = "tmp/corrections_en2fr.data.src.fte.utf8.lc";
//        String textPath = null;
//        String outfile = "tmp/lm-features.lm";
//        String batchPath = "";
//        String charset = "UTF-8";
//        LMType taggerType = LMType.TEXT_LM;
        
        try {            
            NGramCountsFeatureExtractor featureExtractor = new NGramCountsFeatureExtractor(lmtype, reverse, batchMode);            
//            NGramCountsFeatureExtractor featureExtractor = new NGramCountsFeatureExtractor(taggerType, true, batchMode);            

            if(lmtype.equals(LMType.NGRAM_COUNTS))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_COUNTS);
            }
            else if(lmtype.equals(LMType.NGRAM_COUNTS_POS))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_COUNTS_POS);
            }
            else if(lmtype.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                featureExtractor.setFeatureType(FeatureType.NGRAM_COUNTS_POS_NOLEX);
            }

            ResourceFlow.setCustomNGramLMPath(true);
            ResourceFlow.setCustomNGramCountsPath(true);
            
            ResourceFlow.setNgramLMTextPath(textPath);
            ResourceFlow.setNgramLMPath(lmPath);
            ResourceFlow.setNgramLMBinaryPath(binaryLMPath);
            ResourceFlow.setNgramCountsPath(lmPath);

            if(lmtype.equals(LMType.NGRAM_COUNTS)
                    || lmtype.equals(LMType.NGRAM_COUNTS_POS)
                    || lmtype.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                ResourceFlow.loadOrMakeLM(lmtype, Language.ENGLISH, true);
                featureExtractor.setCumFreqsList(ResourceFlow.getCumFreqsList());
                featureExtractor.setNgramCountsIndex(ResourceFlow.getNgramCountsIndex());
                featureExtractor.setTopCumFreqList(ResourceFlow.getTopCumFreqList());
            }
            else
            {
                System.err.println("Wrong ngram LM type: " + lmtype.name());
                System.exit(1);
            }
            
//            if(textPath != null && MiscellaneousUtils.fileExists(textPath))
//            {
//                featureExtractor.makeLM(textPath, charset);
//
//                if(binaryLMPath != null && MiscellaneousUtils.fileExists(binaryLMPath))
//                {
//                    featureExtractor.loadLM(textPath + ".lm", charset, binaryLMPath);
//                }
//                else
//                {
//                    featureExtractor.loadLM(textPath + ".lm", charset, null);
//                }
//            }
//            else if(lmPath != null && MiscellaneousUtils.fileExists(lmPath))
//            {
//                if(binaryLMPath != null && MiscellaneousUtils.fileExists(binaryLMPath))
//                {
//                    featureExtractor.loadLM(lmPath, charset, binaryLMPath);
//                }
//                else
//                {
//                    featureExtractor.loadLM(lmPath, charset, null);
//                }
//            }
//            else if(binaryLMPath != null && MiscellaneousUtils.fileExists(binaryLMPath))
//            {
//                featureExtractor.loadBinaryLM(binaryLMPath);
//            }
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(infile, null, null, null, null);
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
