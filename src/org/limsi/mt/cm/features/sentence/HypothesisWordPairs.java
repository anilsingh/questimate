/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import edu.stanford.nlp.mt.base.*;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.corpus.ssf.tree.SSFNode;
import org.limsi.cm.resources.CEResources;

/**
 *
 * @author anil
 */
public class HypothesisWordPairs<T> {
    protected SSFSentence sourceSentence;
    protected SSFSentence targetSentence;
    
    protected String sourceLanguage = "en";
    protected String targetLanguage = "fr";
    
    protected FlatPhraseTable<String> phraseTable;
    
    protected LinkedHashMap<String, LinkedHashMap<String, Integer>> unigramPairs;
    protected LinkedHashMap<String, LinkedHashMap<String, Integer>> bigramPairs;
    
    public HypothesisWordPairs() {
        unigramPairs = new LinkedHashMap<String, LinkedHashMap<String, Integer>>();
        bigramPairs = new LinkedHashMap<String, LinkedHashMap<String, Integer>>();
    }

    /**
     * @return the sourceSentence
     */
    public SSFSentence getSourceSentence() {
        return sourceSentence;
    }

    /**
     * @param sourceSentence the sourceSentence to set
     */
    public void setSourceSentence(SSFSentence sourceSentence) {
        this.sourceSentence = sourceSentence;
    }

    /**
     * @return the targetSentence
     */
    public SSFSentence getTargetSentence() {
        return targetSentence;
    }

    /**
     * @param targetSentence the targetSentence to set
     */
    public void setTargetSentence(SSFSentence targetSentence) {
        this.targetSentence = targetSentence;
    }

    /**
     * @return the sourceLanguage
     */
    public String getSourceLanguage() {
        return sourceLanguage;
    }

    /**
     * @param sourceLanguage the sourceLanguage to set
     */
    public void setSourceLanguage(String sourceLanguage) {
        this.sourceLanguage = sourceLanguage;
    }

    /**
     * @return the targetLanguage
     */
    public String getTargetLanguage() {
        return targetLanguage;
    }

    /**
     * @param targetLanguage the targetLanguage to set
     */
    public void setTargetLanguage(String targetLanguage) {
        this.targetLanguage = targetLanguage;
    }

    /**
     * @return the phraseTable
     */
    public FlatPhraseTable<String> getPhraseTable() {
        return phraseTable;
    }

    /**
     * @param phraseTable the phraseTable to set
     */
    public void setPhraseTable(FlatPhraseTable<String> phraseTable) {
        this.phraseTable = phraseTable;
    }
    
    public void loadPhraseTable(String path) throws IOException
    {
        String model = "/people/anil/work/phrasal.Beta3/work/phrasal-mert/phrasal.11.trans";
//        String phrase = "selection";
//        long startTimeMillis = System.currentTimeMillis();
        System.out.printf("Loading phrase table: %s\n", model);
        FlatPhraseTable<String> ppt = new FlatPhraseTable<String>(null, null,
            model);
//        long totalMemory = Runtime.getRuntime().totalMemory() / (1 << 20);
//        long freeMemory = Runtime.getRuntime().freeMemory() / (1 << 20);
//        double totalSecs = (System.currentTimeMillis() - startTimeMillis) / 1000.0;
//        System.err.printf(
//            "size = %d, secs = %.3f, totalmem = %dm, freemem = %dm\n",
//            foreignIndex.size(), totalSecs, totalMemory, freeMemory);

//        List<TranslationOption<IString>> translationOptions = ppt
//            .getTranslationOptions(new SimpleSequence<IString>(IStrings
//                .toIStringArray(phrase.split("\\s+"))));
//
//        System.out.printf("Phrase: %s\n", phrase);
//
//        if (translationOptions == null) {
//        System.out.printf("No translation options found.");
//        System.exit(-1);
//        }
//
//        System.out.printf("Options:\n");
//        for (TranslationOption<IString> opt : translationOptions) {
//        System.out.printf("\t%s : %s\n", opt.translation,
//            Arrays.toString(opt.scores));
//        }
    }
    
    public void makeUnigramPairs()
    {
        unigramPairs.clear();
        
        List srcLeaves = sourceSentence.getRoot().getAllLeaves();
        List tgtLeaves = targetSentence.getRoot().getAllLeaves();
        
        int scount = srcLeaves.size();
        int tcount = tgtLeaves.size();
        
        for (int i = 0; i < scount; i++) {
            
            String srcString = (String) ((SSFNode) srcLeaves.get(i)).getLexData();
                
            if(unigramPairs.get(srcString) == null) {
                unigramPairs.put(srcString, new LinkedHashMap<String, Integer>());
            }
            
            for (int j = 0; j < tcount; j++) {
                String tgtString = (String) ((SSFNode) tgtLeaves.get(j)).getLexData();
                
                Integer freq = unigramPairs.get(srcString).get(tgtString);
                
                if(freq == null)
                {
                    unigramPairs.get(srcString).put(tgtString, 1);
                }
                else
                {
                    unigramPairs.get(srcString).put(tgtString, freq + 1);                    
                }
            }
        }
    }
    
    public void makeBigramPairs()
    {
        bigramPairs.clear();
        
        List srcLeaves = sourceSentence.getRoot().getAllLeaves();
        List tgtLeaves = targetSentence.getRoot().getAllLeaves();
        
        int scount = srcLeaves.size();
        int tcount = tgtLeaves.size();
        
        for (int i = 0; i < scount - 1; i++) {
            
            String srcString1 = (String) ((SSFNode) srcLeaves.get(i)).getLexData();
            String srcString2 = (String) ((SSFNode) srcLeaves.get(i + 1)).getLexData();
            
            String srcString = srcString1 + " " + srcString2;
                
            if(bigramPairs.get(srcString) == null) {
                bigramPairs.put(srcString, new LinkedHashMap<String, Integer>());
            }
            
            for (int j = 0; j < tcount - 1; j++) {
                String tgtString1 = (String) ((SSFNode) tgtLeaves.get(j)).getLexData();
                String tgtString2 = (String) ((SSFNode) tgtLeaves.get(j + 1)).getLexData();

                String tgtString = tgtString1 + " " + tgtString2;
                
                Integer freq = bigramPairs.get(srcString).get(tgtString);
                
                if(freq == null)
                {
                    bigramPairs.get(srcString).put(tgtString, 1);
                }
                else
                {
                    bigramPairs.get(srcString).put(tgtString, freq + 1);                    
                }
            }
            
        }
    }

    public void filterWordPairs()
    {
        
    }
    
    public void printUnigramPairs(PrintStream ps)
    {
        Iterator<String> sitr = unigramPairs.keySet().iterator();
        
        while(sitr.hasNext())
        {
            String srcString = sitr.next();
            
            LinkedHashMap<String, Integer> tgtStrings = unigramPairs.get(srcString);

            Iterator<String> titr = tgtStrings.keySet().iterator();

            while(titr.hasNext())
            {
                String tgtString = titr.next();
                Integer freq = tgtStrings.get(tgtString);
    
                ps.println(srcString + "::" + tgtString + "     " + freq);
            }            
        }
    }
    
    public void printBigramPairs(PrintStream ps)
    {
        Iterator<String> sitr = bigramPairs.keySet().iterator();
        
        while(sitr.hasNext())
        {
            String srcString = sitr.next();
            
            LinkedHashMap<String, Integer> tgtStrings = bigramPairs.get(srcString);

            Iterator<String> titr = tgtStrings.keySet().iterator();

            while(titr.hasNext())
            {
                String tgtString = titr.next();
                Integer freq = tgtStrings.get(tgtString);
    
                ps.println(srcString + "::" + tgtString + "     " + freq);
            }            
        }
    }
    
    public static void main(String args[])
    {
        CEResources.setSSFProps();
        
        String srcSentence = "He is reading the book and the book is interesting";
        String tgtSentence = "Il est de lire le livre et le livre est interesent";
        
        SSFSentence ssfSrc = new SSFSentenceImpl();
        SSFSentence ssfTgt = new SSFSentenceImpl();
        
        try {
            ssfSrc.makeSentenceFromRaw(srcSentence);
            ssfTgt.makeSentenceFromRaw(tgtSentence);
        } catch (Exception ex) {
            Logger.getLogger(HypothesisWordPairs.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HypothesisWordPairs hypothesisWordPairs = new HypothesisWordPairs();
        
        hypothesisWordPairs.setSourceSentence(ssfSrc);
        hypothesisWordPairs.setTargetSentence(ssfTgt);
        
        hypothesisWordPairs.makeUnigramPairs();
        
        hypothesisWordPairs.printUnigramPairs(System.out);
        
        System.out.println("************************");

        hypothesisWordPairs.makeBigramPairs();
        
        hypothesisWordPairs.printBigramPairs(System.out);
        
//        try {
//            hypothesisWordPairs.loadPhraseTable("/people/anil/work/phrasal.Beta3/work/phrasal-mert/phrasal.11.trans");
//        } catch (IOException ex) {
//            Logger.getLogger(HypothesisWordPairs.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
//        hypothesisWordPairs.printWordPairs(System.out);
    }
}
