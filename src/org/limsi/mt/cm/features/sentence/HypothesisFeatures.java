/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import edu.stanford.nlp.mt.base.FeatureValue;
import edu.stanford.nlp.mt.base.FeatureValueCollection;
import edu.stanford.nlp.mt.base.IString;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.features.FeatureExtractor;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.FormatType;
import sanchay.properties.PropertyTokens;
import sanchay.util.Pair;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class HypothesisFeatures<T> implements FeatureExtractor {

    protected TranslationHypothesis<IString,String> translation;
    
    protected FeatureValues<T> featureValues;
    protected FeatureValues<T> labelValues;
    
    protected PropertyTokens filter;
    
    /**
     * Feature pairs for which operations like calculation of ratio features
     * can be conducted.
     */
    protected static Map<FeatureSubTypesEnum, Pair<String, String>> featurePairs;
    
    public HypothesisFeatures()
    {
        featureValues = new FeatureValuesImpl<T>();
        labelValues = new FeatureValuesImpl<T>();
    }

    public HypothesisFeatures(String filterPath) throws FileNotFoundException, IOException
    {
        this();
        
        if((new File(filterPath)).exists())
        {
            filter = new PropertyTokens(filterPath, "UTF-8");
        }
    }

    /**
     * @return the translation
     */
    public TranslationHypothesis<IString,String> getTranslation() {
        return translation;
    }

    /**
     * @param translation the translation to set
     */
    public void setTranslation(TranslationHypothesis<IString,String> translation) {
        this.translation = translation;
    }

    /**
     * @return the featureValues
     */
    public FeatureValues getFeatureValues() {
        return featureValues;
    }
    
    public void setFeatureValues(FeatureValues featureValues)
    {
        this.featureValues = featureValues;
    }

    public void setFeatureValues(Map<CharSequence, Float> featureMap)
    {
        featureValues = FeatureValueUtils.getFeatureValues(featureMap);
    }

    /**
     * @return the labelValues
     */
    public FeatureValues getLabelValues() {
        return labelValues;
    }
    
    public void setLabelValues(FeatureValues labelValues)
    {
        this.labelValues = labelValues;
    }

    public void setLabelValues(Map<CharSequence, Float> labelMap)
    {
        labelValues = FeatureValueUtils.getFeatureValues(labelMap);
    }

    /**
     * @return the filter
     */
    public PropertyTokens getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(PropertyTokens filter) {
        this.filter = filter;
    }
    
    public void addFeature(String featureName, Object featureValue)
    {
        getFeatureValues().addFeature(featureName, featureValue);
    }
    
    public void addLabel(String labelName, Object labelValue)
    {
        getLabelValues().addFeature(labelName, labelValue);
    }

    public void renameFeature(String oldName, String newName)
    {
        getFeatureValues().renameFeature(oldName, newName);
    }

    public void renameLabel(String oldName, String newName)
    {
        getLabelValues().renameFeature(oldName, newName);
    }

    public void renameFeatures(ArrayList<String> newNames)
    {
        ArrayList<String> oldNames = getFeatureValues().getFeatureNames();
        
        getFeatureValues().renameFeatures(oldNames, newNames);
    }

    public void renameLabels(ArrayList<String> newNames)
    {
        ArrayList<String> oldNames = getLabelValues().getFeatureNames();
        
        getLabelValues().renameFeatures(oldNames, newNames);
    }

    public void renameFeatures(ArrayList<String> oldNames, ArrayList<String> newNames)
    {
        getFeatureValues().renameFeatures(oldNames, newNames);
    }

    public void renameLabels(ArrayList<String> oldNames, ArrayList<String> newNames)
    {
        getLabelValues().renameFeatures(oldNames, newNames);
    }
    
    public HypothesisFeatures normalizeFeatures(SSFSentence sen,
            ArrayList<String> featureNames)
    {            
        HypothesisFeatures<String> normFeatures = new HypothesisFeatures<String>();

        List words = sen.getRoot().getAllLeaves();

        Iterator<Integer> itr = featureValues.getFeatureIndices();
        Iterator<String> itrn = featureNames.iterator();
            
        int i = 0;

        while(itr.hasNext() && itrn.hasNext()) {
            Integer fIndex = itr.next();
            String fname = itrn.next();
            String fvalue = (String) featureValues.getFeatureValueFromIndex(fIndex);
            
            i++;
            
            int len = words.size();
            
            double fv = Double.parseDouble(fvalue);

            normFeatures.addFeature("normalized_" + fname, fv / (double) len);
        }
        
        return normFeatures;
    }

    // To be deleted
    public HypothesisFeatures ratioFeatures(HypothesisFeatures tgtFeatures,
            ArrayList<String> featureNames)
    {            
        HypothesisFeatures<String> ratioFeatures = new HypothesisFeatures<String>();
        
        Iterator<Integer> itrs = featureValues.getFeatureIndices();
        Iterator<Integer> itrt = tgtFeatures.getFeatureValues().getFeatureIndices();
        Iterator<String> itrn = featureNames.iterator();

        int i = 0;
        
        while(itrs.hasNext() && itrt.hasNext() && itrn.hasNext()) {
            Integer fIndexSrc = itrs.next();
            Integer fIndexTgt = itrt.next();
            String fname = itrn.next();

            String fvalueSrc = (String) featureValues.getFeatureValueFromIndex(fIndexSrc);
            String fvalueTgt = (String) tgtFeatures.getFeatureValues().getFeatureValueFromIndex(fIndexTgt);

            if(fvalueTgt == null)
            {
                fvalueTgt = (String) tgtFeatures.getFeatureValues().getFeatureValueFromIndex(i);
            }
            
            i++;

            double ratioVal1 = Double.parseDouble(fvalueSrc) / Double.parseDouble(fvalueTgt);
            double ratioVal2 = Double.parseDouble(fvalueTgt) / Double.parseDouble(fvalueSrc);
            
            if(Double.isNaN(ratioVal1))
            {
                ratioVal1 = 1.0;
            }

            if(Double.isNaN(ratioVal2))
            {
                ratioVal2 = 1.0;
            }

            ratioFeatures.addFeature("ratio1_" + fname, ratioVal1);
            ratioFeatures.addFeature("ratio2_" + fname, ratioVal2);
        }
        
        return ratioFeatures;
    }
    
    public static void findFeaturePairs(List<FeatureType> featureTypes, FeatureValues featureValues)
    {
        featurePairs = new LinkedHashMap<FeatureSubTypesEnum, Pair<String, String>>();
        
        for(FeatureType featureType: featureTypes)
        {                              
            List<FeatureSubTypesEnum> featureSubTypes = featureType.getFeatureSubTypes();

            for (FeatureSubTypesEnum featureSubType : featureSubTypes) {

                Pair<String, String> pair = new Pair<String, String>();
                
                featureValues.getFeaturePair(featureSubType, pair);
                
                if((pair.first != null && pair.second != null)
                        && !(FeatureSubTypesEnum.isValidName(pair.first)
                            && FeatureSubTypesEnum.isValidName(pair.second)))
                {   
                    featurePairs.put(featureSubType, pair);
                }
            }
        }        
    }

    public void addRatioFeatures()
    {            
        Iterator<FeatureSubTypesEnum> itr = featurePairs.keySet().iterator();
        
        while(itr.hasNext())
        {            
            FeatureSubTypesEnum featureSubType = itr.next();
            
            Pair<String, String> pair = featurePairs.get(featureSubType);
                    
            Float val1 = (Float) featureValues.getFeatureValue((String) pair.first);
            Float val2 = (Float) featureValues.getFeatureValue((String) pair.second);
            
            if(val1 == null || val2 == null)
            {
                continue;
            }

            double ratioVal1 = val1 / val2;
//            double ratioVal2 = val2 / val1;

            if(Double.isNaN(ratioVal1))
            {
                if(val1 == 0 && val2 == 0)
                {
                    ratioVal1 = 1.0;
                }
                else
                {
                    ratioVal1 = 2.0;
                }
            }
            else if(Double.isInfinite(ratioVal1))
            {
                ratioVal1 = 2.0;                
            }

//            if(Double.isNaN(ratioVal2))
//            {
//                if(val1 == 0 && val2 == 0)
//                {
//                    ratioVal2 = 1.0;
//                }
//                else
//                {
//                    ratioVal2 = 2.0;
//                }
//            }

            addFeature("ratio" + featureSubType.featureName, ratioVal1);
//            addFeature("ratio2_" + featureSubType.featureSubTypesEnum.featureName, ratioVal2);
        }
    }
    
    public static Map<CharSequence, Float> addRatioFeatures(Map<CharSequence, Float> featureMap)
    {
        FeatureValues featureValues = FeatureValueUtils.getFeatureValues(featureMap);
        
        HypothesisFeatures.findFeaturePairs(FeatureType.getFeatureTypes(), featureValues);
        
        HypothesisFeatures hf = new HypothesisFeatures();

        hf.setFeatureValues(featureValues);

        hf.addRatioFeatures();

        Map<CharSequence, Float> featureMapNew = hf.getFeatureValuesMap();        
        
        return featureMapNew;
    }
    
    public void inFilter()
    {
        Iterator<Integer> itr = featureValues.getFeatureIndices();
        
        while(itr.hasNext()) {
            Integer fIndex = itr.next();
            String fname = featureValues.getFeatureNameFromIndex(fIndex);
            
            if(filter.findToken(fname) == -1)
            {
                featureValues.removeFeature(fIndex);
            }
        }
    }
    
    public void outFilter()
    {
        Iterator<Integer> itr = featureValues.getFeatureIndices();
        
        while(itr.hasNext()) {
            Integer fIndex = itr.next();
            String fname = featureValues.getFeatureNameFromIndex(fIndex);
            
            if(filter.findToken(fname) != -1)
            {
                featureValues.removeFeature(fIndex);
            }
        }
    }
    
    public void extractFeatures()
    {
//        hypothesisWordPairs.makeUnigramPairs();
//        hypothesisWordPairs.makeBigramPairs();
        
        // NBest list features
        FeatureValueCollection<String> features = translation.getFeatures();

        if(features != null)
        {
            for (FeatureValue<String> fv : features) {
                featureValues.addFeature(fv.name, (T) new Double(fv.value));
            }

            double score = translation.getScore();

            featureValues.addFeature("modelScore", (T) new Double(score));
        }
    }
    
    public void readFeatures(String featureString)
    {
        String feats[] = featureString.split("\\s+");
        
        for (int i = 0; i < feats.length; i++)
        {
            String fv[] = feats[i].split(":");
            
            if(fv.length == 1)
            {
                addFeature(""+ (i + 1), fv[0]);                
            }
            else
            {
                addFeature(fv[0], fv[1]);
            }
        }
    }
    
    public void printFeatures(PrintStream ps, FormatType.FeatureCollectionFormat format)
    {
//        DecimalFormat df = new DecimalFormat("0.####E0");
     
        String featureString = "";
        
        Iterator<Integer> itr = featureValues.getFeatureIndices();

        while(itr.hasNext())
        {
            int fIndex = itr.next();

            String featureName = featureValues.getFeatureNameFromIndex(fIndex);

            Float featureValue = (Float) featureValues.getFeatureValue(featureName);                    
            
            if(featureName.equals("tgtLMLogprob"))
            {
                if(featureValue > 0 )
                {
                    int stop = 1;
                    System.out.println("tgtLMLogprob:" + featureValue);
                }
            }

            if(format.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))
            {
                featureString += (featureValues.getFeatureNameFromIndex(fIndex)) + ":" + featureValue + " ";
            }
            else if(format.equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT)
                   || format.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
            {
                featureString += featureValue + ",";
            }
        }

        if(format.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))
        {
            featureString = featureString.trim();
        }
        else if(format.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
        {
            featureString = featureString.substring(0, featureString.lastIndexOf(","));
        }

        ps.println(featureString);
    }
    
    public void saveFeatures(String featureFilePath, String encoding, FormatType.FeatureCollectionFormat format) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(featureFilePath, encoding);
        
        printFeatures(ps, format);
    }
    
    public void testFeatures()
    {
        CEResources.setSSFProps();
        
        String srcSentence = "He is reading the book and the book is interesting";
        String tgtSentence = "Il est de lire le livre et le livre est interesent";
        
        SSFSentence ssfSrc = new SSFSentenceImpl();
        SSFSentence ssfTgt = new SSFSentenceImpl();
        
        try {
            ssfSrc.makeSentenceFromRaw(srcSentence);
            ssfTgt.makeSentenceFromRaw(tgtSentence);
        } catch (Exception ex) {
            Logger.getLogger(HypothesisWordPairs.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public void readSVMLight(String svmlightString)
    {
        String fs[] = svmlightString.split("\\s+");
        
        for (int i = 0; i < fs.length; i++) {
            String fv[] = fs[i].split(":");

            if(fv.length != 2)
            {
                    System.err.printf("Error in feature file format: %s has no value\n", fv[0]);
                    System.err.println(svmlightString);
            }           
 
            addFeature(fv[0], fv[1]);
        }
    }
    
    public void printHeader(PrintStream ps, FormatType.FeatureCollectionFormat format)
    {        
        if(format.equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
        {
            ps.println("@relation 'features'");

            ps.println();

            Iterator<Integer> itr = featureValues.getFeatureIndices();

            while(itr.hasNext())
            {
                int fIndex = itr.next();

                String fname = featureValues.getFeatureNameFromIndex(fIndex);

                // To Do: feature type
                ps.println("@attribute " + fname + " numeric");        
            }        

            ps.println();

            ps.println("@data");
        }
        else if(format.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
        {
            Iterator<Integer> itr = featureValues.getFeatureIndices();

            String header = "";

            while(itr.hasNext())
            {
                int fIndex = itr.next();

                String fname = featureValues.getFeatureNameFromIndex(fIndex);

                // To Do: feature type
                header += fname + ",";        
            }        

            header = header.substring(0, header.lastIndexOf(","));

            ps.println(header);            
        }
    }

    public Map<CharSequence, Float> getFeatureValuesMap()
    {        
        Map<CharSequence, Float> featureAvro = new LinkedHashMap<CharSequence, Float>();
        
        Iterator<Integer> itr = featureValues.getFeatureIndices();

        while(itr.hasNext())
        {
            int fIndex = itr.next();

            String fname = featureValues.getFeatureNameFromIndex(fIndex);
            
            Object fValue = featureValues.getFeatureValueFromIndex(fIndex);
            
            if(fValue instanceof Double || fValue instanceof Float)
            {
                featureAvro.put(fname, ((Number) fValue).floatValue());
            }
        }
        
        return featureAvro;
    }

    public Map<CharSequence, Float> getLabelValuesMap()
    {        
        Map<CharSequence, Float> labelAvro = new LinkedHashMap<CharSequence, Float>();
        
        Iterator<Integer> itr = labelValues.getFeatureIndices();

        while(itr.hasNext())
        {
            int fIndex = itr.next();

            String fname = labelValues.getFeatureNameFromIndex(fIndex);
            
            Object fValue = labelValues.getFeatureValueFromIndex(fIndex);
            
            if(fValue instanceof Double || fValue instanceof Float)
            {
                labelAvro.put(fname, ((Number) fValue).floatValue());
            }
        }
        
        return labelAvro;
    }

    public static List<HypothesisFeatures> merge(List<HypothesisFeatures> hf1List, List<HypothesisFeatures> hf2List)
    {
        List<HypothesisFeatures> hfList = new ArrayList<HypothesisFeatures>(hf1List.size());

        if(hf1List.isEmpty())
        {
            for (int i = 0; i < hf2List.size(); i++){
                hf1List.add(new HypothesisFeatures());
            }            
        }
        else if(hf2List.isEmpty())
        {
            for (int i = 0; i < hf1List.size(); i++){
                hf2List.add(new HypothesisFeatures());
            }            
        }

        if(hf1List.size() != hf2List.size())
        {
            System.err.println("Hypothesis features lists are not of the same size: "
                    + hf1List.size() + " and " + hf2List.size());
        }

        for (int i = 0; i < hf1List.size(); i++){
            HypothesisFeatures hf1 = hf1List.get(i);
            HypothesisFeatures hf2 = hf2List.get(i);
            
            HypothesisFeatures hf = HypothesisFeatures.merge(hf1, hf2);
            
            hfList.add(hf);
        }
        
        return hfList;
    }
    
    public static HypothesisFeatures getHypothesisFeatures(Instances instances, int i)
    {
        Instance instance = instances.instance(i);
        
        HypothesisFeatures hf = new HypothesisFeatures();
        
        int count = instances.numAttributes();
        
        for (int j = 0; j < count; j++) {
            Attribute attribute = instances.attribute(i);
            
            if(attribute.isNumeric())
            {
                hf.addFeature(attribute.name(), instance.value(i));
            }
            else if(attribute.isNominal())
            {
                hf.addFeature(attribute.name(), instance.stringValue(i));
            }
        }
        
        return hf;
    }
    
    public static HypothesisFeatures merge(HypothesisFeatures hf1, HypothesisFeatures hf2)
    {
        HypothesisFeatures hf = new HypothesisFeatures();
        
        hf.setTranslation(hf1.getTranslation());
        
        merge(hf, hf1.getFeatureValues());
        merge(hf, hf2.getFeatureValues());
        
        return hf;
    }

    private static HypothesisFeatures merge(HypothesisFeatures hf, FeatureValues fv)
    {
        Iterator<Integer> itr = fv.getFeatureIndices();

        while(itr.hasNext())
        {
            int fIndex = itr.next();

            String fname = fv.getFeatureNameFromIndex(fIndex);
            
            Object fValue = fv.getFeatureValueFromIndex(fIndex);
            
            hf.addFeature(fname, fValue);
        }
        
        return hf;
    }
      
    public static void main(String argsp[])
    {
        HypothesisFeatures<String> hypothesisFeatures = new HypothesisFeatures<String>();
        
        hypothesisFeatures.testFeatures();
    }
}
