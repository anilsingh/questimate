/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.FeatureValues;
import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.limsi.types.FeatureSubTypesEnum;
import sanchay.util.Pair;

/**
 *
 * @author anil
 */
public class FeatureValuesImpl<FV extends Object> implements FeatureValues<FV> {

    protected LinkedHashMap<Integer, FV> features;
    protected static Index<String> featureIndex = new HashIndex<String>();
    
    public FeatureValuesImpl()
    {
        features = new LinkedHashMap<Integer, FV>();
    }
    
    @Override
    public void addFeature(String featureName, FV featureValue)
    {
        int index = featureIndex.indexOf(featureName, true);
        features.put(index, featureValue);        
    }

    @Override
    public void removeFeature(Integer fIndex)
    {
//        featureIndex.
        features.remove(fIndex);
    }

    @Override
    public void renameFeature(String oldName, String newName)
    {
        int oIndex = featureIndex.indexOf(oldName);

        FV val = features.get(oIndex);

        removeFeature(oIndex);

        addFeature(newName, val);        
    }

    @Override
    public void renameFeatures(ArrayList<String> oldNames, ArrayList<String> newNames)
    {
        Iterator<String> itr = oldNames.iterator();

        int i = 0;
        
        while(itr.hasNext())
        {
            String oname = itr.next();
            String nname = newNames.get(i++);
            
            renameFeature(oname, nname);
        }
    }
    
    @Override
    public FV getFeatureValueFromIndex(int index)
    {
        return features.get(index);
    }

    @Override
    public ArrayList<String> getFeatureNames()
    {
        ArrayList<String> featureNames = new ArrayList<String>();
        
        Iterator<Integer> itr= getFeatureIndices();
        
        while(itr.hasNext())
        {
            int fIndex = itr.next();
            
            featureNames.add(getFeatureNameFromIndex(fIndex));
        }
        
        return featureNames;
    }
    
    @Override
    public String getFeatureNameFromIndex(int index)
    {
        return featureIndex.get(index);
    }
    
    @Override
    public FV getFeatureValue(String featureName)
    {
        int index = featureIndex.indexOf(featureName);
        return features.get(index);
    }

    @Override
    public Iterator<Integer> getFeatureIndices()
    {
        return features.keySet().iterator();
    }

    @Override
    public void getFeaturePair(FeatureSubTypesEnum featureSubType, Pair pair)
    {
        Iterator<Integer> itr= getFeatureIndices();

        String featureName = featureSubType.featureName;            
        
        while(itr.hasNext())
        {
            int fIndex = itr.next();
            
            String fname = getFeatureNameFromIndex(fIndex);

            if(!fname.startsWith("normalized") && fname.endsWith(featureName)
                     && !fname.equals(featureName))
            {
                if(pair.first == null)
                {
                    pair.first = fname;
                }
                else if(pair.second == null)
                {
                    pair.second = fname;
                }
            }
        }        
    }

    @Override
    public int size() {
        return features.size();
    }

    @Override
    public boolean isEmpty() {
        return features.isEmpty();
    }

    @Override
    public boolean containsIndex(Integer i) {
        return features.containsKey(i);
    }

    @Override
    public boolean containsName(String n) {
        int i = featureIndex.indexOf(n);
        
        return features.containsKey(i);
    }

    @Override
    public boolean containsValue(FV fv) {
        return features.containsValue(fv);
    }

    @Override
    public void clear() {
        featureIndex.clear();
        features.clear();
    }
}
