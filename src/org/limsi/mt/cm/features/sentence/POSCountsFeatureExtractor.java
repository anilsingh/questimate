/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import danbikel.lisp.Sexp;
import danbikel.lisp.SexpList;
import danbikel.parser.Parser;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import sanchay.corpus.ssf.tree.SSFLexItem;
import sanchay.corpus.ssf.tree.SSFNode;
import sanchay.corpus.ssf.tree.SSFPhrase;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.mt.cm.Temp;
import org.limsi.cm.util.TagSet;
import org.limsi.cm.util.SyntacticFeatureUtils;
import org.limsi.cm.workflow.ToolFlow;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import org.limsi.types.POSTaggerType;

/**
 *
 * @author anil
 */
public class POSCountsFeatureExtractor extends SentenceLevelFeatureExtractor {
    
    protected static Parser collinsParser;
    
    private TagSet tagSet = TagSet.PTB_POS;
    private POSTaggerType taggerType = POSTaggerType.TREE_TAGGER;
    
    public POSCountsFeatureExtractor (TagSet ts, boolean rev, boolean bm)
    {
        super();
        
        reverse = rev;
        tagSet = ts;
        batchMode = bm;
    }

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            return;
        }

        if(reverse)
        {
            ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getTargetText());
        }
        else
        {
            ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).setDocument(featureCollector.getFeatureExtractionCorpus().getSourceText());            
        }
        
        ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).runTagger();
    }

    /**
     * @return the tagSet
     */
    public TagSet getTagSet() {
        return tagSet;
    }

    /**
     * @param tagSet the tagSet to set
     */
    public void setTagSet(TagSet tagSet) {
        this.tagSet = tagSet;
    }

    /**
     * @return the taggerType
     */
    public POSTaggerType getTaggerType() {
        return taggerType;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setTaggerType(POSTaggerType taggerType) {
        this.taggerType = taggerType;
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
                
                ToolFlow.getPosTaggerWrapper(taggerType, featureType.getResourceType().getSrcLangauge()).tagSentence(ssfSrc);
                
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(intoThisHF == null) {
            intoThisHF = new HypothesisFeatures<String>();
        }

//        String rawText = ssfSrc.convertToRawText();
//        
//        rawText = rawText.replaceAll("\\(", "[");
//        rawText = rawText.replaceAll("\\)", "]");
//        
//        String srcString = "(" + rawText + ")";
//        
//        SexpList list;
//        
//        try {
//            list = Sexp.read(srcString).list();
//
////            SexpList list = Sexp.read("(S (NP (DT The) (NN cat)) (VP (VBD sat) (PP (IN on) (NP (DT the) (NN mat)))) (. .))").list();
//
//            Sexp result = collinsParser.parse(list);        
//
////            System.out.println("Parsed output: ");
////
////            POSCountsFeatureExtractor.printSentence(result, System.out, 0, null);
//
//    //            printSentence(list, System.out, 0, null);
//
//    //            SSFNode root = lisp2SSF(list, null);            
//            SSFNode root = POSCountsFeatureExtractor.lisp2SSF((SexpList) result, null);            
        SSFNode srcRoot = ssfSrc.getRoot();            

        int len = srcRoot.getAllLeaves().size();

        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {

            if(featureSubType.equals(FeatureSubTypesEnum.POS_VERB_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_FUNCTION_WORD_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_PREPOSITION_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_NOUN_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_PRONOUN_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_MODIFIER_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_WH_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_NUMBER_WORD_COUNT)
                    || featureSubType.equals(FeatureSubTypesEnum.POS_SYMBOL_COUNT))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        SyntacticFeatureUtils.countSyntacticCategory((SSFPhrase) srcRoot, featureSubType, tagSet), len, true, true, reverse);
            }
        }            
            
//            root.print(System.out);
//        } catch (IOException ex) {
//            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        return intoThisHF;
    }
    
    public static SSFNode printSentence(Sexp sent, PrintStream ps, int tabs, SSFNode prev)
    {
        if(prev == null)
        {
            try {
                prev = new SSFPhrase("0", "S", "S", "");
            } catch (Exception ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        SSFNode node =  null;
        
        if(sent.isSymbol())
        {
            for (int i = 0; i < tabs; i++) {
                ps.print("\t");
            }
            
            ps.println(sent);    
            
//            if(sent.isList())
//            {
//                SSFPhrase phrase = new SSFPhrase();
//                
//                phrase.setName(sent.toString());
////
////                    if(sent.)
////                    phrase
////                }
//            }
//            else
//            {
//                SSFLexItem lexItem = new SSFLexItem();
//            }
            
            return null;
        }
        
        if(sent.isList())
        {
            SexpList list = sent.list();
            
            Iterator itr = list.iterator();
            
            while(itr.hasNext())
            {
                Sexp isent = (Sexp) itr.next();
                printSentence(isent, ps, tabs + 1, prev);
            }
        }

        return null;
    }
    
    public static SSFNode lisp2SSF(SexpList list, SSFNode node)
    {
        if(list.size() == 2 && list.get(0).isSymbol() && list.get(1).isSymbol())
        {
            SSFLexItem inode = new SSFLexItem();

            Sexp lisp1 = list.get(0);
            Sexp lisp2 = list.get(1);

            inode.setName(lisp1.toString());
            inode.setLexData(lisp2.toString());

            node.add(inode);
            
            return node;
        }
        else
        {
            SSFPhrase inode = new SSFPhrase();
            
            if(node != null)
                node.add(inode);

            Iterator itr = list.iterator();

            int i = 0;

            while(itr.hasNext())
            {
                Sexp ilisp = (Sexp) itr.next();

                if(i++ == 0 && ilisp.isSymbol())
                {
                    inode.setName(ilisp.toString());

                    continue;
                }

                lisp2SSF((SexpList) ilisp, inode);
            }
            
            return inode;
        }
    }
    
    public static SSFStory readLispFile(String path, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        SSFStory story = new SSFStoryImpl();

        LineNumberReader reader;

        if (path.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(path)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    path), encoding));
        }
        
        int s = 0;
        
        for (String lispLine; (lispLine = reader.readLine()) != null;)
        {
//            System.out.println(lispLine);
            SexpList list;

            list = Sexp.read(lispLine).list();
            
            SSFPhrase root = (SSFPhrase) lisp2SSF(list, null);
            
            SSFSentence sentence = new SSFSentenceImpl();
            
            sentence.setId("" + ++s);
            
            sentence.setRoot(root);
            
            story.addSentence(sentence);
        }
        
        reader.close();
        
        return story;
    }
    
    public static SSFSentence treetaggerPOS2SSF(String[] lines)
    {
        Pattern space = Pattern.compile("\\t");
        
        SSFSentence sentence = new SSFSentenceImpl();
        SSFPhrase root = new SSFPhrase();
        
        for (int i = 0; i < lines.length; i++)
        {
            if(lines[i].isEmpty())
            {
                continue;
            }
            
            String fields[] = space.split(lines[i]);

            SSFLexItem lex = new SSFLexItem();
            
            lex.setLexData(fields[0]);
            lex.setName(fields[1]);
            
            lex.setAttributeValue("stem", fields[2]);
            
            root.add(lex);
        }
        
        sentence.setRoot(root);
        
        return sentence;
    }
    
    public static SSFStory readTreetaggerPOSFile(String path, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        SSFStory story = new SSFStoryImpl();

        LineNumberReader reader;

        if (path.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(path)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    path), encoding));
        }
        
        int s = 0;

        Pattern newline = Pattern.compile("\n");
        String linesStr = "";
        
        for (String ttLine; (ttLine = reader.readLine()) != null;)
        {
            ttLine = ttLine.trim();
            
            if(ttLine.startsWith("<SENTENCE-END>"))
            {
                String lines[] = newline.split(linesStr);
                
                SSFSentence sentence = treetaggerPOS2SSF(lines);

                sentence.setId("" + ++s);
            
                story.addSentence(sentence);
                
                linesStr = "";

                System.out.printf("Loaded sentence %d\r", s);
                
                continue;
            }            
            
            linesStr += ttLine + "\n";
        }
        
        if(!linesStr.isEmpty())
        {
            String lines[] = newline.split(linesStr);

            SSFSentence sentence = treetaggerPOS2SSF(lines);

            sentence.setId("" + ++s);

            story.addSentence(sentence);
        }
        
        reader.close();
        
        return story;
    }

    public static void treetaggerPOS2POSNoLex(String treetaggerPOSPath,
            String posNoLexPath, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        LineNumberReader reader;

        if (treetaggerPOSPath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(treetaggerPOSPath)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    treetaggerPOSPath), encoding));
        }
        
        PrintStream ps = new PrintStream(posNoLexPath, encoding);
        
        int s = 0;

        Pattern newline = Pattern.compile("\n");
        String linesStr = "";
        
        for (String ttLine; (ttLine = reader.readLine()) != null;)
        {
            ttLine = ttLine.trim();
            
            if(ttLine.startsWith("<SENTENCE-END>"))
            {
                String lines[] = newline.split(linesStr);
                
                SSFSentence sentence = treetaggerPOS2SSF(lines);
            
                ps.println(SyntacticFeatureUtils.ssf2POSNoLex(sentence));
                
                linesStr = "";
                
                System.err.printf("Processing sentence: " + ++s + "\r");
                
                continue;
            }            
            
            linesStr += ttLine + "\n";
        }
        
        if(!linesStr.isEmpty())
        {
            String lines[] = newline.split(linesStr);

            SSFSentence sentence = treetaggerPOS2SSF(lines);

            ps.println(SyntacticFeatureUtils.ssf2POSNoLex(sentence));
        }
        
        reader.close();
        ps.close();
    }

    public static void treetaggerPOS2POSTagged(String treetaggerPOSPath,
            String posTaggedPath, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        LineNumberReader reader;

        if (treetaggerPOSPath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(treetaggerPOSPath)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    treetaggerPOSPath), encoding));
        }
        
        PrintStream ps = new PrintStream(posTaggedPath, encoding);
        
        int s = 0;

        Pattern newline = Pattern.compile("\n");
        String linesStr = "";
        
        for (String ttLine; (ttLine = reader.readLine()) != null;)
        {
            ttLine = ttLine.trim();
            
            if(ttLine.startsWith("<SENTENCE-END>"))
            {
                String lines[] = newline.split(linesStr);
                
                SSFSentenceImpl sentence = (SSFSentenceImpl) treetaggerPOS2SSF(lines);
            
                ps.print(sentence.convertToPOSTagged());
                
                linesStr = "";
                
                System.err.printf("Processing sentence: " + ++s + "\r");
                
                continue;
            }            
            
            linesStr += ttLine + "\n";
        }
        
        if(!linesStr.isEmpty())
        {
            String lines[] = newline.split(linesStr);

            SSFSentence sentence = treetaggerPOS2SSF(lines);

            ps.print(sentence.convertToPOSTagged());
        }
        
        reader.close();
        ps.close();
    }
    
    public static SSFSentence readPOSTagged(String senStr)
    {
        SSFSentence sen = new SSFSentenceImpl();
        
        SSFPhrase root = new SSFPhrase();
        
        sen.setRoot(root);
        
        String words[] = senStr.split("\\s+");
        
        for (int i = 0; i < words.length; i++) {
            String wordTag = words[i];
            
            String wt[] = wordTag.split("_");

            if(wt.length == 2)
            {
                try {
                    SSFLexItem lex = new SSFLexItem("0", wt[0], wt[1], "");

                    root.addChild(lex);
                } catch (Exception ex) {
                    Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else
            {
                System.err.println("Something wrong with the format: " + wordTag);
            }
        }
        
        return sen;
    }
    
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tagger", "t", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tagset", "ts", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("nbest", "nb", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("language", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        POSTaggerType taggerType = POSTaggerType.TREE_TAGGER;
        TagSet tagset = TagSet.PTB_POS;
        String infile = "";
        String nbestfile = "";
        String outfile = "";
        Language language = Language.ENGLISH;
        boolean reverse = false;
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("POSCountsFeatureExtractor [-cs charset] -r (reverse) [-b batchPath] [-t posTaggerType] [-ts tagset] [-l language] [-nb nbestListPath] [-o outfile] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }
        
        if (opt.getSet().isSet("ts")) {
            String ts = opt.getSet().getOption("ts").getResultValue(0);  
            System.out.println("Tagset: " + ts);
            tagset = TagSet.valueOf(ts);
        }

        if (opt.getSet().isSet("t")) {
            String tagger = opt.getSet().getOption("t").getResultValue(0);
            taggerType = POSTaggerType.valueOf(tagger);
            System.out.println("POS Tagger type: " + taggerType.name());
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }
        
        if (opt.getSet().isSet("l")) {
            String lang = opt.getSet().getOption("l").getResultValue(0);  
            language = Language.getLanguage(lang);
            System.out.println("Language: " + language);
        }
        
        if (opt.getSet().isSet("nb")) {
            nbestfile = opt.getSet().getOption("nb").getResultValue(0);  
            System.out.println("N-best list file: " + nbestfile);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse: " + reverse);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode: " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);
        
            System.out.println("Input file: " + infile);
        }
        
        try {
            
            POSCountsFeatureExtractor featureExtractor = new POSCountsFeatureExtractor(tagset, reverse, batchMode);
            
            featureExtractor.getFeatureType().getResourceType().setSrcLangauge(language);
            featureExtractor.setTaggerType(taggerType);

            featureExtractor.setFeatureType(FeatureType.POSCOUNTS);
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(infile, null, null, null, nbestfile);
            }

            if(!reverse)
            {
                featureExtractor.preprocessData();
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
            ToolFlow.getPosTaggerWrapper(taggerType, language).clearTagger();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
