/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import edu.stanford.nlp.mt.base.IString;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.cm.workflow.ToolFlow;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import org.limsi.types.LMType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.properties.PropertyTokens;

/**
 *
 * @author anil
 */
public class SoulLMFeatureExtractor extends SentenceLevelFeatureExtractor {

    protected LMType type;

    protected PropertyTokens soulProbs;
    protected PropertyTokens hypSoulProbs;
    
    protected PropertyTokens hypothesesPT;
    
    public static String SOUL_PREFIX = ".soul-";
    public static String SOUL_SUFFIX_SENT = "sentprobs";
    public static String SOUL_SUFFIX_WRD = "wordprobs";
    
    public SoulLMFeatureExtractor(LMType type, boolean rev, boolean bm)
    {
        super();
                
        this.type = type;
        this.reverse = rev;
        batchMode = bm;        
    }

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            return;
        }
        
        if(featureType.equals(FeatureType.SOUL_LM))
        {
            String pathForSoul = "";
            String cs = featureType.getResourceType().getSrcLangauge().getCharset().charsetName;
            
            if(reverse && featureCollector.getFeatureExtractionCorpus().getTargetText() != null)
            {
                pathForSoul = featureCollector.getFeatureExtractionCorpus().getTargetText().getSSFFile();
            }
            else
            {
                pathForSoul = featureCollector.getFeatureExtractionCorpus().getSourceText().getSSFFile();
            }
                
            pathForSoul += ".raw";
        
            File sFile = new File(pathForSoul);

            if(sFile.exists())
            {
                sFile.delete();
            }
            
            if(type.isLowercase())
            {
                if(reverse && featureCollector.getFeatureExtractionCorpus().getTargetText() != null)
                {
                    featureCollector.getFeatureExtractionCorpus().getTargetText().saveLowerCaseRawText(pathForSoul, cs);                    
                }
                else
                {
                    featureCollector.getFeatureExtractionCorpus().getSourceText().saveLowerCaseRawText(pathForSoul, cs);
                }
            }
            else
            {
                if(reverse && featureCollector.getFeatureExtractionCorpus().getTargetText() != null)
                {
                    featureCollector.getFeatureExtractionCorpus().getTargetText().saveRawText(pathForSoul, cs);                
                }
                else
                {                    
                    featureCollector.getFeatureExtractionCorpus().getSourceText().saveRawText(pathForSoul, cs);                
                }
            }
        
            sFile = new File(pathForSoul + SOUL_PREFIX + SOUL_SUFFIX_WRD);

            if(sFile.exists())
            {
                sFile.delete();
            }
        
            sFile = new File(pathForSoul + SOUL_PREFIX + SOUL_SUFFIX_SENT);

            if(sFile.exists())
            {
                sFile.delete();
            }
            
            String output[] = ToolFlow.runSoulLM(featureType.getResourceType().getSrcLangauge(), pathForSoul);

            MiscellaneousUtils.printStringArray(output, System.out);

            soulProbs = new PropertyTokens(sFile.getAbsolutePath(), cs);
            
            System.out.println("Soul probs file: " + pathForSoul);            
        }
    }    

    @Override
    public void extractAllFeaturesPrelim(int srcIndex) throws FileNotFoundException, IOException
    {
        if(!reverse || featureCollector.getFeatureExtractionCorpus().getNbestList() == null)
        {
            return;
        }
        
        String srcCS = featureType.getResourceType().getSrcLangauge().getCharset().charsetName;
        
        hypothesesPT = new PropertyTokens();
        
        List<TranslationHypothesis<IString, String>> nblist = featureCollector.getFeatureExtractionCorpus().getNbestList().getNbestLists().get(srcIndex);

        int hcount = nblist.size();

        for (int i = 0; i < hcount; i++) {  
            if(type.isLowercase())
            {
                hypothesesPT.addToken(nblist.get(i).getTranslation().toString().toLowerCase());
            }
            else
            {
                hypothesesPT.addToken(nblist.get(i).getTranslation().toString());                
            }
        }        
        
        String soulPath = featureCollector.getFeatureExtractionJob().getSrcPath() + HYPOTHESES_FILE_SUFFIX;

        File sFile = new File(soulPath);

        if(sFile.exists())
        {
            sFile.delete();
        }
        
        hypothesesPT.save(soulPath, srcCS);
        
        sFile = new File(soulPath + SOUL_PREFIX + SOUL_SUFFIX_WRD);
        
        if(sFile.exists())
        {
            sFile.delete();
        }
        
        sFile = new File(soulPath + SOUL_PREFIX + SOUL_SUFFIX_SENT);
        
        if(sFile.exists())
        {
            sFile.delete();
        }
                        
        String output[] = ToolFlow.runSoulLM(featureType.getResourceType().getSrcLangauge(), soulPath);

        MiscellaneousUtils.printStringArray(output, System.out);

        hypSoulProbs = new PropertyTokens(sFile.getAbsolutePath(), srcCS);
            
        System.out.println("Soul probs file: " + soulPath);
    }

    @Override
    public List<HypothesisFeatures> extractAllFeatures(int srcIndex, List<HypothesisFeatures> intoList)
    {
        try {
            extractAllFeaturesPrelim(srcIndex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SoulLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SoulLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return super.extractAllFeatures(srcIndex, intoList);
    }

    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        double soulScore = 0.0;
                
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }

            soulScore = Double.parseDouble(hypSoulProbs.getToken(hypIndex));
        }
        else
        {
            soulScore = Double.parseDouble(soulProbs.getToken(srcIndex));
        }
        
        int len = ssfSrc.countWords();

        if(intoThisHF == null) {
            intoThisHF = new HypothesisFeatures<String>();
        }
 
        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            if(featureSubType.equals(FeatureSubTypesEnum.SOUL_LM_SCORE))
            {
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName, soulScore, len, true, true, reverse);
            }
        }
        
        return intoThisHF;
    }    
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 1, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("language", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        Language language = Language.ENGLISH;
        boolean reverse = false;
        String infile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";
        LMType lmtype = LMType.SOUL_LM;

        if (!opt.check()) {
            System.out.println("SoulLMFeatureExtractor [-cs charset] [-r <ifreverse?>]\n"
                    + "[-o outfile] [-r <ifreverse>] [-b batchPath] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }
        
        if (opt.getSet().isSet("l")) {
            String lang = opt.getSet().getOption("l").getResultValue(0);  
            language = Language.getLanguage(lang);
            System.out.println("Language: " + language);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse: " + reverse);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file: " + infile);
        }
        
        try {            
            SoulLMFeatureExtractor featureExtractor = new SoulLMFeatureExtractor(lmtype, reverse, batchMode);            

            featureExtractor.setFeatureType(FeatureType.SOUL_LM);
            
            FeatureType.SOUL_LM.getResourceType().setSrcLangauge(language);

//            ResourceFlow.setCustomNGramLMPath(true);
//            ResourceFlow.setCustomNGramCountsPath(true);
//            
//            ResourceFlow.setNgramLMTextPath(textPath);
//            ResourceFlow.setNgramLMPath(lmPath);
//            ResourceFlow.setNgramLMBinaryPath(binaryLMPath);
//            ResourceFlow.setNgramCountsPath(lmPath);
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(infile, null, null, null, null);
                featureExtractor.preprocessData();
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
