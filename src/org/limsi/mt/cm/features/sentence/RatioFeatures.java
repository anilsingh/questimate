/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.limsi.cm.resources.CEResources;
import org.limsi.types.FormatType;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class RatioFeatures {

    protected String srcFeaturePath;
    protected String tgtFeaturePath;
    protected String charset;

    protected boolean batchMode;
    protected ArrayList<String> featureNames;

    protected SanchayTableModel batchPaths;
    
    public RatioFeatures(boolean bm, String fns, String cs)
    {
        charset = cs;
        
        CEResources.setSSFProps();
        
        batchMode = bm;

        if(fns != null)
        {
            featureNames = new ArrayList<String>();

            String fn[] = fns.split(",");

            featureNames.addAll(Arrays.asList(fn));        
        }
    }
    
    public void setPaths(String spath, String tpath)
    {
        srcFeaturePath = spath;
        tgtFeaturePath = tpath;
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void loadBatchPaths(String batchPaths) throws FileNotFoundException, IOException {
        this.batchPaths = new SanchayTableModel(batchPaths, "UTF-8");
        
        this.batchPaths.read(batchPaths, "UTF-8");
        
//        this.batchPaths.print(System.out);
    }
    
    public void ratioFeatures(String outpath, String cs)
            throws UnsupportedEncodingException, IOException
    {
        charset = cs;
        
        LineNumberReader srcReader;
        LineNumberReader tgtReader;

        if (srcFeaturePath.endsWith(".gz")) {
            srcReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(srcFeaturePath)), charset));
        } else {
            srcReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    srcFeaturePath), charset));
        }

        if (tgtFeaturePath.endsWith(".gz")) {
            tgtReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(tgtFeaturePath)), charset));
        } else {
            tgtReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    tgtFeaturePath), charset));
        }

        PrintStream ps = new PrintStream(outpath, charset);

        int s = 0;

        for (String srcLine, tgtLine; (srcLine = srcReader.readLine()) != null && (tgtLine = tgtReader.readLine()) != null;)
        {
            System.err.printf("Processing sentence: " + ++s + "\r");
            
            srcLine = srcLine.trim();
            tgtLine = tgtLine.trim();

            HypothesisFeatures<String> hfSrc = new HypothesisFeatures<String>(); 
            HypothesisFeatures<String> hfTgt = new HypothesisFeatures<String>(); 

            hfSrc.readFeatures(srcLine);
            hfTgt.readFeatures(tgtLine);
            
            HypothesisFeatures<String> hfRatio = hfSrc.ratioFeatures(hfTgt, featureNames);
            
            hfRatio.printFeatures(ps, FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT);
        }
        
        srcReader.close();
        tgtReader.close();
        ps.close();
    }

    public void ratioFeaturesBatch(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception
    {
        if(batchMode == false)
        {
            ratioFeatures(outFilePath, encoding);
            return;
        }

        if(batchPaths == null)
        {
            System.err.println("Batch paths not loaded.");
            return;
        }
        
        int rcount = batchPaths.getRowCount();
        
        for (int i = 0; i < rcount; i++)
        {
            featureNames = new ArrayList<String>();
    
            String featureNamesString = (String) batchPaths.getValueAt(i, 0);

            String fn[] = featureNamesString.split(",");
            
            featureNames.addAll(Arrays.asList(fn));

            srcFeaturePath = (String) batchPaths.getValueAt(i, 1);
            tgtFeaturePath = (String) batchPaths.getValueAt(i, 2);
            String outpath = (String) batchPaths.getValueAt(i, 3);
            charset = (String) batchPaths.getValueAt(i, 4);

            System.out.printf("Processing files %s and %s\n", srcFeaturePath, tgtFeaturePath);
            
            ratioFeatures(outpath, charset);
        }
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 2);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("fn", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String featureNames = "";
        String srcfile = "";
        String tgtfile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("RatioFeatures [-cs charset] [-fn featureNames] [-b batchPath] [-o outfile] [infile outfile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }

        if (opt.getSet().isSet("fn")) {
            featureNames = opt.getSet().getOption("fn").getResultValue(0);
            System.out.println("Feature names: " + featureNames);
        }

        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }

        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            srcfile = opt.getSet().getData().get(0);
            tgtfile = opt.getSet().getData().get(1);

            System.out.println("Source input file " + srcfile);
            System.out.println("Target input file " + tgtfile);
        }
        
        try {            
            RatioFeatures ratioFeatures = new RatioFeatures(batchMode, featureNames, charset);
            
            if(batchMode)
            {
                ratioFeatures.loadBatchPaths(batchPath);
            }
            else
            {
                ratioFeatures.setPaths(srcfile, tgtfile);
            }
            
            ratioFeatures.ratioFeaturesBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                
}
