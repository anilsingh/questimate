/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.limsi.cm.util.FeatureValueUtils;
import sanchay.corpus.ssf.SSFSentence;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;

/**
 *
 * @author anil
 */
public class SurfaceFeatureExtractor extends SentenceLevelFeatureExtractor {
    
    public SurfaceFeatureExtractor (boolean rev, boolean bm) {
        super();
        
        reverse = rev;
        batchMode = bm;
   }

    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);

        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();        
        
        double slen = ssfSrc.getRoot().getAllLeaves().size();

        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            if(featureSubType.equals(FeatureSubTypesEnum.SENTENCE_LENGH))
            {
                double value = slen;
                
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, slen, false, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.AVG_TOKEN_LENGH))
            {
                double value = MiscellaneousUtils.averageTokenLength(ssfSrc);
                
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, slen, false, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.TYPE_TOKEN_RATIO))
            {
                double value = MiscellaneousUtils.averageTypeTokenRatio(ssfSrc);                
                
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, slen, false, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.NUMBER_PUNC))
            {
                double value = MiscellaneousUtils.numOfPunctuations(ssfSrc);
                                
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, slen, true, true, reverse);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.NUMBER_CAPS))
            {
                double value = MiscellaneousUtils.numOfCapitalLetters(ssfSrc);
                                
                FeatureValueUtils.addFeature(intoThisHF, featureSubType.featureName,
                        value, slen, true, true, reverse);
            }
        }

        return intoThisHF;
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String infile = "";
        String outfile = "";
        boolean reverse = false;
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("SurfaceFeatureExtractor [-cs charset] [-o outfile] [-b batchPath] [infilet]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse " + reverse);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file " + infile);
        }
        
        try {            
            SurfaceFeatureExtractor featureExtractor = new SurfaceFeatureExtractor(reverse, batchMode);            

            featureExtractor.setFeatureType(FeatureType.SURFACE);
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(infile, null, null, null, null);
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
