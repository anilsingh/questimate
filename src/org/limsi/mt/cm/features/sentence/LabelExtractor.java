/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.LabelType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.properties.PropertiesTable;
import ter.core.Alignment;
import ter.core.TerScorer;

/**
 *
 * @author anil
 */
public class LabelExtractor extends SentenceLevelFeatureExtractor {
    
    protected LabelType type;
    protected boolean detailed = false;
    
    protected TerScorer terScorer;
        
    public LabelExtractor (LabelType type, boolean bm) {
        super();
        
        this.type = type;
        batchMode = bm;
          
//        CEResources.loadNBestList(false);
//
//        nbestList = CEResources.getNbestList();
   }

    @Override
    public void preprocessData() throws IOException, ClassNotFoundException, TreeTaggerException
    {
        if(!reverse)
        {
            return;
        }
        
        if(type.equals(LabelType.HTER))
        {
            terScorer = new TerScorer();
            
            terScorer.setCase(!type.isLowercase());
        }
        else if(type.equals(LabelType.MANUAL) || type.equals(LabelType.MANUAL_WEIGHTED_AVG))
        {
            featureCollector.getFeatureExtractionCorpus().loadLabels();
        }
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        if(!reverse)
        {
            return intoThisHF;
        }

        SSFSentence ssfSrc = getCopy(srcIndex, reverse);
        SSFSentence ssfRef = getReferenceCopy(srcIndex, false);
        SSFSentence ssfPE = getReferenceCopy(srcIndex, true);
        
        String labelString = null;
        
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(type.isLowercase())
        {
            ssfSrc.convertToLowerCase();

            if(ssfRef != null)
            {
                ssfRef.convertToLowerCase();
            }

            if(ssfPE != null)
            {
                ssfPE.convertToLowerCase();
            }
        }
        
        if(intoThisHF == null) {
            intoThisHF = new HypothesisFeatures<String>();
        }        
        
        int len = ssfSrc.getRoot().getAllLeaves().size();
        
        double score = 0.0;
        
        String hyp = ssfSrc.convertToRawText().trim();

        String ref = null;
        String pedit = null;

        if(ssfRef != null)
        {
            ref = ssfRef.convertToRawText().trim();
        }
 
        if(ssfPE != null)
        {
            pedit = ssfPE.convertToRawText().trim();
        }

        if(type.equals(LabelType.MANUAL_WEIGHTED_AVG) && featureCollector.getFeatureExtractionCorpus().getMultipleLabels() != null)
        {
            PropertiesTable labelTable = featureCollector.getFeatureExtractionCorpus().getMultipleLabels();
            PropertiesTable weightsTable = featureCollector.getFeatureExtractionCorpus().getMultipleLabelWeights();
            
            String weightString = null;
            
            int ccount = labelTable.getColumnCount();
            
            for (int i = 0; i < ccount; i++) {
                labelString = (String) labelTable.getValueAt(srcIndex, i);

                score = Double.parseDouble(labelString);

                if(weightsTable != null)
                {
                    weightString = (String) weightsTable.getValueAt(srcIndex, i);

                    double weight = Double.parseDouble(weightString);
                    
                    score *= weight;
                }
                
                String featureName = FeatureSubTypesEnum.LABEL.featureName + (i + 1);
                
                featureType.registerFeatureName(featureName);

                FeatureValueUtils.addLabel(intoThisHF, featureName, score, len, false);
            }            
        }
        
        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            if(featureSubType.equals(FeatureSubTypesEnum.LABEL))
            {
                if((type.equals(LabelType.MANUAL) || type.equals(LabelType.MANUAL_WEIGHTED_AVG))
			&& featureCollector.getFeatureExtractionCorpus().getLabels() != null)
                {
                    labelString = featureCollector.getFeatureExtractionCorpus().getLabels().getToken(srcIndex);

                    score = Double.parseDouble(labelString);
                
                    FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                }
            }
        
            if(type.equals(LabelType.HTER))
            {
                if(ssfRef != null)
                {
                    terScorer.setRefLen(ssfRef.countWords());

                    Alignment alignment = terScorer.TER(hyp, ref);

                    alignment.populateScoreDetails();

                    if(featureSubType.equals(FeatureSubTypesEnum.HTER_REF))
                    {
    //                    score = alignment.score();
                        score = alignment.score() > 1.0 ? 1.0: alignment.score();

                        FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                    }
    //                else if(featureSubType.equals(FeatureSubTypesEnum.CLIPPED_HTER_REF))
    //                {                    
    //                    score = alignment.score() > 1.0 ? 1.0: alignment.score();
    //                    
//                        FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
    //                }

                    if(type.isDetailed())
                    {
                        if(featureSubType.equals(FeatureSubTypesEnum.NUM_DEL_HTER_REF))
                        {
                            score = alignment.numDel;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_INS_HTER_REF))
                        {
                            score = alignment.numIns;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_SUB_HTER_REF))
                        {
                            score = alignment.numSub;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_SHIFTS_HTER_REF))
                        {
                            score = alignment.numSft;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_WSHIFTS_HTER_REF))
                        {
                            score = alignment.numWsf;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_EDITS_HTER_REF))
                        {
                            score = alignment.numEdits;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_WORDS_HTER_REF))
                        {
                            score = alignment.numWords;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                    }
                }

                if(ssfPE != null)
                {
                    terScorer.setRefLen(ssfPE.countWords());

                    Alignment alignment = terScorer.TER(hyp, pedit);

                    alignment.populateScoreDetails();

                    if(featureSubType.equals(FeatureSubTypesEnum.HTER_POST_EDIT))
                    {
    //                    score = alignment.score();
                        score = alignment.score() > 1.0 ? 1.0: alignment.score();

                        FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                    }
    //                else if(featureSubType.equals(FeatureSubTypesEnum.CLIPPED_HTER_POST_EDIT))
    //                {                    
    //                    score = alignment.score() > 1.0 ? 1.0: alignment.score();
    //                    
//                        FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
    //                }

                    if(type.isDetailed())
                    {
                        if(featureSubType.equals(FeatureSubTypesEnum.NUM_DEL_HTER_POST_EDIT))
                        {
                            score = alignment.numDel;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_INS_HTER_POST_EDIT))
                        {
                            score = alignment.numIns;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_SUB_HTER_POST_EDIT))
                        {
                            score = alignment.numSub;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_SHIFTS_HTER_POST_EDIT))
                        {
                            score = alignment.numSft;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_WSHIFTS_HTER_POST_EDIT))
                        {
                            score = alignment.numWsf;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_EDITS_HTER_POST_EDIT))
                        {
                            score = alignment.numEdits;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                        else if(featureSubType.equals(FeatureSubTypesEnum.NUM_WORDS_HTER_POST_EDIT))
                        {
                            score = alignment.numWords;

                            FeatureValueUtils.addLabel(intoThisHF, featureSubType.featureName, score, len, false);
                        }
                    }
                }
            }
        }

        return intoThisHF;
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 2);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("labelType", "lt", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("d", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String srcInfile = "";
        String tgtInfile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";
        boolean detailed = false;
        LabelType type = LabelType.HTER;

        if (!opt.check()) {
            System.out.println("TERExtractor [-cs charset] [-o outfile] [-lt labelType [-d detailed]] [-b batchPath] [infilesrc infiletgt]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }
 
        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        if (opt.getSet().isSet("lt")) {
            String ltypeString = opt.getSet().getOption("lt").getResultValue(0);  
            System.out.println("Label type: " + ltypeString);
            type = LabelType.valueOf(ltypeString);
        }
        
        if (opt.getSet().isSet("d")) {
            detailed = true;  
            System.out.println("Detailed info about TER: " + detailed);
            type.setDetailed(true);
        }
        else
        {
            type.setDetailed(false);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            srcInfile = opt.getSet().getData().get(0);
            tgtInfile = opt.getSet().getData().get(1);

            System.out.println("Source input file " + srcInfile);
            System.out.println("Target input file " + tgtInfile);

            CEAnnotatedCorpusWMT12.setSourcePath(srcInfile);
            CEAnnotatedCorpusWMT12.setTargetPath(tgtInfile);
        }
        
        try {            
            LabelExtractor featureExtractor = new LabelExtractor(type, batchMode);            
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {
                
//                featureExtractor.loadCorpusData();
            }
            
            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
