/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features.sentence;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import sanchay.corpus.ssf.SSFSentence;
import org.limsi.cm.resources.WordTranslationScores;
import org.limsi.cm.workflow.ResourceFlow;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import org.limsi.types.TranslationModelType;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;

/**
 *
 * @author anil
 */
public class WordTranslationFeatureExtractor extends SentenceLevelFeatureExtractor {

    protected TranslationModelType type;
    
    protected WordTranslationScores wordTranslationScores;
    
    public WordTranslationFeatureExtractor () {
        super();
   }

   public WordTranslationFeatureExtractor (TranslationModelType type, boolean reverse, boolean bm) {
        this();

        this.type = type;
        this.reverse = reverse;
        batchMode = bm;
    }

    /**
     * @return the wordTranslationScores
     */
    public WordTranslationScores getWordTranslationScores() {
        return wordTranslationScores;
    }

    /**
     * @param wordTranslationScores the wordTranslationScores to set
     */
    public void setWordTranslationScores(WordTranslationScores wordTranslationScores) {
        this.wordTranslationScores = wordTranslationScores;
    }
    
    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {     
        SSFSentence sent;

        SSFSentence ssfSrc = getCopy(srcIndex, false);
        SSFSentence ssfTgt = getCopy(srcIndex, true);
        
        if(featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfTgt = featureCollector.getFeatureExtractionCorpus().getTargetText().getSentence(srcIndex);            
            ssfTgt = new SSFSentenceImpl();

            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfTgt.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(reverse)
        {
            sent = ssfSrc;

            ssfSrc = ssfTgt;

            ssfTgt = sent;
        }

        if(type.isLowercase())
        {
            ssfSrc.convertToLowerCase();        
            ssfTgt.convertToLowerCase();
        }
        
        int slen = ssfSrc.getRoot().getAllLeaves().size();
        int tlen = ssfTgt.getRoot().getAllLeaves().size();

        if(intoThisHF == null)
            intoThisHF = new HypothesisFeatures<String>();
        
        List<FeatureSubTypesEnum> featureSubTypes = getFeatureType().getFeatureSubTypes();

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            
            if(featureSubType.equals(FeatureSubTypesEnum.IBM1_SCORE))
            {
                double value = wordTranslationScores.getIBM1Score(ssfSrc, ssfTgt);
                
                String prefix = "s2t";

                if(reverse)
                {
                    prefix = "t2s";
                }

                intoThisHF.addFeature(prefix + featureSubType.featureName, value);
                intoThisHF.addFeature("normalized_slen_" + prefix + featureSubType.featureName, value / (double) slen);
                intoThisHF.addFeature("normalized_tlen_" + prefix + featureSubType.featureName, value / (double) tlen);            
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.AVG_NUM_TRANS02))
            {
                double value = wordTranslationScores.getAvgNumTranslations(ssfSrc, 0.02);
                
                String prefix = "s2t";

                if(reverse)
                {
                    prefix = "t2s";
                }

                intoThisHF.addFeature(prefix + featureSubType.featureName, value);
            }
            else if(featureSubType.equals(FeatureSubTypesEnum.AVG_NUM_TRANS001))
            {
                double value = wordTranslationScores.getAvgNumTranslations(ssfSrc, 0.001);
                
                String prefix = "s2t";

                if(reverse)
                {
                    prefix = "t2s";
                }

                intoThisHF.addFeature(prefix + featureSubType.featureName, value);
            }
        }
                
        return intoThisHF;
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 2, 3);

        opt.getSet().addOption("srcLanguage", "sl", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("tgtLanguage", "tl", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("reverse", "r", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("batchMode", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        Language srcLang = Language.ENGLISH;
        Language tgtLang = Language.FRENCH;
        String charset = "UTF-8";
        String srcInfile = "";
        String tgtInfile = "";
        String wtfile = "";
        String outfile = "";
        boolean reverse = false;
        boolean batchMode = false;
        String batchPath = "";
        TranslationModelType tmtype = TranslationModelType.IBM1;

        if (!opt.check()) {
            System.out.println("WordTranslationFeatureExtractor -sl srcLang -tl tgtLang -r (reverse) [-cs charset] [-o outfile] [-b batchPath] [srcInfile tgtInfile] wtScores");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("srcLanguage")) {
            String langStr = opt.getSet().getOption("srcLanguage").getResultValue(0);
            srcLang = Language.getLanguage(langStr);
            System.out.println("Source language: " + srcLang.getName());
        }

        if (opt.getSet().isSet("tgtLanguage")) {
            String langStr = opt.getSet().getOption("tgtLanguage").getResultValue(0);
            tgtLang = Language.getLanguage(langStr);
            System.out.println("Target language: " + tgtLang.getName());
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }
        
        if (opt.getSet().isSet("r")) {
            reverse = true;  
            System.out.println("Reverse: " + reverse);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode: " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(batchMode)
        {
            wtfile = opt.getSet().getData().get(0);
        }
        else
        {
            srcInfile = opt.getSet().getData().get(0);
            tgtInfile = opt.getSet().getData().get(1);
        
            System.out.println("Source input file: " + srcInfile);
            System.out.println("Target input file: " + tgtInfile);

            wtfile = opt.getSet().getData().get(2);
        }
        
        try {
            
            ResourceFlow.setCustomWordTranslationScoresPath(true);
            ResourceFlow.setWordTranslationScoresPath(wtfile);
            
            srcLang.setCharset(Language.Charset.valueOf(charset));
            tgtLang.setCharset(Language.Charset.valueOf(charset));
            
            ResourceFlow.loadWordTranslationData(srcLang, tgtLang, batchMode);
            
            WordTranslationFeatureExtractor featureExtractor = new WordTranslationFeatureExtractor(tmtype, reverse, batchMode);
            
            featureExtractor.setFeatureType(FeatureType.IBM1);
            
            if(batchMode)
            {
                featureExtractor.loadBatchPaths(batchPath);
            }
            else
            {                
//                featureExtractor.loadCorpusData(srcInfile, tgtInfile, null, null, null);
            }
            
            featureExtractor.setWordTranslationScores(ResourceFlow.getWordTranslationScores());

            featureExtractor.extractAllBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
