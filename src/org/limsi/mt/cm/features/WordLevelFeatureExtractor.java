/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;

/**
 *
 * @author anil
 */
public abstract class WordLevelFeatureExtractor extends SentenceLevelFeatureExtractor
        implements FeatureExtractor {

    @Override
    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        SSFSentence ssfSrc = getCopy(srcIndex, reverse);
                
        if(reverse && featureCollector.getFeatureExtractionCorpus().getNbestList() != null)
        {
            ssfSrc = new SSFSentenceImpl();
            
            String hypothesis = getTranslationHypothesis(srcIndex, hypIndex).getTranslation().toString();

            try {
                ssfSrc.makeSentenceFromRaw(hypothesis);
            } catch (Exception ex) {
                Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int len = ssfSrc.countWords();

        if(intoThisHF == null) {
            intoThisHF = new HypothesisFeatures<String>();
        }
        
        for (int i = 0; i < len; i++) {
            
            collectHypothesisFeatures(srcIndex, hypIndex, i, intoThisHF);            
        }
         
        return intoThisHF;
    }    

    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            int wordIndex, HypothesisFeatures intoThisHF)
    {
        HypothesisFeatures hf = null;
        
        return hf;        
    }
    
}
