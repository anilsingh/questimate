/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features;

import java.util.ArrayList;
import java.util.Iterator;
import org.limsi.types.FeatureSubTypesEnum;
import sanchay.util.Pair;

/**
 *
 * @author anil
 */
public interface FeatureValues<FV extends Object> {

    void addFeature(String featureName, FV featureValue);

    void removeFeature(Integer fIndex);

    void renameFeature(String oldName, String newName);

    void renameFeatures(ArrayList<String> oldNames, ArrayList<String> newNames);

    void clear();

    boolean containsIndex(Integer i);

    boolean containsName(String n);

    boolean containsValue(FV fv);

    Iterator<Integer> getFeatureIndices();

    String getFeatureNameFromIndex(int index);

    ArrayList<String> getFeatureNames();

    FV getFeatureValue(String featureName);

    FV getFeatureValueFromIndex(int index);
    
    void getFeaturePair(FeatureSubTypesEnum featureSubType, Pair pair);

    boolean isEmpty();

    int size();    
}
