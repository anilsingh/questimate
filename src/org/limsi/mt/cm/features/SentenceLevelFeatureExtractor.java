/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.features;

import edu.stanford.nlp.mt.base.IString;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.util.TagSet;
import org.limsi.cm.workflow.FeatureCollector;
import org.limsi.cm.workflow.FeatureExtractionJob;
import org.limsi.cm.workflow.ResourceFlow;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.mt.cm.features.sentence.LabelExtractor;
import org.limsi.mt.cm.features.sentence.LatticeFeatureExtractor;
import org.limsi.mt.cm.features.sentence.NBestListFeatureExtractor;
import org.limsi.mt.cm.features.sentence.NGramCountsFeatureExtractor;
import org.limsi.mt.cm.features.sentence.NGramLMFeatureExtractor;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;
import org.limsi.mt.cm.features.sentence.SoulLMFeatureExtractor;
import org.limsi.mt.cm.features.sentence.SurfaceFeatureExtractor;
import org.limsi.mt.cm.features.sentence.WordTranslationFeatureExtractor;
import org.limsi.types.FeatureType;
import org.limsi.types.FormatType;
import org.limsi.types.LMType;
import org.limsi.types.LabelType;
import org.limsi.types.TranslationModelType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public abstract class SentenceLevelFeatureExtractor implements FeatureExtractor {

    protected FeatureCollector featureCollector;
    
    protected FeatureType featureType;
    
    protected boolean reverse;
    protected boolean batchMode;

    protected SanchayTableModel batchPaths;
    
    protected static String HYPOTHESES_FILE_SUFFIX = ".hyp";
    
    public SentenceLevelFeatureExtractor() {
        CEResources.setSSFProps();
        
//        loadCorpusData();
    }

    /**
     * @return the featureCollector
     */
    public FeatureCollector getFeatureCollector() {
        return featureCollector;
    }

    /**
     * @param featureCollector the featureCollector to set
     */
    public void setFeatureCollector(FeatureCollector featureCollector) {
        this.featureCollector = featureCollector;
    }
    
    public void preprocessData() throws  IOException, ClassNotFoundException, TreeTaggerException
    {
//        ToolFlow.getPosTaggerWrapper().setDocument(sourceText);
//        ToolFlow.getPosTaggerWrapper().runTagger(srcLanguage);
    }
    
    protected SSFSentence getCopy(int srcIndex, boolean target)
    {
        SSFSentence ssfSentence = null;
  
        try {
            if(!target || featureCollector.getFeatureExtractionCorpus().getTargetText() == null)
            {
                ssfSentence = (SSFSentence) featureCollector.getFeatureExtractionCorpus().getSourceText().getSentence(srcIndex).getCopy();
            }
            else if(featureCollector.getFeatureExtractionCorpus().getTargetText() != null)
            {
                ssfSentence = (SSFSentence) featureCollector.getFeatureExtractionCorpus().getTargetText().getSentence(srcIndex).getCopy();                
            }
        } catch (Exception ex) {
            Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ssfSentence;
    }
    
    protected SSFSentence getReferenceCopy(int srcIndex, boolean postEdited)
    {
        SSFSentence ssfSentence = null;
  
        try {
            if(!postEdited)
            {
                if(featureCollector.getFeatureExtractionCorpus().getReferenceText() != null)
                {
                    ssfSentence = (SSFSentence) featureCollector.getFeatureExtractionCorpus().getReferenceText().getSentence(srcIndex).getCopy();
                }
            }
            else 
            {
                if(featureCollector.getFeatureExtractionCorpus().getPostEditedText() != null)
                {
                    ssfSentence = (SSFSentence) featureCollector.getFeatureExtractionCorpus().getPostEditedText().getSentence(srcIndex).getCopy();                
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ssfSentence;
    }

    /**
     * @return the featureType
     */
    public FeatureType getFeatureType() {
        return featureType;
    }

    /**
     * @param featureType the featureType to set
     */
    public void setFeatureType(FeatureType featureType) {
        this.featureType = featureType;
    }

    /**
     * @return whether reverse
     */
    public boolean isReverse() {
        return reverse;
    }

    /**
     * @param reverse whether to set as reverse
     */
    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    /**
     * @return the batchMode
     */
    public boolean isBatchMode() {
        return batchMode;
    }

    /**
     * @param batchMode the batchMode to set
     */
    public void setBatchMode(boolean batchMode) {
        this.batchMode = batchMode;
    }

    /**
     * @return the batchPaths
     */
    public SanchayTableModel getBatchPaths() {
        return batchPaths;
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void setBatchPaths(SanchayTableModel batchPaths) {
        this.batchPaths = batchPaths;
    }
    
    public TranslationHypothesis getTranslationHypothesis(int srcIndex, int hypIndex)
    {
        List<List<TranslationHypothesis<IString, String>>> nblists = featureCollector.getFeatureExtractionCorpus().getNbestList().getNbestLists();

        return nblists.get(srcIndex).get(hypIndex);
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void loadBatchPaths(String batchPaths) throws FileNotFoundException, IOException {
        this.batchPaths = new SanchayTableModel(batchPaths, "UTF-8");
        
        this.batchPaths.read(batchPaths, "UTF-8");
        
//        this.batchPaths.print(System.out);
    }

    public HypothesisFeatures collectHypothesisFeatures(int srcIndex, int hypIndex,
            HypothesisFeatures intoThisHF)
    {
        HypothesisFeatures hf = null;
        
        return hf;        
    }

    public HypothesisFeatures testFeatureExtraction()
    {
//        int srcIndex = (int) Math.random() * nbestList.getNbestLists().size();
//        
//        int hypIndex = (int) (Math.random() * nbestList.getNbestLists().get(srcIndex).size());
        
        HypothesisFeatures hf = collectHypothesisFeatures(0, 0, null);
//        HypothesisFeatures hf = collectHypothesisFeatures(srcIndex, hypIndex, null);
        
//        hf.printFeatures(System.out);
        
        return hf;
    }

    public List<HypothesisFeatures> extractAllFeatures(List<HypothesisFeatures> intoList)
    {
        int srcCount = featureCollector.getFeatureExtractionCorpus().getSourceText().countSentences();
        
        List<HypothesisFeatures> hfList = new LinkedList<HypothesisFeatures>();
        
        if(intoList != null)
        {
            hfList = intoList;
        }
        
        for (int i = 0; i < srcCount; i++)
        {
            System.out.printf("Processing sentence %d\r", (i + 1));

            if(intoList == null)
            {
                HypothesisFeatures hf = collectHypothesisFeatures(i, 0, null);
                hfList.add(hf);
            }
            else
            {
                HypothesisFeatures hf = intoList.get(i);
                hf = collectHypothesisFeatures(i, 0, hf);

                hfList.add(hf);
            }
        }
        
        return hfList;
    }

    /**
     * Any necessary preliminary processing for hypothesis-wise feature extraction.
     * This can be especially useful for features that have to be extracted using
     * external tools.
     * @param srcIndex 
     */
    public void extractAllFeaturesPrelim(int srcIndex) throws FileNotFoundException, IOException
    {
        
    }

    public List<HypothesisFeatures> extractAllFeatures(int srcIndex, List<HypothesisFeatures> intoList)
    {
        int srcCount = featureCollector.getFeatureExtractionCorpus().getSourceText().countSentences();
        
        List<HypothesisFeatures> hfList = new LinkedList<HypothesisFeatures>();

        System.out.printf("Processing sentence %d\r", (srcIndex + 1));
        
        if(intoList != null)
        {
            hfList = intoList;
        }
        
        if(featureCollector.getFeatureExtractionCorpus().getNbestList() == null
                || featureCollector.getFeatureExtractionJob().isOneBest())
        {
            if(intoList == null)
            {
                HypothesisFeatures hf = collectHypothesisFeatures(srcIndex, 0, null);
                hfList.add(hf);
            }
            else
            {
//                HypothesisFeatures hf = intoList.get(srcIndex);
                HypothesisFeatures hf = intoList.get(0);
                hf = collectHypothesisFeatures(srcIndex, 0, hf);

                hfList.set(0, hf);
//                hfList.add(hf);
            }
        }
        else
        {
            List<List<TranslationHypothesis<IString, String>>> nblists = featureCollector.getFeatureExtractionCorpus().getNbestList().getNbestLists();
           
//            if(srcCount != nblists.size())
//            {
//                System.err.println("The number of sentences in the source corpus not the same as in the nbest list: "
//                        + srcCount + " : " + nblists.size());
//            }
            
            List<TranslationHypothesis<IString, String>> nblist = nblists.get(srcIndex);

            int hcount = nblist.size();

            for (int j = 0; j < hcount; j++) {  

//                System.out.printf("Processing hypothesis %d\r", (j + 1));

                if(intoList == null)
                {
                    HypothesisFeatures hf = collectHypothesisFeatures(srcIndex, j, null);
                    hfList.add(hf);
                }
                else
                {
                    HypothesisFeatures hf = intoList.get(j);
                    hf = collectHypothesisFeatures(srcIndex, j, hf);
                    
                    hfList.set(j, hf);

//                    hfList.add(hf);
                }                    
            }
        }
        
        return hfList;
    }
     
    public void extractAll(String outFilePath, String encoding)  throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        PrintStream ps = new PrintStream(outFilePath, encoding);
        
        int count = featureCollector.getFeatureExtractionCorpus().getSourceText().countSentences();
        
        for (int i = 0; i < count; i++) {
            System.out.printf("Processing sentence: %d\r", (i + 1));
            
            if(featureCollector.getFeatureExtractionCorpus().getNbestList() == null)
            {
                HypothesisFeatures hf = collectHypothesisFeatures(i, 0, null);

                hf.printFeatures(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);            
            }
            else
            {
                List<List<TranslationHypothesis<IString, String>>> nblists = featureCollector.getFeatureExtractionCorpus().getNbestList().getNbestLists();

    //            if(srcCount != nblists.size())
    //            {
    //                System.err.println("The number of sentences in the source corpus not the same as in the nbest list: "
    //                        + srcCount + " : " + nblists.size());
    //            }

                List<TranslationHypothesis<IString, String>> nblist = nblists.get(i);

                int hcount = nblist.size();

                for (int j = 0; j < hcount; j++) {  

                    System.out.printf("Processing hypothesis: %d\r", (j + 1));

                    HypothesisFeatures hf = collectHypothesisFeatures(i, j, null);
                    
                    ps.print(i + " ");

                    hf.printFeatures(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);            
                }
            }
        }
    }
     
    public void extractAllBatch(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception
    {
        if(batchMode == false)
        {
            extractAll(outFilePath, encoding);
            return;
        }

        if(batchPaths == null)
        {
            System.err.println("Batch paths not loaded.");
            return;
        }
        
        int rcount = batchPaths.getRowCount();
        int ccount = batchPaths.getColumnCount();
        
        for (int i = 0; i < rcount; i++)
        {
//            srcPath = (String) batchPaths.getValueAt(i, 0);
//            tgtPath = (String) batchPaths.getValueAt(i, 2);
//            outPath = (String) batchPaths.getValueAt(i, 4);
//            
//            if(ccount > 6)
//            {
//                nbestListPath = (String) batchPaths.getValueAt(i, 6);
//            }
//
//            System.out.printf("Processing files %s and %s\n", srcPath, tgtPath);
            
//            loadCorpusData(srcPath, tgtPath, null, null, nbestListPath);
//            
//            extractAll(outPath, featureType.getTgtCharset());
        }
    }

//    public HypothesisFeatures extractAllFeatures()
//    {
//        int srcCount = sourceText.countSentences();
//        
//        List<HypothesisFeatures> hfList = new LinkedList<HypothesisFeatures>();
//        
//        for (int i = 0; i < srcCount; i++)
//        {
//            if(intoList == null)
//            {
//                HypothesisFeatures hf = collectHypothesisFeatures(i, 0, null);
//                hfList.add(hf);
//            }
//            else
//            {
//                HypothesisFeatures hf = intoList.get(i);
//                hf = collectHypothesisFeatures(i, 0, hf);
//                
//                hfList.add(hf);
//            }
//        }
//        
////        hf.printFeatures(System.out);
//        
//        return hfList;
//    }

    public void printFeatures(PrintStream ps)
    {
        List<HypothesisFeatures> hfList = extractAllFeatures(null);
        
        Iterator<HypothesisFeatures> itr = hfList.iterator();
        
        while(itr.hasNext())
        {
            HypothesisFeatures hf = itr.next();
            
            hf.printFeatures(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);
        }
    }
    
    public void saveFeatures(String featureFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(featureFilePath, encoding);
        
        printFeatures(ps);
    }
    
    public static SentenceLevelFeatureExtractor createFeatureExtractor(FeatureType featureType,
            FeatureExtractionJob featureExtractionJob, boolean reverse) throws FileNotFoundException, IOException
    {
        SentenceLevelFeatureExtractor featureExtractor = null;
        
        if(featureType.equals(FeatureType.SURFACE))
        {
            featureExtractor = new SurfaceFeatureExtractor(reverse, false);
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS))
        {
            featureExtractor = new NGramCountsFeatureExtractor(LMType.NGRAM_COUNTS, reverse, false);
            
//            ((NGramCountsFeatureExtractor) featureExtractor).setNgramLMCounts(ResourceFlow.getNgramLMCounts());
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS))
        {
            featureExtractor = new NGramCountsFeatureExtractor(LMType.NGRAM_COUNTS_POS, reverse, false);
            
//            ((NGramCountsFeatureExtractor) featureExtractor).setNgramLMCounts(ResourceFlow.getNgramLMCounts());
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
        {
            featureExtractor = new NGramCountsFeatureExtractor(LMType.NGRAM_COUNTS_POS_NOLEX, reverse, false);
            
//            ((NGramCountsFeatureExtractor) featureExtractor).setNgramLMCounts(ResourceFlow.getNgramLMCounts());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM))
        {
            featureExtractor = new NGramLMFeatureExtractor(LMType.TEXT_LM, reverse, false);
            
//            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
//            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS))
        {
            featureExtractor = new NGramLMFeatureExtractor(LMType.POS_TAGGED_LM, reverse, true);
            
//            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
//            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
        {
            featureExtractor = new NGramLMFeatureExtractor(LMType.POS_NOLEX_LM, reverse, true);
            
//            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
//            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.IBM1))
        {
            featureExtractor = new WordTranslationFeatureExtractor(TranslationModelType.IBM1, reverse, false);
            
//            ((WordTranslationFeatureExtractor) featureExtractor).setWordTranslationScores(ResourceFlow.getWordTranslationScores());
        }
//        else if(featureType.equals(FeatureType.SOUL_LM))
//        {
//            featureExtractor = new SoulLMFeatureExtractor();
//        }
        else if(featureType.equals(FeatureType.POSCOUNTS))
        {
//            if(!reverse)
//            {
                featureExtractor = new POSCountsFeatureExtractor(TagSet.getTagSet(featureType.getResourceType().getSrcLangauge()), reverse, false);
//            }
//            else
//            {
//                featureExtractor = new POSCountsFeatureExtractor(TagSet.getTagSet(featureType.getResourceType().getTgtLangauge()), reverse, false);                
//            }
        }
        else if(featureType.equals(FeatureType.SOUL_LM))
        {
            featureExtractor = new SoulLMFeatureExtractor(LMType.SOUL_LM, reverse, false);            
        }
        else if(featureType.equals(FeatureType.MODEL))
        {
            featureExtractor = new NBestListFeatureExtractor(reverse);            
        }
        else if(featureType.equals(FeatureType.LATTICE))
        {
            featureExtractor = new LatticeFeatureExtractor(featureExtractionJob.getWordGraphPath(),
                    featureExtractionJob.getWordGraphSuffix(),
                    featureExtractionJob.getTempDirPath(),
                    featureExtractionJob.getWordGraphFormat(), reverse);            
        }
        else if(featureExtractionJob.getLabelType() != null && featureType.equals(FeatureType.LABEL))
        {
            featureExtractor = new LabelExtractor(featureExtractionJob.getLabelType(), false);
        }
        
        return featureExtractor;
    }

    public static void setResources(SentenceLevelFeatureExtractor featureExtractor,
            FeatureType featureType, boolean reverse) throws FileNotFoundException, IOException
    {        
        featureExtractor.setReverse(reverse);
        
        if(featureType.equals(FeatureType.SURFACE))
        {
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS))
        {
            ((NGramCountsFeatureExtractor) featureExtractor).setCumFreqsList(ResourceFlow.getCumFreqsList());
            ((NGramCountsFeatureExtractor) featureExtractor).setNgramCountsIndex(ResourceFlow.getNgramCountsIndex());
            ((NGramCountsFeatureExtractor) featureExtractor).setTopCumFreqList(ResourceFlow.getTopCumFreqList());
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS))
        {
            ((NGramCountsFeatureExtractor) featureExtractor).setCumFreqsList(ResourceFlow.getCumFreqsList());
            ((NGramCountsFeatureExtractor) featureExtractor).setNgramCountsIndex(ResourceFlow.getNgramCountsIndex());
            ((NGramCountsFeatureExtractor) featureExtractor).setTopCumFreqList(ResourceFlow.getTopCumFreqList());
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
        {
            ((NGramCountsFeatureExtractor) featureExtractor).setCumFreqsList(ResourceFlow.getCumFreqsList());
            ((NGramCountsFeatureExtractor) featureExtractor).setNgramCountsIndex(ResourceFlow.getNgramCountsIndex());
            ((NGramCountsFeatureExtractor) featureExtractor).setTopCumFreqList(ResourceFlow.getTopCumFreqList());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM))
        {
            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS))
        {
            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
        {
            ((NGramLMFeatureExtractor) featureExtractor).setBerkeleyLM(ResourceFlow.getBerkeleyLM());
            ((NGramLMFeatureExtractor) featureExtractor).setWordIndexer(ResourceFlow.getWordIndexer());
        }
        else if(featureType.equals(FeatureType.IBM1))
        {
            ((WordTranslationFeatureExtractor) featureExtractor).setWordTranslationScores(ResourceFlow.getWordTranslationScores());
        }
        else if(featureType.equals(FeatureType.SOUL_LM))
        {
//            ((WordTranslationFeatureExtractor) featureExtractor). = new SoulLMFeatureExtractor(reverse, false);
        }
        else if(featureType.equals(FeatureType.POSCOUNTS))
        {
            if(reverse)
            {
                ((POSCountsFeatureExtractor) featureExtractor).setTagSet(TagSet.getTagSet(featureType.getResourceType().getSrcLangauge()));
            }
        }
//        else if(featureType.equals(FeatureType.LABEL))
//        {
//            featureExtractor = new LabelExtractor(false, false);
//        }
    }
}
