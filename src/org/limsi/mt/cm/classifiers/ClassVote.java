package org.limsi.mt.cm.classifiers;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.HypothesisConfidence;
import weka.classifiers.Classifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;

public class ClassVote extends Classifier {

    String classifierName;
    String[] classifierOptions;
    String[] classLabels;
    double[] classWeights;

    FilteredClassifier[] m_Classifiers;
    Instances m_Instances;

    double absErr;

    @Override
    public void buildClassifier(Instances data) {

        m_Instances = new Instances(data);
        
        m_Classifiers = new FilteredClassifier[classLabels.length];
        
        for (int i = 0; i < classLabels.length; i++) {
            String lbl = classLabels[i];
            
            try {
                m_Classifiers[i] = getClassifier(lbl, classifierOptions);
            } catch (Exception ex) {
                Logger.getLogger(ClassVote.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public String getRevision()
    {
        String rev = "Number of classifiers built: " + m_Classifiers.length + "\n";

//        for (int i = 0; i < classLabels.length; i++) {
//            rev += "Number of attributes in classifier for " + classLabels[i] + ": " + m_Classifiers[i].numAttributes() + "\n";
//        }
        
        return rev;
    }

    @Override
    public double classifyInstance(Instance instance) {
        
        double preds[] = new double[m_Classifiers.length];
        double pred = 0.0;
        double actual = 0.0;
        
        for (int i = 0; i < m_Classifiers.length; i++) {            
            
            Classifier m_Classifier = m_Classifiers[i];
 
//            Instance newInstance = new Instance(instance);
//            Instance newInstance = (Instance) instance.copy();
//
//            for (int j = 0; j < classLabels.length; j++) {
//                
//                if(classLabels[j].equals(instance.classAttribute().name()))
//                {
//                    continue;
//                }
//
////                Attribute a = m_Instances.attribute(classLabels[j]);                
//                
////                newInstance.deleteAttributeAt(a.index());
////                newInstance = 
//                newInstance = deleteAttribute(newInstance, classLabels[j], j);
////                newInstance.se
//            }
                       
            try {
                preds[i] = m_Classifier.classifyInstance(instance);
                
//                if(i == preds.length - 1)
//                {
//                    System.out.println(preds[i]);
//                }
//                else
//                {
//                    System.out.print(preds[i] + "\t");
//                }
                
            } catch (Exception ex) {
                Logger.getLogger(ClassVote.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            pred += classWeights[i] * preds[i];
    
            Attribute a = m_Instances.attribute(classLabels[i]);
            
            actual += classWeights[i] * instance.value(a);
        }


        absErr += Math.abs(actual - pred);
            
        if(pred <= 1.0)
        {
            int x = 0;
        }
        
//        pred /= (double) m_Classifiers.length;
        
        return pred;
    }
    
    public Instances averageOfClasses(Instances instances)
    {
        Instances newData = new Instances(instances);
        
        Add addFilter = new Add();
        
        addFilter.setAttributeIndex("last");
//        addFilter.setNominalLabels("1,2,3,4,5");
        addFilter.setAttributeName("label");

        try {
            addFilter.setInputFormat(newData);

            newData = Filter.useFilter(newData, addFilter);
            
        } catch (Exception ex) {
            Logger.getLogger(ClassVote.class.getName()).log(Level.SEVERE, null, ex);
        }

        Attribute label = newData.attribute("label");

        newData.setClass(label);
        
        int count = newData.numInstances();
        
        for (int i = 0; i < count; i++) {
            Instance instance = newData.instance(i);
        
            double pred = 0.0;

            for (int j = 0; j < classLabels.length; j++) {
                
                String name = classLabels[j];
                
                Attribute a = newData.attribute(name);
                
//                if(a.isNumeric())
//                {
                    pred += classWeights[j] * instance.value(a);
//                }
//                else if(a.isNominal())
//                {
//                    pred += classWeights[j] * Double.parseDouble(instance.stringValue(a));
//                }
            }
            
            if(pred <= 1.0)
            {
                int x = 0;
            }
            
//            instance.setClassValue(pred);
            instance.setValue(label, pred);
        }

        newData = HypothesisConfidence.removeAttributes(newData, null, classLabels);
        
//        newData.setClassIndex(newData.numAttributes() - 1);
        
        return newData;
    }
    
    public double getClassValue(Instance instance)
    {
        double actual = 0.0;

        for (int i = 0; i < classLabels.length; i++) {

            String name = classLabels[i];

            Attribute a = m_Instances.attribute(name);

//            if(a.isNumeric())
//            {
                actual += classWeights[i] * instance.value(a);
//            }
//            else if(a.isNominal())
//            {
//                actual += classWeights[i] * Double.parseDouble(instance.stringValue(a));
//            }
        }        
        
        return actual;
    }
    
    public double getNumClasses()
    {
        return classLabels.length;
    }
    
    public double getAbsoluteError(){
        return absErr;
    }
    
    public void resetAbsoluteError(){
        absErr = 0;
    }

    @Override
    public String[] getOptions() {
        String[] options = new String[2];

        int current = 0;

        while (current < options.length) {
            options[current++] = "";
        }

        return options;
    }
    
    private FilteredClassifier getClassifier(String classLabel, String[] options) throws Exception
    {
        Classifier classifier = Classifier.forName(classifierName, options);
        
        if(classifier == null)
        {
            System.out.println("Classifier not found: " + classifierName);
        }

        Instances clsInstances = new Instances(m_Instances);
        
        Attribute label = clsInstances.attribute(classLabel);

        clsInstances.setClass(label);
        
        Remove rm = new Remove();
        
        String rmAttribs = "";
        
        for (int i = 0; i < classLabels.length; i++) {
            String clsLbl = classLabels[i];
            
            if(clsLbl.equals(classLabel))
            {
                continue;
            }
            
            Attribute a = clsInstances.attribute(clsLbl);
            int aIndex = a.index();

            if(i == classLabels.length - 1)
            {
                rmAttribs += (aIndex + 1);
            }
            else
            {
                rmAttribs += (aIndex + 1) + ",";
            }
        }
        
        if(rmAttribs.endsWith(","))
        {
            rmAttribs = MiscellaneousUtils.replaceLast(rmAttribs, ",", "");
        }

        rm.setAttributeIndices(rmAttribs);
        
        FilteredClassifier fclassifier = new FilteredClassifier();
        
        fclassifier.setFilter(rm);
        fclassifier.setClassifier(classifier);
        
//        Instances clsInstances = HypothesisConfidence.getInstances(m_Instances, classLabel, classLabels);
//        
//        c_Instances.add(clsInstances);
        
        fclassifier.buildClassifier(clsInstances);
        
        return fclassifier;
    }

    public static ClassVote getInstance(String classifierName, String[] classLabels, String[] options, double[] classWeights) {
        ClassVote classVote = new ClassVote();

        classVote.classifierName = classifierName;
        classVote.classifierOptions = options;
        classVote.classLabels = classLabels;
        classVote.classWeights = classWeights;
        
        return classVote;
    }
}
