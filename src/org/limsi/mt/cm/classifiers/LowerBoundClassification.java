package org.limsi.mt.cm.classifiers;

import weka.core.Instances;
import weka.core.Instance;
import weka.classifiers.Classifier;
import java.util.Enumeration;

public class LowerBoundClassification extends Classifier
{
	Instances m_Instances;
	int m_nAttributes;
	//this method build your model
	public void buildClassifier(Instances data)
	{
		m_Instances = new Instances(data);
		m_nAttributes = data.numAttributes();
		Enumeration enu = m_Instances.enumerateInstances();

                int total = m_Instances.numInstances();
                
//                System.out.println("Total number of instances: " + total);

		while (enu.hasMoreElements())
		{
			Instance _instance = (Instance) enu.nextElement();
			int n = _instance.numAttributes();
			double x,y;
		
			for (int i = 0; i < n; i++)
			{
				x = _instance.value(i);
				y = x;
			}
		}
	}

	//this method lets you to test the instance using the classifier
	public double classifyInstance(Instance instance)
	{
            return 3.0;
//		Enumeration enu = m_Instances.enumerateInstances();
//		double distance = 9999999;
//		double classValue = -1;

//		while (enu.hasMoreElements())
//		{
//			Instance _instance = (Instance) enu.nextElement();
//			double _distance = CalculateDistance(instance,_instance);

//			if (_distance < distance)
//			{
//				distance = _distance;
//				classValue = _instance.classValue();
//			}
//		}
		
//		return classValue;

//		return returnAverageClassValue(m_Instances);
	}

	public double calculateDistance(Instance i1, Instance i2)
	{
		double s=0;

		for (int i = 0;i < m_nAttributes-1;i++)
		{
			double p = (i1.value(i)-i2.value(i));
			s += p*p;
		}

		return s;
	}

	public double returnAverageClassValue(Instances instances)
	{
		double s=0;

		Enumeration enu = instances.enumerateInstances();

		while (enu.hasMoreElements())
		{
			Instance instance = (Instance) enu.nextElement();
			int n = instance.numAttributes();
		
			double c = instance.value(n - 1);
			
			s += c;
		}
                
                int total = instances.numInstances();
                
//                System.out.println("Total number of instances: " + total);

		s = s / (double) instances.numInstances();

		return s;
	}

	//this function is used to show the list of the parameter options
	public String [] getOptions()
	{
		String [] options = new String [2];

		int current = 0;

		while (current < options.length)
		{
			options[current++] = "";
		}

		return options;
	}
}
