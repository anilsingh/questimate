package org.limsi.mt.cm.classifiers;

import weka.core.Instances;
import weka.core.Instance;
import java.util.Enumeration;

public class LowerBoundRegression extends BaselineClassifier {

    Instances m_Instances;
    int m_nAttributes;

    //this method build your model
    public void buildClassifier(Instances data) {
        m_Instances = new Instances(data);
        m_nAttributes = data.numAttributes();
        Enumeration enu = m_Instances.enumerateInstances();

        int total = m_Instances.numInstances();

//                System.out.println("Total number of instances: " + total);

        while (enu.hasMoreElements()) {
            Instance _instance = (Instance) enu.nextElement();
            int n = _instance.numAttributes();
            double x, y;

            for (int i = 0; i < n; i++) {
                x = _instance.value(i);
                y = x;
            }
        }
    }

    //this method lets you to test the instance using the classifier
    public double classifyInstance(Instance instance) {
        return returnAverageClassValue(allInstances);
    }

    public double returnAverageClassValue(Instances instances) {
        double s = 0;

        Enumeration enu = instances.enumerateInstances();

        while (enu.hasMoreElements()) {
            Instance instance = (Instance) enu.nextElement();

            double c = instance.classValue();

            s += c;
        }

        int total = instances.numInstances();

//                System.out.println("Total number of instances: " + total);

        s = s / (double) total;

        return s;
    }

    //this function is used to show the list of the parameter options
    public String[] getOptions() {
        String[] options = new String[2];

        int current = 0;

        while (current < options.length) {
            options[current++] = "";
        }

        return options;
    }
}
