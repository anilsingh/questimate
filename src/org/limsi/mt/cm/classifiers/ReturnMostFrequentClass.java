package org.limsi.mt.cm.classifiers;

import weka.core.Instances;
import weka.core.Instance;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReturnMostFrequentClass extends BaselineClassifier {

    Instances m_Instances;
    int m_nAttributes;

    String mfCls;
    int mfClsIndex = -1;
    //this method build your model

    public void buildClassifier(Instances data) {
        m_Instances = new Instances(data);
        m_nAttributes = data.numAttributes();

        Enumeration enu = m_Instances.enumerateInstances();

        int total = m_Instances.numInstances();

//                System.out.println("Total number of instances: " + total);

        while (enu.hasMoreElements()) {
            Instance _instance = (Instance) enu.nextElement();
            int n = _instance.numAttributes();
            double x, y;

            for (int i = 0; i < n; i++) {
                x = _instance.value(i);
                y = x;
            }
        }

        mfCls = getMostFrequentClass(allInstances);
        
        mfClsIndex = allInstances.classAttribute().indexOfValue(mfCls);
        
        System.err.println("Most frequent class: " + mfCls);
    }

    //this method lets you to test the instance using the classifier
    public double classifyInstance(Instance instance) {
        return mfClsIndex;
    }

    public String getMostFrequentClass(Instances instances) {

        Map<String, Integer> classFrequencies = new LinkedHashMap<String, Integer>();

        Enumeration enu = instances.enumerateInstances();

        while (enu.hasMoreElements()) {
            Instance instance = (Instance) enu.nextElement();

            String c = instance.stringValue(allInstances.classAttribute());

            if(classFrequencies.get(c) == null)
            {
                classFrequencies.put(c, 1);
            }
            else
            {
                classFrequencies.put(c, classFrequencies.get(c) + 1);
            }
        }

        int freq = -1;
        
        String clsVal = null;
        
        Iterator<String> itr = classFrequencies.keySet().iterator();
        
        while(itr.hasNext())
        {
            String clsLabel = itr.next();
            
            if(classFrequencies.get(clsLabel) > freq)
            {
                clsVal = clsLabel;
                freq = classFrequencies.get(clsLabel);
            }
        }
        
        return clsVal;
    }

    //this function is used to show the list of the parameter options
    public String[] getOptions() {
        String[] options = new String[2];

        int current = 0;

        while (current < options.length) {
            options[current++] = "";
        }

        return options;
    }
}
