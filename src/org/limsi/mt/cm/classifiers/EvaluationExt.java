/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.classifiers;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.mt.cm.HypothesisConfidence;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.RevisionHandler;
import weka.core.Summarizable;

/**
 *
 * @author anil
 */
public class EvaluationExt extends Evaluation implements Summarizable, RevisionHandler, Serializable {

    /** For serialization */
  private static final long serialVersionUID = -7010314486866816271L;
  
  public EvaluationExt(Instances data) throws Exception {
    super(data, null);
  }
  
  public double correctness(Classifier cl, Instances instances)
  {
//    double[] classCounts = new double[m_NumClasses];
//    double classCountSum = 0;
      
      double numPred = 0;
      double numCorrect = 0;
    
        for (int i = 0; i < instances.numInstances(); i++) {
            try {
                double correct = instances.instance(i).classValue();
                // Predicted
                double pred = cl.classifyInstance(instances.instance(i));
                
                if(correct == pred)
                {
                    numCorrect++;
                }
                
                numPred++;
                
            } catch (Exception ex) {
                Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return numCorrect / numPred;
//    for (int i = 0; i < m_NumClasses; i++) {
//      for (int j = 0; j < m_NumClasses; j++) {
//        classCounts[i] += m_ConfusionMatrix[i][j];
//      }
//      classCountSum += classCounts[i];
//    }
//
//    double truePosTotal = 0;
//    for (int i = 0; i < m_NumClasses; i++) {
////      double temp = truePositiveRate(i);
//      double temp = numTruePositives(i);
//      truePosTotal += temp;
//    }
//
//    return truePosTotal / classCountSum;      
  }
}
