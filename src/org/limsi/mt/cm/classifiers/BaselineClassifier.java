/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm.classifiers;

import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public abstract class BaselineClassifier extends Classifier {

    protected Instances allInstances;

    /**
     * @return the allInstances
     */
    public Instances getAllInstances() {
        return allInstances;
    }

    /**
     * @param allInstances the allInstances to set
     */
    public void setAllInstances(Instances allInstances) {
        this.allInstances = allInstances;
    }
    
}
