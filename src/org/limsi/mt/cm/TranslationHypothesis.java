                                                                                                                                                                                                                                                                                                                                            /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import edu.stanford.nlp.mt.base.FeatureValueCollection;
import edu.stanford.nlp.mt.base.Sequence;
import java.io.PrintStream;
import sanchay.corpus.ssf.features.FeatureStructure;

/**
 *
 * @author anil
 */
public class TranslationHypothesis<TK extends Object, FV extends Object> {
    protected Sequence<TK> translation;
    protected FeatureValueCollection<FV> features;
    
    protected double score;
    protected double latticeSourceId;
    
    protected PhraseAlignment phraseAlignment;
    protected WordAlignment wordAlignment;

    public TranslationHypothesis() {
        
    }

    public TranslationHypothesis(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score, double latticeId) {
        this.translation = sequenceStored;
        this.features = featureValues;
        this.score = score;
        this.latticeSourceId = latticeId;
        this.phraseAlignment = null;
        this.wordAlignment = null;
    }

    public TranslationHypothesis(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score, PhraseAlignment palignment, WordAlignment walignment) {
        this.translation = sequenceStored;
        this.features = featureValues;
        this.score = score;
        this.phraseAlignment = palignment;
        this.wordAlignment = walignment;
        this.latticeSourceId = -1;
    }

    public TranslationHypothesis(Sequence<TK> sequenceStored, FeatureValueCollection<FV> featureValues, double score) {
        this.translation = sequenceStored;
        this.features = featureValues;
        this.score = score;
        this.phraseAlignment = null;
        this.wordAlignment = null;
        this.latticeSourceId = -1;
    }

    /**
     * @return the translation
     */
    public Sequence<TK> getTranslation() {
        return translation;
    }

    /**
     * @param translation the translation to set
     */
    public void setTranslation(Sequence<TK> translation) {
        this.translation = translation;
    }

    /**
     * @return the features
     */
    public FeatureValueCollection<FV> getFeatures() {
        return features;
    }

    /**
     * @param features the features to set
     */
    public void setFeatures(FeatureValueCollection<FV> features) {
        this.features = features;
    }

    /**
     * @return the score
     */
    public double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * @return the latticeSourceId
     */
    public double getLatticeSourceId() {
        return latticeSourceId;
    }

    /**
     * @param latticeSourceId the latticeSourceId to set
     */
    public void setLatticeSourceId(double latticeSourceId) {
        this.latticeSourceId = latticeSourceId;
    }

    /**
     * @return the phraseAlignment
     */
    public PhraseAlignment getPhraseAlignment() {
        return phraseAlignment;
    }

    /**
     * @param alignment the phraseAlignment to set
     */
    public void setPhraseAlignment(PhraseAlignment alignment) {
        this.phraseAlignment = alignment;
    }

    /**
     * @return the wordAlignment
     */
    public WordAlignment getWordAlignment() {
        return wordAlignment;
    }

    /**
     * @param wordAlignment the wordAlignment to set
     */
    public void setWordAlignment(WordAlignment wordAlignment) {
        this.wordAlignment = wordAlignment;
    }
    
    private void read(String str, int format)
    {
        
    }
    
    public void print(PrintStream ps)
    {
        
    }
}
