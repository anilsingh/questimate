/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import edu.stanford.nlp.mt.base.*;

/**
 *
 * @author anil<br/>
 * Based on the Phrasal class ScoredFeaturizedTranslation by Daniel Cer
*/
public class ScoredFeaturizedAlignedTranslation<TK, FV> extends
    FeaturizedTranslation<TK, FV> implements
    Comparable<ScoredFeaturizedTranslation<TK, FV>> {
  public final long latticeSourceId;
  public double score;

  /**
	 * 
	 */
  public ScoredFeaturizedAlignedTranslation(Sequence<TK> translation,
      FeatureValueCollection<FV> features, double score) {
    super(translation, features);
    this.score = score;
    this.latticeSourceId = -1;
  }

  /**
	 * 
	 */
  public ScoredFeaturizedAlignedTranslation(Sequence<TK> translation,
      FeatureValueCollection<FV> features, double score, long latticeSourceId) {
    super(translation, features);
    this.score = score;
    this.latticeSourceId = latticeSourceId;
  }

  /**
   * Have sort place things in descending order
   */
  @Override
  public int compareTo(ScoredFeaturizedTranslation<TK, FV> o) {
    return (int) Math.signum(o.score - score);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(translation).append(" |||");
    if (features == null) {
    	sb.append(" NoFeatures: 0");
    } else {
	    for (FeatureValue<FV> fv : features) {
	      if (fv == null) {
	    	  sb.append(" ").append("NullFeature: 0");
	      } else {
	    	  sb.append(" ").append(fv.name).append(": ").append(fv.value);
	      }
	    }
    }
    sb.append(" ||| ").append(score);
    if (latticeSourceId >= 0)
      sb.append(" ||| ").append(latticeSourceId);

    return sb.toString();
  }
}
