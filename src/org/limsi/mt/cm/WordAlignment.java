/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author anil
 */
public class WordAlignment {
    protected int s2t[][];
    
    public WordAlignment(String s, int format) {
        read(s, format);        
    }
    
    public WordAlignment(int a[][]) {
        s2t = a;        
    }
    
    private void read(String s, int format)
    {
        String stringRep = s.intern();
        if (stringRep.equals("")) {
            s2t = null;
            return;
        }
        
//        if (format == Formats.PHRASAL_NBEST_FORMAT) {
//                        
//            if (stringRep.equals("")) {
//                s2t = null;
//            } else {
//                String[] els = stringRep.split(" ");
//                
//                Map<Integer, Integer> amap = new LinkedHashMap<Integer, Integer>(els.length);
//
//                for (int i = 0; i < els.length; ++i) {
//                    if (!els[i].equals("")) {
////                        System.err.println("**" + els[i] + "**");
//                        String[] els2 = els[i].split("=");
//                        
//                        if(els2.length != 2)
//                        {
//                            System.err.println("Error: Two fields required. Found " + els2.length + " fields.");
//                        }
//
//                        int tIndex = Integer.parseInt(els2[1]);
//
//                        String[] els3 = els2[0].split("-");
//                        
//                        if(els3.length == 1)
//                        {
//                            amap.put(Integer.parseInt(els2[0]), tIndex);
//                        }
//                        else
//                        {
//                            int start = Integer.parseInt(els3[0]);
//                            int end = Integer.parseInt(els3[1]);
//
//                            for (int j = start; j <= end; j++)
//                            {
//                                amap.put(j, tIndex);
//                            }
//                        }
//                    }
//                }
//                                                
//                s2t = new int[amap.size()][2];
//                
//                Iterator<Integer> itr = amap.keySet().iterator();
//                
//                int i = 0;
//                while(itr.hasNext())
//                {
//                    Integer sIndex = itr.next();
//                    Integer tIndex = amap.get(sIndex);
//                    
//                    s2t[i][0] = sIndex;
//                    s2t[i++][1] = tIndex;
//                }
//            }
//        } else
        if (format == Formats.MOSES_NBEST_FORMAT || format == Formats.PHRASAL_NBEST_FORMAT) {
            if (stringRep.equals("")) {
                s2t = null;
            } else {
                String[] els = stringRep.split(" ");
                s2t = new int[els.length][2];
                for (int i = 0; i < s2t.length; ++i) {
                    if (!els[i].equals("")) {
//                        System.err.println("**" + els[i] + "**");
                        String[] els2 = els[i].split("-");
                        
                        if(els2.length != 2)
                        {
                            System.err.println("Error: Two fields required. Found " + els2.length + " fields.");
                        }

                        s2t[i][0] = Integer.parseInt(els2[0]);
                        s2t[i][1] = Integer.parseInt(els2[1]);
                    }
                }
            }
        }
        /* GIZA++ format */
//        else {
//            if (stringRep.equals("I-I")) {
//                s2t = null;
//            } else {
//                String[] els = stringRep.split(";");
//                s2t = new int[els.length][];
//                for (int i = 0; i < s2t.length; ++i) {
//                    // System.err.printf("(%d): %s\n",i,els[i]);
//                    if (!els[i].equals("()")) {
//                        String[] els2 = els[i].split(",");
//                        s2t[i] = new int[els2.length];
//                        for (int j = 0; j < s2t[i].length; ++j) {
//                            // System.err.printf("(%d): %s\n",j,els2[j]);
//                            String num = els2[j].replaceAll("[()]", "");
//                            s2t[i][j] = Integer.parseInt(num);
//                        }
//                    }
//                }
//            }
//        }
    }
 
    public int[] s2t(int si) {
        List<Integer> s2tAlignments = new LinkedList<Integer>();
        
        for (int i = 0; i < s2t.length; i++) {
            if(s2t[i][0] == si)
                s2tAlignments.add(s2t[i][1]);
        }
        
        if(s2tAlignments.size() == 0)
            return null;
        
        int s2tInt[] = new int[s2tAlignments.size()];

        for (int i = 0; i < s2tInt.length; i++) {
            s2tInt[i] = s2tAlignments.get(i);
        }
        
        return s2tInt;
    }
 
    public int s2tFirst(int si) {
        for (int i = 0; i < s2t.length; i++) {
            if(s2t[i][0] == si)
                return s2t[i][1];
        }
        
        return -1;
    }

    private static String toStr(int[][] a2b) {
        StringBuilder sb = new StringBuilder();
        for (int ei = 0; ei < a2b.length; ++ei) {
            if (ei > 0) {
                sb.append(" ");
            }
            sb.append("(");
            if (a2b[ei] != null) {
                int i = 0;
                for (int fi : a2b[ei]) {
                    if (i++ > 0) {
                        sb.append(",");
                    }
                    sb.append(fi);
                }
            }
            sb.append(")");
        }
        return sb.toString();
    }

    public String s2tStr() {
        return toStr(s2t);
    }

    public String t2sStr() {
        return toStr(getReverseAlignment(s2t));        
    }
    
    public String getHyphenatedString()
    {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < s2t.length; i++) {
            sb.append(s2t[i][0]);
            sb.append('-');
            sb.append(s2t[i][1]);
            sb.append(' ');
        }        
        
        return sb.toString().trim();
    }
   
    public static WordAlignment getReverseAlignment(WordAlignment a2b)
    {
        return new WordAlignment(getReverseAlignment(a2b.s2t));
    }        
   
    public static int[][] getReverseAlignment(int a[][])
    {
        int r[][] = new int[a.length][2];
        
        for (int i = 0; i < a.length; i++) {
            r[i][0] = a[i][1];
            r[i][1] = a[i][0];            
        }
        
        return r;
    } 
    
    public static void main(String args[])
    {
        WordAlignment wa = new WordAlignment("0-0 1-1 2-2 2-3 2-4 3-5 4-6 5-8 6-7 7-9 8-10 9-11 12-12 10-13 11-16", Formats.MOSES_NBEST_FORMAT);
        
        System.out.println(wa.getHyphenatedString());
        System.out.println(WordAlignment.getReverseAlignment(wa).getHyphenatedString());
    }
}
