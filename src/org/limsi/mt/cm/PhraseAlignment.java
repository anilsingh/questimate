/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

/**
 *
 * @author anil
 */
public class PhraseAlignment {

    protected int phraseAlignment[][];
    
    public PhraseAlignment(String s, int format) {
        read(s, format);        
    }
    
    public PhraseAlignment(int a[][]) {
        phraseAlignment = a;
    }
    
    private void read(String s, int format)
    {
        String stringRep = s.intern();

        if (format == Formats.PHRASAL_NBEST_FORMAT || format == Formats.MOSES_NBEST_FORMAT) {
                        
            if (stringRep.equals("")) {
                phraseAlignment = null;
            } else {
                String[] els = stringRep.split(" ");
                
                phraseAlignment = new int[els.length][4];

                for (int i = 0; i < els.length; ++i) {
                    if (!els[i].equals("")) {
//                        System.err.println("**" + els[i] + "**");
                        String[] els2 = els[i].split("=");
                        
                        if(els2.length != 2)
                        {
                            System.err.println("Error: Two fields required. Found " + els2.length + " fields.");
                        }

                        String[] els3 = els2[0].split("-");
                        
                        if(els3.length == 1)
                        {
                            phraseAlignment[i][0] = Integer.parseInt(els2[0]);
                            phraseAlignment[i][1] = Integer.parseInt(els2[0]);
                        }
                        else
                        {
                            phraseAlignment[i][0] = Integer.parseInt(els3[0]);
                            phraseAlignment[i][1] = Integer.parseInt(els3[1]);
                        }

                        String[] els4 = els2[1].split("-");
                        
                        if(els4.length == 1)
                        {
                            phraseAlignment[i][2] = Integer.parseInt(els4[0]);
                            phraseAlignment[i][3] = Integer.parseInt(els4[0]);
                        }
                        else
                        {
                            phraseAlignment[i][2] = Integer.parseInt(els4[0]);
                            phraseAlignment[i][3] = Integer.parseInt(els4[1]);
                        }
                    }
                }
            }
        }
//        else if (format == Formats.MOSES_NBEST_FORMAT) {
//            if (stringRep.equals("")) {
//                s2t = null;
//            } else {
//                String[] els = stringRep.split(" ");
//                s2t = new int[els.length][2];
//                for (int i = 0; i < s2t.length; ++i) {
//                    if (!els[i].equals("")) {
////                        System.err.println("**" + els[i] + "**");
//                        String[] els2 = els[i].split("-");
//                        
//                        if(els2.length != 2)
//                        {
//                            System.err.println("Error: Two fields required. Found " + els2.length + " fields.");
//                        }
//
//                        s2t[i][0] = Integer.parseInt(els2[0]);
//                        s2t[i][1] = Integer.parseInt(els2[1]);
//                    }
//                }
//            }
//        }
//        /* GIZA++ format */
//        else {
//            if (stringRep.equals("I-I")) {
//                s2t = null;
//            } else {
//                String[] els = stringRep.split(";");
//                s2t = new int[els.length][];
//                for (int i = 0; i < s2t.length; ++i) {
//                    // System.err.printf("(%d): %s\n",i,els[i]);
//                    if (!els[i].equals("()")) {
//                        String[] els2 = els[i].split(",");
//                        s2t[i] = new int[els2.length];
//                        for (int j = 0; j < s2t[i].length; ++j) {
//                            // System.err.printf("(%d): %s\n",j,els2[j]);
//                            String num = els2[j].replaceAll("[()]", "");
//                            s2t[i][j] = Integer.parseInt(num);
//                        }
//                    }
//                }
//            }
//        }
    }
 
    public int[] getAlignment(int i) {
        return (phraseAlignment != null) ? phraseAlignment[i] : null;
    }

    private static String toStr(int[][] pAlignment) {
        StringBuilder sb = new StringBuilder();
        for (int ei = 0; ei < pAlignment.length; ++ei) {
            if (ei > 0) {
                sb.append(" ");
            }

            if (pAlignment[ei] != null) {
                if(pAlignment[ei][0] != pAlignment[ei][1])
                {
                    sb.append(pAlignment[ei][0]);
                    sb.append('-');
                    sb.append(pAlignment[ei][1]);
                }
                else
                    sb.append(pAlignment[ei][0]);

                sb.append('=');

                if(pAlignment[ei][2] != pAlignment[ei][3])
                {
                    sb.append(pAlignment[ei][2]);
                    sb.append('-');
                    sb.append(pAlignment[ei][3]);
                }
                else
                    sb.append(pAlignment[ei][2]);
            }
        }
        return sb.toString();
    }

    public String s2tStr() {
        return toStr(phraseAlignment);
    }

    public String t2sStr() {
        return toStr(getReverseAlignment(phraseAlignment));        
    }
   
    public static int[][] getReverseAlignment(int a[][])
    {
        int r[][] = new int[a.length][];
        
        for (int i = 0; i < a.length; i++) {
            r[i][0] = a[i][1];
            r[i][1] = a[i][0];            
        }
        
        return r;
    }    
}
