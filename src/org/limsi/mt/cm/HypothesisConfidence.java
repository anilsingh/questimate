/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.mt.cm;

import com.google.common.base.Joiner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.OptionSet;
import ml.options.Options;
import org.limsi.cm.avro.AvroIO;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.gui.AttributeViewerJPanel;
import org.limsi.mt.cm.classifiers.BaselineClassifier;
import org.limsi.mt.cm.classifiers.ClassVote;
import org.limsi.mt.cm.classifiers.EvaluationExt;
import org.limsi.mt.cm.classifiers.LowerBoundRegression;
import org.limsi.mt.cm.classifiers.ReturnMostFrequentClass;
import org.limsi.mt.cm.classifiers.ReturnRandomClass;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import org.limsi.types.AvroType;
import org.limsi.types.FormatType;
import sanchay.properties.PropertyTokens;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.*;
import weka.core.converters.AbstractFileSaver;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.converters.SVMLightLoader;
import weka.core.converters.SVMLightSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author anil
 */
public class HypothesisConfidence implements Runnable {

    protected SSFStory sourceText;
    protected SSFStory targetText;
    protected ArrayList<Double> predictions;
    protected Classifier classifier;
    protected ArrayList wekaSetup;
    
    protected String classLabel;
    
    protected PropertyTokens attributeFilter;
    protected boolean excludeFilter;
   
    public static String[] metricsNamesNumeric = new String[]{"correl", "mae",
        "rmse", "rae", "rrse"};
    public static String[] metricsNamesNominal = new String[]{"kappa", "mae",
        "rmse", "tprate", "fprate", "precision",
        "recall", "fmeasure", "auc"};

    private double[] getMetricsNumeric(Evaluation eval) {
        double metrics[] = null;

        try {
            metrics = new double[]{eval.correlationCoefficient(),
                eval.meanAbsoluteError(),
                eval.rootMeanSquaredError(),
                eval.relativeAbsoluteError(),
                eval.rootRelativeSquaredError()};
        } catch (Exception ex) {
            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        }

        return metrics;
    }

    private double[] getMetricsNominal(Evaluation eval) {
        double metrics[] = new double[]{eval.kappa(),
            eval.meanAbsoluteError(),
            eval.rootMeanSquaredError(),
            eval.weightedTruePositiveRate(),
            eval.weightedFalsePositiveRate(),
            eval.weightedPrecision(),
            eval.weightedRecall(),
            eval.weightedFMeasure(),
            eval.weightedAreaUnderROC()};

        return metrics;
    }

    public HypothesisConfidence(String attributeFilterPath, boolean excludeFilter, String classLabel) {
        sourceText = new SSFStoryImpl();
        targetText = new SSFStoryImpl();

        wekaSetup = new ArrayList();
        
        try {
            
            if(attributeFilterPath != null && MiscellaneousUtils.fileExists(attributeFilterPath))
            {
                attributeFilter = new PropertyTokens(attributeFilterPath, "UTF-8");

                this.excludeFilter = excludeFilter;
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.classLabel = classLabel;
    }

    public HypothesisConfidence(Classifier cl, String attributeFilterPath, boolean excludeFilter, String classLabel) {
        this(attributeFilterPath, excludeFilter, classLabel);

        if(cl == null)
        {
            System.err.println("Custom classifier: " + classifier);

            if(classifier.equals("org.limsi.mt.cm.classifiers.ReturnMostFrequentClass"))
            {
                cl = new ReturnMostFrequentClass();
            }
            else if(classifier.equals("org.limsi.mt.cm.classifiers.ReturnRandomClass"))
            {
                cl = new ReturnRandomClass();
            }
            else if(classifier.equals("org.limsi.mt.cm.classifiers.LowerBoundRegression"))
            {
                cl = new LowerBoundRegression();
            }
        }

        classifier = cl;
    }

    /**
     * @return the sourceText
     */
    public SSFStory getSourceText() {
        return sourceText;
    }

    /**
     * @param sourceText the sourceText to set
     */
    public void setSourceText(SSFStory sourceText) {
        this.sourceText = sourceText;
    }

    /**
     * @return the targetText
     */
    public SSFStory getTargetText() {
        return targetText;
    }

    /**
     * @param targetText the targetText to set
     */
    public void setTargetText(SSFStory targetText) {
        this.targetText = targetText;
    }

    /**
     * @return the predictions
     */
    public ArrayList<Double> getConfidenceEstimates() {
        return predictions;
    }

    /**
     * @param predictions the predictions to set
     */
    public void setConfidenceEstimates(ArrayList<Double> confidenceEstimates) {
        this.predictions = confidenceEstimates;
    }

    public void readText(String srcPath, String srcCS,
            String tgtPath, String tgtCS) throws FileNotFoundException, IOException, Exception {
        getSourceText().readFile(srcCS, srcCS);
        getTargetText().readFile(srcCS, srcCS);
    }

    public static Instances getInstances(Instances data, String classLabel, String[] classLabels) throws Exception {
        Instances newData = new Instances(data);

        if (classLabel == null) {
            if (newData.classIndex() == -1) {
                newData.setClassIndex(newData.numAttributes() - 1);
            }
        } else {
            Attribute clsAttrib = newData.attribute(classLabel);
            newData.setClass(clsAttrib);

            newData = removeAttributes(newData, classLabel, classLabels);
            
            System.out.println("Class attribute set to: " + classLabel);
        }

        return newData;
    }
    
    public static Instances removeAttributes(Instances data, String exceptThis, String[] attribNames)
    {
        if(attribNames == null)
        {
            return data;
        }
        
        Instances newData = new Instances(data);
        Remove rm = new Remove();

        String rmAttribs = "";

        for (int i = 0; i < attribNames.length; i++) {
            String clsLbl = attribNames[i];

            if(exceptThis != null && clsLbl.equals(exceptThis))
            {
                continue;
            }

            Attribute a = newData.attribute(clsLbl);
            int aIndex = a.index();

            if(i == attribNames.length - 1)
            {
                rmAttribs += (aIndex + 1);
            }
            else
            {
                rmAttribs += (aIndex + 1) + ",";
            }
        }

        rm.setAttributeIndices(rmAttribs);
        
        try {
            rm.setInputFormat(newData);
            
            newData = Filter.useFilter(newData, rm);        
        } catch (Exception ex) {
            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newData;
    }

//    public static Instances removeAttribute(Instances data, String name) throws Exception {
//        Attribute a = data.attribute(name);
//
//        int exAttribIndex = a.index();
//
//        String[] remOptions = new String[2];
//        remOptions[0] = "-R";
//        remOptions[1] = "" + (exAttribIndex + 1);
//
//        Remove remove = new Remove();
//
//        remove.setOptions(remOptions);
//
//        remove.setInputFormat(data);
//
//        Instances newData = Filter.useFilter(data, remove);
//
////        newData = newData.stringFreeStructure();
//        
//        return newData;
//    }
    
    public static Classifier getFilteredClassifier(Classifier classifier, Instances instances,
            PropertyTokens filter, boolean excludeFilter)
    {
        if(filter == null || filter.countTokens() == 0)
        {
            return classifier;
        }

        Remove rm = new Remove();
        
        String rmAttribs = "";
        
        int count = filter.countTokens();
        
        for (int i = 0; i < count; i++) {
            String attributeName = filter.getToken(i);
            
            Attribute a = instances.attribute(attributeName);

            int aIndex = a.index();

            if(i == count - 1)
            {
                rmAttribs += (aIndex + 1);
            }
            else
            {
                rmAttribs += (aIndex + 1) + ",";
            }
        }
        
        if(rmAttribs.endsWith(","))
        {
            rmAttribs = MiscellaneousUtils.replaceLast(rmAttribs, ",", "");
        }
        
        if(!excludeFilter)
        {
            int clsIndex = instances.classIndex();
            
            rmAttribs += "," + clsIndex;

            rm.setAttributeIndices(rmAttribs);
            
            rm.setInvertSelection(true);
        }
        else
        {
            rm.setAttributeIndices(rmAttribs);
        }
        
        FilteredClassifier fclassifier = new FilteredClassifier();
        
        fclassifier.setFilter(rm);
        fclassifier.setClassifier(classifier);

        return fclassifier;
    }

    public static Instances getInstances(String featureFilePath, String classLabel) {
        Instances newData = null;
        
        if(featureFilePath.endsWith("." + FormatType.FeatureCollectionFormat.AVRO_FORMAT.getFormatExtension()))
        {
            System.out.println("Loading Avro file: " + featureFilePath);
            
            try {
                newData = AvroIO.readInstances(featureFilePath, AvroType.POST_EDITED_CORPUS_ENTRY);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(AttributeViewerJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(featureFilePath.endsWith("." + FormatType.FeatureCollectionFormat.CSV_FORMAT.getFormatExtension()))
        {
            System.out.println("Loading CSV file: " + featureFilePath);
            
            CSVLoader loader = new CSVLoader();
            
            try {
                
                loader.setSource(new File(featureFilePath));

                newData = loader.getDataSet();
                
            } catch (IOException ex) {
                Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(featureFilePath.endsWith("." + FormatType.FeatureCollectionFormat.CSV_FORMAT.getFormatExtension()))
        {
            System.out.println("Loading SVMLight file: " + featureFilePath);
            
            SVMLightLoader loader = new SVMLightLoader();
            
            try {
                
                loader.setSource(new File(featureFilePath));

                newData = loader.getDataSet();
                
            } catch (IOException ex) {
                Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(featureFilePath.endsWith("." + FormatType.FeatureCollectionFormat.ARFF_FORMAT.getFormatExtension()))
        {
            DataSource source = null;
            BufferedReader reader = null;

            try {
                System.out.println("Loading arff file: " + featureFilePath);
            
                reader = new BufferedReader(new FileReader(featureFilePath));
                newData = new Instances(reader);
                reader.close();

            } catch (Exception ex) {
                Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(newData == null)
        {
            System.out.println("Instances could not be loaded from the file: " + featureFilePath);            
        }
        else
        {
            System.out.printf("Loaded %d instances.\n", newData.numInstances());                        
        }

        if (classLabel == null) {
            if (newData.classIndex() == -1) {
                newData.setClassIndex(newData.numAttributes() - 1);
            }
        } else {
            Attribute clsAttrib = newData.attribute(classLabel);
            newData.setClass(clsAttrib);
        }
        
        return newData;
    }

    public static void saveInstances(Instances instances, String featureFilePath,
            FormatType.FeatureCollectionFormat format) throws IOException {
        
        AbstractFileSaver saver = null;

        if(format.equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
        {
            saver = new ArffSaver();
        }
        else if(format.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))
        {
            saver = new SVMLightSaver();
        }
        else if(format.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
        {
            saver = new CSVSaver();
        }
        
        if(saver != null)
        {
            saver.setInstances(instances);
            saver.setFile(new File(featureFilePath));
            saver.writeBatch();
        }
        else
        {
            System.out.println("Could not save the file: " + featureFilePath);
        }
    }

    public static int copyAttributes(int attribIndices[], Instances fromInstances, Instances toInstances) {
        if (toInstances.numInstances() != fromInstances.numInstances()) {
            return -1;
        }

        for (int i = 0; i < attribIndices.length; i++) {
            int j = attribIndices[i];

            copyAttribute(j, fromInstances, toInstances);
        }

        return 0;
    }

    public static void copyAttribute(int attribIndex, Instances fromInstances, Instances toInstances) {
        Attribute attrib = fromInstances.attribute(attribIndex);

        int numAttribs = toInstances.numAttributes();

        int toIndex = numAttribs - 1;
        String name = attrib.name();

        if (toInstances.attribute(name) != null) {
            name = name + "-1";
        }

        toInstances.insertAttributeAt(attrib.copy(name), toIndex);

        int numInstances = fromInstances.numInstances();

        for (int i = 0; i < numInstances; i++) {
            Instance fromInstance = fromInstances.instance(i);
            Instance toInstance = toInstances.instance(i);

            if (attrib.isNumeric()) {
                toInstance.setValue(toIndex, fromInstance.value(attribIndex));
            } else if (attrib.isNominal()) {
                toInstance.setValue(toIndex, fromInstance.stringValue(attribIndex));
            }
        }
    }

    public static void makeClass(int attribIndex, Instances instances) {
    }

    public void trainEstimator(Instances data, String modelFilePath) throws Exception {
        // further options...
        if(attributeFilter == null)
        {
            classifier.buildClassifier(data);
        }
        else
        {
            classifier = getFilteredClassifier(classifier, data, attributeFilter, excludeFilter);
            classifier.buildClassifier(data);
        }
        
//        if(classifier instanceof ClassVote)
//        {
//            ((ClassVote) classifier).printSummary(System.out);        
//        }
        
        System.out.println("After building...");
        System.out.println(classifier.getRevision());

        // save model + header
        wekaSetup.clear();

        wekaSetup.add(classifier);
        wekaSetup.add(new Instances(data, 0));

        System.out.println("Training finished.");

        if (modelFilePath != null && modelFilePath.equals("") == false) {
            SerializationHelper.write(modelFilePath, wekaSetup);
        }
    }

    public void trainEstimator(String featureFilePath, String cs,
            String modelFilePath) throws Exception {
        Instances data = getInstances(featureFilePath, classLabel);

        trainEstimator(data, modelFilePath);
    }

    public static double predictInstance(int i, Instance inst, Classifier cl,
            Instances data, Instances header) throws Exception {
        Instance curr = data.instance(i);
        inst.setDataset(header);

        for (int n = 0; n < header.numAttributes(); n++) {
            Attribute att = data.attribute(header.attribute(n).name());

            // original attribute is also present in the current dataset
            if (att != null) {
                if (att.isNominal()) {
                    // is this label also in the original data?
                    // Note:
                    // "numValues() > 0" is only used to avoid problems with nominal 
                    // attributes that have 0 labels, which can easily happen with
                    // data loaded from a database
                    if ((header.attribute(n).numValues() > 0) && (att.numValues() > 0)) {
                        String label = curr.stringValue(att);
                        int index = header.attribute(n).indexOfValue(label);

                        if (index != -1) {
                            inst.setValue(n, index);
                        }
                    }
                } else if (att.isNumeric()) {
                    inst.setValue(n, curr.value(att));
                } else {
                    throw new IllegalStateException("Unhandled attribute type!");
                }
            }
        }

        // predict class
        double pred = cl.classifyInstance(inst);

        return pred;
    }

    public void predict(Instances data, String modelFilePath) throws Exception {
        predictions = new ArrayList<Double>(sourceText.countSentences());

        // read model and header
        System.out.println("Reading model from: " + modelFilePath);
        wekaSetup = (ArrayList) SerializationHelper.read(modelFilePath);
        Classifier cl = (Classifier) wekaSetup.get(0);
        Instances header = (Instances) wekaSetup.get(1);

        // output predictions
//        System.out.println("actual -> predicted");

        for (int i = 0; i < data.numInstances(); i++) {
            // create an instance for the classifier that fits the training data
            // Instances object returned here might differ slightly from the one
            // used during training the classifier, e.g., different order of
            // nominal values, different number of attributes.
            Instance inst = new Instance(header.numAttributes());

            double pred = predictInstance(i, inst, cl, data, header);

            predictions.add(pred);
        }

        System.out.println("Predicting finished.");
    }

    public void predict(String featureFilePath, String cs,
            String modelFilePath, String outFilePath) throws Exception {
        Instances data = getInstances(featureFilePath, classLabel);

        predict(data, modelFilePath);

        PrintStream ps = null;

        if (outFilePath != null && outFilePath.equals("") == false) {
            ps = new PrintStream(outFilePath, cs);
        }

        for (Double score : predictions) {
            if (ps != null) {
                ps.println(score);
            } else {
                System.out.println(score);
            }
        }

        if (ps != null) {
            System.out.println("Predictions saved to: " + outFilePath);

            ps.close();
        }

        System.out.println("Predicting finished!");
    }

    public double getPrediction(int hypIndex) {
        return predictions.get(hypIndex);
    }

    public void trainTestSplit(String featureFilePath, String cs,
            String modelFilePath, String outFilePath,
            double percentageSplit) throws Exception {
        Instances data = getInstances(featureFilePath, classLabel);
        
        if(classifier instanceof BaselineClassifier)
        {
            ((BaselineClassifier) classifier).setAllInstances(data);
        }

        PrintStream ps = null;
        PrintStream ps1 = null;
        PrintStream ps2 = null;
        PrintStream ps3 = null;

        if (outFilePath != null && outFilePath.equals("") == false) {
            ps = new PrintStream(outFilePath, cs);
            ps1 = new PrintStream(outFilePath + "-metrics-train.csv", cs);
            ps2 = new PrintStream(outFilePath + "-metrics-test.csv", cs);
            ps3 = new PrintStream(outFilePath + "-output", cs);
        }

        if (ps != null) {
            printMetricHeader(data.classAttribute(), ps1);
            printMetricHeader(data.classAttribute(), ps2);
        }

        Instances train = null;
        Instances test = null;

        int trainSize = (int) ((percentageSplit / 100.0) * data.numInstances());
        int testSize = data.numInstances() - trainSize;

        System.out.format("Train size %d, test size %d, total size %d\n",
                trainSize, testSize, data.numInstances());

        train = new Instances(data, 0, trainSize);
        test = new Instances(data, trainSize, testSize);

        trainEstimator(train, modelFilePath);

        Classifier cl = (Classifier) wekaSetup.get(0);

        Evaluation eval = evaluate(train, train, cl);
        System.out.println(eval.toSummaryString("\nEvaluating on training data:\n======\n", false));

        if (ps != null) {
        
            ps.println(cl);
            
            printMetric(data.classAttribute(), eval, ps1);

            ps.println(eval.toSummaryString("\nEvaluating on training data:\n======\n", false));

            if(train.classAttribute().isNominal())
            {
                ps.println(eval.toClassDetailsString());

                ps.println(eval.toMatrixString());
            }
        }

        eval = evaluate(train, test, cl);
        System.out.println(eval.toSummaryString("\nEvaluating on test data:\n======\n", false));

        if (ps != null) {
        
            ps.println(cl);

            printMetric(data.classAttribute(), eval, ps2);

            printPredictions(cl, test, ps3);

            ps.println(eval.toSummaryString("\nEvaluating on training data:\n======\n", false));
 
            if(train.classAttribute().isNominal())
            {
                ps.println(eval.toClassDetailsString());

                ps.println(eval.toMatrixString());
            }
         }

        if (ps != null) {
            System.out.println("Output predictions/classes saved to: " + outFilePath + "-output");
            System.out.println("Metric values on training data saved to: " + outFilePath + "-metrics-train.csv");
            System.out.println("Metric values on test data saved to: " + outFilePath + "-metrics-test.csv");
            System.out.println("Summary output saved to: " + outFilePath);

            ps.close();
            ps1.close();
            ps2.close();
            ps3.close();
        }
    }

    private void printPredictions(Evaluation eval, PrintStream ps) {
        FastVector preds = eval.predictions();

//        System.err.println("Number of predictions: " + preds.size());

        Enumeration enm = preds.elements();

        while (enm.hasMoreElements()) {
            Object pred = enm.nextElement();

            ps.println(pred);
        }
    }

    private void printPredictions(Classifier cl, Instances data, PrintStream ps) {
        ps.println("Actual" + "," + "Predicted");
        
        if(classifier instanceof ClassVote)
        {
            Instances newData = new Instances(data);
            Instances newDataAvg = ((ClassVote) classifier).averageOfClasses(data);
            
            double err = 0;
            ((ClassVote) classifier).resetAbsoluteError();
            
            Attribute a = newData.classAttribute();

            for (int i = 0; i < data.numInstances(); i++) {
                try {
//                    double actual = ((ClassVote) classifier).getClassValue(newData.instance(i));
                    double actual = newDataAvg.instance(i).classValue();
                    
//                    if(a.isNumeric())
//                    {
//                        actual = newDataAvg.instance(i).classValue();
//                    }
//                    else if(a.isNominal())
//                    {
//                        actual = Double.parseDouble(newinstance.stringValue(a));
//                    }
                    
                    double pred = cl.classifyInstance(newData.instance(i));
                   
                    // Actual
                    ps.print(actual + ",");
                    // Predicted
                    ps.println(pred);
                    
                    err += Math.abs(actual - pred);
                } catch (Exception ex) {
                    Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            System.out.println("MAE based on averages: " + err/data.numInstances());
            System.out.println("MAE based on individual scores: " 
                    + ((ClassVote) classifier).getAbsoluteError()/ data.numInstances());
        }
        else
        {
            for (int i = 0; i < data.numInstances(); i++) {
                try {
                    // Actual
                    ps.print(data.instance(i).classValue() + ",");
                    // Predicted
                    ps.println(cl.classifyInstance(data.instance(i)));
                } catch (Exception ex) {
                    Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void printMetricHeader(Attribute classAttrib, PrintStream ps) {
        if (classAttrib.isNominal() && !(classifier instanceof ClassVote)) {
            printMetricHeader(ps, metricsNamesNominal);
        } else if (classifier instanceof ClassVote) {
            printMetricHeader(ps, metricsNamesNumeric);
        } else if (classAttrib.isNominal()) {
            printMetricHeader(ps, metricsNamesNominal);
        } else if (classAttrib.isNumeric()) {
            printMetricHeader(ps, metricsNamesNumeric);
        }
    }

    private void printMetric(Attribute classAttrib, Evaluation eval, PrintStream ps) {
        if (classAttrib.isNominal() && !(classifier instanceof ClassVote)) {
            printMetrics(ps, getMetricsNominal(eval));
        } else if (classifier instanceof ClassVote) {
            printMetrics(ps, getMetricsNumeric(eval));
        } else if (classAttrib.isNumeric()) {
            printMetrics(ps, getMetricsNumeric(eval));
        } else if (classAttrib.isNominal()) {
            printMetrics(ps, getMetricsNominal(eval));
        }
    }

    private void printMetricHeader(PrintStream ps, String[] names) {
        for (int i = 0; i < names.length; i++) {

            if (i < names.length - 1) {
                ps.print(names[i] + ",");
            } else {
                ps.println(names[i]);
            }
        }
    }

    private void printMetrics(PrintStream ps, double[] metrics) {
        for (int i = 0; i < metrics.length; i++) {

            if (i < metrics.length - 1) {
                ps.print(metrics[i] + ",");
            } else {
                ps.println(metrics[i]);
            }
        }
    }

    public void crossValidation(String featureFilePath, String cs,
            String modelFilePath, String outFilePath,
            int seed, int folds, int runs, double percentageSplit) throws Exception {
        Instances data = getInstances(featureFilePath, classLabel);
        
        if(classifier instanceof BaselineClassifier)
        {
            ((BaselineClassifier) classifier).setAllInstances(data);
        }

        boolean splitMode = false;

        if (folds == 0) {
            // Unweka like folds: in terms of percentage split
            folds = 1;

//                System.out.format("Percentage split %d giving %d folds\n", percentageSplit, folds);       

            splitMode = true;
        }

        PrintStream ps = null;
        PrintStream ps1 = null;
        PrintStream ps2 = null;
        PrintStream ps3 = null;

        if (outFilePath != null && outFilePath.equals("") == false) {
            ps = new PrintStream(outFilePath, cs);
            ps1 = new PrintStream(outFilePath + "-metrics-train.csv", cs);
            ps2 = new PrintStream(outFilePath + "-metrics-test.csv", cs);
            ps3 = new PrintStream(outFilePath + "-output", cs);
        }

        if (ps != null) {
            printMetricHeader(data.classAttribute(), ps1);
            printMetricHeader(data.classAttribute(), ps2);
        }

        for (int i = 0; i < runs; i++) {
            System.out.println("Starting run " + (i + 1) + "...");

            seed = i + 1;

            Random rand = new Random(seed);   // create seeded number generator
            Instances randData = new Instances(data);   // create copy of original data
            randData.randomize(rand);

            Instances train = null;
            Instances test = null;

            if (splitMode) {
                int trainSize = (int) ((percentageSplit / 100.0) * data.numInstances());
                int testSize = data.numInstances() - trainSize;

                System.out.format("Train size %d, test size %d, total size %d\n",
                        trainSize, testSize, data.numInstances());

                train = new Instances(randData, 0, trainSize);
                test = new Instances(randData, trainSize, testSize);
            }

            for (int n = 0; n < folds; n++) {
                System.out.println("Starting fold " + (n + 1) + "...");

                if (!splitMode) {
                    trainEstimator(randData, modelFilePath);

                    train = randData.trainCV(folds, n);
                    test = randData.testCV(folds, n);
                } else {
                    trainEstimator(train, modelFilePath);
                }

//                predict(test, modelFilePath);

                Classifier cl = (Classifier) wekaSetup.get(0);

                Evaluation eval = evaluate(train, train, cl);
                System.out.println(eval.toSummaryString("\nEvaluating on training data:\n======\n", false));

                if (ps != null) {
        
                    ps.println(cl);
                    
                    printMetric(data.classAttribute(), eval, ps1);

                    ps.println(eval.toSummaryString("\nEvaluating on training data:\n======\n", false));
                    
                    if(train.classAttribute().isNominal())
                    {
                        ps.println(eval.toClassDetailsString());

                        ps.println(eval.toMatrixString());
                    }
                }

                eval = evaluate(train, test, cl);
                System.out.println(eval.toSummaryString("\nEvaluating on test data:\n======\n", false));

                // Printing predictions for the first fold
                if(i == 0 && n == 0)
                {
                    if (ps != null) {
                        printPredictions(cl, test, ps3);
                    }                    
                }
                
                if (ps != null) {
                    ps.println(cl);
                    
                    printMetric(data.classAttribute(), eval, ps2);

                    ps.println(eval.toSummaryString("\nEvaluating on test data:\n======\n", false));
                    
                    if(train.classAttribute().isNominal())
                    {
                        ps.println(eval.toClassDetailsString());

                        ps.println(eval.toMatrixString());
                    }
                }
            }
        }

        if (ps != null) {

            System.out.println("Metric values on training data saved to: " + outFilePath + "-metrics-train.csv");
            System.out.println("Metric values on test data saved to: " + outFilePath + "-metrics-test.csv");
            System.out.println("Summary output saved to: " + outFilePath);
            
            

            ps.close();
            ps1.close();
            ps2.close();
            ps3.close();
        }
    }

    public Evaluation evaluate(Instances train, Instances test, Classifier cl) throws Exception {
        // evaluate classifier and print some statistics
        
        if(classifier instanceof ClassVote)
        {
//            Instances newTrain = new Instances(train);
            Instances newTest = new Instances(test);
            Instances newTrain = ((ClassVote) classifier).averageOfClasses(train);
//            Instances newTest = ((ClassVote) classifier).averageOfClasses(test);

            Evaluation eval = new Evaluation(newTrain);

            eval.evaluateModel(cl, newTest, new Object[0]);
            
            return eval;
        }
        
//        Evaluation eval = new Evaluation(train);
        Evaluation eval = new EvaluationExt(train);

        eval.evaluateModel(cl, test, new Object[0]);
            
        System.out.println("******************************************************");
        System.out.println("Correctness: " + ((EvaluationExt) eval).correctness(cl, test));
        System.out.println("******************************************************");

        return eval;
    }

    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 2);

        opt.addSet("sset", 1, 2).addOption("split").addOption("c", Options.Multiplicity.ZERO_OR_ONE);
        opt.addSet("cvset", 1, 2).addOption("cv").addOption("c", Options.Multiplicity.ZERO_OR_ONE);

        opt.addOptionAllSets("v", Options.Multiplicity.ZERO_OR_ONE);

        opt.getSet().addBooleanOption("test", "ts", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("train", "tr", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("algo", "ml", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("filter", "af", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("excludeFilter", "eaf", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("charset", "fcs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("model", "m", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("classLabel", "cls", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        opt.getSet("sset").addOption("algo", "ml", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("filter", "af", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addBooleanOption("excludeFilter", "eaf", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("charset", "fcs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("model", "m", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("classLabel", "cls", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("percentage", "p", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("average", "avg", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("sset").addOption("weights", "w", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        opt.getSet("cvset").addOption("algo", "ml", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("filter", "af", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addBooleanOption("excludeFilter", "eaf", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("charset", "fcs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("model", "m", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("classLabel", "cls", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("seed", "s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("runs", "r", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("folds", "f", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("percentage", "p", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("average", "avg", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("cvset").addOption("weights", "w", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

//        String srcCS = "UTF-8";
//        String tgtCS = "UTF-8";
        String classifier = "";
        Classifier cl = null;
        boolean excludeFilter = false;
        String featureFileCS = "UTF-8";
        String filterFilePath = null;
        String classLabel = null;

        String trainFeatureFilePath = "";
        String testFeatureFilePath = "";
        String modelFilePath = "";
        String outFilePath = "";

        int seed = 1;
        int runs = 1;
        int folds = 0;
        double percentageSplit = 0.0; // 80-20 split

        boolean train = false;
        boolean test = false;

//        String srcFilePath = null;
//        String tgtFilePath = null;

        OptionSet set = opt.getMatchingSet();

        if (set == null) {
            System.out.println("HypothesisConfidence [-mode] options");
            System.out.println("\tModes:");
            System.out.println("\t\tDefault: " + "Train a model and/or use it to predict/classify");
            System.out.println("\t\t-split: " + "Train/test split");
            System.out.println("\t\t-cv: " + "N-fold cross-validation (separate training model for each fold)");
            System.out.println("\tFor the default mode:");
            System.out.println("\t\t-tr (-train): " + "Train a model");
            System.out.println("\t\t-ts (-test): " + "Test against a model");
            System.out.println("\t\t-ml (-algo): " + "Machine learning algorithm to use (fully qualified Weka name of the classifier)");
            System.out.println("\t\t-af (-filter): " + "List of attributes to be used as filter");
            System.out.println("\t\t\tOne attribute name on each line.");
            System.out.println("\t\t-eaf (-excludeFilter): " + "Whether to exclude the attributes in the filter list (default: inclusion)");
            System.out.println("\t\t-m (-model): " + "The model path");
            System.out.println("\t\t-o (-output): " + "The output path");
            System.out.println("\tFor the split mode:");
            System.out.println("\t\t-ml (-algo): " + "Machine learning algorithm to use (fully qualified Weka name of the classifier)");
            System.out.println("\t\t-af (-filter): " + "List of attributes to be used as filter");
            System.out.println("\t\t-eaf (-excludeFilter): " + "Whether to exclude the attributes in the filter list (default: inclusion)");
            System.out.println("\t\t-cs (-charset): " + "The charset of the data files");
            System.out.println("\t\t-m (-model): " + "The model path");
            System.out.println("\t\t-o (-output): " + "The output path");
            System.out.println("\t\t-p (-percent): " + "Percentage (of training data) for split");
            System.out.println("\t\t-avg (-average): " + "Comma separated list of classes (labels) whose results are to be averaged for output");
            System.out.println("\t\t\tIf there are n classes, n different classifier will be created and there output averaged");
            System.out.println("\t\t-w (-weights): " + "Comma separated list of class weights for the -avg option (weighted average will be the output)");
            System.out.println("\t\t\tNote that the Weka evaluator currently does not work with the -avg option, so you won't get all the usual metrics");
            System.out.println("\tFor the cross-validation mode:");
            System.out.println("\t\t-ml (-algo): " + "Machine learning algorithm to use (fully qualified Weka name of the classifier)");
            System.out.println("\t\t-af (-filter): " + "List of attributes to be used as filter");
            System.out.println("\t\t-eaf (-excludeFilter): " + "Whether to exclude the attributes in the filter list (default: inclusion)");
            System.out.println("\t\t-cs (-charset): " + "The charset of the data files");
            System.out.println("\t\t-m (-model): " + "The model path");
            System.out.println("\t\t-o (-output): " + "The output path");
            System.out.println("\t\t-p (-percent): " + "Percentage (of training data) for split");
            System.out.println("\t\t-s (-seed): " + "Seed valued for random splits");
            System.out.println("\t\t-r (-runs): " + "The number of runs");
            System.out.println("\t\t-f (-folds): " + "The number of folds");
            System.out.println("\t\t\tIf you want separate models for n split, use '-r n' and '-f 0'.");
            System.out.println("\t\t\tIf you want cross-validation as in the Weka GUI, use '-r 1' and '-f n'.");
            System.out.println("\t\t-avg (-average): " + "Comma separated list of classes (labels) whose results are to be averaged for output");
            System.out.println("\t\t-w (-weights): " + "Comma separated list of class weights for the -avg option (weighted average will be the output)");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        boolean d = false;
        boolean cv = false;
        boolean ttsplit = false;
        String[] avgOfClasses = null;
        double weights[] = null;

        if (set.getSetName().equals("cvset")) {
            cv = true;
        }
        else if (set.getSetName().equals("sset")) {
            ttsplit = true;
        }
        else{
            d = true;
        }
        
        if(d)
        {
            if (set.isSet("test")) {
                test = true;
            }

            if (set.isSet("train")) {
                train = true;
            }            
        }

        if (cv || ttsplit || d) {
//            if (set.isSet("scs")) {
//                srcCS = set.getOption("scs").getResultValue(0);
//            }
//
//            if (set.isSet("tcs")) {
//                tgtCS = set.getOption("tcs").getResultValue(0);
//            }

            if (set.isSet("ml")) {
                classifier = set.getOption("ml").getResultValue(0);

                try {
                    cl = Classifier.forName(classifier, new String[]{""});
                } catch (Exception ex) {
                    Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (set.isSet("fcs")) {
                featureFileCS = set.getOption("fcs").getResultValue(0);
            }

            if (set.isSet("af")) {
                filterFilePath = set.getOption("af").getResultValue(0);
            }

            if (set.isSet("cls")) {
                classLabel = set.getOption("cls").getResultValue(0);
                
                System.out.println("Class label: " + classLabel);
            }

            if (set.isSet("eaf")) {
                excludeFilter = true;
            }

//            if (set.isSet("trf")) {
//                trainFeatureFilePath = set.getOption("f").getResultValue(0);
//            }
//
//            if (set.isSet("tsf")) {
//                testFeatureFilePath = set.getOption("f").getResultValue(0);
//                test = true;
//            }

            if (set.isSet("m")) {
                modelFilePath = set.getOption("m").getResultValue(0);
            }

            if (set.isSet("o")) {
                outFilePath = set.getOption("o").getResultValue(0);
            }

            if (cv && set.isSet("s")) {
                seed = Integer.parseInt(set.getOption("s").getResultValue(0));
            }

            if (cv && set.isSet("r")) {
                runs = Integer.parseInt(set.getOption("r").getResultValue(0));
            }

            if (cv && set.isSet("f")) {
                folds = Integer.parseInt(set.getOption("f").getResultValue(0));
            }

            if ((cv || ttsplit) && set.isSet("p")) {
                percentageSplit = Double.parseDouble(set.getOption("p").getResultValue(0));
            }

            if ((cv || ttsplit) && set.isSet("avg")) {
                avgOfClasses = set.getOption("avg").getResultValue(0).split(",");

                System.out.println("Average of classes:"  + Joiner.on(",").skipNulls().join(avgOfClasses));
                
                weights = new double[avgOfClasses.length];
            }

            if ((cv || ttsplit) && set.isSet("weights")) {
                String weightsStr[] = set.getOption("weights").getResultValue(0).split(",");

                System.out.println("Weights set to:"  + Joiner.on(",").skipNulls().join(weightsStr));
                
                if(weightsStr.length != avgOfClasses.length)
                {
                    System.out.println("Error: Number of classes is different from number of weights.");
                    System.exit(1);
                }
                
                for (int i = 0; i < weightsStr.length; i++) {
                    String string = weightsStr[i];
                    weights[i] = Double.parseDouble(string);
                }
                
                cl = ClassVote.getInstance(classifier, avgOfClasses, new String[]{""}, weights);
            }

//            srcFilePath = set.getData().get(0);
//            tgtFilePath = set.getData().get(1);
            
            if(test && !train)
            {
                testFeatureFilePath = set.getData().get(0);                
            }
            else if(!test && train)
            {
                trainFeatureFilePath = set.getData().get(0);                
            }
            else if(test && train)
            {
                trainFeatureFilePath = set.getData().get(0);                
                testFeatureFilePath = set.getData().get(1);                
            }
            else if (set.getData().size() > 0) {
                train = true;
                trainFeatureFilePath = set.getData().get(0);

                if (set.getData().size() > 1) {
                    test = true;
                    testFeatureFilePath = set.getData().get(1);
                }
            }
        }

        HypothesisConfidence hypothesisConfidence = null;

        hypothesisConfidence = new HypothesisConfidence(cl, filterFilePath, excludeFilter, classLabel);

//        if (cl != null) {
//            hypothesisConfidence = new HypothesisConfidence(cl, filterFilePath, excludeFilter, classLabel);
//        } else {
//            hypothesisConfidence = new HypothesisConfidence(filterFilePath, excludeFilter, classLabel);
//        }

        if (cv == false) {
            if (ttsplit == false) {
                try {
                    //        try {
                    //            hypothesisConfidence.readText(srcFilePath, srcCS, tgtFilePath, tgtCS);
                    //        } catch (FileNotFoundException ex) {
                    //            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                    //        } catch (IOException ex) {
                    //            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                    //        } catch (Exception ex) {
                    //            Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                    //        }

                    if(train)
                    {
                        hypothesisConfidence.trainEstimator(trainFeatureFilePath, featureFileCS, modelFilePath);
                    }

                    if (test) {
                        hypothesisConfidence.predict(testFeatureFilePath, featureFileCS, modelFilePath, outFilePath);
                    }

                } catch (Exception ex) {
                    Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    hypothesisConfidence.trainTestSplit(trainFeatureFilePath, featureFileCS,
                            modelFilePath, outFilePath,
                            percentageSplit);
                } catch (Exception ex) {
                    Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            try {
                System.out.format("Cross validation with %d runs, %d folds and %f split\n", runs, folds, percentageSplit);

                hypothesisConfidence.crossValidation(trainFeatureFilePath, featureFileCS,
                        modelFilePath, outFilePath,
                        seed, folds, runs, percentageSplit);
            } catch (Exception ex) {
                Logger.getLogger(HypothesisConfidence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }

    public static void main(String args[]) {
        runWithCmdLineOptions(args);
    }

    @Override
    public void run() {
    }
}
