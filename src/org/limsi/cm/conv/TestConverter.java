/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anil
 */
public class TestConverter {
    
    public void testReadTercomFormat() throws FileNotFoundException, IOException, UnsupportedEncodingException, Exception
    {
        ReadTercomFormat converter = new ReadTercomFormat(false);
        
        converter.setInputPath("/more/work/trace-qe/tercom/corrections_en2fr.data.trans.tercom.sys.sum", "UTF-8");
        
        converter.loadFile();
        
        converter.convert(System.out);
    }
    
    public static void main(String argsp[])
    {
        TestConverter tester = new TestConverter();
        
        try {
            tester.testReadTercomFormat();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TestConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(TestConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
