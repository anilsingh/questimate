/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.types.Format;
import sanchay.properties.PropertyTokens;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class FormatConverter {
    protected boolean batchMode = false;
    protected SanchayTableModel batchPaths;
    protected String charset = "UTF-8";
    protected PropertyTokens inputData;
    protected String inputPath;

    protected Format inFormat;
    protected Format outFormat;

    public void convert(List<String> inPaths, String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
        for (String string : inPaths) {
            setInputPath(string, encoding);
            
            System.out.println("Converting file: " + string);
            
            if(inPaths.size() > 1)
            {
                File ifile = new File(string);
                File ofile = new File(outFilePath);
                
                if(ofile.exists() && ofile.isFile())
                {
                    convert(string + outFormat.getFormatExtension(), encoding);
                }
                else if(!ofile.exists())
                {
                    ofile.mkdirs();
                    
                    ofile = new File(ofile, ifile.getName());
                    
                    convert(MiscellaneousUtils.replaceLast(ofile.getAbsolutePath(),
                            inFormat.getFormatExtension(), "") + outFormat.getFormatExtension(), encoding);
                }
                else if(ofile.isDirectory())
                {
                    ofile = new File(ofile, ifile.getName());
                    
                    convert(MiscellaneousUtils.replaceLast(ofile.getAbsolutePath(),
                            inFormat.getFormatExtension(), "") + outFormat.getFormatExtension(), encoding);
                }
            }
            else
            {
                if(outFilePath == null)
                {
                    convert(MiscellaneousUtils.replaceLast(string,
                            inFormat.getFormatExtension(), "") + outFormat.getFormatExtension(), encoding);
                }
                else
                {
                    convert(outFilePath, encoding);                
                }
            }
        }
    }

    public void convert(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
        PrintStream ps = new PrintStream(outFilePath, encoding);
        
        convert(ps);
        
        ps.close();
    }

    public void convert(PrintStream ps) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
    }
    
    protected void convertFile(int i) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception
    {        
        String inpath = (String) batchPaths.getValueAt(i, 0);
        String inCS = (String) batchPaths.getValueAt(i, 1);
        String outpath = (String) batchPaths.getValueAt(i, 2);
        String outCS = (String) batchPaths.getValueAt(i, 3);
        System.out.printf("Processing file %s\n", inpath);

        loadFile(inpath, inCS);

        convert(outpath, outCS);
    }

    public void convertBatch(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
        if (batchMode == false) {
            convert(outFilePath, encoding);
            return;
        }
        if (batchPaths == null) {
            System.err.println("Batch paths not loaded.");
            return;
        }
        int rcount = batchPaths.getRowCount();
        for (int i = 0; i < rcount; i++) {
            convertFile(i);
        }
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void loadBatchPaths(String batchPaths) throws FileNotFoundException, IOException {
        this.batchPaths = new SanchayTableModel(batchPaths, "UTF-8");
        this.batchPaths.read(batchPaths, "UTF-8");
        //        this.batchPaths.print(System.out);
    }

    public void loadFile() throws FileNotFoundException, IOException {
        inputData = new PropertyTokens(inputPath, charset);
    }

    public void loadFile(String p, String cs) throws FileNotFoundException, IOException {
        setInputPath(p, cs);
        loadFile();
    }

    public void setInputPath(String p, String cs) {
        inputPath = p;
        charset = cs;
    }
    
}
