/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.limsi.cm.workflow.FeatureExtractionFlow;
import org.limsi.mt.cm.HypothesisConfidence;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.Format;
import org.limsi.types.FormatType;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class FeatureFileConverter extends FormatConverter {
       
    public FeatureFileConverter(Format inFormat, Format outFormat)
    {
        this.inFormat = inFormat;
        this.outFormat = outFormat;
    }
    
    @Override
    public void convert(PrintStream ps) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
//        if(inFormat.getFormat().equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT)
//                && outFormat.getFormat().equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))

        if(inFormat.equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
        {
            Instances instances = HypothesisConfidence.getInstances(inputPath, null);
            
            int count = instances.numInstances();
            
            for (int i = 0; i < count; i++) {
                
                HypothesisFeatures hf = HypothesisFeatures.getHypothesisFeatures(instances, i);
                
                if(i == 0)
                {
                    hf.printHeader(ps, (FormatType.FeatureCollectionFormat) outFormat);
                }

                hf.printFeatures(ps, (FormatType.FeatureCollectionFormat) outFormat);
            }
        }
        else
        {
            LineNumberReader reader;

            if (inputPath.endsWith(".gz")) {
                reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                        new FileInputStream(inputPath)), charset));
            } else {
                reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                        inputPath), charset));
            }

            int i = 0;

            for (String line; (line = reader.readLine()) != null;)
            {
                line = line.trim();
                
                HypothesisFeatures hf = new HypothesisFeatures();
                
                if(inFormat.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))
                {
                    hf.readSVMLight(line);
                }
                else if(inFormat.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
                {
                    hf.readFeatures(line);
                }

                if(i == 0)
                {
                    hf.printHeader(ps, (FormatType.FeatureCollectionFormat) outFormat);
                }

                hf.printFeatures(ps, (FormatType.FeatureCollectionFormat) outFormat);
                
                i++;
            }
        }
    }
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 10000);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("inFormat", "if", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("outFormat", "of", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        String charset = "UTF-8";
        List<String> infiles;
        String outfile = null;
        
        FormatType.FeatureCollectionFormat inFormat = FormatType.FeatureCollectionFormat.ARFF_FORMAT;
        FormatType.FeatureCollectionFormat outFormat = FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT;

        if (!opt.check()) {
            System.out.println("AvroIO [-if inFormat] [-of outFormat] [-cs charset] -o [outfile] infiles ...");
            System.out.println("\tFormats:");
            System.out.println("\t\t" + FormatType.FeatureCollectionFormat.ARFF_FORMAT);
            System.out.println("\t\t" + FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT);
            System.out.println("\t\t" + FormatType.FeatureCollectionFormat.CSV_FORMAT);
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }

        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }

        if (opt.getSet().isSet("if")) {
            String fmt = opt.getSet().getOption("if").getResultValue(0);  
            inFormat = FormatType.FeatureCollectionFormat.valueOf(fmt);
            System.out.println("Input format: " + inFormat);
        }

        if (opt.getSet().isSet("of")) {
            String fmt = opt.getSet().getOption("of").getResultValue(0);  
            outFormat = FormatType.FeatureCollectionFormat.valueOf(fmt);
            System.out.println("Output format: " + outFormat);
        }

        infiles = opt.getSet().getData();

        System.out.println("Input files: " + infiles);

        try {
            
            FeatureFileConverter featureFileConverter = new FeatureFileConverter(inFormat, outFormat);

            featureFileConverter.convert(infiles, outfile, charset);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {
//        args = new String[] {"-if", "SVMLIGHT_FORMAT", "-of", "ARFF_FORMAT",
//            "-o", "/more/work/trace-qe/features/feature-set-17-03-2013/hter/arff",
//                "/more/work/trace-qe/features/feature-set-17-03-2013/hter/corrections_en2fr.data-features.svmlight",
//                "/more/work/trace-qe/features/feature-set-17-03-2013/hter/corrections_fr2en.data-features.svmlight"};
                
        runWithCmdLineOptions(args);
    }
}
