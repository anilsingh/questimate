/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import sanchay.corpus.ssf.SSFStory;

/**
 *
 * @author anil
 */
public class GenerateTercomFormat extends FormatConverter {
 
    public GenerateTercomFormat(boolean b) {
        batchMode = b;
    }

    public void convert(PrintStream ps) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
        int count = inputData.countTokens();
        
        File f = new File(inputPath);
        
        for (int i = 0; i < count; i++) {
            String line = inputData.getToken(i);
            
            line += " (" + f.getName() + "-" + (i + 1) + ")";
            
            ps.println(line);
        }        
    }
    
    public static void generateTercomFormat(SSFStory document, String outpath, String cs, boolean lowercase)
    {
        
    }
       
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String infile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("GenerateTercomFormat [-cs charset] [-o outfile] [-b batchPath] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file " + infile);
        }
        
        try {            
            GenerateTercomFormat generateTercomFormat = new GenerateTercomFormat(batchMode);            

            generateTercomFormat.setInputPath(infile, charset);
            
            if(batchMode)
            {
                generateTercomFormat.loadBatchPaths(batchPath);
            }
            else
            {
                
                generateTercomFormat.loadFile();
            }
            
            generateTercomFormat.convertBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GenerateTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenerateTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GenerateTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GenerateTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }            
}
