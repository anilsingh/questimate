/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.limsi.cm.resources.CEResources;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;

/**
 *
 * @author anil
 */
public class POSTaggedFormatConvertor {
    
    public static String TREE_TAGGER = "TREE_TAGGER";
    public static String SINGLE_LINE_UNDERSCORE = "SINGLE_LINE_UNDERSCORE";
    public static String SINGLE_LINE_UNDERSCORE_NOLEX = "SINGLE_LINE_UNDERSCORE_NOLEX";

    protected String charset = "UTF-8";
    protected String inFormat = "TREE_TAGGER";
    protected String outFormat = "SINGLE_LINE_UNDERSCORE";
    protected String infile = "";
    protected String outfile = "";
        
    public POSTaggedFormatConvertor(String inFormat, String outFormat,
            String infile, String outfile, String charset)
    {
        this.inFormat = inFormat;
        this.outFormat = outFormat;
        this.infile = infile;
        this.outfile = outfile;
        this.charset = charset;
    }
    
    public void convert() throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        if(inFormat.equalsIgnoreCase(TREE_TAGGER) && outFormat.equalsIgnoreCase(SINGLE_LINE_UNDERSCORE))
        {
            POSCountsFeatureExtractor.treetaggerPOS2POSTagged(infile, outfile, charset);
        }
        else if(inFormat.equalsIgnoreCase(TREE_TAGGER) && outFormat.equalsIgnoreCase(SINGLE_LINE_UNDERSCORE_NOLEX))
        {
            POSCountsFeatureExtractor.treetaggerPOS2POSNoLex(infile, outfile, charset);
        }
    }
    
    public static void main(String args[])
    {
        Options opt = new Options(args, 1);

//        opt.getSet().addOption("cs", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("if", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("of", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        
        String charset = "UTF-8";
        String inFormat = "TREE_TAGGER";
        String outFormat = "SINGLE_LINE_UNDERSCORE";
        String infile = "";
        String outfile = "";

        if (!opt.check()) {
        // Print usage hints
            System.out.println("POSTaggedFormatConvertor [-cs charset] [-if input format] [-of output format] -o outfile infile");
            System.exit(1);
        }

        // Normal processing

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("if")) {
            inFormat = opt.getSet().getOption("if").getResultValue(0);  
            System.out.println("Input format " + inFormat);
        }
        
        if (opt.getSet().isSet("of")) {
            outFormat = opt.getSet().getOption("of").getResultValue(0);  
            System.out.println("Output format " + outFormat);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }

        infile = opt.getSet().getData().get(0);
        
        System.out.println("Input file " + infile);
        
        try {
                    
            CEResources.setSSFProps();
            
            POSTaggedFormatConvertor posFormatConvertor = new POSTaggedFormatConvertor(inFormat,
                    outFormat, infile, outfile, charset);
            
            posFormatConvertor.convert();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
