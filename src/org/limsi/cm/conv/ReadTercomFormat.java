/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.conv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ml.options.Options;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.FormatType;

/**
 *
 * @author anil
 */
public class ReadTercomFormat extends FormatConverter {
 
    public ReadTercomFormat(boolean b) {
        batchMode = b;
    }

    public void convert(PrintStream ps, FormatType.FeatureCollectionFormat format) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception {
        int count = inputData.countTokens();
            
        Pattern p = Pattern.compile("\\|");
        
        String fnames[] = null;
            
        int mnum = 0;
        
        int recordIndex = 0;
                
        for (int i = 0; i < count; i++) {
            String line = inputData.getToken(i);
            
            Matcher m = p.matcher(line);
            
            if(m.find())
            {
                if(mnum == 0)
                {
                    fnames = p.split(line);
                    
                    fnames = MiscellaneousUtils.trimStrings(fnames);
                }
                else
                {            
                    String fields[] = p.split(line);

                    fields = MiscellaneousUtils.trimStrings(fields);

                    HypothesisFeatures<String> hf = new HypothesisFeatures<String>();
                    
                    for (int j = 1; j < fields.length; j++) {
                                                
                        hf.addFeature(fnames[j], fields[j]);

                        if(fnames[j].equals("TER"))
                        {
                            double ter = Double.parseDouble(fields[j]);
                            
                            ter = ter / 100.0;
                            
                            if(ter > 1.0)
                            {
                                ter = 1.0;
                            }

                            hf.addFeature("ClippedTER", ter);
                        }
                    }
                    
                    if(recordIndex == 0)
                    {
                        hf.printHeader(ps, format);
                    }

                    hf.printFeatures(ps, format);
                }

                mnum++;
            }
        }
    }
  
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String infile = "";
        String outfile = "";
        boolean batchMode = false;
        String batchPath = "";

        if (!opt.check()) {
            System.out.println("GenerateTercomFormat [-cs charset] [-o outfile] [-b batchPath] [infile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file " + outfile);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;  
            batchPath = opt.getSet().getOption("b").getResultValue(0);  
            System.out.println("Batch mode " + batchMode);
            System.out.println("Batch paths located at: " + batchPath);
        }

        if(!batchMode)
        {
            infile = opt.getSet().getData().get(0);

            System.out.println("Input file " + infile);
        }
        
        try {            
            ReadTercomFormat readTercomFormat = new ReadTercomFormat(batchMode);            

            readTercomFormat.setInputPath(infile, charset);
            
            if(batchMode)
            {
                readTercomFormat.loadBatchPaths(batchPath);
            }
            else
            {
                
                readTercomFormat.loadFile();
            }
            
            readTercomFormat.convertBatch(outfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ReadTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReadTercomFormat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }            
}
