/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.wg;

import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.openfst.Convert;
import edu.cmu.sphinx.fst.operations.ArcSort;
import edu.cmu.sphinx.fst.operations.Compose;
import edu.cmu.sphinx.fst.operations.Determinize;
import edu.cmu.sphinx.fst.operations.ILabelCompare;
import edu.cmu.sphinx.fst.operations.OLabelCompare;
import edu.cmu.sphinx.fst.operations.Project;
import edu.cmu.sphinx.fst.operations.ProjectType;
import edu.cmu.sphinx.fst.operations.RmEpsilon;
import edu.cmu.sphinx.fst.semiring.LogSemiring;
import edu.cmu.sphinx.fst.semiring.ProbabilitySemiring;
import edu.cmu.sphinx.fst.semiring.RealSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import edu.cmu.sphinx.fst.semiring.TropicalSemiring;
import edu.stanford.nlp.util.Index;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.cm.util.NGramLMUtils;
import org.limsi.cm.workflow.ToolFlow;
import org.limsi.fst.FstUtils;
import org.limsi.types.FormatType;
import sanchay.mlearning.lm.ngram.NGramLM;
import sanchay.mlearning.lm.ngram.impl.NGramLMImpl;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class GenerateNGramFstsWG {
    
    private WordGraph wordGraph;
    private Fst latticeFst;
    private Fst ngramFst;
    private Fst composedFst;
    
    private Index uniqNGrams;
    
    private Semiring semiring;
    
    private File inDirectory;
    private File outDirectory;
    private String basename;
    private String charset;
    
    private String iname;
    private String oname;
    
    private WordGraphReader wordGraphReader;
    
//    public static final String NCODE_FORMAT = "NCODE_FORMAT";
//    public static final String WMT12_FORMAT = "WMT12_FORMAT";
//    public static final String MOSES_FORMAT = "MOSES_FORMAT";
//    public static final String FST_FORMAT = "FST_FORMAT";
    
    private FormatType.WordGraphFormat format = FormatType.WordGraphFormat.NCODE_FORMAT;
//    private boolean batchMode = false;
    
    private FilenameFilter fileFilter;

    /**
     * This is the constructor to be used if you are going to use the WordGraphReader
     * to generate FSTs from a large word graph file. The constructor will create an
     * instance of a WordGraphReader and will use that to incrementally read WordGraphs
     * and generate FSTs from them.
     * @param fmt
     * @param sr
     * @param inDir
     * @param basename
     * @param outDir
     * @param cs 
     */
    public GenerateNGramFstsWG(FormatType.WordGraphFormat fmt, String sr, String inDir, String basename,
            String outDir, String cs) {
        this(fmt, sr);

        inDirectory = new File(inDir);
        outDirectory = new File(outDir);
        this.basename = basename;
        charset = cs;

        wordGraphReader = new WordGraphReader(inDir, basename, cs);
    }

    public GenerateNGramFstsWG(FormatType.WordGraphFormat fmt, String sr, String inDir,
            String basename, String outDir, String cs, WordGraphReader wordGraphReader) {
        this(fmt, sr);

        inDirectory = new File(inDir);
        outDirectory = new File(outDir);
        this.basename = basename;
        charset = cs;

        this.wordGraphReader = wordGraphReader;
    }
    
    private GenerateNGramFstsWG(FormatType.WordGraphFormat fmt, String sr) {
//    public GenerateNGramFstsWG(String fmt, String sr, boolean batchMode) {
        format = fmt;
        this.semiring = getSemiring(sr);
//        this.batchMode = batchMode;
    }

    public void generateFsts(int whichGram, boolean bm) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        generateFsts(inDirectory, basename, outDirectory, charset, whichGram, bm);
    }
    
    private void generateFsts(File inDir, String basename,
            File outDir, String cs, int whichGram, boolean bm) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        inDirectory = inDir;
        outDirectory = outDir;
        
        if(bm == false && format.equals(FormatType.WordGraphFormat.OPEN_FST_FORMAT))
        {
            basename = MiscellaneousUtils.replaceLast(basename, ".fst.txt", "");
        }
        
        this.basename = basename;
        charset = cs;
        
        final String bn = basename;
        
        if(bm)
        {
            String[] inputFiles;
            
            fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File directory, String fileName) {
                    return fileName.endsWith(bn);
                }
            };

            inputFiles = inDirectory.list(fileFilter);
            
            for (int i = 0; i < inputFiles.length; i++) {
                String inputFile = inputFiles[i];
                
                File ifile = new File(inputFile);

                wordGraphReader = new WordGraphReader(inDir.getAbsolutePath(), basename, cs);
                generateFsts(inDir, ifile.getName(), outDir, cs, whichGram, false);
            }
            
            return;
        }
        
        wordGraph = new WordGraph(inDirectory.getAbsolutePath(), basename, charset);

        iname = inDirectory.getAbsolutePath() + "/" + basename;
//        oname = outDirectory.getAbsolutePath() + "/" + basename;
        
        System.out.println("Processing file: " + iname);

        if(format.equals(FormatType.WordGraphFormat.NCODE_FORMAT))
        {        
            while((wordGraph = wordGraphReader.readWordGraphNCODE(iname,
                    charset, semiring)) != null)
            {
                generateNGramFsts(wordGraph, basename, whichGram, true, new NGramLMImpl(null, "word", 4), false);
            } 
        }
        else if(format.equals(FormatType.WordGraphFormat.WMT12_FORMAT))
        {
            while((wordGraph = wordGraphReader.readWordGraphWMT12(iname,
                    charset, semiring)) != null)
            {
                generateNGramFsts(wordGraph, basename, whichGram, true, new NGramLMImpl(null, "word", 4), false);
            }        
        }
        else if(format.equals(FormatType.WordGraphFormat.OPEN_FST_FORMAT))
        {        
            while((wordGraph = wordGraphReader.readWordGraphFST(iname,
                    charset, semiring)) != null)
            {
                generateNGramFsts(wordGraph, basename, whichGram, true, new NGramLMImpl(null, "word", 4), false);
            }        
        }

        System.out.println("\tLattices read: " + wordGraphReader.wordGraphsRead());
//        
//        Iterator<WordGraph> itr = wordGraphs.iterator();
//        
//        while(itr.hasNext())
//        { 
//            wordGraph = itr.next();
//            
//            String onm = oname;
//            String bnm = basename;
//
//            oname += "-" + i;
//            basename += "-" + i;
//
//            latticeFst = WordGraph.wordGraph2Fst(wordGraph, oname, charset, semiring);
//
//            System.out.println("\tProcessing lattice : " + i);
//
//            i++;
//
//            if(whichGram == 2)
//            {
//                generateBigramFsts();
//            }
//            else if(whichGram == 3)
//            {
//                generateBigramFsts();
//            }
//            else if(whichGram == 4)
//            {
//                generateFourGramFsts();
//            }
//            
//            oname = onm;
//            bnm = basename;
//        }
    }
    
    public Fst generateNGramFsts(WordGraph wordGraph, String basename,
            int whichGram, boolean computePosteriors, NGramLM ngPosteriorProbs,
            boolean cleanupGeneratedFiles) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        this.wordGraph = wordGraph;
        
        iname = inDirectory.getAbsolutePath() + "/" + basename;
        oname = outDirectory.getAbsolutePath() + "/" + basename;

        String onm = oname;
        String bnm = basename;
        
        int i = wordGraphReader.wordGraphsRead();

        oname += "-" + i;
        basename += "-" + i;

        latticeFst = WordGraph.wordGraph2Fst(wordGraph, oname, charset, semiring);

        System.out.println("\tProcessing lattice : " + i);
        System.out.println("\tNumber of edges : " + wordGraph.countEdges());

        if(whichGram == 2)
        {
            generateBigramFsts();
        }
        else if(whichGram == 3)
        {
            generateBigramFsts();
        }
        else if(whichGram == 4)
        {
            generateFourGramFsts();
        }

        if(computePosteriors)
        {
            String pathPrefix = outDirectory.getAbsolutePath() + "/" + basename;

            System.out.println("Running LatticeFst on path prefix: " + pathPrefix);

            ToolFlow.runLatticeFst(pathPrefix, 0);            
            
            if(ngPosteriorProbs != null)
            {
                SanchayTableModel ngProbs = new SanchayTableModel();
                ngProbs.read(pathPrefix + ".post", charset);
                
                NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 1);

                if(MiscellaneousUtils.fileExists(pathPrefix + ".bg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".bg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 2);
                }

                if(MiscellaneousUtils.fileExists(pathPrefix + ".tg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".tg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 3);
                }

                if(MiscellaneousUtils.fileExists(pathPrefix + ".fg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".fg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 4);
                }
                
                ngPosteriorProbs.saveNGramLM(pathPrefix + ".post.lm", charset, false);
            }

//            for (int j = 1; j <= whichGram; j++) {
//                ToolFlow.runLatticeFst(pathPrefix, j);            
//            }
        }

        oname = onm;
        bnm = basename;
        
        if(cleanupGeneratedFiles)
        {
            cleanupGeneratedFiles();
        }
        
        return latticeFst;
    }

    public void readPosteriorProbabilities(String basename, int whichGram,
	NGramLM ngPosteriorProbs)
		throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        iname = inDirectory.getAbsolutePath() + "/" + basename;
        oname = outDirectory.getAbsolutePath() + "/" + basename;

        String onm = oname;
        String bnm = basename;
        
        int i = wordGraphReader.wordGraphsRead();

        oname += "-" + i;
        basename += "-" + i;

            String pathPrefix = outDirectory.getAbsolutePath() + "/" + basename;

            System.out.println("Reading probabilities for path prefix: " + pathPrefix);

            if(ngPosteriorProbs != null)
            {
                SanchayTableModel ngProbs = new SanchayTableModel();
                ngProbs.read(pathPrefix + ".post", charset);
                
                NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 1);

                if(MiscellaneousUtils.fileExists(pathPrefix + ".bg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".bg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 2);
                }

                if(MiscellaneousUtils.fileExists(pathPrefix + ".tg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".tg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 3);
                }

                if(MiscellaneousUtils.fileExists(pathPrefix + ".fg.post"))
                {
                    ngProbs = new SanchayTableModel();
                    ngProbs.read(pathPrefix + ".fg.post", charset);

                    NGramLMUtils.copyNGramPosteriorProbs(ngProbs, ngPosteriorProbs, 4);
                }
                
                ngPosteriorProbs.saveNGramLM(pathPrefix + ".post.lm", charset, false);
            }

        oname = onm;
        bnm = basename;
    }
    
    private void cleanupGeneratedFiles()
    {
        int i = wordGraphReader.wordGraphsRead();
        System.out.println("**Deleting the generated files for the lattice : " + i + "**");

        final String prefix = basename + "-" + i;

        File[] outputFiles;

        fileFilter = new FilenameFilter() {
            @Override
            public boolean accept(File directory, String fileName) {
                return fileName.startsWith(prefix);
            }
        };

        outputFiles = outDirectory.listFiles(fileFilter);

        for (i = 0; i < outputFiles.length; i++) {
            File outputFile = outputFiles[i];


	    if(outputFile.getName().endsWith(".post") || outputFile.getName().endsWith(".fst"))
	    {
		continue;
	    }

            System.out.println("**Deleting the generated file : " + outputFile.getAbsolutePath());
            
            outputFile.delete();
        }
    }
    
    private void generateBigramFsts() throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        System.out.println("\tGenerating bigrams...");
        
        WordGraph bgwg = wordGraph.generateBigramGraph();

        ngramFst = WordGraph.wordGraph2Fst(bgwg, oname + ".bg", charset, semiring);

        ArcSort.apply(latticeFst, new OLabelCompare());

        ArcSort.apply(ngramFst, new ILabelCompare());

        composedFst = Compose.compose(latticeFst, ngramFst, new LogSemiring(), true);

        Project.apply(composedFst, ProjectType.OUTPUT);

        composedFst = RmEpsilon.get(composedFst);

        composedFst = Determinize.get(composedFst);
        
        Convert.export(composedFst, oname + ".bg.composed");

        uniqNGrams = FstUtils.getUniqueOutputLabels(composedFst);
        
        MiscellaneousUtils.saveIndex(uniqNGrams, oname + ".bg.uniq", charset);
    }
    
    private void generateTrigramFsts() throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        generateBigramFsts();
        
        System.out.println("\tGenerating trigrams...");        
        
        WordGraph tgwg = wordGraph.generateTrigramGraph(uniqNGrams);

        ngramFst = WordGraph.wordGraph2Fst(tgwg, oname + ".tg", charset, semiring);

        ArcSort.apply(latticeFst, new OLabelCompare());

        ArcSort.apply(ngramFst, new ILabelCompare());

        composedFst = Compose.compose(latticeFst, ngramFst, new LogSemiring(), true);

        Project.apply(composedFst, ProjectType.OUTPUT);

        composedFst = RmEpsilon.get(composedFst);

        composedFst = Determinize.get(composedFst);
        
        Convert.export(composedFst, oname + ".tg.composed");

        uniqNGrams = FstUtils.getUniqueOutputLabels(composedFst);
        
        MiscellaneousUtils.saveIndex(uniqNGrams, oname + ".tg.uniq", charset);
    }
    
    private void generateFourGramFsts() throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        generateTrigramFsts();

        System.out.println("\tGenerating four grams...");
        
        WordGraph fgwg = wordGraph.generateFourGramGraph(uniqNGrams);

        ngramFst = WordGraph.wordGraph2Fst(fgwg, oname + ".fg", charset, semiring);

        ArcSort.apply(latticeFst, new OLabelCompare());

        ArcSort.apply(ngramFst, new ILabelCompare());

        composedFst = Compose.compose(latticeFst, ngramFst, new LogSemiring(), true);

        Project.apply(composedFst, ProjectType.OUTPUT);

        composedFst = RmEpsilon.get(composedFst);

        composedFst = Determinize.get(composedFst);
        
        Convert.export(composedFst, oname + ".fg.composed");

        uniqNGrams = FstUtils.getUniqueOutputLabels(composedFst);
    }
    
    public static Semiring getSemiring(String sr)
    {
        if(sr == null)
        {
            return new LogSemiring();            
        }
        
        if(sr.equalsIgnoreCase("log"))
        {
            return new LogSemiring();
        }
        else if(sr.equalsIgnoreCase("real"))
        {
            return new RealSemiring();
        }
        else if(sr.equalsIgnoreCase("tropical"))
        {
            return new TropicalSemiring();
        }
        else if(sr.equalsIgnoreCase("probability"))
        {
            return new ProbabilitySemiring();
        }
        
        return new LogSemiring();
    }
       
    public static void main(String args[])
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("f", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("g", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("b", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("n", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String semiring = "";
        String indir = "";
        String basename = "";
        String outdir = "";
        int whichGram = 2;
        boolean batchMode = false;
        FormatType.WordGraphFormat format = FormatType.WordGraphFormat.NCODE_FORMAT;

        if (!opt.check()) {
            System.out.println("GenerateNGramFsts [-cs charset] [-f format] [-g whichGram] [-b batchMode] -o outdir -b basename indir");
            System.out.println("In case of batch mode, the basename will be interpreted as a Java file filter expression.");
            System.out.println("(Only the filename suffix implemented at present, e.g. .GRAPH)");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }
        
        if (opt.getSet().isSet("f")) {
            String formatStr = opt.getSet().getOption("f").getResultValue(0);  
            
            format = FormatType.WordGraphFormat.valueOf(formatStr);
            
            System.out.println("Input format " + format);
        }
        
        if (opt.getSet().isSet("s")) {
            semiring = opt.getSet().getOption("s").getResultValue(0);  
            System.out.println("Semiring: " + semiring);
        }
        
        if (opt.getSet().isSet("g")) {
            String gramStr = opt.getSet().getOption("g").getResultValue(0);  
            whichGram = Integer.parseInt(gramStr);
            System.out.println("whichGram: " + whichGram);
        }
        
        if (opt.getSet().isSet("b")) {
            batchMode = true;
            System.out.println("Batch mode: " + batchMode);
        }
        
        if (opt.getSet().isSet("o")) {
            outdir = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output path " + outdir);
        }
        
        if (opt.getSet().isSet("n")) {
            basename = opt.getSet().getOption("n").getResultValue(0);  
            System.out.println("Basename " + basename);
        }

        indir = opt.getSet().getData().get(0);

        System.out.println("Input path " + indir);
        
        try {            
            GenerateNGramFstsWG generateNGramFsts = new GenerateNGramFstsWG(format, semiring, indir, basename, outdir, charset);            
            
            generateNGramFsts.generateFsts(whichGram, batchMode);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GenerateNGramFstsWG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenerateNGramFstsWG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GenerateNGramFstsWG.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GenerateNGramFstsWG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
