/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.wg;

import edu.stanford.nlp.mt.base.Sequence;
import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.PrintStream;
import java.util.Iterator;
import org.limsi.mt.cm.WordAlignment;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.mt.cm.features.sentence.FeatureValuesImpl;

/**
 *
 * @author anil
 */
public class WordGraphEdge<TK extends Object, FV extends Object> {
    
    protected WordGraphNode<TK, FV> startNode;
    protected WordGraphNode<TK, FV> endNode;

    protected int sourceSequenceID;
    protected int targetSequenceID;
    
    protected WordAlignment wordAlignment;

    protected FeatureValues<FV> features;
    
    protected double translationModelProb;
    protected double languageModelProb;

    protected double score;

    public WordGraphEdge() {
    }

    /**
     * @return the startNode
     */
    public WordGraphNode<TK, FV> getStartNode() {
        return startNode;
    }

    /**
     * @param startNode the startNode to set
     */
    public void setStartNode(WordGraphNode<TK, FV> startNode) {
        this.startNode = startNode;
    }

    /**
     * @return the endNode
     */
    public WordGraphNode<TK, FV> getEndNode() {
        return endNode;
    }

    /**
     * @param endNode the endNode to set
     */
    public void setEndNode(WordGraphNode<TK, FV> endNode) {
        this.endNode = endNode;
    }

    /**
     * @return the sourceSequenceID
     */
    public int getInputSequenceID() {
        return sourceSequenceID;
    }

    /**
     * @param sourceSequenceID the sourceSequenceID to set
     */
    public void setInputSequenceID(int sourceSequenceID) {
        this.sourceSequenceID = sourceSequenceID;
    }

    /**
     * @return the targetSequenceID
     */
    public int getOutputSequenceID() {
        return targetSequenceID;
    }

    /**
     * @param targetSequenceID the targetSequenceID to set
     */
    public void setOutputSequenceID(int targetSequenceID) {
        this.targetSequenceID = targetSequenceID;
    }

    /**
     * @return the wordAlignment
     */
    public WordAlignment getWordAlignment() {
        return wordAlignment;
    }

    /**
     * @param wordAlignment the wordAlignment to set
     */
    public void setWordAlignment(WordAlignment wordAlignment) {
        this.wordAlignment = wordAlignment;
    }

    /**
     * @return the features
     */
    public FeatureValues<FV> getFeatures() {
        return features;
    }

    /**
     * @param features the features to set
     */
    public void setFeatures(FeatureValues<FV> features) {
        this.features = features;
    }

    /**
     * @return the translationModelProb
     */
    public double getTranslationModelProb() {
        return translationModelProb;
    }

    /**
     * @param translationModelProb the translationModelProb to set
     */
    public void setTranslationModelProb(double translationModelProb) {
        this.translationModelProb = translationModelProb;
    }

    /**
     * @return the languageModelProb
     */
    public double getLanguageModelProb() {
        return languageModelProb;
    }

    /**
     * @param languageModelProb the languageModelProb to set
     */
    public void setLanguageModelProb(double languageModelProb) {
        this.languageModelProb = languageModelProb;
    }

    /**
     * @return the score
     */
    public double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(double score) {
        this.score = score;
    }

    public void printFeatures(PrintStream ps)
    {        
        Iterator<Integer> itr = features.getFeatureIndices();

        int i = 0;
        while(itr.hasNext())
        {
            int fIndex = itr.next();
            
            Object fValue = features.getFeatureValueFromIndex(fIndex);

            if(i < features.size() - 1)
            {
                if(fValue instanceof Double)
                {
                    ps.print((features.getFeatureNameFromIndex(i)) + "=" + fValue + ",");
                }
                else
                {
                    ps.print((features.getFeatureNameFromIndex(i)) + "=" + fValue + ",");
                }
            }
            else
            {
                if(fValue instanceof Double)
                {
                    ps.print((features.getFeatureNameFromIndex(i)) + "=" + fValue);
                }
                else
                {
                    ps.print((features.getFeatureNameFromIndex(i)) + "=" + fValue);
                }
            }                
            
            i++;
        }

//        ps.println("");
    }
}
