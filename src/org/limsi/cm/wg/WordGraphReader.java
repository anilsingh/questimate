/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.wg;

import edu.cmu.sphinx.fst.semiring.LogSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anil
 */
public class WordGraphReader {

    private FileInputStream fileInputStream;
    private DataInputStream dataInputStream;
    private BufferedReader bufferedReader;
        
    private WordGraph currentWordGraph;
    
    private Semiring semiring = new LogSemiring();
    
    private String directory;
    private String basename;
    private String charset;
    
    private int numGraph;
    private boolean first;
    private boolean fresh = true;
    private boolean start = false;

    public WordGraphReader(String dir, String basename, String cs) {        
        this();
        
        if(dir.endsWith("/") == false)
        {
            dir += "/";
        }
        
        directory = dir;
        this.basename = basename;
        charset = cs;
    }
    
    public WordGraphReader() {
        
    }

    public WordGraph readWordGraphWMT13(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        if(fresh)
        {
            semiring = sr;

            fileInputStream = new FileInputStream(graphPath);
            dataInputStream = new DataInputStream(fileInputStream);
            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream, encoding));
                
            numGraph = 1;
            
            fresh = false;
        }
        
        currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);

        first = true;
        
        String strLine = "";
        String prevId = "";
        String curId = "";
        
        while ((strLine = bufferedReader.readLine()) != null)
        {
            curId = strLine.split("[\\s]")[0];
                
            if(prevId.equals("") == false && curId.equals(prevId) == false)
            {
                System.out.println("Read lattice " + numGraph + " from file " + graphPath);

                currentWordGraph.splitOutputPhrases();

                currentWordGraph.markFinalStates();

                currentWordGraph.labelToEdgeMap();    

//                currentWordGraph.detectCycles();
                
//                wgs.add(currentWordGraph);
                numGraph++;
//                currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);
                first = true;
                
                return currentWordGraph;
            }
            else
            {            
                WordGraphEdge link = currentWordGraph.addLinkWMT13(strLine);
                
                if(link == null)
                {
                    continue;
                }

                if(first)
                {
                    currentWordGraph.startEdge = link;
                    first = false;

                    if(link == null)
                    {
                        System.err.println("@Start edge is null");
                    }                        
                    else
                    {
                        System.err.println("@Start edge is not null");
                    }                        
                }
            }
            
            prevId = curId;
        }    

        if(!fresh)
        {
            fileInputStream.close();
            dataInputStream.close();
            bufferedReader.close();
        }
        
        if(currentWordGraph.countEdges() > 0 && currentWordGraph.countNodes() > 0)
        {
            System.out.println("Read lattice " + numGraph + " from file " + graphPath);

            currentWordGraph.splitOutputPhrases();

            currentWordGraph.markFinalStates();

            currentWordGraph.labelToEdgeMap();    

            numGraph++;
        }
        else
        {
            currentWordGraph = null;
        }
        
        return currentWordGraph;
    }
    
    public WordGraph readWordGraphWMT12(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        if(fresh)
        {
            semiring = sr;

            fileInputStream = new FileInputStream(graphPath);
            dataInputStream = new DataInputStream(fileInputStream);
            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream, encoding));
                
            numGraph = 1;
            
            fresh = false;
        }
        
        currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);

        first = true;
        start = false;
        
        String strLine;
        
        while ((strLine = bufferedReader.readLine()) != null)
        {
            if(strLine.startsWith("</wgraph>"))
            {
                System.out.println("Read lattice " + numGraph + " from file " + graphPath);

                currentWordGraph.splitOutputPhrases();

                currentWordGraph.markFinalStates();

                currentWordGraph.labelToEdgeMap();    

//                currentWordGraph.detectCycles();
                
//                wgs.add(currentWordGraph);
                numGraph++;
//                currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);
                first = true;
                start = false;
                
                return currentWordGraph;
            }
            else
            {            
                if(strLine.startsWith("<wgraph>"))
                {
                    start = true;
                    continue;
                }
                else if(strLine.startsWith("VERSION=")
                        || strLine.startsWith("UTTERANCE="))
                {
                    continue;
                }

                if(start)
                {
                    WordGraphEdge link = currentWordGraph.addLinkWMT12(strLine);

                    if(first)
                    {
                        currentWordGraph.startEdge = link;
                        first = false;
                        
                        if(link == null)
                        {
                            System.err.println("@Start edge is null");
                        }                        
                        else
                        {
                            System.err.println("@Start edge is not null");
                        }                        
                    }
                }
            }
        }    
        
        currentWordGraph = null;

        if(!fresh)
        {
            fileInputStream.close();
            dataInputStream.close();
            bufferedReader.close();
        }
        
        return currentWordGraph;
    }
        
    public WordGraph readWordGraphNCODE(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        if(fresh)
        {
            semiring = sr;

            fileInputStream = new FileInputStream(graphPath);
            dataInputStream = new DataInputStream(fileInputStream);
            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream, encoding));
                
            numGraph = 1;
            
            fresh = false;
        }
        
        currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);

        first = true;
        
        String strLine;
        
        while ((strLine = bufferedReader.readLine()) != null)
        {
            if(strLine.equals("<END>"))
            {
                System.out.println("Read lattice " + numGraph + " from file " + graphPath);

                currentWordGraph.splitOutputPhrases();

                currentWordGraph.markFinalStates();

                currentWordGraph.labelToEdgeMap();    

                currentWordGraph.detectCycles();
                
//                wgs.add(currentWordGraph);
                numGraph++;
//                currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);
                first = true;
                
                return currentWordGraph;
            }
            else
            {            
                WordGraphEdge link = currentWordGraph.addLinkNCODE(strLine);

                if(first)
                {
                    currentWordGraph.startEdge = link;
                    first = false;
                }
            }
        }    
        
        currentWordGraph = null;

        if(!fresh)
        {
            fileInputStream.close();
            dataInputStream.close();
            bufferedReader.close();
        }
        
        return currentWordGraph;
    }

    public WordGraph readWordGraphFST(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        if(fresh)
        {
            semiring = sr;

//            fileInputStream = new FileInputStream(graphPath);
//            dataInputStream = new DataInputStream(fileInputStream);
//            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream, encoding));
                
            numGraph = 1;
            
            fresh = false;
        }
        
        currentWordGraph = new WordGraph(directory, basename + "_" + numGraph, charset);
        
        currentWordGraph.readFstAsWordGraph(directory + basename + "_" + numGraph, encoding, sr);
        
        numGraph++;

//        first = true;
//        
//        String strLine;
//        
//        while ((strLine = bufferedReader.readLine()) != null)
//        {
//            if(strLine.equals("<END>"))
//            {
//                System.out.println("Read lattice " + numGraph + " from file " + graphPath);
//
//                currentWordGraph.splitOutputPhrases();
//
//                currentWordGraph.markFinalStates();
//
//                currentWordGraph.labelToEdgeMap();    
//
//                currentWordGraph.detectCycles();
//                
////                wgs.add(currentWordGraph);
//                numGraph++;
////                currentWordGraph = new WordGraph(directory, basename + "-" + numGraph, charset);
//                first = true;
//                
//                return currentWordGraph;
//            }
//            else
//            {            
//                WordGraphEdge link = currentWordGraph.addLinkNCODE(strLine);
//
//                if(first)
//                {
//                    currentWordGraph.startEdge = link;
//                    first = false;
//                }
//            }
//        }    
//        
//        currentWordGraph = null;
//
//        if(!fresh)
//        {
//            fileInputStream.close();
//            dataInputStream.close();
//            bufferedReader.close();
//        }
        
        return currentWordGraph;
    }
        
    public WordGraph readFstAsWordGraph(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        semiring = sr;
        
        currentWordGraph = new WordGraph();
        
        currentWordGraph.readFstAsWordGraph(graphPath, encoding, sr);
//        currentWordGraph.readOpenFstFormat(graphPath, encoding, sr);
        
        return currentWordGraph;
    }
    
    public int wordGraphsRead()
    {
        return numGraph - 1;
    }
    
    public static void main(String[] args)
    {
        WordGraphReader wgreader = new WordGraphReader("/extra/work/fst/fst-test", "doc_147.fr.fte.utf8", "UTF-8");
        
        WordGraph wg;
        try {
            while((wg = wgreader.readWordGraphNCODE("/extra/work/fst/fst-test/doc_147.fr.fte.utf8.GRAPH",
                    "UTF-8", new LogSemiring())) != null)
            {
                System.out.println("\tNumber of nodes: " + wg.countNodes());
                System.out.println("\tNumber of edges: " + wg.countEdges());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WordGraphReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(WordGraphReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WordGraphReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
