/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.wg;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;
import edu.cmu.sphinx.fst.Arc;
import edu.cmu.sphinx.fst.Fst;
import edu.cmu.sphinx.fst.State;
import edu.cmu.sphinx.fst.openfst.Convert;
import edu.cmu.sphinx.fst.semiring.LogSemiring;
import edu.cmu.sphinx.fst.semiring.Semiring;
import edu.stanford.nlp.mt.base.IString;
import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.corpora.CEAnnotatedSentence;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.mt.cm.features.sentence.FeatureValuesImpl;
import rationals.NoSuchStateException;
import rationals.Transition;
import rationals.transductions.Transducer;
import rationals.transductions.TransducerRelation;
import sanchay.properties.PropertyTokens;

/**
 *
 * @author anil
 */
public class WordGraph<T extends CEAnnotatedSentence> {
 
    protected WordGraphEdge startEdge;
    
    protected LinkedHashMap<Integer, WordGraphNode> nodes;
    protected Table<Integer, Integer, WordGraphEdge> edges;
    
    protected LinkedHashMap<Integer, Set<WordGraphEdge>> labelToEdgeMap;
    
    protected Index<String> inputIndex;
    protected Index<String> outputIndex;
    
    protected Semiring semiring = new LogSemiring();
    
    protected String directory;
    protected String basename;
    protected String charset;
    
    public static final int finalStateEnd = Integer.MAX_VALUE;
    
    public static final String WORD_SEPERATOR_REGEX = "_\\|_";
    public static final String WORD_SEPERATOR = "_|_";
    
    private int maxStateID = Integer.MIN_VALUE;
    private boolean inputSplit = false;
    private boolean outputSplit = true;
    
    private LinkedHashMap<WordGraphNode, WordGraphEdge> finalStates;

    public WordGraph(String dir, String basename, String cs) {        
        this();
        
        if(dir.endsWith("/") == false)
        {
            dir += "/";
        }
        
        directory = dir;
        this.basename = basename;
        charset = cs;
    }

    public WordGraph() {        
        initGraph(null);
    }

    private void initGraph(T sentence)
    {
        inputIndex = new HashIndex<String>();
        outputIndex = new HashIndex<String>();

        nodes = new LinkedHashMap<Integer, WordGraphNode>();
        edges = HashBasedTable.create();
        
        labelToEdgeMap = new LinkedHashMap<Integer, Set<WordGraphEdge>>(); 
//        wordTrellis.
        
        inputIndex.add("<eps>");
        outputIndex.add("<eps>");
        
        finalStates = new LinkedHashMap<WordGraphNode, WordGraphEdge>();
    }
    
    public Iterator<WordGraphEdge> getEdgeIterator()
    {
        return edges.values().iterator();
    }
    
    public int countEdges()
    {
        return edges.size();
    }
    
    public Iterator<WordGraphNode> getNodeIterator()
    {
        return nodes.values().iterator();
    }
    
    public int countNodes()
    {
        return nodes.size();
    }

    public Transducer getTransducer() throws NoSuchStateException
    {
        Map<WordGraphNode, rationals.State> nodeMap = new LinkedHashMap<WordGraphNode, rationals.State>();

        return getTransducer(nodeMap);
    }
    
    public Transducer getTransducer(Map<WordGraphNode, rationals.State> nodeMap) throws NoSuchStateException
    {        
        Transducer transducer = new Transducer();
        
        Iterator<Integer> itr = nodes.keySet().iterator();
        
        while(itr.hasNext())
        {
            Integer nodeId = itr.next();
            
            WordGraphNode node = nodes.get(nodeId);
            
            rationals.State s = transducer.addState(inputSplit, inputSplit);
            
            nodeMap.put(node, s);
        }

        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();

        if(startEdge != null)
        {
            transducer.addTransition(new Transition(nodeMap.get(startEdge.getStartNode()),
                    new TransducerRelation(new Integer(startEdge.getInputSequenceID()), new Integer(startEdge.getOutputSequenceID())),
                    nodeMap.get(startEdge.getEndNode())));            
        }
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> edgeItr = cells.iterator();
        
        while(edgeItr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = edgeItr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            if(link == startEdge) {
                continue;
            }

            if(link.getEndNode() != null)
            {
                transducer.addTransition(new Transition(nodeMap.get(link.getStartNode()),
                        new TransducerRelation(new Integer(link.getInputSequenceID()), new Integer(link.getOutputSequenceID())),
                        nodeMap.get(link.getEndNode())));
            }
        }        
        
        return transducer;
    }

    public void filter(Transducer filter, Map<WordGraphNode, rationals.State> nodeMap)
    {
        Iterator itr = filter.delta().iterator();
        
        while(itr.hasNext())
        {
            Transition transition = (Transition) itr.next();

            rationals.State os = transition.start();
            rationals.State oe = transition.end();
            
            
        }
    }
    
    public void minimize() throws NoSuchStateException
    {
        Map<WordGraphNode, rationals.State> nodeMap = new LinkedHashMap<WordGraphNode, rationals.State>();

        Transducer transducer = getTransducer(nodeMap);
        
        filter(transducer, nodeMap);
    }

    public WordGraphEdge addLinkWMT13(String linkData)
    {
        String fields[] = linkData.split("\\s+");

        WordGraphEdge<IString, String> link = new WordGraphEdge<IString, String>();
        WordGraphNode<IString, String> start = null;
        WordGraphNode<IString, String> end = null;
        
        for (int i = 0; i < fields.length; i++)
        {    
//            if(fields[i].startsWith("hyp="))
//            {
//                if(fields[i].equals("hyp=0"))
//                {
//                    start = new WordGraphNode<IString, String>();        
//                    
//                    start.setNodeID(0);
//                    
//                    link.setStartNode(start);
//                    
//                    nodes.put(0, start);
//                }
//            }
            if(fields[i].startsWith("back="))
            {
                String parts[] = fields[i].split("=");

                int nodeId = Integer.parseInt(parts[1]);
                
                WordGraphNode<IString, String> node = nodes.get(nodeId);
                
                if(node == null)
                {
                    start = new WordGraphNode<IString, String>();        
                    
                    start.setNodeID(nodeId);
                    
                    nodes.put(nodeId, start);
                }
                else
                {
                    start = node;
                }
                                                
                link.setStartNode(start);
                
                finalStates.remove(start);
            }
            else if(fields[i].startsWith("hyp=") && linkData.contains("recombined") == false)
            {
                if(fields[i].equals("hyp=0"))
                {
                    return null;
                }
                
                String parts[] = fields[i].split("=");

                int nodeId = Integer.parseInt(parts[1]);
                
                WordGraphNode<IString, String> node = nodes.get(nodeId);
                
                if(node == null)
                {
                    end = new WordGraphNode<IString, String>();        
                    
                    end.setNodeID(nodeId);
                    
                    nodes.put(nodeId, end);
                }
                else
                {
                    end = node;
                }
                
                link.setEndNode(end);

                finalStates.put(end, link);
            }
            else if(fields[i].startsWith("recombined="))
            {
                return null;
//                String parts[] = fields[i].split("=");
//
//                int nodeId = Integer.parseInt(parts[1]);
//                
//                WordGraphNode<IString, String> node = nodes.get(nodeId);
//                
//                if(node == null)
//                {
//                    end = new WordGraphNode<IString, String>();        
//                    
//                    end.setNodeID(nodeId);
//                    
//                    nodes.put(nodeId, end);
//                }
//                else
//                {
//                    end = node;
//                }
//                
//                link.setEndNode(end);
//
//                finalStates.put(end, link);
            }
            else if(fields[i].startsWith("transition="))
            {
                String parts[] = fields[i].split("=");
                
                double score = semiring.one();
 
                try {
                    score = Double.parseDouble(parts[1]);
                } catch(NumberFormatException ne)
                {
                    System.err.println("NumberFormatException " + ne.getMessage());
                    System.err.println(linkData);
                }

                link.setScore(score);
            }
            else if(fields[i].startsWith("out="))
            {
                String parts[] = fields[i].split("=");
                
                String input = parts[1];
                String output = parts[1];

                int wrdIndex = inputIndex.indexOf(input.replaceAll(" ", WORD_SEPERATOR), true);

                link.setInputSequenceID(wrdIndex);

                wrdIndex = outputIndex.indexOf(output.replaceAll(" ", WORD_SEPERATOR), true);

                link.setOutputSequenceID(wrdIndex);                        
            }
            
            if(i == fields.length - 1)
            {
                if(end != null && end.getNodeID() != -1)
                {
                    edges.put(start.getNodeID(), end.getNodeID(), link);
                }
                else
                {
//                    link.setEndNode(null);
                    edges.put(start.getNodeID(), finalStateEnd, link);

//                    finalStates.put(end, link);
                }
                                
                start = null;
                end = null;                
            }
        }
        
        return link;
    }
        
    public WordGraphEdge addLinkWMT12(String linkData)
    {
        String fields[] = linkData.split("\t");

        WordGraphEdge<IString, String> link = null;
        WordGraphNode<IString, String> start = null;
        WordGraphNode<IString, String> end = null;
        
        FeatureValues featureValues = null;
        
        for (int i = 0; i < fields.length; i++)
        {    
            if(fields[i].startsWith("J="))
            {
                String parts[] = fields[i].split("=");
                
                link = new WordGraphEdge<IString, String>();
                
                int linkId = Integer.parseInt(parts[1]);
                
//                System.err.println("Adding link: " + linkId);
                
                featureValues = new FeatureValuesImpl();
        
                if(fields[i].startsWith("J=0"))
                {
                    startEdge = link;
                }
            }
            else if(fields[i].startsWith("S="))
            {
                String parts[] = fields[i].split("=");

                int nodeId = Integer.parseInt(parts[1]);
                
                WordGraphNode<IString, String> node = nodes.get(nodeId);
                
                if(node == null)
                {
                    start = new WordGraphNode<IString, String>();        
                    
                    start.setNodeID(nodeId);
                    
                    nodes.put(nodeId, start);
                }
                else
                {
                    start = node;
                }
                
                link.setStartNode(start);
                
                finalStates.remove(start);
            }
            else if(fields[i].startsWith("E="))
            {
                String parts[] = fields[i].split("=");

                int nodeId = Integer.parseInt(parts[1]);
                
                WordGraphNode<IString, String> node = nodes.get(nodeId);
                
                if(node == null)
                {
                    end = new WordGraphNode<IString, String>();
                    
                    end.setNodeID(nodeId);

                    nodes.put(nodeId, end);
                }
                else
                {
                    end = node;
                }

                link.setEndNode(end);
                
                edges.put(start.getNodeID(), end.getNodeID(), link);

                finalStates.put(end, link);
            }
            else if(fields[i].startsWith("a=") || fields[i].startsWith("l=")
                    || fields[i].startsWith("r="))
            {
                String parts[] = fields[i].split("=");
                
                String iparts[] = parts[1].split(",\\s+");
                
                for (int j = 0; j < iparts.length; j++)
                {
                    String featureName = parts[0] + "_" + (j + 1);
                    
//                    featureValues.addFeature(featureName, Double.parseDouble(iparts[j]));
                }
            }
            else if(fields[i].startsWith("w="))
            {
                fields[i] = fields[i].trim().substring(2);
                
                int ind = fields[i].lastIndexOf(", c=");
                
                String cStr = fields[i].substring(ind + 4);
                
                double c = semiring.one();
 
                try {
                    c = Double.parseDouble(cStr);
                } catch(NumberFormatException ne)
                {
                    System.err.println("NumberFormatException " + ne.getMessage());
                    System.err.println(linkData);
                }
                
                fields[i] = fields[i].replaceAll(", c=" + cStr, "");
                
                ind = fields[i].lastIndexOf(" : pC=");
                
                String pCStr = fields[i].substring(ind + 6);
                
                double pC = semiring.one();
 
                try {
                    pC = Double.parseDouble(pCStr);
                } catch(NumberFormatException ne)
                {
                    System.err.println("NumberFormatException " + ne.getMessage());
                    System.err.println(linkData);
                }

                link.setScore(pC);

                fields[i] = fields[i].replaceAll(" : pC=" + pCStr, "");

                ind = fields[i].lastIndexOf(" :");
                
                String aStr = fields[i].substring(ind + 2);

                fields[i] = fields[i].replaceAll(" :" + aStr, "");

                ind = fields[i].indexOf('|');
                
                String input = fields[i].substring(0, ind);
                String output = fields[i].substring(ind + 1);

                int wrdIndex = inputIndex.indexOf(input.replaceAll(" ", WORD_SEPERATOR), true);

                link.setInputSequenceID(wrdIndex);

                wrdIndex = outputIndex.indexOf(output.replaceAll(" ", WORD_SEPERATOR), true);

                link.setOutputSequenceID(wrdIndex);
                        
//                String  parts[] = fields[i].split(" :");
//                
//                parts[0] = parts[0].trim().substring(2);
//                
//                String io[] = parts[0].split("\\|");
//                
//                int wrdIndex = inputIndex.indexOf(io[0].replaceAll(" ", WORD_SEPERATOR), true);
//
//                link.setInputSequenceID(wrdIndex);
//                
//                if(io.length == 1)
//                {
//                    System.err.println("Output value not found: " + parts[0]);
//                    System.err.println(linkData);
//                    System.err.println(fields[i]);
//                }
//
//                wrdIndex = outputIndex.indexOf(io[1].replaceAll(" ", WORD_SEPERATOR), true);
//
//                link.setOutputSequenceID(wrdIndex);
//                
//                String iparts[] = parts[2].split(",");
//                
//                iparts[0] = iparts[0].trim().substring(3);
//                
//                try {
//                    link.setScore(Double.parseDouble(iparts[0]));
//                } catch(NumberFormatException ne)
//                {
//                    System.err.println("NumberFormatException " + ne.getMessage());
//                    System.err.println(linkData);
//                    System.err.println(parts[2]);                    
//                    System.err.println(iparts[0]);                    
//                }
            }
//            else if(fields[i].startsWith("w="))
//            {
//                String parts[] = fields[i].split("=");
//            }
//            else if(fields[i].startsWith(":"))
//            {
//                String parts[] = fields[i].split(":");
//            }
            
            if(i == fields.length - 1)
            {
                link.setFeatures(featureValues);

//                link = null;
                start = null;
                end = null;                
                
                featureValues = null;
            }
        }
        
        return link;
    }
    
    void markFinalStates()
    {
        Iterator<WordGraphNode> itr = finalStates.keySet().iterator();
        
        while(itr.hasNext())
        {
            WordGraphNode fs = itr.next();
            
            WordGraphEdge pe = finalStates.get(fs);
            
            WordGraphEdge fe = new WordGraphEdge();
            
            fe.setStartNode(fs);
            fe.setEndNode(null);
            
            fe.setScore(pe.getScore());

            edges.put(fs.getNodeID(), finalStateEnd, fe);
        }
    }
    
    public WordGraphEdge addArc(int fromState, int toState, String input,
            String output, FeatureValues weights, double score)
    {
        WordGraphNode start = null;
        WordGraphNode end = null;
        WordGraphEdge link = new WordGraphEdge();
        
        if(nodes.get(fromState) != null)
        {
            start = nodes.get(fromState);
        }
        else
        {
            start = new WordGraphNode<IString, String>();

            start.setNodeID(fromState);
            
            nodes.put(fromState, start);
        }
        
        link.setStartNode(start);

        // Not final state
        if(toState != -1)
        {
            if(nodes.get(toState) != null)
            {
                end = nodes.get(toState);
            }
            else
            {
                end = new WordGraphNode<IString, String>();

                end.setNodeID(toState);
            
                nodes.put(toState, end);
            }
            
            link.setFeatures(weights);
            
            link.setScore(score);
            
            if(inputIndex == outputIndex)
            {
                if(input == null)
                {
                    input = output;
                }
                else if(output == null)
                {
                    output = input;
                }
            }
            
            int wrdIndex = inputIndex.indexOf(input, true);

            link.setInputSequenceID(wrdIndex);

            wrdIndex = outputIndex.indexOf(output, true);

            link.setOutputSequenceID(wrdIndex);

            link.setEndNode(end);
        }

        if(end != null)
        {
            edges.put(start.getNodeID(), end.getNodeID(), link);
        }
        else
        {
            link.setEndNode(null);
            edges.put(start.getNodeID(), finalStateEnd, link);
        }

        return link;
    }
    
    public WordGraphEdge addLinkNCODE(String linkData)
    {
        String fields[] = linkData.split("\t");
                
        int fromState = Integer.parseInt(fields[0]);
        
        WordGraphEdge link;
        
        if(fields.length > 1)
        {
            int toState = Integer.parseInt(fields[1]);

            double score = Double.parseDouble(fields[3]);

            fields = fields[2].split("\\|\\|\\|");

            String input = fields[0];
            String output = fields[1];

            fields = fields[2].split(",");

            FeatureValues featureValues = new FeatureValuesImpl();

            for (int i = 0; i < fields.length; i++)
            {
                String fv[] = fields[i].split("=");

                featureValues.addFeature(fv[0], fv[1]);
            }
                
            link = addArc(fromState, toState, input, output, featureValues, score);
        }
        else
        {
            link = addArc(fromState, -1, null, null, null, semiring.one());            
            finalStates.put(link.getStartNode(), link);
        }
        
        return link;
    }
    
    public WordGraphEdge addLinkFst(String linkData, Semiring sr, PropertyTokens isymsPT, PropertyTokens osymsPT)
    {  
        String fields[] = linkData.split("\\s+");
                
        int fromState = Integer.parseInt(fields[0]);
        
        double score = sr.zero();
        
        WordGraphEdge link = null;

        if(fields.length  == 1)
        {
            link = addArc(fromState, -1, null, null, null, sr.one());                        

            return link;
        }
        else if(fields.length  == 2)
        {
            score = Double.parseDouble(fields[1]);
            link = addArc(fromState, -1, null, null, null, score);                        

            return link;
        }
        
        if(fields.length > 2)
        {
            int toState = Integer.parseInt(fields[1]);

//            int ilabel = Integer.parseInt(fields[2]);
//            int olabel = Integer.parseInt(fields[3]);
//
            String input = null;
            String output = null;
            
            if(isymsPT == null)
            {
                output = fields[2];

                if(fields.length == 3)
                {
                    score = sr.one();                    
                }
                else
                {
                    score = Double.parseDouble(fields[3]);
                }
            }
            else if(osymsPT == null)
            {
                input = fields[2];

                if(fields.length == 3)
                {
                    score = sr.one();                    
                }
                else
                {
                    score = Double.parseDouble(fields[3]);
                }
            }
            else
            {
                input = fields[2];
                output = fields[3];

                if(fields.length == 4)
                {
                    score = sr.one();                    
                }
                else
                {
                    score = Double.parseDouble(fields[4]);
                }
            }

//            FeatureValues featureValues = new FeatureValuesImpl();
//
//            for (int i = 0; i < fields.length; i++)
//            {
//                String fv[] = fields[i].split("=");
//
//                featureValues.addFeature(fv[0], fv[1]);
//            }
                
            link = addArc(fromState, toState, input, output, null, score);
        }
//        else
//        {
//            addArc(fromState, -1, null, null, null, semiring.one());            
//        }
        
        return link;
    }
    
    public State addArc(String linkData, HashMap<Integer, State> stateMap,
            Fst fst, Semiring sr, Index isyms, Index osyms, boolean first)
    {
        if(linkData.equals("<END>"))
            return null;
 
        Arc arc = null;
        
        State prevState = null;
        State nextState = null;
        
        String fields[] = linkData.split("\t");
                
        int fromState = Integer.parseInt(fields[0]);

        prevState = stateMap.get(fromState);
        if (prevState == null) {
            prevState = new State(semiring.zero());
            fst.addState(prevState);
            stateMap.put(fromState, prevState);
        }
        
        if(first)
        {
            fst.setStart(prevState);
        }
        
        if(fields.length > 1)
        {                    
            int toState = Integer.parseInt(fields[1]);

            nextState = stateMap.get(toState);
            if (nextState == null) {
                nextState = new State(semiring.zero());
                fst.addState(nextState);
                stateMap.put(toState, nextState);
            }

            float score = Float.parseFloat(fields[3]);

            fields = fields[2].split("\\|\\|\\|");

            String input = fields[0];
            String output = fields[1];

            fields = fields[2].split(",");

//            FeatureValues featureValues = new FeatureValuesImpl();
//
//            for (int i = 0; i < fields.length; i++)
//            {
//                String fv[] = fields[i].split("=");
//
//                featureValues.addFeature(fv[0], fv[1]);
//            }
            
            int ilabel = isyms.indexOf(input, true);
            int olabel = osyms.indexOf(output, true);
            
            arc = new Arc(ilabel, olabel, score, nextState);

            prevState.addArc(arc);
        }
        else
        {
            prevState.setFinalWeight(sr.zero());
            
//            if(prevState.getNumArcs() > 0)
//            {
//                prevState.deleteArc(0);
//            }
        }
        
        return prevState;
    }
    
    public void readWordGraph(String graphData)
    {
        String linkLines[] = graphData.split("\n");
        
        boolean first = true;
        
        for (int i = 0; i < linkLines.length; i++)
        {
            WordGraphEdge link = addLinkNCODE(linkLines[i]);
            
            if(first)
            {
                startEdge = link;
                first = false;
            }
//            addLinkWMT12(linkLines[i]);
            if(link == null)
            {
                break;
            }
        }
        
//        splitOutputPhrases();
    
        labelToEdgeMap();    
    }
    
    public void readWordGraphWMT12(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        semiring = sr;
        
        FileInputStream fstream = new FileInputStream(graphPath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, encoding));
        
        String strLine;
        boolean start = false;
        boolean first = true;
        
        while ((strLine = br.readLine()) != null && !strLine.startsWith("</wgraph>"))
        {
            if(strLine.startsWith("<wgraph>"))
            {
                start = true;
                continue;
            }
            else if(strLine.startsWith("VERSION=")
                    || strLine.startsWith("UTTERANCE="))
            {
                continue;
            }
            
            if(start)
            {
                WordGraphEdge link = addLinkWMT12(strLine);
                
                if(first)
                {
                    startEdge = link;
                    first = false;
                }
            }
        }        
        
        markFinalStates();
        
        splitOutputPhrases();
    
        labelToEdgeMap();    
        
        fstream.close();
        in.close();
        br.close();
        
//        readWordGraphNCODE(strLines);
    }
    
    public ArrayList<WordGraph> readWordGraphNCODE(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        ArrayList<WordGraph> wgs = new ArrayList<WordGraph>();
        
        semiring = sr;
        
        FileInputStream fstream = new FileInputStream(graphPath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, encoding));
        
        String strLine;
        
        WordGraph wg = this;
                
        int i = 0;
        boolean first = true;
        
        while ((strLine = br.readLine()) != null)
        {
            if(strLine.equals("<END>"))
            {
                System.out.println("Read lattice " + (i + 1) + " from file " + graphPath);

                wg.splitOutputPhrases();

                wg.markFinalStates();

                wg.labelToEdgeMap();    

                wg.detectCycles();
                
                wgs.add(wg);
                i++;
                wg = new WordGraph(directory, basename + "-" + i, charset);
                first = true;
            }
            else
            {            
                WordGraphEdge link = wg.addLinkNCODE(strLine);

                if(first)
                {
                    wg.startEdge = link;
                    first = false;
                }
            }
        }        
                 
        fstream.close();
        in.close();
        br.close();
        
        return wgs;
    }

    public ArrayList<WordGraph> readFstAsWordGraph(String graphPath, String encoding, Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {        
        ArrayList<WordGraph> wgs = new ArrayList<WordGraph>();
        
        WordGraph wg = this;

        wg.semiring = sr;
        
        PropertyTokens isymsPT = null;
        
        if(MiscellaneousUtils.fileExists(graphPath + ".input.syms"))
        {
            isymsPT = new PropertyTokens(graphPath + ".input.syms", encoding);
        }
        else
        {
            wg.inputIndex = wg.outputIndex;
        }
        
        PropertyTokens osymsPT = null;
        
        if(MiscellaneousUtils.fileExists(graphPath + ".output.syms"))
        {
            osymsPT = new PropertyTokens(graphPath + ".output.syms", encoding);            
        }
        else
        {
            wg.outputIndex = wg.inputIndex;
        }
        
        FileInputStream fstream = new FileInputStream(graphPath + ".fst.txt");
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, encoding));
        
        String strLine;
        boolean first = true;
        
        while ((strLine = br.readLine()) != null)
        {
            WordGraphEdge link = wg.addLinkFst(strLine, sr, isymsPT, osymsPT);
            
            if(first)
            {
                wg.startEdge = link;
                first = false;
            }
        }        
        
        wg.splitOutputPhrases();
    
        wg.labelToEdgeMap();    
        
        markFinalStates();
        
        fstream.close();
        in.close();
        br.close();
        
        wgs.add(wg);
                
        return wgs;
    }
    
    public Fst readWordGraphAsFst(String graphPath, String encoding,
            Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        HashMap<Integer, State> stateMap = new HashMap<Integer, State>();

        Fst fst = new Fst(sr);
//        
//        State prevState = new State(1);
//        
//        fst.addState(prevState);
//        
//        fst.setStart(prevState);
//        
//        stateMap.put(prevState.getId(), prevState);
        
        Index<String> isyms = new HashIndex<String>();
        Index<String> osyms = new HashIndex<String>();
        
        isyms.add("<eps>");
        osyms.add("<eps>");
        
        FileInputStream fstream = new FileInputStream(graphPath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, encoding));
        
        String strLine;
        boolean firstLine = true;
        State s = null;
        
        while ((strLine = br.readLine()) != null && !strLine.equals("<END>"))
        {
            if(firstLine)
            {
                s = addArc(strLine, stateMap, fst, sr, isyms, osyms, true);
                firstLine = false;
            }
            else
            {
                s = addArc(strLine, stateMap, fst, sr, isyms, osyms, false);                
            }
            
            if(s == null)
            {
                break;
            }
        }        
        
        String[] is = new String[isyms.size()];        
        isyms.toArray(is);
        
        fst.setIsyms(is);
        
        String[] os = new String[osyms.size()];        
        osyms.toArray(os);
        
        fst.setOsyms(os);
                
        fstream.close();
        in.close();
        br.close();
        
        return fst;
    }

    public int addLinkOpenFst(String linkData)
    {
        String fields[] = linkData.split("\\s+");

        WordGraphEdge link = null;
        boolean first = true;
        
        int fromState = Integer.parseInt(fields[0]);
        
        if(fields.length > 2)
        {
            int toState = Integer.parseInt(fields[1]);

            String input = fields[2];
            String output = fields[3];

            double score = semiring.one();
            
            if(fields.length > 4)
            {
                score = Double.parseDouble(fields[4]);
            }

            FeatureValues featureValues = new FeatureValuesImpl();
//
//            for (int i = 0; i < fields.length; i++)
//            {
//                String fv[] = fields[i].split("=");
//
//                featureValues.addFeature(fv[0], fv[1]);
//            }
                
            link = addArc(fromState, toState, input, output, featureValues, score);
            
            if(first)
            {
                startEdge = link;
                first = false;
            }
        }
        else
        {
            double score = semiring.one();

            if(fields.length > 1)
            {
                score = Double.parseDouble(fields[1]);
            }

            link = addArc(fromState, -1, null, null, null, score);            
        }
        
        return 0;
    }
    
    public void readOpenFstFormat(String graphPath, String encoding,
            Semiring sr) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        semiring = sr;
        
        FileInputStream fstream = new FileInputStream(graphPath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, encoding));
        
        String strLine;

        WordGraphEdge link = null;
        boolean first = true;
        
        while ((strLine = br.readLine()) != null)
        {
            addLinkOpenFst(strLine);
            
            if(first)
            {
                startEdge = link;
                first = false;
            }            
        }        
        
        splitOutputPhrases();
        
        markFinalStates();
    
        labelToEdgeMap();    
        
        fstream.close();
        in.close();
        br.close();        
    }
    
    public static Fst wordGraph2Fst(WordGraph wg, String basename, String encoding, Semiring semiring) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        wg.saveAllFst(basename, encoding);
        
        Fst fst = Convert.importFst(basename, semiring);

        return fst;
    }
    
    void labelToEdgeMap()
    {
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            Set<WordGraphEdge> set = labelToEdgeMap.get(link.getInputSequenceID());
            
            if(set == null)
            {
                set = new LinkedHashSet<WordGraphEdge>();
                labelToEdgeMap.put(link.getInputSequenceID(), set);
            }

            set.add(link);
        }
    }
    
    private void calculateMaxStateID()
    {
        maxStateID = Integer.MIN_VALUE;
        
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            if(link.getStartNode() == null) 
            {
                int i = 1;
            }

            int id = link.getStartNode().getNodeID();
            
            if(id > maxStateID)
            {
                maxStateID = id;
            }

            if(link.getEndNode() != null)
            {
                id = link.getEndNode().getNodeID();

                if(id > maxStateID)
                {
                    maxStateID = id;
                }
            }
        }
    }
    
    // Only for splitting output phrases into words
    void splitOutputPhrases()
    {
        calculateMaxStateID();
        
        System.out.println("maxStateId before splitting phrases: " + maxStateID);
        
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();

        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cellsCopy = new LinkedHashSet<Cell<Integer, Integer, WordGraphEdge>>();
        
        cellsCopy.addAll(cells);
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cellsCopy.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();

            int fromState = link.getStartNode().getNodeID();
            
            int toState = finalStateEnd;
            
            if(link.getEndNode() != null)
            {
                toState = link.getEndNode().getNodeID();
            }
                        
            String input = inputIndex.get(link.getInputSequenceID());
            String output = outputIndex.get(link.getOutputSequenceID());
            
            String words[] = output.split(WORD_SEPERATOR_REGEX);
          
            if(words.length > 1)
            {
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                                        
                    if(i == 0)
                    {
                        addArc(fromState, ++maxStateID, "<0>", word, link.getFeatures(), semiring.one());            
                    }
                    else if(i == words.length - 1)
                    {
                        addArc(maxStateID, toState, input, word, link.getFeatures(), link.getScore());                                    
                    }
                    else
                    {
                        addArc(maxStateID, ++maxStateID, "<0>", word, link.getFeatures(), semiring.one());                                                            
                    }
                }
                
                edges.remove(fromState, toState);
            }
        }        
        
        reallocateOutputIndex();
    }
    
    // Leave out the phrases that have been split
    private void reallocateOutputIndex()
    {
        Index<String> oindex = new HashIndex<String>();
        
        oindex.add("<eps>");
        
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            int id = link.getOutputSequenceID();

            if(id != -1)
            {
                String output = outputIndex.get(id);
                
                int newID = oindex.indexOf(output, true);
                
                link.setOutputSequenceID(newID);
            }
        }        
        
        outputIndex = oindex;
    }
    
    public void save(String path, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, cs);
        
        print(ps);
        
        ps.close();
    }
    
    public void print(PrintStream ps)
    {
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            printLinkNCODE(ps, link);
        }
    }

    protected void printLinkWMT12(PrintStream ps, WordGraphEdge<IString, String> link, int linkId)
    {
        ps.print("J=" + linkId + "\t");
        ps.print("S=" + link.getStartNode().getNodeID() + "\t");
        ps.print("E=" + link.getEndNode().getNodeID() + "\t");
        ps.println("");
    }
    
    public Set<Integer> getSucceedingLabels(String label)
    {
        int labelIndex = inputIndex.indexOf(label);

        Set<WordGraphEdge> ceset = labelToEdgeMap.get(labelIndex);
        
        Set<Integer> slset = new LinkedHashSet<Integer>();
        
        Iterator<WordGraphEdge> itr = ceset.iterator();

        while(itr.hasNext())
        {
            WordGraphEdge e = itr.next();
            
            if(e.getInputSequenceID() == labelIndex)
            {
                slset.add(e.getOutputSequenceID());
            }
        }        
        
        return slset;
    }
    
    public boolean isSucceededBy(String label, String byLabel)
    {
        Set<Integer> slset = getSucceedingLabels(label);
        
        if(slset.contains(inputIndex.indexOf(byLabel))) {
            return true;
        }
        
        return false;
    }
    
    public boolean containsNGram(String[] ngrams)
    {
        return true;
        
//        for (int i = 0; i < ngrams.length - 1; i++) {
//            
//            if(isSucceededBy(ngrams[i], ngrams[i + 1]) == false)
//            {
//                return false;
//            }
//        }
//        
//        return true;
    }

    public void detectCycles()
    {
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            if(link == null || link.getEndNode() == null || link.getStartNode() == null)
            {
                continue;
            }
            
            WordGraphEdge<IString, String> revLink = edges.get(link.getEndNode().getNodeID(), link.getStartNode().getNodeID());
            
            if(revLink != null)
            {
                System.out.println("A cycle detected between states: " + link.getStartNode().getNodeID()
                        + " and " + link.getEndNode().getNodeID() + ", labeled"
                        + inputIndex.get(link.getInputSequenceID()) + ":" + outputIndex.get(link.getOutputSequenceID()));
                
                printLinkNCODE(System.out, link);
                printLinkNCODE(System.out, revLink);
            }
        }
    }

    protected void printLinkNCODE(PrintStream ps, WordGraphEdge<IString, String> link)
    {
        if(link.getEndNode() == null)
        {
            ps.println(link.getStartNode().getNodeID());            
        }
        else
        {
            ps.print(link.getStartNode().getNodeID() + "\t");
            ps.print(link.getEndNode().getNodeID() + "\t");
            ps.print(inputIndex.get(link.getInputSequenceID()) + "|||");
            ps.print(outputIndex.get(link.getOutputSequenceID()) + "|||");
            link.printFeatures(ps);
            ps.print("\t" + link.getScore());
            ps.println("");
        }
    }
    
    public void saveInputSymbols(String path, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, cs);
        
        printInputSymbols(ps);
        
        ps.close();
    }
    
    public void printInputSymbols(PrintStream ps)
    {
        Iterator<String> itr = inputIndex.iterator();
        
        while(itr.hasNext())
        {
            String sym = itr.next();

            if(!(sym.contains(WORD_SEPERATOR) && inputSplit))
            {
                ps.println(sym + "\t" + inputIndex.indexOf(sym));
            }
        }        
    }
    
    public void saveOutputSymbols(String path, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, cs);
        
        printOutputSymbols(ps);
        
        ps.close();
    }
    
    public void printOutputSymbols(PrintStream ps)
    {
        Iterator<String> itr = outputIndex.iterator();
        
        while(itr.hasNext())
        {
            String sym = itr.next();
            
            if(!sym.contains(WORD_SEPERATOR) || !outputSplit)
            {
               ps.println(sym + "\t" + outputIndex.indexOf(sym));
            }
        }        
    }
    
    public void saveStateSymbols(String path, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, cs);
        
        printStateSymbols(ps);
        
        ps.close();
    }
    
    public void printStateSymbols(PrintStream ps)
    {
        Iterator<Integer> itr = nodes.keySet().iterator();
        
        while(itr.hasNext())
        {
            int nodeID = itr.next();
            
            ps.println(nodeID + "\t" + nodeID);
        }        
    }
    
    public void saveFST(String path, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, cs);
        
        printFST(ps);
        
        ps.close();
    }
    
    public void printFST(PrintStream ps)
    {
        Set<Table.Cell<Integer,Integer, WordGraphEdge>> cells = edges.cellSet();

        if(startEdge != null)
        {
            ps.print(startEdge.getStartNode().getNodeID()
                    + "\t" + startEdge.getEndNode().getNodeID() + "\t");

            ps.println(inputIndex.get(startEdge.getInputSequenceID())
                    + "\t" + outputIndex.get(startEdge.getOutputSequenceID())
                    + "\t" + startEdge.getScore());
        }
        else
        {
            System.err.println("Start edge is null");
        }
        
        Iterator<Cell<Integer,Integer, WordGraphEdge>> itr = cells.iterator();
        
        while(itr.hasNext())
        {
            Cell<Integer,Integer, WordGraphEdge> cell = itr.next();
            
            WordGraphEdge<IString, String> link = cell.getValue();
            
            if(link == startEdge || link.getStartNode().getNodeID() == -1) {
                continue;
            }

            if(link.getEndNode() == null || link.getEndNode().getNodeID() == -1)
            {
                ps.println(link.getStartNode().getNodeID()
                        + "\t" + link.getScore());            
            }
            else
            {            
                ps.print(link.getStartNode().getNodeID()
                        + "\t" + link.getEndNode().getNodeID() + "\t");

                ps.println(inputIndex.get(link.getInputSequenceID())
                        + "\t" + outputIndex.get(link.getOutputSequenceID())
                        + "\t" + link.getScore());
            }
        }        
    }
    
    protected void addBigram(String ug1, String ug2)
    {
        int i1 = inputIndex.indexOf(ug1, true);
        int i2 = inputIndex.indexOf(ug2, true);
            
        if(i1 == 0 || i2 == 0)
        {
            return;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());

        String ng = ug1 + WORD_SEPERATOR + ug2;

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = ng;

        addArc(fromState, toState, input, output, null, semiring.one());

        addArc(toState, -1, null, null, null, semiring.one());
    }
    
    protected void addTrigram(String ug1, String ug2, String ug3, Index memIndex)
    {
        int i1 = inputIndex.indexOf(ug1, true);
        int i2 = inputIndex.indexOf(ug2, true);
        int i3 = inputIndex.indexOf(ug3, true);
            
        if(i1 == 0 || i2 == 0 || i3 == 0)
        {
            return;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());
        
        i2 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2, true);

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());

        String ng = ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3;

        i3 = memIndex.indexOf(ug2 + WORD_SEPERATOR + ug3, true);

        fromState = toState;
        toState = i3;
        
        input = ug3;
        output = ng;

        addArc(fromState, toState, input, output, null, semiring.one());

        addArc(toState, -1, null, null, null, semiring.one());
    }
    
    protected void addFourGram(String ug1, String ug2, String ug3, String ug4, Index memIndex)
    {
        int i1 = inputIndex.indexOf(ug1, true);
        int i2 = inputIndex.indexOf(ug2, true);
        int i3 = inputIndex.indexOf(ug3, true);
        int i4 = inputIndex.indexOf(ug4, true);
            
        if(i1 == 0 || i2 == 0 || i3 == 0 || i4 == 0)
        {
            return;
        }
        
        int fromState = 0;
        int toState = i1;
        
        String input = ug1;
        String output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());
        
        i2 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2, true);

        fromState = toState;
        toState = i2;
        
        input = ug2;
        output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());
        
        i3 = memIndex.indexOf(ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3, true);

        fromState = toState;
        toState = i3;
        
        input = ug3;
        output = "<eps>";

        addArc(fromState, toState, input, output, null, semiring.one());
        
        i4 = memIndex.indexOf(ug2 + WORD_SEPERATOR + ug3 + WORD_SEPERATOR + ug4, true);

        String ng = ug1 + WORD_SEPERATOR + ug2 + WORD_SEPERATOR + ug3 + WORD_SEPERATOR + ug4;

        fromState = toState;
        toState = i4;
        
        input = ug4;
        output = ng;

        addArc(fromState, toState, input, output, null, semiring.one());

        addArc(toState, -1, null, null, null, semiring.one());
    }
        
    public WordGraph generateBigramGraph()
    {
        WordGraph wg = new WordGraph();
        wg.inputSplit = true;
        wg.outputSplit = false;
        wg.inputIndex = outputIndex;

        List<String> words = outputIndex.objectsList();
        
        Iterator<String> itr1 = words.iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();            
            
            if(ug1.contains(WORD_SEPERATOR))
            {
                continue;
            }
                        
            Iterator<String> itr2 = words.iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();
            
                if(ug2.contains(WORD_SEPERATOR))
                {
                    continue;
                }

                wg.addBigram(ug1, ug2);
            }
        }
        
        return wg;
    }
            
    public WordGraph generateTrigramGraph(Index<String> bigramSet) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        WordGraph wg = new WordGraph();

        wg.inputSplit = true;
        wg.outputSplit = false;
        wg.inputIndex = outputIndex;
        
        Index<String> memIndex = new HashIndex<String>();
        
        MiscellaneousUtils.copyIndex(outputIndex, memIndex);

//        Fst fst = wordGraph2Fst(this, directory + basename, charset);
//        
//        WordGraph bgraph = generateBigramGraph();
//        
//        Fst bgFst = wordGraph2Fst(bgraph, directory + basename + ".bg", charset);
//        
//        Fst bgComposedFst = Compose.compose(fst, bgFst, semiring, true);
//        
//        Project.apply(bgComposedFst, ProjectType.OUTPUT);
//        
//        String[] bigrams = bgComposedFst.getOsyms();
//        
//        Set<String> bigramSet = MiscellaneousUtils.getSet(bigrams);
        
        List<String> words = outputIndex.objectsList();
        
        Iterator<String> itr1 = words.iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();
                                    
            Iterator<String> itr2 = words.iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();
                
                if(bigramSet.contains(ug1 + WORD_SEPERATOR + ug2) == false)
                {
                    continue;
                }

                Iterator<String> itr3 = words.iterator();

                while(itr3.hasNext())
                {
                    String ug3 = itr3.next();

                    wg.addTrigram(ug1, ug2, ug3, memIndex);
                }
            }
        }
        
        return wg;
    }
            
    public WordGraph generateFourGramGraph(Index<String> trigramSet) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
//        Fst fst = wordGraph2Fst(this, directory + basename, charset);
//        
//        WordGraph tgraph = generateTrigramGraph();
//        
//        Fst tgFst = wordGraph2Fst(tgraph, directory + basename + ".tg", charset);
//        
//        Fst tgComposedFst = Compose.compose(fst, tgFst, semiring, true);
//        
//        Project.apply(tgComposedFst, ProjectType.OUTPUT);
//        
//        String[] trigrams = tgComposedFst.getOsyms();
//        
//        Set<String> trigramSet = MiscellaneousUtils.getSet(trigrams);

        WordGraph wg = new WordGraph();

        wg.inputSplit = true;
        wg.outputSplit = false;
        wg.inputIndex = outputIndex;
        
        Index<String> memIndex = new HashIndex<String>();
        
        MiscellaneousUtils.copyIndex(outputIndex, memIndex);

        List<String> words = outputIndex.objectsList();
        
        Iterator<String> itr1 = words.iterator();
        
        while(itr1.hasNext())
        {
            String ug1 = itr1.next();
                                    
            Iterator<String> itr2 = words.iterator();
        
            while(itr2.hasNext())
            {
                String ug2 = itr2.next();

                Iterator<String> itr3 = words.iterator();

                while(itr3.hasNext())
                {
                    String ug3 = itr3.next();
                                    
                    if(trigramSet.contains(ug1
                            + WORD_SEPERATOR + ug2
                            + WORD_SEPERATOR + ug3) == false)
                    {
                        continue;
                    }

                    Iterator<String> itr4 = words.iterator();

                    while(itr4.hasNext())
                    {
                        String ug4 = itr4.next();

                        wg.addFourGram(ug1, ug2, ug3, ug4, memIndex);
                    }
                }
            }
        }
        
        return wg;
    }

//    public static WordGraph createGraph(T sentence)
//    {
//        return null;
//    }
    
    public WordGraph generateBigramGraph(String[] words)
    {
        for (int i = 0; i < words.length; i++) {
            String string = words[i];
            outputIndex.add(string);
        }
        
        WordGraph wg = generateBigramGraph();
        
        return wg;
    }
    
    public void saveAll(String pathPrefix, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        saveFST(pathPrefix + ".fst.text", cs);
        saveInputSymbols(pathPrefix + ".isyms.text", cs);
        saveOutputSymbols(pathPrefix + ".osyms.text", cs);
    }
    
    public void saveAllFst(String pathPrefix, String cs) throws FileNotFoundException, UnsupportedEncodingException
    {
        saveInputSymbols(pathPrefix + ".input.syms", cs);
        saveOutputSymbols(pathPrefix + ".output.syms", cs);
        saveStateSymbols(pathPrefix + ".states.syms", cs);
        saveFST(pathPrefix + ".fst.txt", cs);
    }

    public static void main(String args[])
    {
//        String dir = "/people/anil/work/fst-test/";
//        String dir = "/extra/work/fst/fst-test/";
        String dir = "/home/anil/work/questimate/tmp/sample-fst/";
        
        String basename = "task1-3.test_1";
        
//        String odir = "clean/";
        String odir = "tmp/";
        
        String cs = "UTF-8";
        
//        WordGraph wg = new WordGraph();
        WordGraph wg = new WordGraph(dir, basename, cs);
        
//        wg = wg.generateBigramGraph(new String[] {"w1", "w2"});     
                
        try {
//            Fst fst = wg.readWordGraphAsFst(dir + basename, "UTF-8", new LogSemiring());

//            fst = RmEpsilon.get(fst);
//            
//            fst = Determinize.get(fst);
            
//            Convert.export(fst, dir + odir + basename);
            
//            wg.readWordGraphNCODE(dir + basename, "UTF-8");
//            wg.readWordGraphWMT12(dir + basename, "UTF-8", new LogSemiring());
//            wg.readOpenFstFormat(dir + odir + basename + ".text", "UTF-8", new LogSemiring());
////            wg.save(dir + odir + basename, "UTF-8");
//            wg.saveAllFst(dir + odir + basename, "UTF-8");
//
//            WordGraph bgwg = wg.generateBigramGraph();
//            bgwg.saveAllFst(dir + odir + basename + ".bg", cs);

            wg.readFstAsWordGraph(dir + basename, "UTF-8", new LogSemiring());
            wg.saveAllFst(odir + basename, "UTF-8");
            
//            Index<String> uniqBG = MiscellaneousUtils.readIndex(dir + odir + "wmtgraph.bg.uniq", cs);
//            
//            WordGraph wgtg = wg.generateTrigramGraph(uniqBG);
//            wgtg.saveAllFst(dir + odir + basename + ".tg", cs);
            
//            Fst fst1 = wordGraph2Fst(wg, dir + odir + basename, "UTF-8");
//            ArcSort.apply(fst1, new ILabelCompare());
//            
//            Fst fst2 = wordGraph2Fst(wgtg, dir + odir + basename + ".tg", "UTF-8");
//            ArcSort.apply(fst2, new OLabelCompare());
//            
//            Fst composedFst = Compose.compose(fst1, fst2, new LogSemiring(), true);
//            
//            Convert.export(composedFst, dir + odir + basename + ".tg.log.composed");
            
//            wg.save(dir + "clean/" + basename + "-split.lattice", "UTF-8");
//
//            wg = new WordGraph(dir, basename, cs);
//            wg.readWordGraphNCODE(dir + "clean/" + basename + "-split.lattice", "UTF-8");
//
//            wg.saveAllFst(dir + "clean/" + basename + "-split", "UTF-8");
//            wg.generateBigramGraph().saveAllFst(dir + "clean/" + basename + "-split.bg", cs);
            
//            wg.readWordGraphNCODE(prefix + "wgraph", "UTF-8");
//            wg.saveAllFst(prefix + "wgraph", "UTF-8");
////            
//            wg.generateBigramGraph().saveAllFst(prefix + "wgraph.bg", "UTF-8");
//            wg.generateTrigramGraph().saveAll(prefix + "wgraph.tg", "UTF-8");
//            wg.generateFourGramGraph().saveAll("/people/anil/work/ngrampost/wgraph.fg", "UTF-8");
            
//        wg.saveAllFst(prefix + "w1w2", "UTF-8");

//        Fst fst2 = WordGraph2Fst(wg, prefix + "w1w2", "UTF-8");
        
//        Fst fst1 = Convert.importFst(prefix + "w", new TropicalSemiring());
//        
//        ArcSort.apply(fst1, new ILabelCompare());
//        
//        Fst fst2 = Convert.importFst(prefix + "w1w2", new TropicalSemiring());
//        
//        ArcSort.apply(fst2, new OLabelCompare());
//        
//        Fst fst3 = Compose.compose(fst1, fst2, new TropicalSemiring(), true);
//        
//        Convert.export(fst3, prefix + "ww1w2");
        
//        Convert.export(fst, prefix + "w1w2-exported");

//        fst = Convert.importFst(prefix + "w1w2-exported", new TropicalSemiring());
//        
//        Convert.export(fst, prefix + "w1w2-re-exported");        
//
//        wg.saveOutputSymbols("/more/work/fst/fst-test/w1w2.osyms.text", "UTF-8");
//
//        wg.saveFST("/more/work/fst/fst-test/w1w2.fst.text", "UTF-8");

//        wg.saveInputSymbols("/people/anil/fst-test/w1w2.isyms.text", "UTF-8");
//
//        wg.saveOutputSymbols("/people/anil/fst-test/w1w2.osyms.text", "UTF-8");
//
//        wg.saveFST("/people/anil/fst-test/w1w2.fst.text", "UTF-8");
        
//            
//            wg.readWordGraphNCODE("/more/work/fst/fst-test/wgraph", "UTF-8");
//            
//            wg.save("/more/work/fst/fst-test/wgraph.print", "UTF-8");
//
//            wg.saveInputSymbols("/more/work/fst/fst-test/wgraph.isyms.text", "UTF-8");
//
//            wg.saveOutputSymbols("/more/work/fst/fst-test/wgraph.osyms.text", "UTF-8");
//
//            wg.saveFST("/more/work/fst/fst-test/wgraph.fst.text", "UTF-8");
//
//            WordGraph ugraph = wg.generateUnigramGraph();
//            
//            ugraph.saveInputSymbols("/more/work/fst/fst-test/wgraph.ug.isyms.text", "UTF-8");
//            ugraph.saveOutputSymbols("/more/work/fst/fst-test/wgraph.ug.osyms.text", "UTF-8");
//            ugraph.saveFST("/more/work/fst/fst-test/wgraph.ug.fst.text", "UTF-8");
//
//            WordGraph bgraph = wg.generateBigramGraph();
//
//            bgraph.saveInputSymbols("/more/work/fst/fst-test/wgraph.bg.isyms.text", "UTF-8");
//            bgraph.saveOutputSymbols("/more/work/fst/fst-test/wgraph.bg.osyms.text", "UTF-8");
//            bgraph.saveFST("/more/work/fst/fst-test/wgraph.bg.fst.text", "UTF-8");

//            WordGraph tgraph = wg.generateTrigramGraph();
//
//            tgraph.saveInputSymbols("/more/work/fst/fst-test/wgraph.tg.isyms.text", "UTF-8");
//            tgraph.saveOutputSymbols("/more/work/fst/fst-test/wgraph.tg.osyms.text", "UTF-8");
//            tgraph.saveFST("/more/work/fst/fst-test/wgraph.tg.fst.text", "UTF-8");
//
//            WordGraph fgraph = wg.generateFourGramGraph();
//
//            fgraph.saveInputSymbols("/more/work/fst/fst-test/wgraph.fg.isyms.text", "UTF-8");
//            fgraph.saveOutputSymbols("/more/work/fst/fst-test/wgraph.fg.osyms.text", "UTF-8");
//            fgraph.saveFST("/more/work/fst/fst-test/wgraph.fg.fst.text", "UTF-8");
                    
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WordGraph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(WordGraph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WordGraph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
