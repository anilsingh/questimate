/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.wg;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author anil
 */
public class WordGraphNode<TK extends Object, FV extends Object> {
    
    protected int nodeID;
    
    public WordGraphNode()
    {

    }

    /**
     * @return the nodeID
     */
    public int getNodeID() {
        return nodeID;
    }

    /**
     * @param nodeID the nodeID to set
     */
    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }    
}
