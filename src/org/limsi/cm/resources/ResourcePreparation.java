/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.properties.KeyValueProperties;

/**
 *
 * @author anil
 */
public class ResourcePreparation {
    
    public static KeyValueProperties toolPros;
    
    public static int TOKENIZE = 0;
    public static int POS_TAG = 1;
    public static int NGRAM_MODEL= 2;
    public static int POS_NGRAM_MODEL= 3;
    
    public static String tokenizerCommand = "";
    public static String posTaggerCommand = "";
    public static String ngramModelCommand = "";
    public static String posNGramModelCommand = "";
    
    public ResourcePreparation(String propsPath, String cs)
    {
        try {
            toolPros = new KeyValueProperties(propsPath, cs);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        tokenizerCommand = toolPros.getPropertyValue("tokenizerCommand");
        posTaggerCommand = toolPros.getPropertyValue("posTaggerCommand");
        ngramModelCommand = toolPros.getPropertyValue("ngramModelCommand");
        posNGramModelCommand = toolPros.getPropertyValue("posNGramModelCommand");
    }
    
    public void prepareData(int type, String inPath, String outPath)
    {
        if(type == TOKENIZE)
        {
            tokenize(inPath, outPath);
        }
        else if(type == POS_TAG)
        {
            posTag(inPath, outPath);
        }
        else if(type == NGRAM_MODEL)
        {
            nGramModel(inPath, outPath);
        }
        else if(type == POS_NGRAM_MODEL)
        {
            posNGramModel(inPath, outPath);
        }
    }

    public void tokenize(String inPath, String outPath)
    {
        String command = tokenizerCommand;
        String[] envp = null;
        
        command = String.format(command, inPath, outPath);
        
        try {
            Runtime.getRuntime().exec(command, envp);
        } catch (IOException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void posTag(String inPath, String outPath)
    {
        String command = posTaggerCommand;
        String[] envp = null;
        
        command = String.format(command, inPath, outPath);

        try {
            Runtime.getRuntime().exec(command, envp);
        } catch (IOException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    public void nGramModel(String inPath, String outPath)
    {        
        String command = ngramModelCommand;
        String[] envp = null;
        
        command = String.format(command, inPath, outPath);

        try {
            Runtime.getRuntime().exec(command, envp);
        } catch (IOException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void posNGramModel(String inPath, String outPath)
    {        
        String command = posNGramModelCommand;
        String[] envp = null;
        
        command = String.format(command, inPath, outPath);

        try {
            Runtime.getRuntime().exec(command, envp);
        } catch (IOException ex) {
            Logger.getLogger(ResourcePreparation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
