/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import edu.stanford.nlp.mt.base.*;
import edu.stanford.nlp.mt.metrics.NISTTokenizer;
import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.mt.cm.Formats;
import org.limsi.mt.cm.PhraseAlignment;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.WordAlignment;
import sanchay.properties.PropertyTokens;
import org.limsi.cm.util.NBestFeatureUtils;

/**
 *
 * @author anil
 */
public class NBestList {

    static public final String NBEST_SEP = " |||";

    protected List<List<TranslationHypothesis<IString, String>>> nbestLists;

    public static final String DEBUG_PROPERTY = "NBestListDebug";
    public static final boolean DEBUG = Boolean.parseBoolean(System.getProperty(
            DEBUG_PROPERTY, "false"));

    protected String filePath;
    protected String encoding;

    public final Index<String> featureIndex;
    public final Index<String> featureIndexFull;
//    public final Map<Sequence<IString>, Sequence<IString>> sequenceSelfMap;

    public final boolean tokenizeNIST;
    
//    public final Map<Sequence<IString>, Sequence<IString>> sequenceSelfMap;

    public final int format;

    public NBestList(boolean tokenizeNIST, int format, String path, String encoding) {
        this.tokenizeNIST = tokenizeNIST;
        this.format = format;
        this.filePath = path;
        this.encoding = encoding;

        featureIndex = new HashIndex<String>();
        featureIndexFull = new HashIndex<String>();
        
//        sequenceSelfMap = null;

        nbestLists = new ArrayList<List<TranslationHypothesis<IString, String>>>();
       
        try {
            read(tokenizeNIST, format);
        } catch (IOException ex) {
            Logger.getLogger(NBestList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the nbestLists
     */
    public List<List<TranslationHypothesis<IString, String>>> getNbestLists() {
        return nbestLists;
    }
    
    private void read(boolean tokenizeNIST, int format) throws IOException
    {
        LineNumberReader reader;
        if (filePath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(filePath)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    filePath), encoding));
        }

        Runtime rt = Runtime.getRuntime();
        long preNBestListLoadMemUsed = rt.totalMemory() - rt.freeMemory();
        long startTimeMillis = System.currentTimeMillis();
        

        List<TranslationHypothesis<IString, String>> currentNbest = new LinkedList<TranslationHypothesis<IString, String>>();

        Pattern space = Pattern.compile(" ");
        String lastId = null;
        String[] emptyStringArray = new String[0];
        for (String inline; (inline = reader.readLine()) != null;) {
            StringTokenizer toker = new StringTokenizer(inline);
            List<String> listFields = new LinkedList<String>();
            do {
                StringBuilder sb = new StringBuilder();
                boolean first = true;
                do {
                    String token = toker.nextToken();
                    if ("|||".equals(token)) {
                        break;
                    }
                    if (!first) {
                        sb.append(" ");
                    } else {
                        first = false;
                    }
                    sb.append(token);
                } while (toker.hasMoreTokens());
                listFields.add(sb.toString());
            } while (toker.hasMoreTokens());

            String[] fields = listFields.toArray(emptyStringArray);

            // fields = tripplePipes.split(inline);
            if (fields.length < 3) {
                System.err.printf("Warning: bad nbest-list format: %s\n", inline);
                System.err.printf(
                        "Warning: expected at least 3 fields, but found only %d\n",
                        fields.length);
                continue;
            }
            String id = fields[0];
            String translation = fields[1];
            if (tokenizeNIST) {
                translation = NISTTokenizer.tokenize(translation);
            }
            String featuresStr = fields[2];
            String scoreStr = (fields.length >= 4 ? fields[3] : "0");
            String latticeIdStr = (fields.length >= 5 ? fields[4] : null);
            String walignmentStr = (fields.length >= 6 ? fields[5] : null);
                        
            PhraseAlignment palignment = null;
            WordAlignment walignment = null;
            
            // System.err.printf("reading id: %s\n", id);
            if (lastId == null) {
                lastId = id;
            } else if (!id.equals(lastId)) {
                int intId; // = -1;
                try {
                    intId = Integer.parseInt(id);
                } catch (NumberFormatException e) {
                    throw new RuntimeException(String.format(
                            "id '%s' can not be parsed as an integer value (line: %d)\n", id,
                            reader.getLineNumber()));
                }
                
                int intLastId = Integer.parseInt(lastId);
                
//                if (getNbestLists().size() > intId) {
//                    throw new RuntimeException("n-best list ids are out of order:\n"
//                           + "\t" + getNbestLists().size() + " > " + intId);
//                }

                while (getNbestLists().size() < intLastId) {
                    // System.err.printf("Inserting empty: %d/%d\n", intLastId,
                    // nbestLists.size());
                    getNbestLists().add(new ArrayList<TranslationHypothesis<IString, String>>());
                }

                // System.err.printf("Inserting NON-empty: %d/%d\n", intLastId,
                // nbestLists.size());
                getNbestLists().add(new ArrayList<TranslationHypothesis<IString, String>>(
                        currentNbest));
                currentNbest.clear();
                lastId = id;
                if (DEBUG) {
                    System.err.printf("Doing %s Memory: %d MiB\n", id,
                            (rt.totalMemory() - rt.freeMemory()) / (1024 * 1024));
                }
            }

            double score;
            try {
                score = Double.parseDouble(scoreStr);
            } catch (NumberFormatException e) {
                throw new RuntimeException(
                        String.format(
                        "Contents of score field, '%s', cannot be parsed as a double value. (line: %d, %s)",
                        scoreStr, reader.getLineNumber(), filePath));
            }

            long latticeId = -1;
            if (latticeIdStr != null) {
                if (latticeIdStr.indexOf('=') == -1 && latticeIdStr.indexOf('-') == -1) {
                    try {
                        latticeId = Long.parseLong(latticeIdStr);
                    } catch (NumberFormatException e) {
                        throw new RuntimeException(
                                String.format(
                                "Contents of lattice id field, '%s', cannot be parsed as a long integer value (line: %d)",
                                latticeIdStr, reader.getLineNumber()));
                    }
                } else if(latticeIdStr.indexOf('=') != -1) {
                    // phrase alignment instead of latticeId
                    palignment = new PhraseAlignment(latticeIdStr, Formats.PHRASAL_NBEST_FORMAT);
                } else if(latticeIdStr.indexOf('=') == -1 && latticeIdStr.indexOf('-') != -1) {
                    // phrase alignment instead of latticeId
                    walignment = new WordAlignment(latticeIdStr, Formats.PHRASAL_NBEST_FORMAT);
                }
            }
            
            if(walignmentStr != null && walignmentStr.equals("") == false && walignment == null)
                    walignment = new WordAlignment(walignmentStr, Formats.PHRASAL_NBEST_FORMAT);

//            System.out.println("Line number: " + reader.getLineNumber());
            
            FeatureValueCollection<String> featureValues = readFeatures(featuresStr, format, reader.getLineNumber());

//            String[] featureFields = space.split(featuresStr);
//            String featureName = "unlabeled";
//            Map<String, List<Double>> featureMap = new HashMap<String, List<Double>>();
//            featureMap.put(featureName, new ArrayList<Double>());
//            for (String field : featureFields) {
//                if (field.endsWith(":")) {
//                    featureName = field.substring(0, field.length() - 1);
//                    featureMap.put(featureName, new ArrayList<Double>());
//                    continue;
//                }
//                try {
//                    featureMap.get(featureName).add(new Double(field));
//                } catch (NumberFormatException e) {
//                    throw new RuntimeException(
//                            String.format(
//                            "Feature value, '%s', can not be parsed as a double value. (line: %d)",
//                            field, reader.getLineNumber()));
//                }
//            }
//
//            List<FeatureValue<String>> featureValuesTmp = new LinkedList<FeatureValue<String>>();
//
//            for (String feature : featureMap.keySet()) {
//                if (featureIndex != null) {
//                    featureIndex.indexOf(feature, true);
//                }
//                List<Double> values = featureMap.get(feature);
//                if (values.size() == 1) {
//                    String featureNameStored = featureNameSelfMap.get(feature);
//                    if (featureNameStored == null) {
//                        featureNameSelfMap.put(feature, feature);
//                        featureNameStored = feature;
//                    }
//                    featureValuesTmp.add(new FeatureValue<String>(featureNameStored,
//                            values.get(0), true));
//                } else {
//                    for (int i = 0; i < values.size(); i++) {
//                        String composedName = feature + "_" + i;
//                        String featureNameStored = featureNameSelfMap.get(composedName);
//                        if (featureNameStored == null) {
//                            featureNameSelfMap.put(composedName, composedName);
//                            featureNameStored = composedName;
//                        }
//                        featureValuesTmp.add(new FeatureValue<String>(featureNameStored,
//                                values.get(i), true));
//                    }
//                }
//            }
//
//            boolean useSparse = featureIndex.size() >= MAX_DENSE_SIZE;
//            FeatureValueCollection<String> featureValues = useSparse ? new SparseFeatureValueCollection<String>(
//                    featureValuesTmp, featureIndex)
//                    : new DenseFeatureValueCollection<String>(featureValuesTmp,
//                    featureIndex);

            Sequence<IString> sequence = new RawIStringSequence(
                    IStrings.toIStringArray(space.split(translation)));
//            Sequence<IString> sequenceStored = sequenceSelfMap.get(sequence);
//            if (sequenceStored == null) {
//                sequenceSelfMap.put(sequence, sequence);
//                sequenceStored = sequence;
//            }
            TranslationHypothesis<IString, String> sfTrans;
            if (latticeId != -1) {
                sfTrans = new TranslationHypothesis<IString, String>(
                        sequence, featureValues, score, latticeId);
            } else if (palignment != null || walignment != null) {
                sfTrans = new TranslationHypothesis<IString, String>(
                        sequence, featureValues, score, palignment, walignment);
            } else {
                sfTrans = new TranslationHypothesis<IString, String>(
                        sequence, featureValues, score);
            }
            currentNbest.add(sfTrans);
        }

        int intLastId = (lastId == null ? -1 : Integer.parseInt(lastId));

        while (getNbestLists().size() < intLastId) {
            // System.err.printf("Inserting empty: %d/%d\n", intLastId,
            // nbestLists.size());
            getNbestLists().add(new ArrayList<TranslationHypothesis<IString, String>>());
        }

        getNbestLists().add(new ArrayList<TranslationHypothesis<IString, String>>(
                currentNbest));
        
        removeEmpty();

//        featureNameSelfMap = null;
//        ErasureUtils.noop(sequenceSelfMap);
//        ErasureUtils.noop(featureNameSelfMap);
        System.gc();

        long postNBestListLoadMemUsed = rt.totalMemory() - rt.freeMemory();
        long loadTimeMillis = System.currentTimeMillis() - startTimeMillis;
        System.err.printf(
                "Done loading Flat n-best lists: %s (mem used: %d MiB time: %.3f s)\n",
                filePath, (postNBestListLoadMemUsed - preNBestListLoadMemUsed)
                / (1024 * 1024), loadTimeMillis / 1000.0);

        reader.close();        
    }

    private FeatureValueCollection<String> readFeatures(String featuresStr, int format, int lineNumber)
    {
        FeatureValueCollection<String> featureValues = null;

        Map<String, String> featureNameSelfMap = new HashMap<String, String>();

        Pattern space = Pattern.compile(" ");

        String[] featureFields = space.split(featuresStr);
        
//        if(format == Formats.MOSES_NBEST_FORMAT)
//        {
//        }
//        else if(format == Formats.BINCODER_NBEST_FORMAT || format == Formats.PHRASAL_NBEST_FORMAT)
//        {
            String featureName = "unlabeled";
            Map<String, List<Double>> featureMap = new HashMap<String, List<Double>>();
            featureMap.put(featureName, new ArrayList<Double>());
            for (String field : featureFields) {
                if (field.endsWith(":")) {
                featureName = field.substring(0, field.length() - 1);
                featureMap.put(featureName, new ArrayList<Double>());
                continue;
                }
                try {
                featureMap.get(featureName).add(new Double(field));
                } catch (NumberFormatException e) {
                throw new RuntimeException(
                    String
                        .format(
                            "Feature value, '%s', can not be parsed as a double value. (line: %d)",
                            field, lineNumber));
                }
            }
//        }

        List<FeatureValue<String>> featureValuesTmp = new LinkedList<FeatureValue<String>>();

        for (String feature : featureMap.keySet()) {
            if (featureIndex != null)
            featureIndex.indexOf(feature, true);
            List<Double> values = featureMap.get(feature);
            if (values.size() == 1) {
                String featureNameStored = featureNameSelfMap.get(feature);
                if (featureNameStored == null) {
                    featureNameSelfMap.put(feature, feature);
                    featureNameStored = feature;
                    featureIndexFull.indexOf(featureNameStored, true);
                }
                featureValuesTmp.add(new FeatureValue<String>(featureNameStored,
                    values.get(0), true));
            } else {
                for (int i = 0; i < values.size(); i++) {
                    String composedName = feature + "_" + i;
                    String featureNameStored = featureNameSelfMap.get(composedName);
                    if (featureNameStored == null) {
                    featureNameSelfMap.put(composedName, composedName);
                    featureNameStored = composedName;
                    featureIndexFull.indexOf(featureNameStored, true);
                    }
                    featureValuesTmp.add(new FeatureValue<String>(featureNameStored,
                        values.get(i), true));
                }
            }
        }
        
//        featureValues = new DenseFeatureValueCollection<String>(featureValuesTmp, featureIndex);
        featureValues = new DenseFeatureValueCollection<String>(featureValuesTmp, featureIndexFull);
        
        return featureValues;
    }
    
    private void removeEmpty()
    {
        Iterator<List<TranslationHypothesis<IString, String>>> itr = nbestLists.iterator();
        List<List<TranslationHypothesis<IString, String>>> remHypLists = new LinkedList<List<TranslationHypothesis<IString, String>>>();
        
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> transHypList = itr.next();

            if(transHypList.isEmpty())
            {
                remHypLists.add(transHypList);
            }
        }        

        itr = remHypLists.iterator();
        
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> transHypList = itr.next();

            nbestLists.remove(transHypList);
        }
    }

  public String printMosesFormat() {
    DecimalFormat df = new DecimalFormat("0.####E0");
    StringBuilder sbuf = new StringBuilder();
    for (int i = 0; i < getNbestLists().size(); i++) {
      for (int j = 0; j < getNbestLists().get(i).size(); j++) {
        TranslationHypothesis<IString, String> tr = getNbestLists().get(i)
            .get(j);
        sbuf.append(i).append(NBEST_SEP).append(' ').append(tr.getTranslation())
            .append(NBEST_SEP);
        for (FeatureValue<String> fv : tr.getFeatures()) {
          sbuf.append(' ')
              .append(fv.name)
              .append(": ")
              .append(
                  (fv.value == (int) fv.value ? (int) fv.value : df
                      .format(fv.value)));
        }
        if (tr.getScore() != 0.0)
          sbuf.append(NBEST_SEP).append(' ').append(df.format(tr.getScore()));
        if (    tr.getLatticeSourceId() != -1) {
          sbuf.append(NBEST_SEP).append(' ');
          sbuf.append(tr.getLatticeSourceId());
        }
        if (    tr.getPhraseAlignment() != null) {
          sbuf.append(NBEST_SEP).append(' ');
          sbuf.append(tr.getPhraseAlignment().s2tStr());
        }
        if (    tr.getWordAlignment() != null) {
          sbuf.append(NBEST_SEP).append(' ');
          sbuf.append(tr.getWordAlignment().s2tStr());
        }
        sbuf.append("\n");
      }
    }
    return sbuf.toString();
  }
    
    static public void main(String[] args) throws IOException {
//        if (args.length != 1) {
//            System.err.printf("Usage:\n\tjava ...(flat nbest list)\n");
//            System.exit(-1);
//        }

//        String nbestListFilename = args[0];
//        String nbestListFilename = "/people/anil/work/phrasal.Beta3/work/phrasal-mert/phrasal.11.nbest.gz";
//        String nbestListFilename = "/vol/corpora6/smt/lehaison/WMT/wmt12/en2fr/bincode/optim/09_subLMs.minfr1.tnb0/mert.bWW.tW.CFB.3/run9.out.NBEST.gz";
//        String nbestListFilename = "/vol/corpora6/smt/lehaison/WMT/wmt12/en2fr/moses/newsco+epps/optim/09/1/run.best300.out";
        String nbestListFilename = "/people/anil/work/wmt12-quality-estimation/wmt12qe.test.output.1000-best";
        
        NBestList nbestList = new NBestList(false, Formats.MOSES_NBEST_FORMAT, nbestListFilename, "UTF-8");

        System.out.print("Original size: " + nbestList.getNbestLists().size());

        PropertyTokens targetSentences = new PropertyTokens();
        
        targetSentences.read("/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa", "UTF-8");
        
        NBestFeatureUtils.filterNBestList(nbestList, targetSentences);
        
//        NBestList nbestList = new NBestList(nbestListFilename, null, false, NBestListEx.PHRASAL_FORMAT);
//        System.out.print(nbestList.printMosesFormat());
        System.out.print("Hypotheses left: " + nbestList.getNbestLists().size());
    }
}
