/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import edu.stanford.nlp.mt.base.*;
import edu.stanford.nlp.mt.decoder.feat.IsolatedPhraseFeaturizer;
import edu.stanford.nlp.mt.decoder.util.Scorer;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author anil
 */
public class PhraseTranslationTable<FV> implements PhraseTable<IString> {

    FlatPhraseTable<FV> flatPhraseTable;

    public PhraseTranslationTable(IsolatedPhraseFeaturizer<IString, FV> ipf, Scorer<FV> scorer, String string) throws IOException {
        flatPhraseTable = new FlatPhraseTable<FV>(ipf, scorer, string);
    }

    public PhraseTranslationTable(IsolatedPhraseFeaturizer<IString, FV> ipf, Scorer<FV> scorer, String string, boolean bln) throws IOException {
        flatPhraseTable = new FlatPhraseTable<FV>(ipf, scorer, string, bln);
    }

    @Override
    public List<TranslationOption<IString>> getTranslationOptions(Sequence<IString> sqnc) {
        return flatPhraseTable.getTranslationOptions(sqnc);
    }

    @Override
    public int longestForeignPhrase() {
        return flatPhraseTable.longestForeignPhrase();
    }

    @Override
    public String getName() {
        return flatPhraseTable.getName();
    }

    public FlatPhraseTable getFlatPhraseTable() {
        return flatPhraseTable;
    }

    static public void main(String[] args) throws Exception {
//    if (args.length != 2) {
//      System.out
//          .println("Usage:\n\tjava ...FlatPhraseTable (phrasetable file) (entry to look up)");
//      System.exit(-1);
//    }

//    String model = args[0];
//    String phrase = args[1];
//        String model = "/people/anil/work/phrasal.Beta3/work/models/phrases.gz";
        String model = "/people/anil/work/wmt12-quality-estimation/phrase-table.tar.gz";
//        String phrase = "dernier";
        String phrase = "dernier";
        long startTimeMillis = System.currentTimeMillis();
        System.out.printf("Loading phrase table: %s\n", model);
        FlatPhraseTable<String> ppt = new FlatPhraseTable<String>(null, null,
                model);
        long totalMemory = Runtime.getRuntime().totalMemory() / (1 << 20);
        long freeMemory = Runtime.getRuntime().freeMemory() / (1 << 20);
        double totalSecs = (System.currentTimeMillis() - startTimeMillis) / 1000.0;
//    System.err.printf(
//        "size = %d, secs = %.3f, totalmem = %dm, freemem = %dm\n",
//        foreignIndex.size(), totalSecs, totalMemory, freeMemory);

        List<TranslationOption<IString>> translationOptions = ppt.getTranslationOptions(new SimpleSequence<IString>(IStrings.toIStringArray(phrase.split("\\s+"))));

        System.out.printf("Phrase: %s\n", phrase);

        if (translationOptions == null) {
            System.out.printf("No translation options found.");
            System.exit(-1);
        }

        System.out.printf("Options:\n");
        for (TranslationOption<IString> opt : translationOptions) {
            System.out.printf("\t%s : %s\n", opt.translation,
                    Arrays.toString(opt.scores));
        }
    }
}
