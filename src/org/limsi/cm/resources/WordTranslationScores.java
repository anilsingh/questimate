/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.tree.SSFNode;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.mt.cm.WordAlignment;
import sanchay.tree.SanchayMutableTreeNode;

/**
 *
 * @author anil
 */
public class WordTranslationScores {

    protected String filePath;
    protected String encoding;

    protected static Index<String> sourceIndex = new HashIndex<String>();
    protected static Index<String> targetIndex = new HashIndex<String>();
    
    protected LinkedHashMap<Integer, LinkedHashMap<Integer, Double>> scores;

    protected LinkedHashMap<Double, LinkedHashMap<Integer, Integer>> avgNumTranslations;
    
    public WordTranslationScores(String path, String encoding) {
        
        scores = new LinkedHashMap<Integer, LinkedHashMap<Integer, Double>>();
        avgNumTranslations = new LinkedHashMap<Double, LinkedHashMap<Integer, Integer>>();
        
        this.filePath = path;
        this.encoding = encoding;
        
        try {
            loadScores();
        } catch (IOException ex) {
            Logger.getLogger(WordTranslationScores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadScores() throws IOException
    {
        System.out.printf("Loading scores from file %s\n", filePath);
        
        LineNumberReader reader;
        
        if (filePath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(filePath)), CEAnnotatedCorpusWMT12.getEncoding()));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    filePath), CEAnnotatedCorpusWMT12.getEncoding()));
        }
        
        Pattern space = Pattern.compile(" ");

        int i = 0;
        
        for (String inline; (inline = reader.readLine()) != null;)
        {
            if(i++ > 0 && i % 10000 == 0)
            {
               System.out.printf("Processed %d lines\r", i);            
            }
            
            String fields[] = space.split(inline);
            
            if(fields.length != 3)
            {
                System.err.print("Error: there should be three fields: " + inline);
            }
            
            String srcWord = fields[0];
            String tgtWord = fields[1];
            Double score = Double.parseDouble(fields[2]);

            int srcWordIndex = sourceIndex.indexOf(srcWord, true);
            
            if(scores.get(srcWordIndex) == null)
            {
                scores.put(srcWordIndex, new LinkedHashMap<Integer, Double>());
            }
            
            int tgtWordIndex = targetIndex.indexOf(tgtWord, true);
            
            scores.get(srcWordIndex).put(tgtWordIndex, score);
        }

        System.out.printf("Total %d source words and %d target words found\n",
                sourceIndex.size(), targetIndex.size());
        
        reader.close();
    }
    
    public double getScore(String srcWord, String tgtWord)
    {
        double score = 0.0;

        int srcWordIndex = sourceIndex.indexOf(srcWord);
        
        if(srcWordIndex == -1)
        {
            return score;
        }
        
        LinkedHashMap<Integer, Double> ds = scores.get(srcWordIndex);

        int tgtWordIndex = targetIndex.indexOf(tgtWord, true);

        if(tgtWordIndex == -1)
        {
            return score;
        }
        
//            score = scores.get(srcIndex).get(tgtIndex);
//        System.out.println(srcWord + " : " + tgtWord);
//        System.out.println("ds : " + ds);
        
        if(ds != null && ds.get(tgtWordIndex) != null)
            score = ds.get(tgtWordIndex);
        
        return score;
    }
    
    public void printScores(PrintStream ps)
    {
        DecimalFormat df = new DecimalFormat("0.0000000");

        int i = 0;
        
        Iterator<Integer> itr = scores.keySet().iterator();
        
        while(itr.hasNext()) {
            
            Integer swIndex = itr.next();
            
            String sw = sourceIndex.get(i);
            
            LinkedHashMap<Integer, Double> im = scores.get(swIndex);
            
            Iterator<Integer> itr1 = im.keySet().iterator();
            
            while(itr1.hasNext())
            {            
                Integer twIndex = itr1.next();

                String tw = targetIndex.get(i);
                
                ps.println(sw + " " + tw + " " + df.format(im.get(twIndex)));
            }     
            
            i++;
        }
    }
    
    public double getIBM1Score(SSFSentence srcSentence, SSFSentence tgtSentence)
    {
        double score = 0.0;

        List<SanchayMutableTreeNode> srcWords = null;
        List<SanchayMutableTreeNode> tgtWords = null;

        srcWords = srcSentence.getRoot().getAllLeaves();
        tgtWords = tgtSentence.getRoot().getAllLeaves();
                
        int scount = srcWords.size();
        int tcount = tgtWords.size();
        
        for (int i = 0; i < tcount; i++) {
            
            String tgtWord = ((SSFNode) tgtWords.get(i)).getLexData();

            double ijScore = 0.0;

            for (int j = 0; j < scount; j++) {
                              
                String srcWord = ((SSFNode) srcWords.get(j)).getLexData();

//                System.err.println("Processing word pair: " + srcWord + "::" + tgtWord
//                        +"at index" + i + " aligned to " + aj);

                ijScore += getScore(srcWord, tgtWord);
            }

            if(ijScore != 0.0)
                score += Math.log(ijScore);                
        }

        score = score - (tcount * Math.log(scount + 1));                
        
        return score;
    }
    
    public double getIBM2Score(SSFSentence srcSentence, SSFSentence tgtSentence,
            WordAlignment wrdAlignment, boolean reverse)
    {
        if(!reverse) {
            wrdAlignment = WordAlignment.getReverseAlignment(wrdAlignment);
        }
        
        double score = 0.0;

        List srcWords = null;
        List tgtWords = null;

        if(reverse)
        {
            srcWords = tgtSentence.getRoot().getAllLeaves();
            tgtWords = srcSentence.getRoot().getAllLeaves();
        }
        else
        {
            srcWords = srcSentence.getRoot().getAllLeaves();
            tgtWords = tgtSentence.getRoot().getAllLeaves();
        }
                
        int scount = srcWords.size();
        int tcount = tgtWords.size();
        
        for (int i = 0; i < tcount; i++) {
            
            String tgtWord = ((SSFNode) tgtWords.get(i)).getLexData();
            
            int aj[] = wrdAlignment.s2t(i);
            
            if(aj == null)
                continue;
            
            for (int j = 0; j < aj.length; j++) {
                      
                String srcWord = ((SSFNode) srcWords.get(aj[j])).getLexData();

                System.err.println("Processing word pair: " + srcWord + "::" + tgtWord
                        +"at index" + i + " aligned to " + aj);

                double iajScore = getScore(srcWord, tgtWord);

                if(iajScore != 0.0)
                    score += Math.log(iajScore);                
            }
        }
        
        return score;
    }
    
    public int getNumTranslations(String srcWrd, double probThreshold)
    {
        int numTrans = 0;

        int swIndex = sourceIndex.indexOf(srcWrd);

        LinkedHashMap<Integer, Integer> avgNumTrans = avgNumTranslations.get(probThreshold);
        
        if(avgNumTrans == null)
        {
            avgNumTrans = new LinkedHashMap<Integer, Integer>();
            
            avgNumTranslations.put(probThreshold, avgNumTrans);            
        }
        
        if(avgNumTrans.get(swIndex) != null)
        {
            numTrans = avgNumTrans.get(swIndex);
        }
        else
        {
            LinkedHashMap<Integer, Double> im = scores.get(swIndex);

            if(im == null)
                return 0;

            Iterator<Integer> itr = im.keySet().iterator();

            while(itr.hasNext())
            {            
                Integer twIndex = itr.next();

                double prob = 0.0;

                if(im.get(twIndex) != null)
                    prob = im.get(twIndex);

                if(prob >= probThreshold)
                    numTrans++;
            }                 

            avgNumTrans.put(swIndex, numTrans);
        }
                                
        return numTrans;
    }
    
    public double getAvgNumTranslations(SSFSentence srcSentence, double probThreshold)
    {
        double avgNum = 0.0;
        
        List words = srcSentence.getRoot().getAllLeaves();
        
        for (Object lexItem : words) {
            avgNum += (double) getNumTranslations(((SSFNode) lexItem).getLexData(), probThreshold);
        }
        
        return (avgNum / (double) words.size());
    }
}
