/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import edu.cmu.sphinx.fst.semiring.Semiring;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.wg.WordGraph;

/**
 *
 * @author anil
 */
public class WordGraphs {
    
    protected List<WordGraph> wordGraphs;

    public static FilenameFilter graphFileNameFilter = new WordGraphFileFilter();

    public WordGraphs() {
        wordGraphs = new LinkedList<WordGraph>();
    }

    /**
     * @return the wordGraphs
     */
    public List<WordGraph> getWordGraphs() {
        return wordGraphs;
    }
    
    public void readWordGraphs(String dirPath, String encoding, Semiring semiring) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        File dir = new File(dirPath);

        File graphFiles[] = dir.listFiles(graphFileNameFilter);
        
        for (int i = 0; i < graphFiles.length; i++)
        {
            WordGraph wordGraph = new WordGraph();
            
            wordGraph.readWordGraphNCODE(dirPath, CEAnnotatedCorpusWMT12.getEncoding(), semiring);
            
            wordGraphs.add(wordGraph);
        }
    }

    private static class WordGraphFileFilter implements FilenameFilter {

        public WordGraphFileFilter() {
        }

        public boolean accept(File dir, String name) {
            return name.endsWith(".sgml");
        }
    }
    
}
