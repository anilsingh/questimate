/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.resources;

import org.limsi.cm.resources.NBestList;
import danbikel.parser.Parser;
import danbikel.parser.Settings;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.stanford.nlp.mt.base.FlatPhraseTable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.GlobalProperties;
import sanchay.corpus.ssf.SSFProperties;
import sanchay.corpus.ssf.features.impl.FSProperties;
import sanchay.corpus.ssf.features.impl.FeatureStructuresImpl;
import sanchay.corpus.ssf.tree.SSFNode;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.mt.cm.Formats;
import org.limsi.mt.cm.Temp;
import org.limsi.mt.cm.features.sentence.NBestListFeatureExtractor;

/**
 *
 * @author anil
 */
public class CEResources {

    protected static WordNetDatabase wordnet;
    protected static Parser collinsParser;
    protected static FlatPhraseTable<String> phraseTranslationTable;
    protected static NBestList nbestList;

    protected static WordTranslationScores s2tWordTranslationScores;
    protected static WordTranslationScores t2sWordTranslationScores;

    protected static String wordnetPath = "/people/anil/work/wn/WordNet-3.0/dict";
   
    protected static String phraseTranslationTablePath = "/people/anil/work/wmt12-quality-estimation/phrase-table.tar.gz";
    
    protected static String nbestListPath = "/people/anil/work/wmt12-quality-estimation/wmt12qe.training.output.1000-best";
//    protected static String nbestListPath = "/people/anil/work/wmt12-quality-estimation/wmt12qe.test.output.1000-best";
//    protected static String nbestListPath = "/people/artem/Projects/SMT/extrafeat/newstest2012.NBEST";
    
    protected static String collinsParsetSettingPath = "/people/anil/work/dbparser/settings/collins.properties";
    protected static String collinsParsetDataPath = "/people/anil/work/dbparser/wsj-02-21.obj.gz";
    
    protected static String src2TgtWordTranslationScoresPath = "/people/anil/work/wmt12-quality-estimation/resources/lex.e2s";
    protected static String tgt2SrcWordTranslationScoresPath = "/people/anil/work/wmt12-quality-estimation/resources/lex.s2e";
    
    public static boolean SSF_PROPS_SET = false;
    
    public CEResources() {
        setSSFProps();
    }

    /**
     * @return the wordnetPath
     */
    public static String getWordnetPath() {
        return wordnetPath;
    }

    /**
     * @param aWordnetPath the wordnetPath to set
     */
    public static void setWordnetPath(String aWordnetPath) {
        wordnetPath = aWordnetPath;
    }

    /**
     * @return the nbestListPath
     */
    public static String getNbestListPath() {
        return nbestListPath;
    }

    /**
     * @param aNbestListPath the nbestListPath to set
     */
    public static void setNbestListPath(String aNbestListPath) {
        nbestListPath = aNbestListPath;
    }

    /**
     * @return the collinsParsetSettingPath
     */
    public static String getCollinsParsetSettingPath() {
        return collinsParsetSettingPath;
    }

    /**
     * @param aCollinsParsetSettingPath the collinsParsetSettingPath to set
     */
    public static void setCollinsParsetSettingPath(String aCollinsParsetSettingPath) {
        collinsParsetSettingPath = aCollinsParsetSettingPath;
    }

    /**
     * @return the collinsParsetDataPath
     */
    public static String getCollinsParsetDataPath() {
        return collinsParsetDataPath;
    }

    /**
     * @param aCollinsParsetDataPath the collinsParsetDataPath to set
     */
    public static void setCollinsParsetDataPath(String aCollinsParsetDataPath) {
        collinsParsetDataPath = aCollinsParsetDataPath;
    }

    /**
     * @return the src2TgtWordTranslationScoresPath
     */
    public static String getSrc2TgtWordTranslationScoresPath() {
        return src2TgtWordTranslationScoresPath;
    }

    /**
     * @param aSrc2TgtWordTranslationScoresPath the src2TgtWordTranslationScoresPath to set
     */
    public static void setSrc2TgtWordTranslationScoresPath(String aSrc2TgtWordTranslationScoresPath) {
        src2TgtWordTranslationScoresPath = aSrc2TgtWordTranslationScoresPath;
    }

    /**
     * @return the tgt2SrcWordTranslationScoresPath
     */
    public static String getTgt2SrcWordTranslationScoresPath() {
        return tgt2SrcWordTranslationScoresPath;
    }

    /**
     * @param aTgt2SrcWordTranslationScoresPath the tgt2SrcWordTranslationScoresPath to set
     */
    public static void setTgt2SrcWordTranslationScoresPath(String aTgt2SrcWordTranslationScoresPath) {
        tgt2SrcWordTranslationScoresPath = aTgt2SrcWordTranslationScoresPath;
    }

    /**
     * @return the wordnet
     */
    public static WordNetDatabase getWordnet() {
        return wordnet;
    }

    /**
     * @param aWordnet the wordnet to set
     */
    public static void setWordnet(WordNetDatabase aWordnet) {
        wordnet = aWordnet;
    }

    /**
     * @return the collinsParser
     */
    public static Parser getCollinsParser() {
        return collinsParser;
    }

    /**
     * @param aCollinsParser the collinsParser to set
     */
    public static void setCollinsParser(Parser aCollinsParser) {
        collinsParser = aCollinsParser;
    }

    /**
     * @return the phraseTranslationTable
     */
    public static FlatPhraseTable<String> getPhraseTranslationTable() {
        return phraseTranslationTable;
    }

    /**
     * @param aPhraseTranslationTable the phraseTranslationTable to set
     */
    public static void setPhraseTranslationTable(FlatPhraseTable<String> aPhraseTranslationTable) {
        phraseTranslationTable = aPhraseTranslationTable;
    }

    /**
     * @return the nbestList
     */
    public static NBestList getNbestList() {
        return nbestList;
    }

    /**
     * @param aNbestList the nbestList to set
     */
    public static void setNbestList(NBestList aNbestList) {
        nbestList = aNbestList;
    }

    /**
     * @return the s2tWordTranslationScores
     */
    public static WordTranslationScores getS2tWordTranslationScores() {
        return s2tWordTranslationScores;
    }

    /**
     * @param aS2tWordTranslationScores the s2tWordTranslationScores to set
     */
    public static void setS2tWordTranslationScores(WordTranslationScores aS2tWordTranslationScores) {
        s2tWordTranslationScores = aS2tWordTranslationScores;
    }

    /**
     * @return the t2sWordTranslationScores
     */
    public static WordTranslationScores getT2sWordTranslationScores() {
        return t2sWordTranslationScores;
    }

    /**
     * @param aT2sWordTranslationScores the t2sWordTranslationScores to set
     */
    public static void setT2sWordTranslationScores(WordTranslationScores aT2sWordTranslationScores) {
        t2sWordTranslationScores = aT2sWordTranslationScores;
    }

    /**
     * @return the phraseTranslationTablePath
     */
    public static String getPhraseTranslationTablePath() {
        return phraseTranslationTablePath;
    }

    /**
     * @param aPhraseTranslationTablePath the phraseTranslationTablePath to set
     */
    public static void setPhraseTranslationTablePath(String aPhraseTranslationTablePath) {
        phraseTranslationTablePath = aPhraseTranslationTablePath;
    }
    
    public static void setSSFProps()
    {
        if(SSF_PROPS_SET)
            return;
        
        FSProperties fsp = new FSProperties();
        SSFProperties ssfp = new SSFProperties();

        try {
            fsp.read(GlobalProperties.resolveRelativePath("props/fs-mandatory-attribs.txt"),
                    GlobalProperties.resolveRelativePath("props/fs-other-attribs.txt"),
                    GlobalProperties.resolveRelativePath("props/fs-props.txt"),
                    GlobalProperties.resolveRelativePath("props/ps-attribs.txt"),
                    GlobalProperties.resolveRelativePath("props/dep-attribs.txt"),
                    GlobalProperties.resolveRelativePath("props/sem-attribs.txt"),
                    GlobalProperties.getIntlString("UTF-8")); //throws java.io.FileNotFoundException;

            //Read SSF properties file.
            ssfp.read("props/ssf-props.txt", "UTF-8"); //throws java.io.FileNotFoundException;

            //Set properties files
            FeatureStructuresImpl.setFSProperties(fsp);
            SSFNode.setSSFProperties(ssfp);
            
            SSF_PROPS_SET = true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();        
        }
    }
    
    public static void loadResources(boolean reload)
    {
        // WordNet
        loadWordNet(reload);

        // Collin's Parser
        loadCollinsParser(reload);
        
        // Phrase translation table
        loadPhraseTranslationTable(reload);
        
        //NBest list
        loadNBestList(reload);
        
        //WordTranslationScores
        loadWordTranslationScores(reload);
    }
    
    public static void loadWordNet(boolean reload)
    {
        if(wordnet != null && reload == false)
            return;
        
        System.setProperty("wordnet.database.dir", getWordnetPath());
        setWordnet(WordNetDatabase.getFileInstance());              
    }
    
    public static void loadCollinsParser(boolean reload)
    {
        if(collinsParser != null && reload == false)
            return;
        
        setCollinsParser(null);

        try {
            Settings.load(getCollinsParsetSettingPath());
            
            try {
                setCollinsParser(new Parser(getCollinsParsetDataPath()));
            } catch (RemoteException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(Temp.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    public static void loadPhraseTranslationTable(boolean reload)
    {
        if(phraseTranslationTable != null && reload == false)
            return;
        
//        long startTimeMillis = System.currentTimeMillis();
        System.out.printf("Loading phrase table: %s\n", getPhraseTranslationTablePath());
        
        try {
            setPhraseTranslationTable(new FlatPhraseTable<String>(null, null, getPhraseTranslationTablePath()));
        } catch (IOException ex) {
            Logger.getLogger(CEResources.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loadNBestList(boolean reload)
    {
        if(nbestList != null && reload == false)
            return;
    
        System.out.println("Loading the n-best list...");
        setNbestList(new NBestList(false, Formats.MOSES_NBEST_FORMAT, getNbestListPath(), "UTF-8"));
        
//        System.out.println(nbestList.printMosesFormat());
    }
    
    public static void loadWordTranslationScores(boolean reload)
    {
        if(getS2tWordTranslationScores() == null || reload == true)
            setS2tWordTranslationScores(new WordTranslationScores(getSrc2TgtWordTranslationScoresPath(), CEAnnotatedCorpusWMT12.getEncoding()));
        
        if(getT2sWordTranslationScores() == null || reload == true)
            setT2sWordTranslationScores(new WordTranslationScores(getTgt2SrcWordTranslationScoresPath(), CEAnnotatedCorpusWMT12.getEncoding()));
    }
}
