/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import ml.options.OptionSet;
import ml.options.Options;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.resources.CEResources;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;
import sanchay.corpus.ssf.tree.SSFNode;

/**
 *
 * @author anil
 */
public class CaseUtils {
    
    protected SSFStory text;
    
    protected String textPath;
    protected String charset;
    protected String outPath;
    
    public static int LC = 0;
    public static int UC = 1;
    public static int NC = 2;
    
    public CaseUtils()
    {
        CEResources.setSSFProps();
    }
    
    public void loadText(String inpath, String cs) throws FileNotFoundException, IOException, Exception
    {
        text = new SSFStoryImpl();
        
        text.readFile(inpath, cs);
    }
    
    public void convertToCase(int c)
    {
        int count = text.countSentences();
        
        for (int i = 0; i < count; i++)
        {
            SSFSentence sentence = text.getSentence(i);
            
            List words = sentence.getRoot().getAllLeaves();

            for (Object lexItem : words) {
                if(c == LC)
                {
                    ((SSFNode) lexItem).setLexData(((SSFNode) lexItem).getLexData().toLowerCase());
                }
                else if(c == UC)
                {
                    ((SSFNode) lexItem).setLexData(((SSFNode) lexItem).getLexData().toUpperCase());
                }
                else if(c == NC)
                {
//                    lexItem.setLexData(lexItem.getLexData().toLowerCase());
                }
            }
        }
    }
    
    public void convertToCaseRaw(int c) throws UnsupportedEncodingException, IOException
    {
        LineNumberReader reader;
        PrintStream ps;

        if (textPath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(textPath)), charset));

            ps = new PrintStream(new GZIPOutputStream(
                    new FileOutputStream(outPath)), true, charset);
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    textPath), charset));

            ps = new PrintStream(outPath, charset);
        }
        
        int s = 0;
       
        for (String inLine; (inLine = reader.readLine()) != null;)
        {                
            System.err.printf("Processing sentence: " + ++s + "\r");
            
            inLine = inLine.trim();
        
            if(c == LC)
            {
                inLine = inLine.toLowerCase();
            }
            else if(c == UC)
            {
                inLine = inLine.toUpperCase();
            }
            else if(c == NC)
            {
//                inLine = inLine.toLowerCase();
            }
            
            ps.println(inLine);
        }    
        
        reader.close();
        ps.close();
    }

    public void convertToCase(String inpath, String outpath,
            String outSuffix, String cs, int toCase, boolean raw) throws FileNotFoundException, IOException, Exception
    {
        File inFile = new File(inpath);
        File outFile = new File(outpath);
        
        if(inFile.isDirectory())
        {
            if(outFile.exists() == false)
            {
                outFile.mkdirs();
            }
            
            File inFiles[] = inFile.listFiles();
            
            for (File ifile : inFiles) {
                File ofile = new File(outFile, ifile.getName() + outSuffix);
                
                convertToCase(ifile.getAbsolutePath(),
                        ofile.getAbsolutePath(), outSuffix, cs, toCase, raw);
            }
        }
        else
        {
            if(!raw)
            {
                loadText(inpath, cs);
                
                convertToCase(toCase);

                text.saveRawText(outpath, cs);
            }
            else
            {
                textPath = inpath;
                charset = cs;
                outPath = outpath + outSuffix;
                
                convertToCaseRaw(toCase);
            }
        }
    }
    
    public static int getCase(String c)
    {
        if(c.equalsIgnoreCase("lc"))
            return LC;
        if(c.equalsIgnoreCase("uc"))
            return UC;
        
        return NC;
    }
    
    public static String getCaseName(int c)
    {
        if(c == LC)
            return "LC";
        if(c == UC)
            return "UC";
        
        return "NC";
    }
   
    public static void main(String args[])
    {
        Options opt = new Options(args, 1);

        opt.addSet("convset", 1).addOption("conv").addOption("c", Options.Multiplicity.ZERO_OR_ONE);

        opt.addOptionAllSets("v", Options.Multiplicity.ZERO_OR_ONE);

        opt.getSet("convset").addOption("case", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("convset").addOption("cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("convset").addOption("f", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("convset").addOption("o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet("convset").addOption("os", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String toCase = "LC";
        String charset = "UTF-8";
        String format = "raw";
        String infile = "";
        String outfile = "";
        String outSuffix = ".lc";
        boolean raw = false;

//        if (!opt.check()) {
//            System.out.println("CaseUtils command [options]");
//            System.out.println("CaseUtils -conv [-case toCase] [-cs charset] [-o outfile] [-os outSuffix] [infilesrc]");
//            System.out.println(opt.getCheckErrors());
//            System.exit(1);
//        }
        
        OptionSet set = opt.getMatchingSet();        

        System.out.format("Set matched %s\n", set.getSetName());

        if(set.getSetName().equals("convset"))
        {
            if (set.isSet("case")) {
                toCase = set.getOption("case").getResultValue(0);
                System.out.println("To case " + toCase);
            }

            if (set.isSet("cs")) {
                charset = set.getOption("cs").getResultValue(0);
                System.out.println("Charset " + charset);
            }

            if (set.isSet("f")) {
                format = set.getOption("f").getResultValue(0);
                System.out.println("Format " + format);
                
                if(format.equalsIgnoreCase("raw"))
                    raw = true;
            }

            if (set.isSet("o")) {
                outfile = set.getOption("o").getResultValue(0);  
                System.out.println("Output file " + outfile);
            }

            if (set.isSet("os")) {
                outSuffix = set.getOption("os").getResultValue(0);  
                System.out.println("Output suffix: " + outSuffix);
            }

            infile = set.getData().get(0);
            System.out.println("Input file " + infile);

            CEAnnotatedCorpusWMT12.setSourcePath(infile);
        }
        
        try {            
            CaseUtils caseUtils = new CaseUtils();
            
            caseUtils.convertToCase(infile, outfile, outSuffix, charset, getCase(toCase), raw);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
