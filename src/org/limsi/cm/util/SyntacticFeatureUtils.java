/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import edu.stanford.nlp.ling.CoreLabel;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.tree.SSFLexItem;
import sanchay.corpus.ssf.tree.SSFNode;
import sanchay.corpus.ssf.tree.SSFPhrase;
import org.limsi.types.FeatureSubTypesEnum;
import sanchay.tree.SanchayMutableTreeNode;

/**
 *
 * @author anil
 */
public class SyntacticFeatureUtils {
    
    public static int countSyntacticCategory(SSFPhrase node, FeatureSubTypesEnum category, TagSet tagset)
    {
        List leaves = node.getAllLeaves();
        
        int lcount = leaves.size();

        int count = 0;
        
        for (int i = 0; i < lcount; i++)
        {
            String tag = ((SSFNode) leaves.get(i)).getName();

            if(category.equals(FeatureSubTypesEnum.POS_NOUN_COUNT))
            {
                if(isNoun(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_VERB_COUNT))
            {
                if(isVerb(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_MODIFIER_COUNT))
            {
                if(isModifier(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_WH_COUNT))
            {
                if(isWH(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_NUMBER_WORD_COUNT))
            {
                if(isNumber(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_FUNCTION_WORD_COUNT))
            {
                if(isFunctionWord(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_SYMBOL_COUNT))
            {
                if(isSymbol(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_PREPOSITION_COUNT))
            {
                if(isPreposition(tag, tagset)) {
                    count++;
                }
            }
            else if(category.equals(FeatureSubTypesEnum.POS_PRONOUN_COUNT))
            {
                if(isPronoun(tag, tagset)) {
                    count++;
                }
            }
        }
        
        return count;
    }

    public static boolean isNoun(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("NN") || tag.startsWith("NP")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("NOM") || tag.startsWith("NAM")
                     || tag.startsWith("ABR")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("ACRNM") || tag.startsWith("NC")
                     || tag.startsWith("NM")|| tag.startsWith("NP")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isPronoun(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("PP")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("PRO")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("DM") || tag.startsWith("PP")
                     || tag.startsWith("REL")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isVerb(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("VB")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("VER")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("V")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isModifier(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("JJ") || tag.startsWith("RB")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("ADJ") || tag.startsWith("ADV")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("AD") || tag.startsWith("ORD")
                     || tag.startsWith("QU")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isWH(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("W")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("INT")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isNumber(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.equals("CD")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("NUM")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_SP))
        {
            if(tag.startsWith("CARD") || tag.startsWith("CODE")) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isSymbol(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.startsWith("SYM")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("PUN") || tag.startsWith("SYM") || tag.startsWith("SENT")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("BACKSLASH") || tag.startsWith("CM")
                     || tag.startsWith("CODE") || tag.startsWith("COLON")
                     || tag.startsWith("DASH") || tag.startsWith("FO")
                     || tag.startsWith("FS") || tag.startsWith("LP")
                     || tag.startsWith("PERCT") || tag.startsWith("QT")
                     || tag.startsWith("RP") || tag.startsWith("SEMICOLON")
                     || tag.startsWith("SLASH") || tag.startsWith("SYM")) {
                return true;
            }
        }
        
        return false;
    }
    
    public static boolean isPreposition(String tag, TagSet tagset)
    {
        if(tagset.equals(TagSet.PTB_POS))
        {
            if(tag.equals("IN")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("PRP")) {
                return true;
            }
        }
        else if(tagset.equals(TagSet.TT_POS_FR))
        {
            if(tag.startsWith("PREP")) {
                return true;
            }
        }
        
        return false;        
    }

    public static boolean isFunctionWord(String tag, TagSet tagset)
    {
        if(!isNoun(tag, tagset) && !isVerb(tag, tagset) && !isModifier(tag, tagset)
                && !isWH(tag, tagset) && !isNumber(tag, tagset)
                && !isPronoun(tag, tagset) && !isPreposition(tag, tagset)) {
            return true;
        }
        
        return false;
    }
    
    public static void ssf2POSNoLex(SSFStory story, String savePath, String encoding)
    {
        int scount = story.countSentences();
        
        PrintStream ps = null;
        
        try {
            ps = new PrintStream(savePath, encoding);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SyntacticFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SyntacticFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < scount; i++)
        {
            String sentenceStr = ssf2POSNoLex(story.getSentence(i));
            
            ps.println(sentenceStr);
        }
        
        ps.close();        
    }

    public static String ssf2POSNoLex(SSFSentence sentence)
    {
        SSFPhrase root = sentence.getRoot();

        List leaves = root.getAllLeaves();

        String sentenceStr = "";

        for (int j = 0; j < leaves.size(); j++)
        {
            SSFLexItem lex = (SSFLexItem) leaves.get(j);

            sentenceStr += lex.getName() + " ";
        }

        sentenceStr = sentenceStr.trim();
               
        return sentenceStr;
    }
    
    public static List<CoreLabel> ssf2CoreLabel(SSFSentence sentences)
    {
        List<SanchayMutableTreeNode> words = sentences.getRoot().getAllLeaves();

        List<CoreLabel> coreLabels = new ArrayList<CoreLabel>(words.size());
        
        for (SanchayMutableTreeNode word : words) {

            CoreLabel coreLabel = new CoreLabel();
            
            coreLabel.setValue((String) word.getUserObject());
            coreLabel.setWord((String) word.getUserObject());
            
            coreLabels.add(coreLabel);
        }
        
        return coreLabels;
    }

    public static List<String> ssf2StringList(SSFSentence sentence)
    {
        List<SanchayMutableTreeNode> words = sentence.getRoot().getAllLeaves();
        
        List<String> strings = new ArrayList<String>(words.size());
        
        for (SanchayMutableTreeNode word : words) {
                       
            strings.add((String) word.getUserObject());
        }
        
        return strings;
    }
    
    public static void copyPOSTags(SSFSentence srcSentence, SSFSentence destSentence) throws Exception
    {
        List<SanchayMutableTreeNode> srcWords = srcSentence.getRoot().getAllLeaves();
        List<SanchayMutableTreeNode> destWords = destSentence.getRoot().getAllLeaves();
        
        if(srcWords.size() != destWords.size())
        {
            throw new Exception("Copying POS tags: Source and destination sentence don't have the same number of words.");
        }

        int count = srcWords.size();

        for (int i = 0; i < count; i++) {
            ((SSFNode) destWords.get(i)).setName(((SSFNode) srcWords.get(i)).getName());
        }
    }
}
