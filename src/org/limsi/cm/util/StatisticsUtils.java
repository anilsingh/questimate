/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import java.util.List;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author anil
 */
public class StatisticsUtils {
    public static DescriptiveStatistics getDescriptiveStatistics(List<Double> values)
    {
        DescriptiveStatistics dstat = new DescriptiveStatistics();
        
        int count = values.size();
        
        for (int i = 0; i < count; i++) {
            dstat.addValue(values.get(i));
        }
        
        return dstat;
    }
    
    public static Double[] getDoubleArray(double[] values)
    {
        Double[] dvalues = new Double[values.length];
        
        for (int i = 0; i < values.length; i++) {
            double d = values[i];
            dvalues[i] = d;
        }
        
        return dvalues;
    }
}
