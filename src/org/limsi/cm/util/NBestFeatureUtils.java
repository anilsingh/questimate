/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import edu.stanford.nlp.mt.base.IString;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.corpus.ssf.SSFStory;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.resources.NBestList;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.WordAlignment;
import sanchay.properties.PropertyTokens;

/**
 *
 * @author anil
 */
public class NBestFeatureUtils {

    public static void filterNBestList(NBestList nbestList, PropertyTokens targetCorpus)
    {
        List<List<TranslationHypothesis<IString, String>>> transHypLists = nbestList.getNbestLists();

        List<List<TranslationHypothesis<IString, String>>> remHypLists = new LinkedList<List<TranslationHypothesis<IString, String>>>();
        
        Iterator<List<TranslationHypothesis<IString, String>>> itr = transHypLists.iterator();
        
        int i = 0;
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> transHypList = itr.next();

            if(transHypList.isEmpty())
            {
                remHypLists.add(transHypList);
            }
            else
            {
                String transHyp = transHypList.get(0).getTranslation().toString();

                String sentence = targetCorpus.getToken(i);

                if(transHyp.equalsIgnoreCase(sentence) == false)
                {
                    remHypLists.add(transHypList);

                    System.err.println("Removing hypothesis:");
                    System.err.println(transHyp);
                    System.err.println("Does not match with:");
                    System.err.println(sentence);
                }
            }

            i++;    
        }
        
        itr = remHypLists.iterator();
        
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> transHypList = itr.next();

            transHypLists.remove(transHypList);
        }
    }
    
    public static void extractWordAlignmentsFromNBestList(NBestList nbestList,
            String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(outFilePath, encoding);        
        
        List<List<TranslationHypothesis<IString, String>>> lists = nbestList.getNbestLists();
        
        Iterator<List<TranslationHypothesis<IString, String>>> itr = lists.iterator();
        
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> list = itr.next();
            
            TranslationHypothesis<IString, String> th = list.get(0);
            
            WordAlignment wa = th.getWordAlignment();
            
            ps.println(wa.getHyphenatedString());
        }
        
        ps.close();
    }
    
    public static void prepareSourceCorpusForNBestPairs(String srcCorpusPath, NBestList nbestList,
            String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        PrintStream ps = new PrintStream(outFilePath, encoding);        
        
        List<List<TranslationHypothesis<IString, String>>> lists = nbestList.getNbestLists();
        
        Iterator<List<TranslationHypothesis<IString, String>>> itr = lists.iterator();

        PropertyTokens srcCorpus = new PropertyTokens(srcCorpusPath, encoding);
        
        int srcIndex = 0;
        
        while(itr.hasNext())
        {
            List<TranslationHypothesis<IString, String>> list = itr.next();
        
            Iterator<TranslationHypothesis<IString, String>> itr1 = list.iterator();

            while(itr1.hasNext())
            {
                itr1.next();
                ps.println(srcCorpus.getToken(srcIndex));
            }
            
            srcIndex++;
        }
        
        ps.close();        
    }
    
    public static void main(String args[])
    {
//        CEResources.loadNBestList(false);
//
//        NBestList nbestList = CEResources.getNbestList();
//        try {
//            extractWordAlignmentsFromNBestList(nbestList, "/people/anil/work/wmt12-quality-estimation/training-word-alignments.txt", "UTF-8");
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
//        }

        CEResources.setNbestListPath("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2009.NBEST");
        CEResources.loadNBestList(false);

        NBestList nbestList = CEResources.getNbestList();
        
        try {
            prepareSourceCorpusForNBestPairs("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2009.fr",
                nbestList, "/people/anil/work/confidence-estimation/extracted-features/artem/newstest2009-per-hyp.fr", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        CEResources.setNbestListPath("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2010.NBEST");
        CEResources.loadNBestList(true);

        nbestList = CEResources.getNbestList();
        
        try {
            prepareSourceCorpusForNBestPairs("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2010.fr",
                nbestList, "/people/anil/work/confidence-estimation/extracted-features/artem/newstest2010-per-hyp.fr", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        CEResources.setNbestListPath("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2011.NBEST");
        CEResources.loadNBestList(true);

        nbestList = CEResources.getNbestList();
        
        try {
            prepareSourceCorpusForNBestPairs("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2011.fr",
                nbestList, "/people/anil/work/confidence-estimation/extracted-features/artem/newstest2011-per-hyp.fr", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        CEResources.setNbestListPath("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2012.NBEST");
        CEResources.loadNBestList(true);

        nbestList = CEResources.getNbestList();
        
        try {
            prepareSourceCorpusForNBestPairs("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2012.fr",
                nbestList, "/people/anil/work/confidence-estimation/extracted-features/artem/newstest2012-per-hyp.fr", "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NBestFeatureUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
