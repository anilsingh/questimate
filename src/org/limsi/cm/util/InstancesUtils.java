/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import sanchay.util.Pair;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class InstancesUtils {
   public static double[] getValues(Instances instances, Attribute a)
   {
       int count = instances.numInstances();
       
       double[] values = new double[count];
       
       for (int i = 0; i < count; i++) {
           values[i] = instances.instance(i).value(a);
       }
       
       return values;
   }

   /**
    * Excluded the class value
    * @param instances
    * @param a
    * @return 
    */
   public static float[] getFeatureValues(Instance instance, Attribute classAttribute)
   {
       int count = instance.numAttributes();
       
       float featureValues[] = new float[count - 1];
       
       for (int i = 0, j = 0; i < featureValues.length; i++, j++) {

           Attribute a = instance.attribute(i);
           
           if(a.isNumeric() && !instance.attribute(i).name().equals(classAttribute.name()))
           {
               featureValues[j] = ((new Double(instance.value(a)).floatValue()));
           }
           else
           {
               j--;
           }
       }
       
       return featureValues;
   }

   public static double[] getValues(Instances instances, Attribute a, Attribute condition, String value)
   {
       int count = instances.numInstances();
       
       List<Double> valuesList = new ArrayList(count);
       
       for (int i = 0; i < count; i++) {
           
           Instance instance = instances.instance(i);
           
           if(condition.isNominal() || condition.isString())
           {
               if(instance.stringValue(condition).equals(value))
               {
                   valuesList.add(instance.value(a));
               }
           }
           else if(condition.isNumeric())
           {
               if(instance.value(condition) == Double.parseDouble(value))
               {
                   valuesList.add(instance.value(a));
               }
           }
       }
       
       count = valuesList.size();
       
       double[] values = new double[count];
       
       for (int i = 0; i < values.length; i++) {
           values[i] = valuesList.get(i);
       }
       
       return values;       
   }
   
   public static Index<String> getAttributeNames(Instances instances)
   {
       Index names = new HashIndex();
       
       int count = instances.numAttributes();
       
       for (int i = 0; i < count; i++) {
           names.add(instances.attribute(i).name());
       }
       
       return names;
   }
   
   public static Pair<List<Attribute>, List<Attribute>> getAttributeDiff(Instances instancesLeft, Instances instancesRight)
   {
       Pair<List<Attribute>, List<Attribute>> attributeDiff = new Pair();
       
       List<Attribute> onlyInLeft = new ArrayList<Attribute>();
       List<Attribute> onlyInRight = new ArrayList<Attribute>();

       Index<String> attribNamesLeft = getAttributeNames(instancesLeft);
       Index<String> attribNamesRight = getAttributeNames(instancesRight);
       
       Iterator<String> itr = attribNamesLeft.iterator();
       
       while(itr.hasNext())
       {
           String name = itr.next();
           
           if(attribNamesRight.indexOf(name) == -1)
           {
               onlyInLeft.add(instancesLeft.attribute(name));
           }
       }
       
       itr = attribNamesRight.iterator();
       
       while(itr.hasNext())
       {
           String name = itr.next();
           
           if(attribNamesLeft.indexOf(name) == -1)
           {
               onlyInLeft.add(instancesRight.attribute(name));
           }
       }
       
       attributeDiff.first = onlyInLeft;
       attributeDiff.second = onlyInRight;
       
       return attributeDiff;
   }          
   
   public static boolean[] getSelectedList(List<Attribute> selected, Instances instances)
   {
       int tcount = instances.numAttributes();
       Index<String> names = getAttributeNames(instances);
       
       boolean[] booleanSelected = new boolean[tcount];
       
       int scount = selected.size();

       for (int i = 0; i < scount; i++) {
           Attribute a = selected.get(i);
           
           int ind = names.indexOf(a.name());

            if(ind != -1)
            {
                booleanSelected[ind] = true;
            }
       }
       
       return booleanSelected;
   }
}
