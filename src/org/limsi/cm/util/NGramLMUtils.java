/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import edu.berkeley.nlp.lm.ArrayEncodedProbBackoffLm;
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.map.NgramMap;
import edu.berkeley.nlp.lm.values.ProbBackoffPair;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sanchay.mlearning.lm.ngram.NGram;
import sanchay.mlearning.lm.ngram.NGramCount;
import sanchay.mlearning.lm.ngram.NGramLM;
import sanchay.mlearning.lm.ngram.impl.NGramCounts;
import sanchay.mlearning.lm.ngram.impl.NGramImpl;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class NGramLMUtils {
    
    public static int countNumberOfBackoffs(NGramCounts sentenceLM, int order,
            NgramLanguageModel<String> berkeleyLM,
            WordIndexer<String> wordIndexer)
    {
        int count = 0;
        
        for (int i = 1; i <= order; i++) {
            count += countNumberOfBackoffs(sentenceLM, i, wordIndexer);
        }
        
        return count;
    }

    public static int countNumberOfBackoffs(NGramLM sentenceLM, int order,
            ArrayEncodedProbBackoffLm<String> berkeleyLM,
            WordIndexer<String> wordIndexer)
    {
        NgramMap<ProbBackoffPair> ngramMap = berkeleyLM.getNgramMap();

        int count = 0;
        
        for (int i = 1; i <= order; i++) {
            count += countNumberOfBackoffs(sentenceLM, i, wordIndexer);
        }
        
        return count;
    }

    public static int countNumberOfBackoffs(NGramCounts nglm, int whichGram,
            WordIndexer<String> wordIndexer)
    {
        int count = 0;
        
        Iterator<List<Integer>> itr = nglm.getNGramKeys(whichGram);
        
        while(itr.hasNext())
        {
            List<Integer> indices = itr.next();
            
            NGramCount ng = nglm.getNGram(indices, whichGram);
            
            try {
                
                int[] wdIndices = getNgramIndices(nglm, ng, whichGram, wordIndexer);
                
                count += countBackoffs(wdIndices);
                
            } catch (Exception ex) {
                Logger.getLogger(NGramLMUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return count;
    }
    
    public static int[] getNgramIndices(NGramCounts nglm, NGramCount ng, int whichGram,
            WordIndexer<String> wordIndexer) throws Exception
    {
        int indices[] = new int[whichGram];
        
        List<Integer> ngIndices = ng.getIndices();
        
        String plainString = NGramImpl.getPlainString(nglm, ngIndices);
        
        String wds[] = plainString.split("\\s+");
        
        if(whichGram != wds.length)
        {
            throw new Exception("Error: Ngram order not equal to ngram string length.");
        }
        
        String unk = wordIndexer.getUnkSymbol();
        int unkIndex = wordIndexer.getIndexPossiblyUnk(unk);
        
        for (int i = 0; i < wds.length; i++) {
            String string = wds[i];
            
            int stringIndex = wordIndexer.getIndexPossiblyUnk(string);
           
            if(stringIndex != unkIndex)
            {
                indices[i] = stringIndex;
            }
            else
            {
                indices[i] = -1;
            }
        }
                
        return indices;
    }
    
     public static int countBackoffs(int wdIndices[])
     {
         int b = 0;
         boolean found = false;
         
         for (int i = wdIndices.length - 1; i >=  0; i--) {
             int index = wdIndices[i];
             
             if(!found && index == -1)
             {
                 b++;
             }
             else
             {
                 found = true;
             }
         }
         
         return b;
     }
    
     public static void copyNGramPosteriorProbs(SanchayTableModel ngProbs, NGramLM nglm, int whichGram)
     {
         int rcount = ngProbs.getRowCount();
         
         for (int i = 0; i < rcount; i++) {
             NGram ng = new NGramImpl();
             
             String ngStr = (String) ngProbs.getValueAt(i, 0);
             double prob = Double.parseDouble((String) ngProbs.getValueAt(i, 2));
             
             ng.setProb(prob);
             
             nglm.addNGram(ngStr.replaceAll("_\\|_", "@#&"), ng, whichGram);
         }
     }     
}
