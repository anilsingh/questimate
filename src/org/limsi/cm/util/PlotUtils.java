/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import no.uib.jsparklines.renderers.util.AreaRenderer;
import org.apache.commons.math3.stat.Frequency;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.rank.Max;
import org.apache.commons.math3.stat.descriptive.rank.Min;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.jfree.ui.TextAnchor;
import org.limsi.mt.cm.stats.NormalKernelDensityEstimator;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class PlotUtils {
    public static JFreeChart createPieChart(Instances instances, Attribute x) {

        DefaultPieDataset piedataset = new DefaultPieDataset();
        
        Frequency frequencies = new Frequency();

        int count = instances.numInstances();

        for (int i = 0; i < count; i++) {
            Instance instance = instances.instance(i);

            frequencies.addValue(instance.value(x));
        }
        
        Iterator valuesIterator = frequencies.valuesIterator();
        
        while(valuesIterator.hasNext())
        {
            Double val = (Double) valuesIterator.next();
            
            piedataset.setValue(val, frequencies.getCount(val));
        }
        
       JFreeChart plot = ChartFactory.createPieChart("Pie Chart Demo 2", piedataset, true, true, false);
        
       PiePlot pieplot = (PiePlot)plot.getPlot();

        valuesIterator = frequencies.valuesIterator();
        
        Random random = new Random();
        
        while(valuesIterator.hasNext())
        {
            Double val = (Double) valuesIterator.next();
            
            int r = (int) Math.round(random.nextDouble() * 255);
            int g = (int) Math.round(random.nextDouble() * 255);
            int b = (int) Math.round(random.nextDouble() * 255);
            
            pieplot.setSectionPaint("" + val, new Color(r, g, b));
        }

        pieplot.setNoDataMessage("No data available");
//        pieplot.setExplodePercent("Two", 0.5D);
        pieplot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({2} percent)"));
        pieplot.setLabelBackgroundPaint(new Color(220, 220, 220));
        pieplot.setLegendLabelToolTipGenerator(new StandardPieSectionLabelGenerator("Tooltip for legend item {0}"));
        
        return plot;
    }

    public static JFreeChart createStatisticalBarChart(Instances instances, Attribute x, Attribute y) {

        DefaultStatisticalCategoryDataset dataset = new DefaultStatisticalCategoryDataset();

        double[] xValues = InstancesUtils.getValues(instances, x);
        double[] yValues = InstancesUtils.getValues(instances, y);

        LinkedHashMap<Double, List<Double>> yForX = PlotUtils.getYForX(xValues, yValues, 0);

        Iterator<Double> itr = yForX.keySet().iterator();

        while (itr.hasNext()) {
            double xval = itr.next();

            List<Double> yList = yForX.get(xval);
 
            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(yList);
            
            Number mean = dstat.getMean();
            Number sd = dstat.getStandardDeviation();
            
            dataset.add(mean, sd, x.name(), xval);
        }
        
        JFreeChart plot = ChartFactory.createLineChart("Statistical Bar Chart", "Type", "Value", dataset, PlotOrientation.VERTICAL, true, true, false);
        
        plot.setBackgroundPaint(Color.white);
        
        CategoryPlot categoryplot = (CategoryPlot)plot.getPlot();
        
        categoryplot.setBackgroundPaint(Color.lightGray);
        categoryplot.setRangeGridlinePaint(Color.white);
        
        NumberAxis numberaxis = (NumberAxis)categoryplot.getRangeAxis();
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        numberaxis.setAutoRangeIncludesZero(false);
        
        StatisticalBarRenderer statisticalbarrenderer = new StatisticalBarRenderer();
        
        statisticalbarrenderer.setErrorIndicatorPaint(Color.black);
        statisticalbarrenderer.setIncludeBaseInRange(false);
        
        categoryplot.setRenderer(statisticalbarrenderer);

        statisticalbarrenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        statisticalbarrenderer.setBaseItemLabelsVisible(true);
        statisticalbarrenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));

        return plot;
    }
    
    public static JFreeChart createScatterPlot(Instances instances, Attribute x, Attribute y) {
        XYSeriesCollection dataset = new XYSeriesCollection();

        XYSeries series = new XYSeries(x.name() + ":" + y.name(), true, false);

        int count = instances.numInstances();

        for (int i = 0; i < count; i++) {
            Instance instance = instances.instance(i);

            Number xn = instance.value(x);
            Number yn = instance.value(y);

            series.addOrUpdate(xn, yn);
        }

        dataset.addSeries(series);

       JFreeChart plot = ChartFactory.createScatterPlot("Scatter Plot", x.name(), y.name(), dataset, PlotOrientation.VERTICAL, true, false, false);

       XYPlot xyplot = (XYPlot)plot.getPlot();
       
        xyplot.setNoDataMessage("NO DATA");
//        xyplot.setDomainZeroBaselineVisible(true);
//        xyplot.setRangeZeroBaselineVisible(true);
        
        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        
        xylineandshaperenderer.setSeriesOutlinePaint(0, Color.black);
        xylineandshaperenderer.setUseOutlinePaint(true);
        
        NumberAxis numberaxis = (NumberAxis)xyplot.getDomainAxis();
        
        numberaxis.setAutoRangeIncludesZero(false);
        numberaxis.setTickMarkInsideLength(2.0F);
        numberaxis.setTickMarkOutsideLength(0.0F);
        
        NumberAxis numberaxis1 = (NumberAxis)xyplot.getRangeAxis();
        numberaxis1.setTickMarkInsideLength(2.0F);
        numberaxis1.setTickMarkOutsideLength(0.0F);
        
        return plot;
    }

    public static JFreeChart createKernelDesnsityPlot(Instances instances, Attribute x, String typeString) {

        double[] values = InstancesUtils.getValues(instances, x);

        NormalKernelDensityEstimator kernelEstimator = new NormalKernelDensityEstimator();

        ArrayList list = kernelEstimator.estimateDensityFunction(values);

        XYSeriesCollection lineChartDataset = new XYSeriesCollection();
        XYSeries tempSeries = new XYSeries("1");

        double[] xValues = (double[]) list.get(0);
        double[] yValues = (double[]) list.get(1);

        for (int i = 0; i < xValues.length; i++) {
            tempSeries.add(xValues[i], yValues[i]);
        }

        lineChartDataset.addSeries(tempSeries);
        
        Color histogramColor = new Color(110, 196, 97);

        AreaRenderer renderer = new AreaRenderer();
        renderer.setOutline(true);
        renderer.setSeriesFillPaint(0, histogramColor);
        renderer.setSeriesOutlinePaint(0, histogramColor.darker());

        JFreeChart chart = ChartFactory.createXYLineChart(null, x.name(), "Density", lineChartDataset, PlotOrientation.VERTICAL, false, true, false);
        XYPlot plot = chart.getXYPlot();
        plot.setRenderer(renderer);

        return chart;
    }

    public static JFreeChart createHistogram(Instances instances, Attribute x, HistogramType type, String typeString) {
        HistogramDataset dataset = new HistogramDataset();
        dataset.setType(type);

//        XYSeriesCollection splineDataset = new XYSeriesCollection();

//        XYSeries splineSeries = new XYSeries(x.name(), true, false);

        int count = instances.numInstances();

//        double[] values = new double[count];
        double[] values = InstancesUtils.getValues(instances, x);

//        for (int i = 0; i < count; i++) {
//
//            Instance instance = instances.instance(i);
//
//            values[i] = instance.value(x);
//        }

        Percentile percentile = new Percentile(25);
        double low = percentile.evaluate(values);

        percentile = new Percentile(75);
        double high = percentile.evaluate(values);

        double iqr = high - low;

//        double binWidth = 2.0 * iqr * Math.pow(count, (-1.0/3.0));
        double crSize = Math.pow(count, (1.0 / 3.0));

        Max max = new Max();
        Min min = new Min();

//        int binSize = (int) ((max.evaluate(values) - min.evaluate(values)) / crSize);
        double binSize = (3 * iqr) / crSize;

        int binCount = (int) ((max.evaluate(values) - min.evaluate(values)) / binSize);

//        dataset.addSeries(x.name(), values, Math.min(200, binSize));
        
        try
        {
            dataset.addSeries(x.name(), values, Math.max(10, Math.min(200, binCount)));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

//        KernelDensityEstimate kernelDensityEstimate = new KernelDensityEstimate("Gaussian",
//                StatisticsUtils.getDoubleArray(values), 10);
//        
//        KernelDensityEstimatorDistribution distribution = kernelDensityEstimate.getKernelDensityEstimatorDistribution();
//        
//        count = dataset.getItemCount(0);
//        
//        for (int i = 0; i < count; i++) {
//            if(dataset.getY(0, i).intValue() > 1)
//            {
////                splineSeries.add(dataset.getX(0, i), dataset.getY(0, i));
//                splineSeries.add(dataset.getX(0, i), distribution.pdf(dataset.getY(0, i).doubleValue())
//                        * dataset.getY(0, i).doubleValue());
//            }
//        }

//        KernelEstimator kernelEstimator = new KernelEstimator(0.001);
        
//        for (int i = 0; i < count; i++) {
//        for (int i = 0; i < values.length; i++) {
//            double d = values[i];
//            kernelEstimator.addValue(d, 1.0);
//            if(dataset.getY(0, i).intValue() > 0)
//            {
//                kernelEstimator.addValue(dataset.getY(0, i).doubleValue(), 1.0);
//            }
//        }
        
//        double means[] = kernelEstimator.getMeans();
//
//        for (int i = 0; i < means.length; i++) {
//            if(means[i] > 0)
//            {
//                splineSeries.add(i, means[i]);
//            }
//        }

//        splineDataset.addSeries(splineSeries);

        JFreeChart plot = ChartFactory.createHistogram("Histogram", x.name(), typeString, dataset, PlotOrientation.VERTICAL, true, true, false);
        
        XYPlot hplot = plot.getXYPlot();

        hplot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
//        xyplot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
        
//        hplot.setDataset(1, splineDataset);
//        hplot.mapDatasetToRangeAxis(1, 1);
//
//        final ValueAxis domainAxis = hplot.getDomainAxis();
////        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
//        final ValueAxis meanAxis = new NumberAxis("Means");
//        meanAxis.setRange(hplot.getRangeAxis().getRange());
//        hplot.setRangeAxis(1, meanAxis);
//
//        final XYSplineRenderer splineRenderer = new XYSplineRenderer();
////        splineRenderer.setToolTipGenerator(new XYLineToolTipGenerator());
//        hplot.setRenderer(1, splineRenderer);
//        hplot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);        

        ChartUtilities.applyCurrentTheme(plot);

        return plot;
    }

    public static JFreeChart createBoxAndWhiskerChart(Instances instances, Attribute x, Attribute y) {
        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();
        
        double[] xValues = InstancesUtils.getValues(instances, x);
        double[] yValues = InstancesUtils.getValues(instances, y);

        LinkedHashMap<Double, List<Double>> yForX = PlotUtils.getYForX(xValues, yValues, 0);

        Iterator<Double> itr = yForX.keySet().iterator();

        while (itr.hasNext()) {
            double xval = itr.next();

            List<Double> yList = yForX.get(xval);
             
            dataset.add(yList, x.name(), "" + xval);
        }

       JFreeChart plot = ChartFactory.createBoxAndWhiskerChart("Box and Whisker Chart", "Category", "Value", dataset, true);
        
       CategoryPlot categoryplot = (CategoryPlot)plot.getPlot();
        
        plot.setBackgroundPaint(Color.white);
        
        categoryplot.setBackgroundPaint(Color.lightGray);
        categoryplot.setDomainGridlinePaint(Color.white);
        categoryplot.setDomainGridlinesVisible(true);
        categoryplot.setRangeGridlinePaint(Color.white);
        
        NumberAxis numberaxis = (NumberAxis)categoryplot.getRangeAxis();
        
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        return plot;
    }

    public static JFreeChart createXYErrorRendererChart1(Instances instances, Attribute x, Attribute y) {
        XYIntervalSeriesCollection dataset = new XYIntervalSeriesCollection();

        XYIntervalSeries xyintervalseries = new XYIntervalSeries("Series " + x.name());

        double[] xValues = InstancesUtils.getValues(instances, x);
        double[] yValues = InstancesUtils.getValues(instances, y);

        int count = instances.numInstances();

        LinkedHashMap<Double, List<Double>> yForX = PlotUtils.getYForX(xValues, yValues, 0);
        LinkedHashMap<Double, List<Double>> xForY = PlotUtils.getXForYMean(xValues, yForX);

        Iterator<Double> itr = yForX.keySet().iterator();

        while (itr.hasNext()) {
            double xval = itr.next();

            List<Double> yList = yForX.get(xval);
            
            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(yList);
            
            double ymean = dstat.getMean();
            double ysd = dstat.getStandardDeviation();

            List<Double> xList = xForY.get(ymean);
            
            dstat = StatisticsUtils.getDescriptiveStatistics(xList);
            
            double xmean = dstat.getMean();
            double xsd = dstat.getStandardDeviation();
            
            xyintervalseries.add(xmean, xmean - xsd, xmean + xsd, ymean, ymean - ysd, ymean + ysd);
        }

        dataset.addSeries(xyintervalseries);

        NumberAxis xAxis = new NumberAxis(x.name());
        NumberAxis yAxis = new NumberAxis(y.name());

        XYErrorRenderer xyerrorrenderer = new XYErrorRenderer();

        XYPlot xyplot = new XYPlot(dataset, xAxis, yAxis, xyerrorrenderer);

        xyplot.setDomainPannable(true);
        xyplot.setRangePannable(true);
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        JFreeChart plot = new JFreeChart("XY Error Renderer Chart 1", xyplot);

        ChartUtilities.applyCurrentTheme(plot);

        return plot;
    }

    public static JFreeChart createXYErrorRendererChart2(Instances instances, Attribute x, Attribute y) {
        YIntervalSeriesCollection dataset = new YIntervalSeriesCollection();

        YIntervalSeries yintervalseries1 = new YIntervalSeries("Series " + y.name());
//        YIntervalSeries yintervalseries2 = new YIntervalSeries("Series " + y.name());

        XYSeriesCollection splineDataset = new XYSeriesCollection();

        XYSeries splineSeries = new XYSeries(x.name() + ":" + y.name(), true, false);
        
        double[] xValues = InstancesUtils.getValues(instances, x);
        double[] yValues = InstancesUtils.getValues(instances, y);

        LinkedHashMap<Double, List<Double>> yForX = PlotUtils.getYForX(xValues, yValues, 0);

        Iterator<Double> itr = yForX.keySet().iterator();

        while (itr.hasNext()) {
            double xval = itr.next();

            List<Double> yList = yForX.get(xval);
            
//            BoxAndWhiskerItem bwItem = BoxAndWhiskerCalculator.calculateBoxAndWhiskerStatistics(yList, true);
 
            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(yList);
            
            double mean = dstat.getMean();
            double sd = dstat.getStandardDeviation();
            
            yintervalseries1.add(xval, mean, mean - sd, mean + sd);
//            yintervalseries2.add(instance.value(y));
            
            splineSeries.add(xval, mean);
        }
        
        dataset.addSeries(yintervalseries1);
//        dataset.addSeries(yintervalseries2);
        
        splineDataset.addSeries(splineSeries);

        NumberAxis xAxis = new NumberAxis(x.name());
        NumberAxis yAxis = new NumberAxis(y.name());

        XYErrorRenderer xyerrorrenderer = new XYErrorRenderer();

        XYPlot xyplot = new XYPlot(dataset, xAxis, yAxis, xyerrorrenderer);

        xyplot.setDomainPannable(true);
        xyplot.setRangePannable(true);
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        JFreeChart plot = new JFreeChart("XY Error Renderer Chart 2", xyplot);
        
        xyplot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
//        xyplot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);

        xyplot.setDataset(1, splineDataset);
        xyplot.mapDatasetToRangeAxis(1, 1);

        final ValueAxis domainAxis = xyplot.getDomainAxis();
//        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
        final ValueAxis meanAxis = new NumberAxis("Means");
        meanAxis.setRange(yAxis.getRange());
        xyplot.setRangeAxis(1, meanAxis);

        final XYSplineRenderer splineRenderer = new XYSplineRenderer();
//        splineRenderer.setToolTipGenerator(new XYLineToolTipGenerator());
        xyplot.setRenderer(1, splineRenderer);
        xyplot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);        

        ChartUtilities.applyCurrentTheme(plot);

        return plot;
    }

    public static JFreeChart createXYErrorRendererChart3(Instances instances, Attribute x, Attribute y) {
        YIntervalSeriesCollection dataset = new YIntervalSeriesCollection();

        YIntervalSeries xintervalseries = new YIntervalSeries("Series " + x.name());
        YIntervalSeries yintervalseries = new YIntervalSeries("Series " + y.name());

//        XYSeriesCollection splineDataset = new XYSeriesCollection();
//
//        XYSeries xSplineSeries = new XYSeries(y.name() + ":" + x.name(), true, false);
//        XYSeries ySplineSeries = new XYSeries(x.name() + ":" + y.name(), true, false);
        
        double[] xValues = InstancesUtils.getValues(instances, x);
        double[] yValues = InstancesUtils.getValues(instances, y);

        LinkedHashMap<Double, List<Double>> yForX = PlotUtils.getYForX(xValues, yValues, 0);

        Iterator<Double> itr = yForX.keySet().iterator();

        while (itr.hasNext()) {
            double xval = itr.next();

            List<Double> yList = yForX.get(xval);

            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(yList);
            
            double mean = dstat.getMean();
            double sd = dstat.getStandardDeviation();
            
            yintervalseries.add(xval, mean, mean - sd, mean + sd);
            
//            ySplineSeries.add(xval, mean);
        }
        
        dataset.addSeries(yintervalseries);
        
//        splineDataset.addSeries(ySplineSeries);

        LinkedHashMap<Double, List<Double>> xForY = PlotUtils.getYForX(yValues, xValues, 0);

        itr = xForY.keySet().iterator();

        while (itr.hasNext()) {
            double yval = itr.next();

            List<Double> xList = xForY.get(yval);

            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(xList);
            
            double mean = dstat.getMean();
            double sd = dstat.getStandardDeviation();
            
            xintervalseries.add(yval, mean, mean - sd, mean + sd);
            
//            xSplineSeries.add(yval, mean);
        }
        
        dataset.addSeries(xintervalseries);
        
//        splineDataset.addSeries(xSplineSeries);

        NumberAxis xAxis = new NumberAxis(x.name());
        NumberAxis yAxis = new NumberAxis(y.name());

        XYErrorRenderer xyerrorrenderer = new XYErrorRenderer();

        XYPlot xyplot = new XYPlot(dataset, xAxis, yAxis, xyerrorrenderer);

        xyplot.setDomainPannable(true);
        xyplot.setRangePannable(true);
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        JFreeChart plot = new JFreeChart("XY Error Renderer Chart 3", xyplot);
        
//        xyplot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
////        xyplot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
//
//        xyplot.setDataset(1, splineDataset);
//        xyplot.mapDatasetToRangeAxis(1, 1);
//
//        final ValueAxis domainAxis = xyplot.getDomainAxis();
////        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
//        final ValueAxis yMeanAxis = new NumberAxis("Means");
//        yMeanAxis.setRange(yAxis.getRange());
//        xyplot.setRangeAxis(1, yMeanAxis);
//        
//        final XYSplineRenderer ysplineRenderer = new XYSplineRenderer();
////        splineRenderer.setToolTipGenerator(new XYLineToolTipGenerator());
//        xyplot.setRenderer(1, ysplineRenderer);
//
//        ChartUtilities.applyCurrentTheme(plot);

        return plot;
    }

    public static LinkedHashMap<Double, List<Double>> getYForX(double[] xValues, double[] yValues, int minCount) {
        LinkedHashMap<Double, List<Double>> yForX = new LinkedHashMap<Double, List<Double>>();

        for (int i = 0; i < xValues.length; i++) {
            double x = xValues[i];
            double y = yValues[i];

            List yList = yForX.get(x);

            if (yList == null) {
                yList = new ArrayList<Double>();
                yForX.put(x, yList);
            }

            yList.add(y);
        }

        if (minCount > 0) {
            Iterator<Double> itr = yForX.keySet().iterator();

            while (itr.hasNext()) {
                double x = itr.next();

                List yList = yForX.get(x);

                if (yList.size() <= minCount) {
                    yForX.remove(x);
                }
            }
        }

        return yForX;
    }

    public static LinkedHashMap<Double, List<Double>> getXForYMean(double[] xValues,
            LinkedHashMap<Double, List<Double>> yForX) {

        LinkedHashMap<Double, List<Double>> xForY = new LinkedHashMap<Double, List<Double>>();

        for (int i = 0; i < xValues.length; i++) {
            
            double x = xValues[i];

            List<Double> yList = yForX.get(x);
 
            DescriptiveStatistics dstat = StatisticsUtils.getDescriptiveStatistics(yList);

            double yMean = dstat.getMean();

            List xList = xForY.get(yMean);

            if (xList == null) {
                xList = new ArrayList<Double>();
                xForY.put(yMean, xList);
            }

            xList.add(x);
        }

        return xForY;
    }
}
