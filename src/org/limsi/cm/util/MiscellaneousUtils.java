/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.corpora.CEAnnotatedCorpus;
import org.limsi.cm.corpora.CEAnnotatedSentence;
import org.limsi.types.FeatureType;
import org.limsi.types.Language;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.corpus.ssf.tree.SSFNode;
import sanchay.properties.KeyValueProperties;
import sanchay.properties.PropertyTokens;

/**
 *
 * @author anil
 */
public class MiscellaneousUtils {
    public static double averageTokenLength(SSFSentence sentence)
    {
        double avg = 0.0;
        
        List leaves = sentence.getRoot().getAllLeaves();
        
        int count = leaves.size();
        
        for (int i = 0; i < count; i++) {
            avg += ((SSFNode) leaves.get(i)).getLexData().length();
        }
        
        avg /= (double)count;
        
        return avg;
    }

    public static int numOfPunctuations(SSFSentence sentence)
    {
        int num = 0;
        
        List leaves = sentence.getRoot().getAllLeaves();
        
        int count = leaves.size();
        
        for (int i = 0; i < count; i++) {
            
            String lex = ((SSFNode) leaves.get(i)).getLexData();
            
            if(lex.equals(",") || lex.equals(";") || lex.equals(":") || lex.equals("\"")
                    || lex.equals("\'") || lex.equals("(") || lex.equals(")")
                    || lex.equals("[") || lex.equals("]") || lex.equals("/")
                    || lex.equals(".") || lex.equals("!") || lex.equals("-")
                    || lex.equals("?") || lex.equals("+") || lex.equals("=")
                    || lex.equals("&") || lex.equals("%") || lex.equals("@"))
            {            
                num++;
            }
        }
        
        return num;
    }

    public static int numOfCapitalLetters(SSFSentence sentence)
    {
        int num = 0;
        
        List leaves = sentence.getRoot().getAllLeaves();
        
        int count = leaves.size();
        
        for (int i = 0; i < count; i++) {
            
            String lex = ((SSFNode) leaves.get(i)).getLexData();

            for(int j = 0; j < lex.length(); j++)
            {
                char c = lex.charAt(j);
                
                if(Character.isUpperCase(c))
                {
                    num++;
                }
            }
        }
        
        return num;
    }

    public static double averageTypeTokenRatio(SSFSentence sentence)
    {
        double avg = 0.0;
        
        List leaves = sentence.getRoot().getAllLeaves();
        
        int count = leaves.size();
        
        Index<String> types = new HashIndex();
        
        for (int i = 0; i < count; i++) {
            String wrd = ((SSFNode) leaves.get(i)).getLexData();

            if(types.contains(wrd) == false)
            {
//                avg += tokenCount(sentence, wrd);
                
                types.add(wrd);
            }
        }
        
//        avg /= (double)count;
        avg = (double) types.size() / (double) count;
        
        return avg;
    }

    public static int tokenCount(SSFSentence sentence, String wrd)
    {
        int tcount = 0;
        
        List leaves = sentence.getRoot().getAllLeaves();
        
        int count = leaves.size();
        
        for (int i = 0; i < count; i++) {
            if(((SSFNode) leaves.get(i)).getLexData().equals(wrd)) {
                tcount++;
            }
        }
 
        return tcount;
    }  
    
    public static void main(String args[])
    {
//        String sen = "The big black fox jumper over the little lazy lamb : over the lamb jumped the fox !";
        String sen = "a b a b a b";
        
        SSFSentenceImpl sentence = new SSFSentenceImpl();
        
        try {
            sentence.makeSentenceFromRaw(sen);
        } catch (Exception ex) {
            Logger.getLogger(MiscellaneousUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.format("Avg type/token count: %f\n", averageTypeTokenRatio(sentence));
    }

    public static void saveIndex(Index<String> index, String path, String charset) throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(path, charset);
        
        printIndex(index, ps);
        
        ps.close();
    }
    
    public static void printIndex(Index<String> index, PrintStream ps)
    {
        Iterator<String> itr = index.iterator();
        
        while(itr.hasNext())
        {
            String ind = itr.next();
            
            ps.println(ind);
        }
    }
    
    public static void addSourceSentencesToIndex(CEAnnotatedCorpus c,
            Index index)
    {
        Iterator<String> itr = c.getIDs();
        
        while(itr.hasNext())
        {
            String id = itr.next();
            
            CEAnnotatedSentence s = c.getAnnotatedSentence(id);
            
            index.add(s.getSourceSentence().getString());
        }
    }
    
    public static int numSourceSentencesInIndex(CEAnnotatedCorpus c,
            Index index)
    {
        int num = 0;
        
        Iterator<String> itr = c.getIDs();
        
        while(itr.hasNext())
        {
            String id = itr.next();
            
            CEAnnotatedSentence s = c.getAnnotatedSentence(id);

            if(index.contains(s.getSourceSentence().getString()))
            {
                num++;
            }
        }
        
        return num;
    }
    
    public static String[] trimStrings(String strings[])
    {
        String[] tstrings = new String[strings.length];

        for (int i = 0; i < strings.length; i++) {
            tstrings[i] = strings[i].trim();
        }
        
        return tstrings;
    }
    
    public static Set<String> getSet(String strings[])
    {
        Set<String> set = new LinkedHashSet<String>(strings.length);
        
        for (int i = 0; i < strings.length; i++) {
            String string = strings[i];
            set.add(string);
        }
        
        return set;
    }
    
    public static Index<String> readIndex(String listPath, String cs) throws FileNotFoundException, IOException
    {
        PropertyTokens pt = new PropertyTokens(listPath, cs);
        
        Index<String> index = new HashIndex<String>();
        
        int count = pt.countTokens();
        
        for (int i = 0; i < count; i++) {
            String token = pt.getToken(i);
            
            index.add(token);
        }
        
        return index;
    }
    
    public static void copyIndex(Index fromIndex, Index toIndex)
    {
        Iterator itr = fromIndex.iterator();
        
        while(itr.hasNext())
        {
            toIndex.add(itr.next());
        }
    }
    
    public static <T> List<T> indexToList(Index<T> index)
    {
        List<T> list= new ArrayList<T>(index.size());
        
        Iterator<T> itr = index.iterator();
        
        while(itr.hasNext())
        {
            list.add(itr.next());
        }
        
        return list;
    }
    
    public static <T> T[] indexToArray(Index<T> index)
    {
        List<T> list = indexToList(index);
        
        Object [] arrayObj = list.toArray();
        
        return (T[]) arrayObj;
    }
    
    public static <T> Index<T> listToIndex(List<T> list)
    {
        Index<T> index = new HashIndex<T>(list.size());
        
        Iterator<T> itr = list.iterator();
        
        while(itr.hasNext())
        {
            index.add(itr.next());
        }
        
        return index;
    }
    
    public static <T> Index<T> arrayToIndex(T [] array)
    {
        Index<T> index = new HashIndex<T>(array.length);
        
        for (int i = 0; i < array.length; i++) {
            T t = array[i];

            index.add(t);
        }
        
        return index;
    }
    
    public static boolean fileExists(String filepath)
    {
        if(filepath == null)
        {
            return false;
        }
        
        File f = new File(filepath);
        
        return f.exists();
    }
    
    public static String getResourcePath(String resourePath)
    {
        if(resourePath == null)
        {
            return null;
        }

        String parts[] = resourePath.split(":", 2);
        
        if(parts.length > 0)
        {
            return parts[0];
        }
        
        return null;
    }
    
    public static String getResourceCharset(String pathCharset)
    {
        if(pathCharset == null)
        {
            return "UTF-8";
        }
        
        String parts[] = pathCharset.split(":", 2);
        
        if(parts.length == 2)
        {
            return parts[1];
        }
        
        return "UTF-8";
    }
    
    public static List<String> getWordList(SSFSentence sentence)
    {
        List leafList = sentence.getRoot().getAllLeaves();
        
        int count = leafList.size();
        
        ArrayList<String> wordList = new ArrayList<String>();
        
        for (int i = 0; i < count; i++) {
            wordList.add(((SSFNode) leafList.get(i)).getLexData());
        }
        
        return wordList;
    }
    
    public static String replaceLast(String string, String from, String to) {
        int lastIndex = string.lastIndexOf(from);
        
        if (lastIndex < 0) {
            return string;
        }
        
        String tail = string.substring(lastIndex).replaceFirst(from, to);

        return string.substring(0, lastIndex) + tail;
    }    

    /*
     * The possible option names should be separated by single space.
     */
    public static String getOptionValue(String possibleNames, KeyValueProperties options)
    {
        String names[] = possibleNames.split(" ");
        
        for (int i = 0; i < names.length; i++) {
            String value = options.getPropertyValue(names[i]);
            
            if(value != null)
            {
                return value;
            }
        }
        
        return null;
    }
    
    public static String getOptionValue(String shortName, String longName, KeyValueProperties options)
    {
        String value = null;
        
        if(shortName != null)
        {
            value = options.getPropertyValue(shortName);
        }
        
        if(value == null && longName != null)
        {
            value = options.getPropertyValue(longName);
        }

        return value;
    }
    
    public static void printStringArray(String strings[], PrintStream ps)
    {
        for (int i = 0; i < strings.length; i++) {
            String string = strings[i];
            ps.println(i);
        }
    }
    
    public static void setLanguages(List<FeatureType> featureTypes, Language srcLanguage,
            Language tgtLanguage)
    {
        for (FeatureType featureType : featureTypes) {
            try {
    
                featureType.getResourceType().setSrcLangauge(srcLanguage);
                featureType.getResourceType().setTgtLangauge(tgtLanguage);
                
            } catch (PropertyVetoException ex) {
                Logger.getLogger(MiscellaneousUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void printList(List list, PrintStream ps, String sep)
    {
        for (int i = 0; i < list.size(); i++) {
            Object element = list.get(i);

            if(i == list.size() - 1)
            {
                ps.print(element.toString() + sep);            
            }
            else
            {
                ps.print(element.toString() + sep);                            
            }            
        }
    }
    
    public static boolean emptySentence(String sentence)
    {
        boolean emtpy = false;
        
        if(sentence == null || sentence.length() == 0)
        {
            emtpy = true;
        }
        else
        {
            String words[] = sentence.split("[\\s+]");
            
            if(words.length == 0 || (words.length == 1 && words[0].length() == 0))
            {
                emtpy = true;
            }
        }
        
        return emtpy;
    }
    
    public static String getFileName(String filePath)
    {
        File file = new File(filePath);
        
        return file.getName();
    }
    
    public static File getFile(String filePath)
    {
        File file = new File(filePath);
        
        return file;
    }
    
    public static String getFileDirectoryPath(String filePath)
    {
        File file = new File(filePath);
        
        return file.getParentFile().getAbsolutePath();
    }
    
    public static File getFileDirectory(String filePath)
    {
        File file = new File(filePath);
        
        return file.getParentFile();
    }
}
