/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import org.limsi.types.Language;

/**
 *
 * @author anil
 */
public enum TagSet {
    PTB_POS,
    TT_POS_FR,
    TT_POS_SP; 
    
    public static TagSet getTagSet(Language language)
    {
        if(language.equals(Language.FRENCH))
            return TagSet.TT_POS_FR;
        else if(language.equals(Language.SPANISH))
            return TagSet.TT_POS_SP;

        return TagSet.PTB_POS;
   }
}
