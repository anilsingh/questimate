/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.util;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.mt.cm.features.sentence.FeatureValuesImpl;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.FeatureSubTypesEnum;
import org.limsi.types.FeatureType;

/**
 *
 * @author anil
 */
public class FeatureValueUtils {
    
    public static boolean isLabelFeature(String featureName)
    {
        List<FeatureSubTypesEnum> featureSubTypes = FeatureSubTypesEnum.getFeatureSubTypes(FeatureType.LABEL);

        for (FeatureSubTypesEnum featureSubType : featureSubTypes) {
            if(featureSubType.featureName.equals(featureName))
            {
                return true;
            }
        }
        
        return false;
    }
    
    public static void addFeature(HypothesisFeatures intoThisHF, String name,
            double score, double slen, double tlen, boolean sNt, boolean reverse)
    {
        
    }
    
    public static void addLabel(HypothesisFeatures intoThisHF, String name,
            double score, double len, boolean addNormalized)
    {
        intoThisHF.addLabel(name, score);            

        if(addNormalized)
        {                
            intoThisHF.addLabel("normalized" + name, score/len);            
        }        
    }
    
    public static void addFeature(HypothesisFeatures intoThisHF, String name,
            double score, double len, boolean addNormalized, boolean sNt, boolean reverse)
    {
        if(isLabelFeature(name))
        {
            addLabel(intoThisHF, name, score, len, addNormalized);
            
            return;
        }
        
        String cname = name.charAt(0) + name.substring(1);
        
        if(sNt)
        {
            if(!reverse)
            {
                intoThisHF.addFeature("src" + cname, score);            

                if(addNormalized)
                {                
                    intoThisHF.addFeature("normalizedSrc" + cname, score/len);            
                }
            }
            else
            {
                intoThisHF.addFeature("tgt" + cname, score);            

                if(addNormalized)
                {                
                    intoThisHF.addFeature("normalizedTgt" + cname, score/len);            
                }
            }
        }        
        else
        {
            intoThisHF.addFeature(name, score);            

            if(addNormalized)
            {                
                intoThisHF.addFeature("normalized" + cname, score/len);            
            }                            
        }
    }

//    public static void printFeatures(PrintStream ps)
//    {
////        hypothesisWordPairs.printUnigramPairs(ps);
////        hypothesisWordPairs.printBigramPairs(ps);
//        
////        FeatureValueCollection<String> features = translation.getFeatures();
//
////        DecimalFormat df = new DecimalFormat("0.####E0");
//        
//        Iterator<Integer> itr = hypothesisFeatures.keySet().iterator();
//
//        int i = 0;
//        while(itr.hasNext())
//        {
//            int fIndex = itr.next();
//            
//            Object fValue = hypothesisFeatures.get(fIndex);
//            
//            if(fValue instanceof Double)
//            {
////                ps.print((fIndex+1) + ":" + (df.format(fValue) + " "));
////                ps.print((fIndex+1) + ":" + fValue + " ");
//                ps.print((featureIndex.get(i)) + ":" + fValue + " ");
//            }
//            else
//            {
////                ps.print((fIndex+1) + ":" + fValue + " ");
//                ps.print((featureIndex.get(i)) + ":" + fValue + " ");
//            }
//            
//            i++;
//        }
//
//        ps.println("");
//    }
    

    public static void svmlight2Encog(String svmFilePath, String lableFilePath,
            String encogFilePath, String encoding) throws IOException
    {
        File svmFile = new File(svmFilePath);
        File encogFile = new File(encogFilePath);

        if(svmFile.isDirectory())
        {
            if(encogFile.isFile())
            {
                throw new IOException("svmFilePath is a directory, but the output path is a file");
            }
            
            if(!encogFile.exists())
            {
                encogFile.mkdirs();
            }
            
            File svmFiles[] = svmFile.listFiles();
            
            for (int i = 0; i < svmFiles.length; i++) {
                File efile = new File(encogFile, svmFiles[i].getName());
                
                svmlight2Encog(svmFiles[i].getAbsolutePath(), lableFilePath, efile.getAbsolutePath(), encoding);
            }
        }
        else if(svmFile.isFile() && !encogFile.isDirectory())
        {
            if(svmFile.getName().contains("README") || svmFile.getName().contains("readme"))
                return;
            
            System.out.println("Processing file: " + svmFilePath);
            
            LineNumberReader svmReader;
            LineNumberReader lableReader;

            DecimalFormat df = new DecimalFormat("0.00");

            if (svmFilePath.endsWith(".gz")) {
                svmReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                        new FileInputStream(svmFilePath)), encoding));
            } else {
                svmReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                        svmFilePath), encoding));
            }

            if (lableFilePath.endsWith(".gz")) {
                lableReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                        new FileInputStream(lableFilePath)), encoding));
            } else {
                lableReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                        lableFilePath), encoding));
            }

            PrintStream ps = new PrintStream(encogFilePath + ".csv", encoding);

            String headerStr = "";

            int k = 0;

            for (String svmLine, lableLine; (svmLine = svmReader.readLine()) != null && (lableLine = lableReader.readLine()) != null;)
            {
                svmLine = svmLine.trim();
                lableLine = lableLine.trim();

                String featureStr = "";

                if(!svmLine.isEmpty())
                {
                    String features[] = svmLine.split("\\s+");

                    for (int i = 0; i < features.length; i++) {

                        String fv[] = features[i].split(":");
                        
                        if(fv.length != 2)
                        {
                            return;
                        }

                        if(k == 0) {
                            headerStr += fv[0] + "\t";
                        }

                        featureStr += df.format(Double.parseDouble(fv[1])) + "\t";
                    }

                    if(k++ == 0) {
                        ps.println(headerStr + "score");
                    }

                    ps.print(featureStr);
                    ps.print(lableLine);
                    ps.println("");
                }
            }

            svmReader.close();
            ps.close();
        }
        else
        {
            throw new IOException("Something wrong with the paths you have given");
        }
    }
    
    public static void generateFeatureCombinations(String featureDirPath,
            String outDirPath, String encoding)
    {
        
    }
    
    public static void fixNaNValues(Map<CharSequence, Float> featureValues)
    {
        if(featureValues == null)
        {
            return;
        }
        
        Iterator<CharSequence> itr = featureValues.keySet().iterator();

        while(itr.hasNext())
        {
            CharSequence name = itr.next();

            Float value = featureValues.get(name);

            if(Float.isNaN(value))
            {
                featureValues.put(name, new Float(0));
            }
            else if(Float.isInfinite(value))
            {
                featureValues.put(name, new Float(2));
            }
        }
        
    }
    
    public static FeatureValues getFeatureValues(Map<CharSequence, Float> map)
    {
        FeatureValues featureValues = new FeatureValuesImpl();
        
        Iterator<CharSequence> itr = map.keySet().iterator();
        
        while(itr.hasNext())
        {
            CharSequence key = itr.next();
            
            Float value = map.get(key);
            
            featureValues.addFeature(key.toString(), value);
        }
        
        return featureValues;
    }
    
    public static <T> Map<String, T> getStringMap(Map<CharSequence, T> map)
    {
        Map<String, T> stringMap = new LinkedHashMap<String, T>();
        
        Iterator<CharSequence> itr = map.keySet().iterator();
        
        while(itr.hasNext())
        {
            CharSequence key = itr.next();
            
            T value = map.get(key);
            
            stringMap.put(key.toString(), value);
        }
        
        return stringMap;
    }
    
    public static void main(String args[])
    {
        try {
//            FeatureValueUtils.svmlight2Encog("/people/anil/work/confidence-estimation/quality_estimation/features/test_baseline_features.svmlight",
//                    "/people/anil/work/wmt12-quality-estimation/test_set/encog-features/test_baseline_features.csv", "UTF-8");
//            FeatureValueUtils.svmlight2Encog("/people/anil/work/confidence-estimation/quality_estimation/features/test_baseline_features.svmlight",
//                    "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_system.effort",
//                    "/people/anil/work/wmt12-quality-estimation/test_set/encog-features/test_baseline_features.csv",
//                    "UTF-8");
            FeatureValueUtils.svmlight2Encog("/people/anil/work/confidence-estimation/quality_estimation/features",
                    "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_system.effort",
                    "/people/anil/work/wmt12-quality-estimation/test_set/encog-features",
                    "UTF-8");
//            FeatureValueUtils.svmlight2Encog("/people/anil/work/confidence-estimation/quality_estimation/features/train_baseline_features.svmlight",
//                    "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_system.effort",
//                    "/people/anil/work/wmt12-quality-estimation/training_set/encog-features/train_baseline_features.csv",
//                    "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\tscore",
//                    "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(FeatureValueUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
