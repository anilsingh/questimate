/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 *
 * @author anil
 */
public class LinuxCommand {

    public static String[] executeCommand(String command, boolean waitForResponse) {

        String response = "";

        ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", command);
//        pb.redirectErrorStream(true);

//        System.out.println("Linux command: " + command);

        try {
            Process shell = pb.start();

            if (waitForResponse) {

                // To capture output from the shell
                InputStream shellIn = shell.getInputStream();

                // Wait for the shell to finish and get the return code
                int shellExitStatus = shell.waitFor();
//                System.out.println("Exit status" + shellExitStatus);

//                response = convertStreamToStr(shellIn);
                response = getOutput(shellIn);

//                System.out.println("Output:");
                System.out.println(shellExitStatus);

                shellIn.close();
            }

        } catch (IOException e) {
            System.out.println("Error occured while executing Linux command. Error Description: "
                    + e.getMessage());
        } catch (InterruptedException e) {
            System.out.println("Error occured while executing Linux command. Error Description: "
                    + e.getMessage());
        }

        return response.split("\n");
    }

    public static String convertStreamToStr(InputStream is) throws IOException {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is,
                        "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }
    
    public static String getOutput(InputStream is) throws IOException
    {
       InputStreamReader isr = new InputStreamReader(is);
       
       BufferedReader br = new BufferedReader(isr);
       
       StringBuilder sb = new StringBuilder();
       String line;

//       System.out.printf("Output of running %s is:", 
//          toolCommand);

       while ((line = br.readLine()) != null) {
         System.out.println(line);
         sb.append(line);
         sb.append("\n");
       }        
       
       return sb.toString();
    }
    
    public static String escapeString(String string)
    {
        string = string.replaceAll("\"", "\\\\\"");
        
        return string;
    }
    
    public static void main(String args[])
    {
        String response[] = executeCommand("cat build.xml", true);
        
        for (int i = 0; i < response.length; i++) {
            String string = response[i];
            System.out.println(string);
        }
    }
}

