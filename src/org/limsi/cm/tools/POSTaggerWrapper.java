/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.tools;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import ml.options.Options;
import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;
import org.limsi.cm.corpora.CEAnnotatedCorpusWMT12;
import org.limsi.cm.resources.CEResources;
import org.limsi.cm.util.SyntacticFeatureUtils;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;
import org.limsi.mt.cm.props.CEProperties;
import org.limsi.types.Language;
import org.limsi.types.POSTaggerType;
import sanchay.common.types.CorpusType;
import sanchay.corpus.ssf.SSFSentence;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFSentenceImpl;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import sanchay.corpus.ssf.tree.SSFNode;
import sanchay.tree.SanchayMutableTreeNode;

/**
 *
 * @author anil
 */
public class POSTaggerWrapper {

    private TokenizerWrapper stafordTokenizer;
    private MaxentTagger stanfordPOSTagger;
    
    private TreeTaggerWrapper treeTaggerWrapper;
    private final List<List<String>> taggedWords = new ArrayList<List<String>>();
    
    protected POSTaggerType type = POSTaggerType.TREE_TAGGER;
    protected Language language = Language.ENGLISH;

    protected SSFStory document;

    public POSTaggerWrapper(POSTaggerType type, Language l) {        
        CEResources.setSSFProps();
        stafordTokenizer = new TokenizerWrapper();

        this.type = type;
        language = l;
    }

    /**
     * @return the document
     */
    public SSFStory getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(SSFStory document) {
        this.document = document;
    }

    /**
     * @return the language
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Language language) {
        this.language = language;
    }
    
    public void prepareTagger() throws IOException, ClassNotFoundException
    {
        if(type.equals(POSTaggerType.TREE_TAGGER))
        {
            System.setProperty("treetagger.home", CEProperties.getProperty("TreeTaggerHome", CEProperties.TOOL_COMMANDS));
            
//            String lexiconPath = CEProperties.getProperty("TreeTaggerHome", CEProperties.TOOL_COMMANDS)
//                    + "/lib/" + language.getName().toLowerCase() + "-lexicon-utf8.txt";
//
//            System.out.println("Lexicon path: " + lexiconPath);
            
//            String[] TT_ARGS = { "-quiet", "-no-unknown",
//		"-sgml", "-token", "-lemma", "-lex", lexiconPath};

            String[] TT_ARGS = { "-quiet", "-no-unknown",
		"-sgml", "-token", "-lemma"};
            
            treeTaggerWrapper = new TreeTaggerWrapper<String>();
            
            treeTaggerWrapper.setArguments(TT_ARGS);

            try {
                if(language.equals(Language.ENGLISH))
                {
                    treeTaggerWrapper.setModel(CEProperties.getProperty("TreeTaggerModelEnglishPath", CEProperties.CORE_DATA_PATHS)
                            + ":iso8859-1");
                }
                else if(language.equals(Language.FRENCH))
                {
                    treeTaggerWrapper.setModel(CEProperties.getProperty("TreeTaggerModelFrenchPath", CEProperties.CORE_DATA_PATHS)
                            + ":iso8859-1");
                }
                else if(language.equals(Language.SPANISH))
                {
                    treeTaggerWrapper.setModel(CEProperties.getProperty("TreeTaggerModelSpanishPath", CEProperties.CORE_DATA_PATHS)
                            + ":iso8859-1");
                }
            } catch (IOException ex) {
                Logger.getLogger(POSTaggerWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }

            treeTaggerWrapper.setHandler(new TokenHandler<String>() {
                    @Override
                    public void token(String token, String pos, String lemma) {
                        List<String> taggedWord = new ArrayList<String>();
                        taggedWord.add(token);
                        taggedWord.add(pos);
                        taggedWord.add(lemma);

                        taggedWords.add(taggedWord);
                    }
            });
        }
        else if(type.equals(POSTaggerType.STANFORD_TAGGER))
        {
            String modelPath = null;
            String configPath;

            if(language.equals(Language.ENGLISH))
            {
                modelPath = CEProperties.getProperty("StanfordTaggerEnglishModelPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(language.equals(Language.FRENCH))
            {
                modelPath = CEProperties.getProperty("StanfordTaggerFrenchModelPath", CEProperties.CORE_DATA_PATHS);
            }

            readModel(modelPath);            
        }
    }
    
    public void clearTagger()
    {
        if(type.equals(POSTaggerType.TREE_TAGGER))
        {
            treeTaggerWrapper.destroy();
        }
        else if(type.equals(POSTaggerType.STANFORD_TAGGER))
        {
            
        }
    }

    public void loadSSFCorpus(String path, String cs) throws FileNotFoundException, IOException, Exception
    {
        document = new SSFStoryImpl();
        
        System.out.println("Loading corpus...");
        document.readFile(path, cs, CorpusType.RAW);
        System.out.printf("Loaded %d sentences\n", document.countSentences());
    }
    
    private String getTaggerCommand(Language language)
    {
        String toolCommand = null;
        
        if(type.equals(POSTaggerType.TREE_TAGGER))
        {
            if(language.equals(Language.ENGLISH))
            {
                toolCommand = CEProperties.getProperty("TreeTaggerEnglish", CEProperties.TOOL_COMMANDS);
            }
            else if(language.equals(Language.FRENCH))
            {
                toolCommand = CEProperties.getProperty("TreeTaggerFrench", CEProperties.TOOL_COMMANDS);
            }
            else if(language.equals(Language.FRENCH))
            {
                toolCommand = CEProperties.getProperty("TreeTaggerSpanish", CEProperties.TOOL_COMMANDS);
            }
        }
        
        return toolCommand;
    }
        
    private void readModel(String modelPath) throws IOException, ClassNotFoundException
    {
        stanfordPOSTagger = new MaxentTagger(modelPath);
    }
    
    private void readModel(String modelPath, TaggerConfig config) throws IOException, ClassNotFoundException
    {
        stanfordPOSTagger = new MaxentTagger(modelPath, config);
    }
    
    public void readModel(String modelPath, TaggerConfig config, boolean printLoading) throws IOException, ClassNotFoundException
    {
        stanfordPOSTagger = new MaxentTagger(modelPath, config, printLoading);
    }

    private void tagSentenceTreeTagger(SSFSentence sentence) throws IOException, TreeTaggerException
    {
        List<String> words = SyntacticFeatureUtils.ssf2StringList(sentence);

        treeTaggerWrapper.process(words);

        List<SanchayMutableTreeNode> nodes = sentence.getRoot().getAllLeaves();
        
        int count = taggedWords.size();
        
        for (int i = 0; i < count; i++) {
            List<String> taggedWord = taggedWords.get(i);
            
            SSFNode node = (SSFNode) nodes.get(i);
            
            node.setName(taggedWord.get(1));
//            node.setAttributeValue("lemma", taggedWord.get(2));
        }

        taggedWords.clear();
    } 
    
    private List<TaggedWord> getTaggedSentenceStanford(SSFSentence sentence)
    {        
        List<CoreLabel> coreLabels = SyntacticFeatureUtils.ssf2CoreLabel(sentence);

        ArrayList<TaggedWord> taggedWds;
        
        taggedWds = stanfordPOSTagger.tagSentence(coreLabels);
        
        return taggedWds;
    }

    public void tagSentence(SSFSentence sentence) throws IOException, TreeTaggerException
    {
        if(type.equals(POSTaggerType.TREE_TAGGER))
        {
            tagSentenceTreeTagger(sentence);
        }
        else if(type.equals(POSTaggerType.STANFORD_TAGGER))
        {
            tagSentenceStanford(sentence);
        }        
    }

    public SSFSentence getTaggedSentence(String sentence) throws Exception
    {
        SSFSentence ssfSent = new SSFSentenceImpl();
        
        ssfSent.makeSentenceFromRaw(sentence);

        tagSentence(ssfSent);
        
        return ssfSent;
    }

    private void tagSentenceStanford(SSFSentence sentence)
    {
        List<SanchayMutableTreeNode> words = sentence.getRoot().getAllLeaves();
        List<TaggedWord> taggedWds = getTaggedSentenceStanford(sentence);
        
        int count = words.size();
        
        for (int i = 0; i < count; i++) {            
            SSFNode sword = (SSFNode) words.get(i);
            
            sword.setName(taggedWds.get(i).tag());
        }
    }
    
    public void tagStory(SSFStory document)
    {
        int count = document.countSentences();
        
        for (int i = 0; i < count; i++) {
            SSFSentence sentence = document.getSentence(i);
            
            tagSentenceStanford(sentence);
        }
    }

    public void runTagger() throws IOException, TreeTaggerException
    {
        if(document.isTagged())
        {
            return;
        }
        
        int scount = document.countSentences();
                
        for (int i = 0; i < scount; i++) {
            System.out.format("Tagging sentence %d\r", (i + 1));
            
            SSFSentence sentence = document.getSentence(i);

            if(type.equals(POSTaggerType.TREE_TAGGER))
            {
                tagSentenceTreeTagger(sentence);
//                List<String> words = SyntacticFeatureUtils.ssf2StringList(sentence);
//                List<SanchayMutableTreeNode> nodes = sentence.getRoot().getAllLeaves();
//
//                treeTaggerWrapper.process(words);       
//
//               int wcount = taggedWords.size();
//
//               for (int j = 0; j < wcount; j++) {
//                   List<String> taggedWord = taggedWords.get(j);
//
//                   SSFNode node = (SSFNode) nodes.get(j);
//
//                   node.setName(taggedWord.get(1));
//                   node.setAttributeValue("lemma", taggedWord.get(2));
//               }
//
//               taggedWords.clear();
            }
            else if(type.equals(POSTaggerType.STANFORD_TAGGER))
            {
                tagSentenceStanford(sentence);
            }
        }
        
        document.isTagged(true);
    } 
    
    public void tagFile(String inPath, String outpath, String cs) throws UnsupportedEncodingException, IOException, Exception
    {
        tagFileSaveNolex(inPath, outpath, null, cs);
    }

    public void tagFileSaveNolex(String inPath, String outpath, String nolexPath, String cs) throws UnsupportedEncodingException, IOException, Exception
    {
        prepareTagger();
        
        System.out.printf("Reading the file %s\n", inPath);
        
        LineNumberReader reader;
        
        if (inPath.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(inPath)), CEAnnotatedCorpusWMT12.getEncoding()));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    inPath), CEAnnotatedCorpusWMT12.getEncoding()));
        }

        PrintStream ps = new PrintStream(outpath, cs);
        PrintStream psNolex = null;
        
        if(nolexPath != null && nolexPath.equals("") == false)
        {
            psNolex = new PrintStream(nolexPath, cs);
        }

        int i = 0;
        
        for (String inline; (inline = reader.readLine()) != null;)
        {
            if(i++ > 0 && i % 100 == 0)
            {
               System.out.printf("Processed %d lines\r", i);            
            }
            
            SSFSentence ssfSentence = getTaggedSentence(inline);
            
            ps.print(ssfSentence.convertToPOSTagged());

            if(nolexPath != null)
            {
                psNolex.print(ssfSentence.makePOSNolex());
            }
        }
        
        reader.close();
        ps.close();

        if(nolexPath != null)
        {
            psNolex.close();
        }
                
        clearTagger();        
    }
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 2, 2);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("language", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tagger", "t", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("nolex", "nl", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        POSTaggerType taggerType = POSTaggerType.TREE_TAGGER;
        Language language = Language.ENGLISH;
        String infile = "";
        String outfile = "";
        String nolexfile = "";

        if (!opt.check()) {
            System.out.println("POSTaggerWrapper [-cs charset] [-l language]"
                   + " [-t posTaggerType] [-nl nolexpath] infile outfile");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }

        if (opt.getSet().isSet("t")) {
            String tagger = opt.getSet().getOption("t").getResultValue(0);
            taggerType = POSTaggerType.valueOf(tagger);
            System.out.println("POS Tagger type: " + taggerType.name());
        }
        
        if (opt.getSet().isSet("l")) {
            String lang = opt.getSet().getOption("l").getResultValue(0);  
            language = Language.getLanguage(lang);
            System.out.println("Language: " + language);
        }
        
        if (opt.getSet().isSet("nl")) {
            nolexfile = opt.getSet().getOption("nl").getResultValue(0);
            System.out.println("Nolex file: " + nolexfile);
        }

        infile = opt.getSet().getData().get(0);
        outfile = opt.getSet().getData().get(1);
        
        try {            
            POSTaggerWrapper POSTaggerWrapper = new POSTaggerWrapper(taggerType, language);            

            POSTaggerWrapper.tagFileSaveNolex(infile, outfile, nolexfile, charset);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {
        runWithCmdLineOptions(args);
        
//        POSTaggerWrapper tagger = new POSTaggerWrapper(POSTaggerType.TREE_TAGGER, Language.FRENCH);
//        
//        try {
//            tagger.tagFileSaveNolex("tmp/corrections_en2fr.data.trans.fte.utf8.lc",
//                    "tmp/corrections_en2fr.data.trans.fte.utf8.lc.postagged",
//                    "tmp/corrections_en2fr.data.trans.fte.utf8.lc.pos.nolex",
//                    "ISO-8859-1");
//            tagger.tagFile("/extra/wmt12/test_set/source.eng", "tmp/tmp.txt", "UTF-8");
            
//            tagger.loadModel("eng");
//            
//            SSFSentence sent = new SSFSentenceImpl();
//            
//            String sentence = "I have mananged to run the Standford tagger !";
//            
//            sent.makeSentenceFromRaw(sentence);
//            
//            tagger.runTagger(Language.ENGLISH, sent);

//            sent.print(System.out);

//            tagger.prepareTagger();
//            
//            tagger.loadSSFCorpus("/extra/wmt12/test_set/target_system.spa", "UTF-8");
//            
//            tagger.runTagger();
//            
//            tagger.clearTagger();
//            
//            SSFStory document = tagger.getDocument();
//            
//            document.savePOSTagged("tmp/tmp.txt", "UTF-8");
//
//            document.savePOSNolex("tmp/corrections_en2fr.data.trans.fte.utf8.lc.pos.nolex", "UTF-8");
            
//            SSFStory document = tagger.runTagger("eng", "tmp/in-50.txt", "UTF-8");
//            document.print(System.out);
//            
////            List<TaggedWord> taggedSentence = tagger.tagSentence(sent);
//            tagger.tagSentence(sent);
//            
//            sent.print(System.out);

//            tagger.loadModel("french");
//            
//            SSFSentence sent = new SSFSentenceImpl();
//            
//            String sentence = "Si ca marche bien .";
//            
//            sent.makeSentenceFromRaw(sentence);
//            
////            List<TaggedWord> taggedSentence = tagger.tagSentence(sent);
//            tagger.tagSentence(sent);
//            
//            sent.print(System.out);
            
//            for (TaggedWord taggedWord : taggedSentence) {
//                System.out.print(taggedWord.tag() + " ");
//            }
            
//            String sentence = "I have mananged to run the Standford's tagger !";
//            String sentence = "In an addendum to his letters to McCain and Graham, Panetta spelled out new specifics of how reductions generate significant operational risks: delay response time to crises, conflicts, and disasters; severely limits our ability to be forward deployed and engaged around the world; and assumes unacceptable risk in future combat operations.";
//            tagger.runTagger("eng", sentence);
//            File f = new File("/people/anil/work/wmt12-quality-estimation/test_set/source.eng");
//            tagger.runTagger("eng", f);
//            tagger.runTagger("eng", "/people/anil/work/wmt12-quality-estimation/test_set/source.eng", "tmp/tmp.txt", "UTF-8");
//            tagger.runTagger("eng", "tmp/in.txt", "tmp/tmp.txt", "UTF-8");
//            
//        } catch (IOException ex) {
//            Logger.getLogger(POSTaggerWrapper.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        catch (ClassNotFoundException ex) {
//            Logger.getLogger(POSTaggerWrapper.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        catch (Exception ex) {
//            Logger.getLogger(POSTaggerWrapper.class.getName()).log(Level.SEVERE, null, ex);
//        }        
    }
}
