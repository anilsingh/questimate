/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.tools;

import edu.berkeley.nlp.lm.ConfigOptions;
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.StringWordIndexer;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.ArpaLmReader;
import edu.berkeley.nlp.lm.io.LmReaders;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.features.sentence.NGramLMFeatureExtractor;
import org.limsi.mt.cm.features.sentence.POSCountsFeatureExtractor;
import org.limsi.types.LMType;
import sanchay.mlearning.lm.ngram.NGramCountsImpl;
import sanchay.mlearning.lm.ngram.impl.NGramCounts;

/**
 *
 * @author anil
 */
public class LanguageModelingWrapper<T> {
    
    private NgramLanguageModel<T> berkeleyNGramLM;
    protected WordIndexer<String> wordIndexer;
    
    private NGramCounts sanchayNGramLM;
    
    private LMType type;

    public LanguageModelingWrapper(LMType type) {
        this.type = type;
    }

    /**
     * @return the berkeleyNGramLM
     */
    public NgramLanguageModel<T> getBerkeleyNGramLM() {
        return berkeleyNGramLM;
    }

    /**
     * @param berkeleyNGramLM the berkeleyNGramLM to set
     */
    public void setBerkeleyNGramLM(NgramLanguageModel<T> berkeleyNGramLM) {
        this.berkeleyNGramLM = berkeleyNGramLM;
    }

    /**
     * @return the sanchayNGramLM
     */
    public NGramCounts getSanchayNGramLM() {
        return sanchayNGramLM;
    }

    /**
     * @param sanchayNGramLM the sanchayNGramLM to set
     */
    public void setSanchayNGramLM(NGramCounts sanchayNGramLM) {
        this.sanchayNGramLM = sanchayNGramLM;
    }

    /**
     * @return the taggerType
     */
    public LMType getType() {
        return type;
    }

    /**
     * @param taggerType the taggerType to set
     */
    public void setType(LMType type) {
        this.type = type;
    }

    /**
     * @return the wordIndexer
     */
    public WordIndexer<String> getWordIndexer() {
        return wordIndexer;
    }

    public void makeLMFromText(String textPath, String lmPath, String cs, boolean contextEncoded) throws FileNotFoundException, FileNotFoundException, IOException
    {
        System.out.println("Creating the n-gram model from " + textPath + " and writing to file " + lmPath);
        
        if(type.getLMTool().equals(LMType.LMTool.BERKELEY))
        {
            final List<String> inputFiles = new ArrayList<String>();

            inputFiles.add(textPath);

            wordIndexer = new StringWordIndexer();
            getWordIndexer().setStartSymbol(ArpaLmReader.START_SYMBOL);
            getWordIndexer().setEndSymbol(ArpaLmReader.END_SYMBOL);
            getWordIndexer().setUnkSymbol(ArpaLmReader.UNK_SYMBOL);

            LmReaders.createKneserNeyLmFromTextFiles(inputFiles, getWordIndexer(), type.getOrder().order(), new File(lmPath), new ConfigOptions());                
            
            if(contextEncoded)
            {                
                if(type.getSmoothing().equals(LMType.LMSmoothing.KN_DISCOUNT))
                {                    
                    berkeleyNGramLM = (NgramLanguageModel<T>) LmReaders.readContextEncodedLmFromArpa(lmPath, wordIndexer, new ConfigOptions(), type.getOrder().order());
                }                
            }
            else
            {
                if(type.getSmoothing().equals(LMType.LMSmoothing.KN_DISCOUNT))
                {
                    berkeleyNGramLM = (NgramLanguageModel<T>) LmReaders.readArrayEncodedLmFromArpa(lmPath, true, wordIndexer, new ConfigOptions(), type.getOrder().order());
                }                
            }            
        }
        else if(type.getLMTool().equals(LMType.LMTool.SANCHAY))
        {
            sanchayNGramLM = new NGramCountsImpl(new File(textPath), "word", type.getOrder().order());
            
            sanchayNGramLM.makeNGramLM(new File(textPath));            
            
            int order = sanchayNGramLM.getNGramOrder();
            
            for (int i = 1; i <= order; i++) {
                sanchayNGramLM.pruneByFrequency(1, i);            
            }
            
            sanchayNGramLM.saveNGramLM(lmPath, cs, true);
        }
    }

    public void makeBinaryFromLM(String lmPath, String binaryPath, String cs, boolean contextEncoded) throws FileNotFoundException, FileNotFoundException, IOException, ClassNotFoundException
    {
        System.out.println("Reading the ARPA file " + lmPath + " and writing to binary file " + binaryPath);

        if(type.getLMTool().equals(LMType.LMTool.BERKELEY))
        {
            wordIndexer = new StringWordIndexer();
            getWordIndexer().setStartSymbol(ArpaLmReader.START_SYMBOL);
            getWordIndexer().setEndSymbol(ArpaLmReader.END_SYMBOL);
            getWordIndexer().setUnkSymbol(ArpaLmReader.UNK_SYMBOL);

            if(contextEncoded)
            {
                berkeleyNGramLM = (NgramLanguageModel<T>) LmReaders.readContextEncodedLmFromArpa(lmPath, wordIndexer);
            }
            else
            {
                berkeleyNGramLM = (NgramLanguageModel<T>) LmReaders.readArrayEncodedLmFromArpa(lmPath, true, wordIndexer);
            }                        

//            if(binaryPath != null)
//            {
//                LmReaders.writeLmBinary(berkeleyNGramLM, binaryPath);                
//            }
        }
        else if(type.getLMTool().equals(LMType.LMTool.SANCHAY))
        {
            sanchayNGramLM = new NGramCountsImpl(new File(lmPath), "word", type.getOrder().order());            
            sanchayNGramLM.readNGramLM(new File(lmPath), cs);
            
//            if(binaryPath != null && binaryPath.equals("") == false)
//            {
//                NGramCountsImpl.saveNGramLMBinary(sanchayNGramLM, new File(binaryPath));
//            }
       }        
    }
    
    public void readBinary(String binaryPath) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        System.out.println("Reading the binary file " + binaryPath);
        
        if(type.getLMTool().equals(LMType.LMTool.BERKELEY))
        {
            berkeleyNGramLM = LmReaders.readLmBinary(binaryPath);

            wordIndexer = (WordIndexer<String>) berkeleyNGramLM.getWordIndexer();
        }
        else if(type.getLMTool().equals(LMType.LMTool.SANCHAY))
        {
            sanchayNGramLM = NGramCountsImpl.loadNGramLMBinary(new File(binaryPath));
        }     
    }
    
    public void saveBinary(String binaryPath) throws FileNotFoundException, IOException, ClassNotFoundException
    {
//        if(type.getLMTool().equals(LMType.LMTool.BERKELEY))
//        {
//            LmReaders.writeLmBinary(berkeleyNGramLM, binaryPath);
//        }
//        else if(type.getLMTool().equals(LMType.LMTool.SANCHAY))
//        {
//            NGramLMImpl.saveNGramLMBinary(sanchayNGramLM, new File(binaryPath));
//        }     
    }

    public void loadOrMakeLM(String textPath, String lmPath, String binaryPath, String charset, boolean contextEncoded) throws FileNotFoundException, FileNotFoundException, IOException, ClassNotFoundException
    {
//        if((binaryPath != null && MiscellaneousUtils.fileExists(binaryPath)))
//        {
//            readBinary(binaryPath);
//        }
        if(lmPath != null && MiscellaneousUtils.fileExists(lmPath))
        {
            makeBinaryFromLM(lmPath, binaryPath, charset, contextEncoded);
        }        
        else if(textPath != null && MiscellaneousUtils.fileExists(textPath))
        {
            makeLMFromText(textPath, lmPath, charset, contextEncoded);

            if(binaryPath != null)
            {
                saveBinary(binaryPath);
            }
        }
    }

    public void makeLM(String textPath, String lmPath, String binaryPath, String charset, boolean contextEncoded) throws FileNotFoundException, FileNotFoundException, IOException, ClassNotFoundException
    {
        if(textPath != null && MiscellaneousUtils.fileExists(textPath))
        {
            makeLMFromText(textPath, lmPath, charset, contextEncoded);

            if(binaryPath != null && binaryPath.equals("") == false)
            {
                saveBinary(binaryPath);
            }
        }
        else if(lmPath != null && MiscellaneousUtils.fileExists(lmPath))
        {
            makeBinaryFromLM(lmPath, binaryPath, charset, contextEncoded);
        }        
    }
        
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 0, 1);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("lmPath", "lm", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("binary", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("text", "t", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("contextEncoded", "ce", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("type", "y", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("order", "n", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("smooth", "s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tool", "tool", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        
        String charset = "UTF-8";
        String tool = LMType.LMTool.BERKELEY.name();
        String order = LMType.LMOrder.THREE.name();
        String type = LMType.TEXT_LM.name();
        String smoothing = LMType.LMSmoothing.WITTEN_BELL.name();
        boolean contextEncoded = false;
        String lmPath = "";
        String binaryLMPath = "";
        String textPath = "";
        LMType lmtype = LMType.TEXT_LM;

        if (!opt.check()) {
            System.out.println("LanguageModelingWrapper [-cs charset] [[-lm lmPath -b binaryLMPath] -text textPath]\n"
                   + "[-order order] [-type type] [-smooth smoothing] [-tool lmtool] [-ce <ifcontextEncoded>]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset: " + charset);
        }

        if (opt.getSet().isSet("lm")) {
            lmPath = opt.getSet().getOption("lm").getResultValue(0);
            System.out.println("lmPath: " + lmPath);
        }

        if (opt.getSet().isSet("binary")) {
            binaryLMPath = opt.getSet().getOption("binary").getResultValue(0);
            System.out.println("binaryLMPath: " + binaryLMPath);
        }

        if (opt.getSet().isSet("text")) {
            textPath = opt.getSet().getOption("text").getResultValue(0);
            System.out.println("textPath: " + textPath);
        }
        
        if (opt.getSet().isSet("ce")) {
            contextEncoded = true;  
            System.out.println("Context encoded: " + contextEncoded);
        }

        if (opt.getSet().isSet("type")) {
            type = opt.getSet().getOption("type").getResultValue(0);
            System.out.println("type: " + type);
        }

        if (opt.getSet().isSet("order")) {
            order = opt.getSet().getOption("order").getResultValue(0);
            System.out.println("order: " + order);
        }

        if (opt.getSet().isSet("smooth")) {
            smoothing = opt.getSet().getOption("smooth").getResultValue(0);
            System.out.println("smoothing: " + smoothing);
        }

        if (opt.getSet().isSet("tool")) {
            tool = opt.getSet().getOption("tool").getResultValue(0);
            System.out.println("lmtool: " + tool);
        }
        
        try {
            lmtype = LMType.valueOf(type);

            lmtype.setLMTool(LMType.LMTool.valueOf(tool));

            lmtype.setOrder(LMType.LMOrder.getLMOrder(Integer.parseInt(order)));

            lmtype.setSmoothing(LMType.LMSmoothing.valueOf(smoothing));
        } catch (PropertyVetoException ex) {
            Logger.getLogger(NGramLMFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {            
            LanguageModelingWrapper languageModelingWrapper = new LanguageModelingWrapper(lmtype);            
            
            languageModelingWrapper.makeLM(textPath, lmPath, binaryLMPath, charset, contextEncoded);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(POSCountsFeatureExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {
       runWithCmdLineOptions(args);
    }    
}
