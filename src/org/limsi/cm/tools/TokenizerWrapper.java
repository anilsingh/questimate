/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.tools;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.objectbank.TokenizerFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anil
 */
public class TokenizerWrapper {
    
    private TokenizerFactory tokenizerFactory;
    
    private Tokenizer ptbTokenizer;
    
    public TokenizerWrapper()
    {
        tokenizerFactory = PTBTokenizer.PTBTokenizerFactory.newWordTokenizerFactory("");
    }

    public List<CoreLabel> tokenize(String sentence)
    {
        List<CoreLabel> words = new ArrayList<CoreLabel>();
        
        ptbTokenizer = tokenizerFactory.getTokenizer(new StringReader(sentence));
     
        return words;
    }
}
