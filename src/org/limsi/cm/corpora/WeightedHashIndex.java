/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.RandomAccess;

/**
 *
 * @author anil
 */
public class WeightedHashIndex<E,W extends Number> extends HashIndex<E>
        implements Index<E>, RandomAccess
{
    ArrayList<W> weights = new ArrayList<W>();
    
    private Class<W> reference;
    
  /**
   * Creates a new Index.
   */
  public WeightedHashIndex(Class<W> classRef) {
    super();
    reference = classRef;
  }

  /**
   * Creates a new Index.
   * @param capacity Initial capacity of Index.
   */
  public WeightedHashIndex(int capacity) {
    super(capacity);
    weights = new ArrayList<W>(capacity);
  }

    protected W getWeightInstance()
    {
        try
        {
            return reference.newInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
    
  /**
   * Clears this Index.
   */

    @Override
  public void clear() {
    super.clear();
    weights.clear();
  }
    
  /**
   * Looks up the weights corresponding to an array of indices, and returns them in a {@link Collection}.
   * This collection is not a copy, but accesses the data structures of the Index.
   *
   * @param indices An array of indices
   * @return a {@link Collection} of the weights corresponding to the indices argument.
   */
  public Collection<W> weights(final int[] indices) {
    return new AbstractList<W>() {
      @Override
      public W get(int index) {
        return weights.get(indices[index]);
      }

      @Override
      public int size() {
        return indices.length;
      }
    };
  }
    
  /**
   * Gets the weight whose index is the integer argument.
   *
   * @param i the integer index to be queried for the corresponding argument
   * @return the object whose index is the integer argument.
   */
  public W getWeight(int i) {
    if (i < 0 || i >= weights.size())
      throw new ArrayIndexOutOfBoundsException("Index " + i + 
                                               " outside the bounds [0," + 
                                               size() + ")");
    return weights.get(i);
  }

  public void setWeight(int i, W w) {
    if (i < 0 || i >= weights.size())
      throw new ArrayIndexOutOfBoundsException("Index " + i + 
                                               " outside the bounds [0," + 
                                               size() + ")");
    weights.set(i, w);
  }
  
  private void addWeight(E o, W w, int objectIndex, int prevSize)
  {
      int sz = size();
      
      if(w == null)
      {
//          w = getWeightInstance();
          w = (W) getWeightInstance(this);
      }
      
      if(sz > prevSize)
      {
          weights.add(w);
      }
      else if(contains(o))
      {
          w = getWeight(objectIndex);
          
          if(w instanceof Integer && o instanceof String)
          {
              incrementWeight(this, objectIndex);
          }
      }
  }

  public static Object getWeightInstance(WeightedHashIndex index)
  {
      if(index.reference.equals(Integer.class))
      {
          return new Integer(1);
      }
      
    return null;
  }
  
  public static void incrementWeight(WeightedHashIndex index, int i)
  {
      Object w = index.getWeight(i);
      
      if(w instanceof Integer)
      {
          index.setWeight(i, ((Integer) w) + 1);
      }
  }

  public int indexOf(E o, boolean add) {
      return indexOf(o, null, add);
  }
  
  public int indexOf(E o, W w, boolean add) {
      int prevSize = size();

      int i = super.indexOf(o, add);

      if(i != -1 && add)
      {
          addWeight(o, w, i, prevSize);
      }

      return i;
  }

  @Override
  public boolean add(E o) {
      return add(o, null);
  }

  public boolean add(E o, W w) {
      int prevSize = size();
      
      boolean add = super.add(o);
            
      if(add)
      {
          int i = indexOf(o);
          
          addWeight(o, w, i, prevSize);
      }
      
      return add;
  }

  public void saveToFilename(String file) {
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(file));
      for (int i = 0, sz = size(); i < sz; i++) {
        bw.write(i + "=" + get(i) + "\t" + getWeight(i) + '\n');
      }
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (bw != null) {
        try {
          bw.close();
        } catch (IOException ioe) {
          // give up
        }
      }
    }
  }

  public void print(PrintStream ps) {
      for (int i = 0, sz = size(); i < sz; i++) {
        ps.println(get(i) + "\t" + getWeight(i));
      }
  }

  public static WeightedHashIndex<String, Integer> loadFromFilename(String file) {
    WeightedHashIndex<String, Integer> index = new WeightedHashIndex<String, Integer>(Integer.class);
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      for (String line; (line = br.readLine()) != null; ) {
        int start = line.indexOf('=');
        if (start == -1 || start == line.length() - 1) {
          continue;
        }
        
        String parts[] = line.split("\\t");
        
        int w = Integer.parseInt(parts[1].substring(parts[1].indexOf("=") + 1));
        
        index.add(parts[0].substring(start + 1), w);
      }
      br.close();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException ioe) {
          // forget it
        }
      }
    }
    return index;
  }
  public void saveToWriter(Writer bw) throws IOException {
    for (int i = 0, sz = size(); i < sz; i++) {
      bw.write(i + "=" + get(i) + "\t" + getWeight(i) + '\n');
    }
  }
  public static WeightedHashIndex<String, Integer> loadFromReader(BufferedReader br) throws IOException {
    WeightedHashIndex<String, Integer> index = new WeightedHashIndex<String, Integer>(Integer.class);
    String line = br.readLine();
    // terminate if EOF reached, or if a blank line is encountered.
    while ((line != null) && (line.length() > 0)) {
      int start = line.indexOf('=');
      if (start == -1 || start == line.length() - 1) {
        continue;
      }
        String parts[] = line.split("\\t");
        
        int w = Integer.parseInt(parts[1].substring(parts[1].indexOf("=") + 1));
        
        index.add(parts[0].substring(start + 1), w);
      index.add(line.substring(start + 1));
      line = br.readLine();
    }
    return index;
  }
  /** Returns a readable version of at least part of the Index contents.
   *
   *  @param n Show the first <i>n</i> items in the Index
   *  @return A String showing some of the index contents
   */
  public String toString(int n) {
    StringBuilder buff = new StringBuilder("[");
    int sz = size();
    if (n > sz) {
      n = sz;
    }
    int i;
    for (i = 0; i < n; i++) {
      E e = get(i);
      W w = getWeight(i);
      buff.append(i).append('=').append(e).append(":").append(w);
      if (i < (sz-1)) buff.append(',');
    }
    if (i < sz) buff.append("...");
    buff.append(']');
    return buff.toString();
  }

  public String toStringOneEntryPerLine(int n) {
    StringBuilder buff = new StringBuilder();
    int sz = size();
    if (n > sz) {
      n = sz;
    }
    int i;
    for (i = 0; i < n; i++) {
      E e = get(i);
      W w = getWeight(i);
      buff.append(e).append(":").append(w);
      if (i < (sz-1)) buff.append('\n');
    }
    if (i < sz) buff.append("...");
    return buff.toString();
  }
}
