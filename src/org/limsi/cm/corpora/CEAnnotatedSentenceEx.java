/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.util.ArrayList;
import org.limsi.cm.wg.WordGraph;

/**
 *
 * @author anil
 */
public class CEAnnotatedSentenceEx extends CEAnnotatedSentence {

    protected ArrayList<CESegment> referenceTranslations;

    protected WordGraph wordGraph;

    public CEAnnotatedSentenceEx() {
        
    }

    /**
     * @return the referenceTranslations
     */
    public ArrayList<CESegment> getReferenceTranslations() {
        return referenceTranslations;
    }

    /**
     * @param referenceTranslations the referenceTranslations to set
     */
    public void setReferenceTranslations(ArrayList<CESegment> referenceTranslations) {
        this.referenceTranslations = referenceTranslations;
    }

    /**
     * @return the wordGraph
     */
    public WordGraph getWordGraph() {
        return wordGraph;
    }

    public void initGraph()
    {
//        getWordGraph().initGraph(this);
    }

    /**
     * @return the first reference translation
     */
    public CESegment getReferenceTranslation()
    {
        return referenceTranslations.get(0);
    }

    /**
     * @param referenceTranslation the referenceTranslation to add
     */
    public void addReferenceTranslation(CESegment referenceTranslation) {
        referenceTranslations.add(referenceTranslation);
    }
    
    public void addReferenceTranslation(String senStr)
    {
        addReferenceTranslation(new CESegment(senStr));
    }    
}
