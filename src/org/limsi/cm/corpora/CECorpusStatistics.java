/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import org.limsi.cm.resources.CEResources;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import sanchay.table.SanchayTableModel;

/**
 *
 * @author anil
 */
public class CECorpusStatistics {
    
    protected SSFStory sourceText;
    protected SSFStory targetText;
    
    protected boolean batchMode;

    protected SanchayTableModel batchPaths;
    
    protected LinkedHashMap<String, Object> statisticsObjects;
    
    public CECorpusStatistics() {
        CEResources.setSSFProps();
//        loadSSFCorpus();
    }    

    /**
     * @return the sourceText
     */
    public SSFStory getSourceText() {
        return sourceText;
    }

    /**
     * @param sourceText the sourceText to set
     */
    public void setSourceText(SSFStory sourceText) {
        this.sourceText = sourceText;
    }

    /**
     * @return the targetText
     */
    public SSFStory getTargetText() {
        return targetText;
    }

    /**
     * @param targetText the targetText to set
     */
    public void setTargetText(SSFStory targetText) {
        this.targetText = targetText;
    }

    /**
     * @return the batchMode
     */
    public boolean isBatchMode() {
        return batchMode;
    }

    /**
     * @param batchMode the batchMode to set
     */
    public void setBatchMode(boolean batchMode) {
        this.batchMode = batchMode;
    }

    /**
     * @return the batchPaths
     */
    public SanchayTableModel getBatchPaths() {
        return batchPaths;
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void setBatchPaths(SanchayTableModel batchPaths) {
        this.batchPaths = batchPaths;
    }

    /**
     * @param batchPaths the batchPaths to set
     */
    public void loadBatchPaths(String batchPaths) throws FileNotFoundException, IOException {
        this.batchPaths = new SanchayTableModel(batchPaths, "UTF-8");
        
        this.batchPaths.read(batchPaths, "UTF-8");
        
//        this.batchPaths.print(System.out);
    }

    public void loadSSFCorpus(String srcPath, String srcCS, String tgtPath, String tgtCS) throws FileNotFoundException, IOException, Exception
    {
        sourceText = new SSFStoryImpl();
        targetText = new SSFStoryImpl();
        
        System.out.println("Loading source corpus...");
        sourceText.readFile(srcPath, srcCS);
        System.out.printf("Loaded %d source sentences\n", sourceText.countSentences());
        System.out.println("Loading target corpus...");
        getTargetText().readFile(tgtPath, tgtCS);
        System.out.printf("Loaded %d target sentences\n", targetText.countSentences());
    }
     
    public void calculateStatistics(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException
    {
        statisticsObjects = new LinkedHashMap<String, Object>();
        
//        loadSSFCorpus();
        
        PrintStream ps = new PrintStream(outFilePath, encoding);
        
        int count = sourceText.countSentences();
        
        for (int i = 0; i < count; i++) {
//            HypothesisFeatures hf = collectHypothesisFeatures(i, 0, null);

//            hf.printFeatures(ps);            
        }
    }
     
    public void calculateStatisticsBatch(String outFilePath, String encoding) throws FileNotFoundException, UnsupportedEncodingException, IOException, Exception
    {
        if(batchMode == false)
        {
            calculateStatistics(outFilePath, encoding);
            return;
        }

        if(batchPaths == null)
        {
            System.err.println("Batch paths not loaded.");
            return;
        }
        
        int rcount = batchPaths.getRowCount();
        
        for (int i = 0; i < rcount; i++)
        {
            String srcpath = (String) batchPaths.getValueAt(i, 0);
            String srcCS = (String) batchPaths.getValueAt(i, 1);
            String tgtpath = (String) batchPaths.getValueAt(i, 2);
            String tgtCS = (String) batchPaths.getValueAt(i, 3);
            String outpath = (String) batchPaths.getValueAt(i, 4);
            String outCS = (String) batchPaths.getValueAt(i, 5);

            System.out.printf("Processing files %s and %s\n", srcpath, tgtpath);
            
            loadSSFCorpus(srcpath, srcCS, tgtpath, tgtCS);
            
            calculateStatistics(outpath, outCS);
        }
    }
    
}
