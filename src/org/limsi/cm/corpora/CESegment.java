/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author anil
 */
public class CESegment {
    
    protected ArrayList<Integer> wdIndices;
    
//    protected static Index<String> wdIndex;
    protected static WeightedHashIndex<String, Integer> wdIndex
            = new WeightedHashIndex<String, Integer>(Integer.class);

    public CESegment() {
        
    }

    public CESegment(String senStr) {
        setString(senStr);
    }

    protected ArrayList<Integer> getIndices()
    {
        return wdIndices;
    }

    protected void setIndices(ArrayList<Integer> wdIndices)
    {
        this.wdIndices = wdIndices;
    }

    public String getString()
    {
        return getString(wdIndices);
    }

    private void setString(String s)
    {
        wdIndices = getIndices(s, true);
    }
    
    public int getLength()
    {
        return wdIndices.size();
    }
    
    public String getWord(int i)
    {
        int wi = wdIndices.get(i);
        
        return wdIndex.get(wi);
    }

    public ArrayList<String> getWords()
    {
        ArrayList<String> words = new ArrayList<String>();
        
        Iterator<Integer> itr = wdIndices.iterator();
        
        while(itr.hasNext())
        {
            int wi = itr.next();
        
            String ws = wdIndex.get(wi);
            
            words.add(ws);
        }
        
        return words;
    }
    
    @Override
    public boolean equals(Object seg)
    {
        if(seg instanceof CESegment)
        {
            return ((CESegment)seg).getString().equals(getString());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.wdIndices != null ? this.wdIndices.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString()
    {
        return getString(wdIndices);
    }
    
    public static ArrayList<Integer> getIndices(String wds, boolean add)
    {
//        if(wdIndex == null)
//        {
////            wdIndex = new HashIndex<String>();
//            wdIndex = new WeightedHashIndex<String, Integer>(Integer.class);
//        }            
        
        String parts[] = wds.split("\\s+");
        
        ArrayList<Integer> indices = new ArrayList<Integer>(parts.length);
        
        for (int i = 0; i < parts.length; i++)
        {
            int wi = wdIndex.indexOf(parts[i], add);
            
            indices.add(wi);
        }
        
        return indices;
    }

    public static String getString(ArrayList<Integer> wdIndices)
    {
//        if(wdIndex == null)
//        {
//            wdIndex = new HashIndex<String>();
//        }            

        String str = "";
        
        for (Integer wi : wdIndices) {
            str += " " + wdIndex.get(wi);
        }
        
        return str;
    }
    
    public static void printIndex(PrintStream ps)
    {
        wdIndex.print(ps);
    }
}
