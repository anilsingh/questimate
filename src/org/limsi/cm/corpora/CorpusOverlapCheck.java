/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.util.MiscellaneousUtils;

/**
 *
 * @author anil
 */
public class CorpusOverlapCheck {
    public static int countSourceSentenceOverlap(CEAnnotatedCorpus c1,
            CEAnnotatedCorpus c2)
    {
        Index<String> index = new HashIndex<String>();

        MiscellaneousUtils.addSourceSentencesToIndex(c1, index);

        int numOverlap = MiscellaneousUtils.numSourceSentencesInIndex(c2, index);
        
        return numOverlap;
    }
    
    public static void checkOverlap(String path1, String path2, String cs)
    {
        CEAnnotatedCorpusTrace<CEAnnotatedSentence> ceCorpus1 = new CEAnnotatedCorpusTrace<CEAnnotatedSentence>(CEAnnotatedSentence.class);
        CEAnnotatedCorpusTrace<CEAnnotatedSentence> ceCorpus2 = new CEAnnotatedCorpusTrace<CEAnnotatedSentence>(CEAnnotatedSentence.class);
        
        ceCorpus1.setCharset(cs);
        ceCorpus2.setCharset(cs);
        
        try {
            ceCorpus1.setFilePath(path1);
            ceCorpus1.readTrace();

            ceCorpus2.setFilePath(path2);
            ceCorpus2.readTrace();

            int n = CorpusOverlapCheck.countSourceSentenceOverlap(ceCorpus1, ceCorpus2);
            System.out.format("Number of overlapping sentences between %s and %s is %d\n", path1, path2, n);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CEAnnotatedCorpusTrace.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CEAnnotatedCorpusTrace.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void main(String args[])
    {
//        checkOverlap("/more/work/trace-qe/corrections/en2fr/corrections_en2fr.data",
//                "/more/work/trace-qe/corrections/agreement/en_first.data", "UTF-8");
//
//        checkOverlap("/more/work/trace-qe/corrections/en2fr/corrections_en2fr.data",
//                "/more/work/trace-qe/corrections/agreement/en_second.data", "UTF-8");
//
//        checkOverlap("/more/work/trace-qe/corrections/fr2en/corrections_fr2en.data",
//                "/more/work/trace-qe/corrections/agreement/fr_first.data", "UTF-8");
//
//        checkOverlap("/more/work/trace-qe/corrections/fr2en/corrections_fr2en.data",
//                "/more/work/trace-qe/corrections/agreement/fr_second.data", "UTF-8");

        checkOverlap("/more/work/trace-qe/corrections/agreement/en_first.data",
                "/more/work/trace-qe/corrections/agreement/en_second.data", "UTF-8");

        checkOverlap("/more/work/trace-qe/corrections/agreement/fr_first.data",
                "/more/work/trace-qe/corrections/agreement/fr_second.data", "UTF-8");
        
        CESegment.printIndex(System.out);
    }
}
