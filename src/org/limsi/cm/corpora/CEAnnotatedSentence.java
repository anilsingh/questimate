/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.io.PrintStream;
import java.util.ArrayList;
import org.limsi.cm.util.MiscellaneousUtils;

/**
 *
 * @author anil
 */
public class CEAnnotatedSentence extends CESentence {

    protected String segmentID;
    protected int segmentSource;
    protected int errorInSourceSentence;
    protected int useOfMachineTranslatedSentence;
    protected int machineTranslationSystem;    
    
    protected static Index<String> corpusSourceIndex;
    protected static Index<String> errorIndex;
    protected static Index<String> mtUseIndex;
    protected static Index<String> mtSystemIndex;

    protected ArrayList<CESegment> correctTranslations;

    public CEAnnotatedSentence() {        
        initIndexes();
        correctTranslations = new ArrayList<CESegment>();
    }
    
    private static void initIndexes()
    {
        if(corpusSourceIndex == null)
        {
            corpusSourceIndex = new HashIndex<String>();
            errorIndex = new HashIndex<String>();
            mtUseIndex = new HashIndex<String>();
            mtSystemIndex = new HashIndex<String>();        
        }
    }

    /**
     * @return the segmentID
     */
    public String getSegmentID() {
        return segmentID;
    }

    /**
     * @param segmentID the segmentID to set
     */
    public void setSegmentID(String segmentID) {
        this.segmentID = segmentID;
    }

    /**
     * @return the segmentSource
     */
    public String getSegmentSource() {
        return corpusSourceIndex.get(segmentSource);
    }

    /**
     * @param segmentSource the segmentSource to set
     */
    public void setSegmentSource(String segmentSource) {
        this.segmentSource = corpusSourceIndex.indexOf(segmentSource, true);
    }

    /**
     * @return the correctTranslations
     */
    public ArrayList<CESegment> getCorrectTranslations() {
        return correctTranslations;
    }

    /**
     * @param correctTranslations the correctTranslations to set
     */
    public void setCorrectTranslations(ArrayList<CESegment> correctTranslations) {
        this.correctTranslations = correctTranslations;
    }

    /**
     * @return the errorInSourceSentence
     */
    public String getErrorInSourceSentence() {
        return errorIndex.get(errorInSourceSentence);
    }

    /**
     * @param errorInSourceSentence the errorInSourceSentence to set
     */
    public void setErrorInSourceSentence(String errorInSourceSentence) {
        this.errorInSourceSentence = errorIndex.indexOf(errorInSourceSentence, true);
    }

    /**
     * @return the useOfMachineTranslatedSentence
     */
    public String getUseOfMachineTranslatedSentence() {
        return mtUseIndex.get(useOfMachineTranslatedSentence);
    }

    /**
     * @param useOfMachineTranslatedSentence the useOfMachineTranslatedSentence to set
     */
    public void setUseOfMachineTranslatedSentence(String useOfMachineTranslatedSentence) {
        this.useOfMachineTranslatedSentence = mtUseIndex.indexOf(useOfMachineTranslatedSentence, true);
    }

    /**
     * @return the machineTranslationSystem
     */
    public String getMachineTranslationSystem() {
        return mtSystemIndex.get(machineTranslationSystem);
    }

    /**
     * @param machineTranslationSystem the machineTranslationSystem to set
     */
    public void setMachineTranslationSystem(String machineTranslationSystem) {
        this.machineTranslationSystem = mtSystemIndex.indexOf(machineTranslationSystem, true);
    }
    
    /**
     * @return the first correct translation
     */
    public CESegment getCorrectTranslation()
    {
        return correctTranslations.get(0);
    }

    /**
     * @param correctTranslation the correctTranslation to add
     */
    public void addCorrectTranslation(CESegment correctTranslation) {
        correctTranslations.add(correctTranslation);
    }
    
    public void addCorrectTranslation(String senStr)
    {
        addCorrectTranslation(new CESegment(senStr));
    }
            
    public void readTrace(String strLine)
    {
//        System.out.println(strLine);
        
        String fields[] = strLine.split("\\|\\|\\|");

//        System.out.println(fields[0]);
        
//        if(fields[0].equalsIgnoreCase("SEGMENT ID"))
//            return;

        setSegmentID(fields[0].trim());

        setSegmentSource(fields[1].trim());
        
//        System.out.println(fields[1].trim());
//        System.out.println(getSegmentSource());
        
        setSourceSentence(new CESegment(fields[2].trim()));
        
        setErrorInSourceSentence(fields[3].trim());
        
        addCorrectTranslation(fields[4].trim());

        setUseOfMachineTranslatedSentence(fields[5].trim());

        setTranslation(fields[6].trim());
        
        if(fields[7].trim().equals("") == false)
            setMachineTranslationSystem(fields[7]);
    }

    public void printTrace(PrintStream ps)
    {
        ps.print(segmentID + " ||| ");
        
        ps.print(getSegmentSource() + " ||| ");
//        System.out.println(getSegmentSource());

        ps.print(sourceSentence + " ||| ");
        
        ps.print(getErrorInSourceSentence() + " ||| ");
        
        ps.print(getCorrectTranslation() + " ||| ");
        
        ps.print(getUseOfMachineTranslatedSentence() + " ||| ");

        ps.print(getTranslation() + " ||| ");
        
        ps.print(getMachineTranslationSystem());
        
        ps.println();
    }
    
    public static void printIndexes(PrintStream ps)
    {
        ps.println("Corpus source index:");
        MiscellaneousUtils.printIndex(corpusSourceIndex, ps);

        ps.println("Translation error index:");
        MiscellaneousUtils.printIndex(errorIndex, ps);

        ps.println("MT system index:");
        MiscellaneousUtils.printIndex(mtSystemIndex, ps);

        ps.println("MT system use index:");
        MiscellaneousUtils.printIndex(mtUseIndex, ps);
    }
}
