/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.util.ArrayList;

/**
 *
 * @author anil
 */
public class CESentence {
    
    protected CESegment sourceSentence;

    protected ArrayList<CESegment> translations;

    public CESentence() {
        translations = new ArrayList<CESegment>();
    }

    /**
     * @return the sourceSentence
     */
    public CESegment getSourceSentence() {
        return sourceSentence;
    }

    /**
     * @param sourceSentence the sourceSentence to set
     */
    public void setSourceSentence(CESegment sourceSentence) {
        this.sourceSentence = sourceSentence;
    }

    /**
     * @return the translations
     */
    public ArrayList<CESegment> getTranslations() {
        return translations;
    }

    /**
     * @param translations the translations to set
     */
    public void setTranslations(ArrayList<CESegment> translations) {
        this.translations = translations;
    }

    /**
     * @return the first translated sentence
     */
    public CESegment getTranslation()
    {
        return translations.get(0);
    }

    /**
     * @param translation the translation to add
     */
    public void addTranslation(CESegment translation) {
        translations.add(translation);
    }
    
    public void setSourceSentence(String senStr)
    {
        sourceSentence = new CESegment(senStr);
    }
    
    public void setTranslation(String senStr)
    {
        addTranslation(new CESegment(senStr));
    }
    
}
