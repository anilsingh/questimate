/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import org.limsi.cm.corpora.CEAnnotatedCorpus;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.mt.base.IString;
import edu.stanford.nlp.mt.base.IStrings;
import edu.stanford.nlp.mt.base.RawIStringSequence;
import edu.stanford.nlp.mt.base.Sequence;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.limsi.mt.cm.AnnotatedTranslation;

/**
 *
 * @author anil
 */
public class CEAnnotatedCorpusWMT12<T extends CEAnnotatedSentence>
        extends CEAnnotatedCorpus<T> {
    protected static String sourceLanguage;
    protected static String targetLanguage;

    protected static String sourceEncoding = "UTF-8";
    protected static String targetEncoding = "UTF-8";
    
    protected static String encoding = "UTF-8";
    
    protected static String sourcePath = "/people/anil/work/wmt12-quality-estimation/training_set/source.eng.tok";
    protected static String targetPath = "/people/anil/work/wmt12-quality-estimation/training_set/target_system.spa.tok";
    protected static String referencePath = "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_reference.spa";
    protected static String postEditedPath = "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_postedited.spa";

    protected static String weightedEffortScroesPath = "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_system.effort";
    protected static String effortScoresPath = "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_system.effort3scores";
    protected static String effortWeightsPath = "/people/anil/work/wmt12-quality-estimation/training_set_annotations/target_system.effort3weights";

    protected static String extraInfoPath = "/people/anil/work/wmt12-quality-estimation/wmt12qe.training";

//    protected static String sourcePath = "/people/anil/work/wmt12-quality-estimation/test_set/source.eng";
//    protected static String sourcePath = "/people/artem/Projects/SMT/extrafeat/newstest2012.fr";
//    protected static String targetPath = "/people/anil/work/wmt12-quality-estimation/test_set/target_system.spa";
//    protected static String referencePath = "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_reference.spa";
//    protected static String postEditedPath = "/people/anil/work/wmt12-quality-estimation/test_set/target_postedited.spa";
//
//    protected static String weightedEffortScroesPath = "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_system.effort";
//    protected static String effortScoresPath = "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_system.effort3scores";
//    protected static String effortWeightsPath = "/people/anil/work/wmt12-quality-estimation/test_set_annotations/target_system.effort3weights";
//
//    protected static String extraInfoPath = "/people/anil/work/wmt12-quality-estimation/wmt12qe.test";
    
    protected static List<AnnotatedTranslation<IString, String>> annotatedTranslations;
    
    public CEAnnotatedCorpusWMT12()
    {
        annotatedTranslations = new ArrayList<AnnotatedTranslation<IString, String>>();
        try {
            readCorpus();
        } catch (IOException ex) {
            Logger.getLogger(CEAnnotatedCorpusWMT12.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the sourceLanguage
     */
    public static String getSourceLanguage() {
        return sourceLanguage;
    }

    /**
     * @param aSourceLanguage the sourceLanguage to set
     */
    public static void setSourceLanguage(String aSourceLanguage) {
        sourceLanguage = aSourceLanguage;
    }

    /**
     * @return the targetLanguage
     */
    public static String getTargetLanguage() {
        return targetLanguage;
    }

    /**
     * @param aTargetLanguage the targetLanguage to set
     */
    public static void setTargetLanguage(String aTargetLanguage) {
        targetLanguage = aTargetLanguage;
    }

    /**
     * @return the sourceEncoding
     */
    public static String getSourceEncoding() {
        return sourceEncoding;
    }

    /**
     * @param aSourceEncoding the sourceEncoding to set
     */
    public static void setSourceEncoding(String aSourceEncoding) {
        sourceEncoding = aSourceEncoding;
    }

    /**
     * @return the targetEncoding
     */
    public static String getTargetEncoding() {
        return targetEncoding;
    }

    /**
     * @param aTargetEncoding the targetEncoding to set
     */
    public static void setTargetEncoding(String aTargetEncoding) {
        targetEncoding = aTargetEncoding;
    }

    /**
     * @return the encoding
     */
    public static String getEncoding() {
        return encoding;
    }

    /**
     * @param aEncoding the encoding to set
     */
    public static void setEncoding(String aEncoding) {
        encoding = aEncoding;
    }

    /**
     * @return the sourcePath
     */
    public static String getSourcePath() {
        return sourcePath;
    }

    /**
     * @param aSourcePath the sourcePath to set
     */
    public static void setSourcePath(String aSourcePath) {
        sourcePath = aSourcePath;
    }

    /**
     * @return the targetPath
     */
    public static String getTargetPath() {
        return targetPath;
    }

    /**
     * @param aTargetPath the targetPath to set
     */
    public static void setTargetPath(String aTargetPath) {
        targetPath = aTargetPath;
    }

    /**
     * @return the referencePath
     */
    public static String getReferencePath() {
        return referencePath;
    }

    /**
     * @param aReferencePath the referencePath to set
     */
    public static void setReferencePath(String aReferencePath) {
        referencePath = aReferencePath;
    }

    /**
     * @return the postEditedPath
     */
    public static String getPostEditedPath() {
        return postEditedPath;
    }

    /**
     * @param aPostEditedPath the postEditedPath to set
     */
    public static void setPostEditedPath(String aPostEditedPath) {
        postEditedPath = aPostEditedPath;
    }

    /**
     * @return the weightedEffortScroesPath
     */
    public static String getWeightedEffortScroesPath() {
        return weightedEffortScroesPath;
    }

    /**
     * @param aWeightedEffortScroesPath the weightedEffortScroesPath to set
     */
    public static void setWeightedEffortScroesPath(String aWeightedEffortScroesPath) {
        weightedEffortScroesPath = aWeightedEffortScroesPath;
    }

    /**
     * @return the effortScoresPath
     */
    public static String getEffortScoresPath() {
        return effortScoresPath;
    }

    /**
     * @param aEffortScoresPath the effortScoresPath to set
     */
    public static void setEffortScoresPath(String aEffortScoresPath) {
        effortScoresPath = aEffortScoresPath;
    }

    /**
     * @return the effortWeightsPath
     */
    public static String getEffortWeightsPath() {
        return effortWeightsPath;
    }

    /**
     * @param aEffortWeightsPath the effortWeightsPath to set
     */
    public static void setEffortWeightsPath(String aEffortWeightsPath) {
        effortWeightsPath = aEffortWeightsPath;
    }

    /**
     * @return the extraInfoPath
     */
    public static String getExtraInfoPath() {
        return extraInfoPath;
    }

    /**
     * @param aExtraInfoPath the extraInfoPath to set
     */
    public static void setExtraInfoPath(String aExtraInfoPath) {
        extraInfoPath = aExtraInfoPath;
    }

    /**
     * @param aAnnotatedTranslations the annotatedTranslations to set
     */
    public static void setAnnotatedTranslations(List<AnnotatedTranslation<IString, String>> aAnnotatedTranslations) {
        annotatedTranslations = aAnnotatedTranslations;
    }

    /**
     * @return the annotatedTranslation
     */
    public static List<AnnotatedTranslation<IString, String>> getAnnotatedTranslations() {
        return annotatedTranslations;
    }
    
    public static void readCorpus() throws IOException
    {
        LineNumberReader sourceReader;
        LineNumberReader targetReader;
        LineNumberReader referenceReader;
        LineNumberReader postEditedReader;
        
        LineNumberReader weightedEffortScroesReader;
        LineNumberReader effortScoresReader;
        LineNumberReader effortWeightsReader;

//        LineNumberReader extraInfoReader;

        if (getSourcePath().endsWith(".gz")) {
            sourceReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getSourcePath())), getEncoding()));
        } else {
            sourceReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getSourcePath()), getEncoding()));
        }

        if (getTargetPath().endsWith(".gz")) {
            targetReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getTargetPath())), getEncoding()));
        } else {
            targetReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getTargetPath()), getEncoding()));
        }

        if (getReferencePath().endsWith(".gz")) {
            referenceReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getReferencePath())), getEncoding()));
        } else {
            referenceReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getReferencePath()), getEncoding()));
        }

        if (getPostEditedPath().endsWith(".gz")) {
            postEditedReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getPostEditedPath())), getEncoding()));
        } else {
            postEditedReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getPostEditedPath()), getEncoding()));
        }

        if (getWeightedEffortScroesPath().endsWith(".gz")) {
            weightedEffortScroesReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getWeightedEffortScroesPath())), getEncoding()));
        } else {
            weightedEffortScroesReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getWeightedEffortScroesPath()), getEncoding()));
        }

        if (getEffortScoresPath().endsWith(".gz")) {
            effortScoresReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getEffortScoresPath())), getEncoding()));
        } else {
            effortScoresReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getEffortScoresPath()), getEncoding()));
        }

        if (getEffortWeightsPath().endsWith(".gz")) {
            effortWeightsReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(getEffortWeightsPath())), getEncoding()));
        } else {
            effortWeightsReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    getEffortWeightsPath()), getEncoding()));
        }
        
//        if (extraInfoPath.endsWith(".gz")) {
//            extraInfoReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
//                    new FileInputStream(extraInfoPath)), encoding));
//        } else {
//            extraInfoReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
//                    extraInfoPath), encoding));
//        }
        
//        String sourceStr;
        String targetStr;
        String referenceStr;
        String postEditedStr;

        String weightedEffortScroesStr;
        String effortScoresStr;
        String effortWeightsStr;

        Pattern space = Pattern.compile(" ");
        
//        String extraInfoStr;
        
//        while((sourceStr = sourceReader.readLine()) == null)
        for (String sourceStr; (sourceStr = sourceReader.readLine()) != null;)
        {
            AnnotatedTranslation<IString, String> annotatedTranslation = new AnnotatedTranslation<IString, String>();

            Sequence<IString> sourceSequence = new RawIStringSequence(IStrings.toIStringArray(space.split(sourceStr)));
            annotatedTranslation.setSource(sourceSequence);
            
            targetStr = targetReader.readLine();
            
            Sequence<IString> targetSequence = new RawIStringSequence(IStrings.toIStringArray(space.split(targetStr)));
            annotatedTranslation.setTranslation(targetSequence);

            referenceStr = referenceReader.readLine();
            
            Sequence<IString> referenceSequence = new RawIStringSequence(IStrings.toIStringArray(space.split(referenceStr)));
            annotatedTranslation.setReference(referenceSequence);

            postEditedStr = postEditedReader.readLine();
            
            Sequence<IString> postEditedSequence = new RawIStringSequence(IStrings.toIStringArray(space.split(postEditedStr)));
            annotatedTranslation.setPostEdited(postEditedSequence);
            
            weightedEffortScroesStr = weightedEffortScroesReader.readLine();
            
            annotatedTranslation.setWeightedAvgEffort(Double.parseDouble(weightedEffortScroesStr));

            effortScoresStr = effortScoresReader.readLine();
            
            annotatedTranslation.setEffortScores(effortScoresStr);

            effortWeightsStr = effortWeightsReader.readLine();
            
            annotatedTranslation.setEffortWeights(effortWeightsStr);
            
//            extraInfoStr = extraInfoReader.
            
            annotatedTranslations.add(annotatedTranslation);
        }
    }
    
    public void print(PrintStream ps)
    {
        AnnotationPipeline ap = new AnnotationPipeline();
        boolean verbose = false;
        ap.addAnnotator(new PTBTokenizerAnnotator(verbose));
        ap.addAnnotator(new WordsToSentencesAnnotator(verbose));
        // ap.addAnnotator(new NERCombinerAnnotator(verbose));
        // ap.addAnnotator(new OldNERAnnotator(verbose));
        // ap.addAnnotator(new NERMergingAnnotator(verbose));
//        ap.addAnnotator(new MorphaAnnotator(verbose));
//        ap.addAnnotator(new POSTaggerAnnotator(verbose));
//        ap.addAnnotator(new UpdateSentenceFromParseAnnotator(verbose));
//        ap.addAnnotator(new NumberAnnotator(verbose));
//        ap.addAnnotator(new QuantifiableEntityNormalizingAnnotator(verbose));
//        ap.addAnnotator(new StemmerAnnotator(verbose));
//        ap.addAnnotator(new MorphaAnnotator(verbose));
        ap.addAnnotator(new ParserAnnotator(verbose, -1));

        NERCombinerAnnotator nera = null;
        
        try {
            nera = new NERCombinerAnnotator(verbose);
        } catch (IOException ex) {
            Logger.getLogger(CEAnnotatedCorpusWMT12.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CEAnnotatedCorpusWMT12.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        for (AnnotatedTranslation<IString, String> annotatedTranslation : annotatedTranslations) {
            annotatedTranslation.print(ps);            
        
//            String text = ("USAir said in the filings that Mr. Icahn first contacted Mr. Colodny last September to discuss the benefits of combining TWA and USAir -- either by TWA's acquisition of USAir, or USAir's acquisition of TWA.");
            Annotation a = new Annotation(annotatedTranslation.getSource().toString());

            ap.annotate(a);
            nera.annotate(a);
            
            ps.println(a.toString());
            
            for (CoreMap sentence : a.get(CoreAnnotations.SentencesAnnotation.class)) {
//                System.out.println(sentence.get(CoreAnnotations.PartOfSpeechAnnotation.class));
//                System.out.println(sentence.get(CoreAnnotations.MorphoGenAnnotation.class));
//                System.out.println(sentence.get(CoreAnnotations.MorphoNumAnnotation.class));
//                System.out.println(sentence.get(CoreAnnotations.MorphoPersAnnotation.class));
//                System.out.println(sentence.get(CoreAnnotations.MorphoCaseAnnotation.class));
                System.out.println(sentence.get(CoreAnnotations.NamedEntityTagAnnotation.class));
                System.out.println(sentence.get(TreeCoreAnnotations.TreeAnnotation.class));
            }
            
            ps.println("*******************************");
        }
    }
    
    public static void main(String args[])
    {
        CEAnnotatedCorpusWMT12 corpus = new CEAnnotatedCorpusWMT12();
        
//        try {
//            corpus.readCorpus();
//        } catch (IOException ex) {
//            Logger.getLogger(CEAnnotatedCorpusWMT12.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        corpus.print(System.out);
    }
}
