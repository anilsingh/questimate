/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.limsi.cm.util.MiscellaneousUtils;

/**
 *
 * @author anil
 */
public class CEAnnotatedCorpusTrace<T extends CEAnnotatedSentence>
        extends CEAnnotatedCorpus<T> {
    
    public CEAnnotatedCorpusTrace(Class<T> classRef) {
        reference = classRef;
        sentences = new LinkedHashMap<String, T>();
    }
        
    public void readTrace() throws FileNotFoundException, IOException
    {
        FileInputStream fstream = new FileInputStream(filePath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, charset));
        
        String strLine = "";
        
        T ceSentence = null;

        while ((strLine = br.readLine()) != null)
        {
            if(strLine.startsWith("SEGMENT ID"))
                continue;
            
            ceSentence = getAnnotatedSentenceInstance();
            
            ceSentence.readTrace(strLine);
            
            String id = ceSentence.getSegmentID();
            
            Pattern p = Pattern.compile("-[0-9]+$");
            
            if(sentences.containsKey(id))
            {
                Matcher m = p.matcher(id);
                
                if(m.matches())
                {
                    String numStr = m.group();
                    
                    int num = Integer.parseInt(numStr);
                    
                    id = id.replaceAll(numStr, "-" + (num + 1));
                    
                    ceSentence.setSegmentID(id);
                }
                else
                {
                    ceSentence.setSegmentID(id + "-2");                    
                }
            }
            
            sentences.put(ceSentence.getSegmentID(), ceSentence);
        }

        in.close();        
    }

    public void printTrace(PrintStream ps)
    {
        ps.print("SEGMENT ID ||| ");
        ps.print("SEGMENT ORIGIN ||| ");
        ps.print("SOURCE ||| ");
        ps.print("ERRORS IN THE SOURCE ||| ");
        ps.print("POST-EDITED HYPOTHESIS ||| ");
        ps.print("AUTOMATIC TRANSLATION USED ||| ");
        ps.print("TRANSLATION HYPOTHESIS ||| ");
        ps.println("TRANSLATION SYSTEM");

        Iterator<String> itr = sentences.keySet().iterator();
        
        while(itr.hasNext())
        {
            String id = itr.next();
            
            sentences.get(id).printTrace(ps);
        }
        
        CEAnnotatedSentence.printIndexes(System.out);
    }

    public void save() throws FileNotFoundException, UnsupportedEncodingException
    {
        PrintStream ps = new PrintStream(filePath, charset);
        
        printTrace(ps);
    }
    
    public static void main(String[] args)
    {
        CEAnnotatedCorpusTrace<CEAnnotatedSentence> ceCorpus = new CEAnnotatedCorpusTrace<CEAnnotatedSentence>(CEAnnotatedSentence.class);
        
//        ceCorpus.setFilePath("/people/wisniews/data/corrections_trace/en/Corpus_LIMSI_EN_1_500.data");
        ceCorpus.setCharset("UTF-8");
        
        try {
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/en2fr/corrections_en2fr.data");
            ceCorpus.readTrace();

            ceCorpus.clear();
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/fr2en/corrections_fr2en.data");
            ceCorpus.readTrace();

            ceCorpus.clear();
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/agreement/en_first.data");
            ceCorpus.readTrace();

            ceCorpus.clear();
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/agreement/en_second.data");
            ceCorpus.readTrace();

            ceCorpus.clear();
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/agreement/fr_first.data");
            ceCorpus.readTrace();

            ceCorpus.clear();
            ceCorpus.setFilePath("/more/work/trace-qe/corrections/agreement/fr_second.data");
            ceCorpus.readTrace();
            
            CEAnnotatedSentence.printIndexes(System.out);
            
//            PrintStream ps = new PrintStream("tmp/tmp.txt", "UTF-8");
//            
//            ceCorpus.printTrace(ps);
            
//            ps.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CEAnnotatedCorpusTrace.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CEAnnotatedCorpusTrace.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
}
