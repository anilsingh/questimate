/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.corpora;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 *
 * @author anil
 */
public class CEAnnotatedCorpus<T extends CEAnnotatedSentence> {
    
    protected Class<T> reference;

    protected String charset;
    
    protected String sourceLangEnc;
    protected String targetLangEnc;

    protected String filePath;
    
    protected LinkedHashMap<String, T> sentences;

    protected CEAnnotatedCorpus() {
        sentences = new LinkedHashMap<String, T>();
    }

    public CEAnnotatedCorpus(Class<T> classRef) {
        reference = classRef;
        sentences = new LinkedHashMap<String, T>();
    }
    
    public void clear()
    {
        sentences.clear();
    }

    protected T getAnnotatedSentenceInstance()
    {
        try
        {
            return reference.newInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @return the charset
     */
    public String getCharset() {
        return charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * @return the sourceLangEnc
     */
    public String getSourceLangEnc() {
        return sourceLangEnc;
    }

    /**
     * @param sourceLangEnc the sourceLangEnc to set
     */
    public void setSourceLangEnc(String sourceLangEnc) {
        this.sourceLangEnc = sourceLangEnc;
    }

    /**
     * @return the targetLangEnc
     */
    public String getTargetLangEnc() {
        return targetLangEnc;
    }

    /**
     * @param targetLangEnc the targetLangEnc to set
     */
    public void setTargetLangEnc(String targetLangEnc) {
        this.targetLangEnc = targetLangEnc;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
    public Iterator<String> getIDs()
    {
        return sentences.keySet().iterator();
    }
    
    public T getAnnotatedSentence(String id)
    {
        return sentences.get(id);
    }
}
