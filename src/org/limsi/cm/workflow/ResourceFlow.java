/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.stanford.nlp.util.Index;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.limsi.cm.resources.WordTranslationScores;
import org.limsi.cm.tools.LanguageModelingWrapper;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.props.CEProperties;
import org.limsi.types.Language;
import org.limsi.types.LMType;
import org.limsi.types.ResourceType;
import sanchay.mlearning.lm.ngram.impl.NGramCounts;
import sanchay.util.UtilityFunctions;

/**
 *
 * @author anil
 */
public class ResourceFlow implements Runnable {
    
    protected static String ngramLMTextPath;
    protected static String ngramLMPath;
    protected static String ngramLMBinaryPath;
    protected static boolean customNGramLMPath;

    protected static NgramLanguageModel<String> berkeleyLM;
    protected static WordIndexer<String> wordIndexer;
    
    protected static String ngramCountsPath;
    protected static boolean customNGramCountsPath;

    private static String ngramCountsBinaryPath;
 
    protected static List<LinkedHashMap<List<Integer>, Long>> cumFreqsList;
    protected static Index<String> ngramCountsIndex;
    protected static List<Long> topCumFreqList = new ArrayList<Long>();
   
    protected static String wordTranslationScoresPath;
    protected static boolean customWordTranslationScoresPath;
    
    protected static WordTranslationScores wordTranslationScores;

    /**
     * @return the ngramLMTextPath
     */
    public static String getNgramLMTextPath() {
        return ngramLMTextPath;
    }

    /**
     * @param aNgramLMTextPath the ngramLMTextPath to set
     */
    public static void setNgramLMTextPath(String aNgramLMTextPath) {
        ngramLMTextPath = aNgramLMTextPath;
    }

    /**
     * @return the ngramLMPath
     */
    public static String getNgramLMPath() {
        return ngramLMPath;
    }

    /**
     * @param aNgramLMPath the ngramLMPath to set
     */
    public static void setNgramLMPath(String aNgramLMPath) {
        ngramLMPath = aNgramLMPath;
    }

    /**
     * @return the ngramLMBinaryPath
     */
    public static String getNgramLMBinaryPath() {
        return ngramLMBinaryPath;
    }

    /**
     * @param aNgramLMBinaryPath the ngramLMBinaryPath to set
     */
    public static void setNgramLMBinaryPath(String aNgramLMBinaryPath) {
        ngramLMBinaryPath = aNgramLMBinaryPath;
    }

    /**
     * @return the customNGramLMPath
     */
    public static boolean isCustomNGramLMPath() {
        return customNGramLMPath;
    }

    /**
     * @param aCustomNGramLMPath the customNGramLMPath to set
     */
    public static void setCustomNGramLMPath(boolean aCustomNGramLMPath) {
        customNGramLMPath = aCustomNGramLMPath;
    }

    /**
     * @return the ngramCountsPath
     */
    public static String getNgramCountsPath() {
        return ngramCountsPath;
    }

    /**
     * @param aNgramCountsPath the ngramCountsPath to set
     */
    public static void setNgramCountsPath(String aNgramCountsPath) {
        ngramCountsPath = aNgramCountsPath;
    }

    /**
     * @return the customNGramCountsPath
     */
    public static boolean isCustomNGramCountsPath() {
        return customNGramCountsPath;
    }

    /**
     * @param aCustomNGramCountsPath the customNGramCountsPath to set
     */
    public static void setCustomNGramCountsPath(boolean aCustomNGramCountsPath) {
        customNGramCountsPath = aCustomNGramCountsPath;
    }

    /**
     * @return the ngramCountsBinaryPath
     */
    public static String getNgramCountsBinaryPath() {
        return ngramCountsBinaryPath;
    }

    /**
     * @param aNgramCountsBinaryPath the ngramCountsBinaryPath to set
     */
    public static void setNgramCountsBinaryPath(String aNgramCountsBinaryPath) {
        ngramCountsBinaryPath = aNgramCountsBinaryPath;
    }

    /**
     * @return the wordTranslationScoresPath
     */
    public static String getWordTranslationScoresPath() {
        return wordTranslationScoresPath;
    }

    /**
     * @param aWordTranslationScoresPath the wordTranslationScoresPath to set
     */
    public static void setWordTranslationScoresPath(String aWordTranslationScoresPath) {
        wordTranslationScoresPath = aWordTranslationScoresPath;
    }

    /**
     * @return the customWordTranslationScoresPath
     */
    public static boolean isCustomWordTranslationScoresPath() {
        return customWordTranslationScoresPath;
    }

    /**
     * @param aCustomWordTranslationScoresPath the customWordTranslationScoresPath to set
     */
    public static void setCustomWordTranslationScoresPath(boolean aCustomWordTranslationScoresPath) {
        customWordTranslationScoresPath = aCustomWordTranslationScoresPath;
    }

    /**
     * @return the wordTranslationScores
     */
    public static WordTranslationScores getWordTranslationScores() {
        return wordTranslationScores;
    }

    /**
     * @return the berkeleyLM
     */
    public static NgramLanguageModel getBerkeleyLM() {
        return berkeleyLM;
    }

    /**
     * @return the wordIndexer
     */
    public static WordIndexer<String> getWordIndexer() {
        return wordIndexer;
    }

    /**
     * @return the cumFreqsList
     */
    public static List<LinkedHashMap<List<Integer>, Long>> getCumFreqsList() {
        return cumFreqsList;
    }

    /**
     * @return the ngramCountsIndex
     */
    public static Index<String> getNgramCountsIndex() {
        return ngramCountsIndex;
    }

    /**
     * @return the topCumFreqList
     */
    public static List<Long> getTopCumFreqList() {
        return topCumFreqList;
    }

    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    
    public static final String PROP_BERKELEYLM = "PROP_BERKELEYLM";
    public static final String PROP_WORDINDEXER = "PROP_WORDINDEXER";
    public static final String PROP_NGRAMLMCOUNTS = "PROP_NGRAMLMCOUNTS";
    public static final String PROP_S2TWORDTRANSLATIONSCORES = "PROP_S2TWORDTRANSLATIONSCORES";
    
    public static void loadResource(ResourceType type, Enum subType, boolean reload) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        if(type.equals(ResourceType.IBM1_SCORES))
        {
            loadWordTranslationData(type.getSrcLangauge(), type.getTgtLangauge(), true);
        }
        else if(type.equals(ResourceType.NGRAM_LM_MODEL)
                || type.equals(ResourceType.NGRAM_POS_LM_MODEL)
                || type.equals(ResourceType.NGRAM_POS_NOLEX_LM_MODEL)
                || type.equals(ResourceType.NGRAM_LM_COUNTS)
                || type.equals(ResourceType.POS_TAGGED_NGRAM_COUNTS)
                || type.equals(ResourceType.POS_NOLEX_NGRAM_COUNTS))
        {
            if(type.getSrcLangauge() != null)
            {
                loadOrMakeLM((LMType) subType, type.getSrcLangauge(), reload);
            }
            else if(type.getTgtLangauge() != null)
            {
                loadOrMakeLM((LMType) subType, type.getTgtLangauge(), reload);
            }
        }
    }

    public static void freeResource(ResourceType type)
    {
        if(type.equals(ResourceType.IBM1_SCORES))
        {
            wordTranslationScores = null;
            
//            System.gc();
        }
        else if(type.equals(ResourceType.NGRAM_LM_MODEL)
                || type.equals(ResourceType.NGRAM_POS_LM_MODEL)
                || type.equals(ResourceType.NGRAM_POS_NOLEX_LM_MODEL))
        {
            berkeleyLM = null;
            wordIndexer = null;
            
//            System.gc();            
        }
        else if(type.equals(ResourceType.NGRAM_LM_COUNTS)
                || type.equals(ResourceType.POS_TAGGED_NGRAM_COUNTS)
                || type.equals(ResourceType.POS_NOLEX_NGRAM_COUNTS))
        {
            cumFreqsList = null;
            ngramCountsIndex = null;
            
            topCumFreqList = null;
            
//            System.gc();
        }        
    }

    public static void loadWordTranslationData(Language srcLanguage, Language tgtLanguage, 
            boolean reload)
    {        
        if(getWordTranslationScores() == null || reload == true)
        {
            if(!isCustomWordTranslationScoresPath())
            {
                wordTranslationScoresPath = getWordTranslationScoresPath(srcLanguage, tgtLanguage);
            }

            wordTranslationScores = new WordTranslationScores(wordTranslationScoresPath, srcLanguage.getCharset().charsetName);
        }
   }
    
    public static String getWordTranslationScoresPath(Language srcLanguage, Language tgtLanguage)
    {
        String path = null;

        if(srcLanguage.equals(Language.ENGLISH) && tgtLanguage.equals(Language.FRENCH))
        {
            path = CEProperties.getProperty("EN2FRWordIBM1ScoresPath", CEProperties.CORE_DATA_PATHS);
        }
        else if(srcLanguage.equals(Language.FRENCH) && tgtLanguage.equals(Language.ENGLISH))
        {
            path = CEProperties.getProperty("FR2ENWordIBM1ScoresPath", CEProperties.CORE_DATA_PATHS);                                
        }
        else if(srcLanguage.equals(Language.ENGLISH) && tgtLanguage.equals(Language.SPANISH))
        {
            path = CEProperties.getProperty("EN2ESWordIBM1ScoresPath", CEProperties.CORE_DATA_PATHS);
        }
        else if(srcLanguage.equals(Language.SPANISH) && tgtLanguage.equals(Language.ENGLISH))
        {
            path = CEProperties.getProperty("ES2ENWordIBM1ScoresPath", CEProperties.CORE_DATA_PATHS);                    
        }

        return path;
    }

    public static void loadOrMakeLM(LMType type, Language language, boolean reload) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        boolean load = false;

        if(reload)
        {
            load = true;
        } 
        else
        {
            if(type.equals(LMType.TEXT_LM)
                    || type.equals(LMType.POS_TAGGED_LM)
                    || type.equals(LMType.POS_NOLEX_LM))
            {
                if(getBerkeleyLM() == null)
                {
                    load = true;
                }
            }
            else if(type.equals(LMType.NGRAM_COUNTS)
                    || type.equals(LMType.NGRAM_COUNTS_POS)
                    || type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                if(getCumFreqsList() == null)
                {
                    load = true;
                }
            }
        }
        
        if(load)
        {
            if(!isCustomNGramLMPath())
            {
                ngramLMTextPath = getLMTextPath(type, language);

                ngramLMPath = getLMPath(type, language);

                ngramLMBinaryPath = getLMBinaryPath(type, language);
                
                ngramCountsPath = getNGramLMCountsPath(type, language);

                ngramCountsBinaryPath = getNGramLMCountsBinaryPath(type, language);
            }
        }        
        
        LanguageModelingWrapper languageModelingWrapper = new LanguageModelingWrapper(type);            
                
        if(type.equals(LMType.TEXT_LM)
                || type.equals(LMType.POS_TAGGED_LM)
                || type.equals(LMType.POS_NOLEX_LM))
        {
            languageModelingWrapper.loadOrMakeLM(ngramLMTextPath, ngramLMPath, ngramLMBinaryPath, language.getCharset().charsetName, false);

            berkeleyLM = languageModelingWrapper.getBerkeleyNGramLM();
            wordIndexer = languageModelingWrapper.getWordIndexer();
        }
        else if(type.equals(LMType.NGRAM_COUNTS)
                || type.equals(LMType.NGRAM_COUNTS_POS)
                || type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
        {
            languageModelingWrapper.loadOrMakeLM(ngramLMTextPath, ngramCountsPath, ngramCountsBinaryPath, language.getCharset().charsetName, false);

            NGramCounts ngramLMCounts = languageModelingWrapper.getSanchayNGramLM();

            cumFreqsList = ngramLMCounts.getCumulativeFrequenciesList();
            ngramCountsIndex = ngramLMCounts.getVocabIndex();
            
            topCumFreqList = new ArrayList<Long>();

            for (int i = 0; i < getCumFreqsList().size(); i++) {
                getTopCumFreqList().add((Long) UtilityFunctions.getLastElement(getCumFreqsList().get(i)));
            }        
        }
    }

    public static String getLMTextPath(LMType type, Language language)
    {
        String textPath = null;

        if(language.equals(Language.ENGLISH))
        {
            if(type.equals(LMType.TEXT_LM) || type.equals(LMType.NGRAM_COUNTS))
            {
                textPath = CEProperties.getProperty("ENLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM) || type.equals(LMType.NGRAM_COUNTS_POS))
            {
                textPath = CEProperties.getProperty("ENPOSLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM) || type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                textPath = CEProperties.getProperty("ENPOSNolexLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.FRENCH))
        {
            if(type.equals(LMType.TEXT_LM) || type.equals(LMType.NGRAM_COUNTS))
            {
                textPath = CEProperties.getProperty("FRLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM) || type.equals(LMType.NGRAM_COUNTS_POS))
            {
                textPath = CEProperties.getProperty("FRPOSLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM) || type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                textPath = CEProperties.getProperty("FRPOSNolexLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.SPANISH))
        {
            if(type.equals(LMType.TEXT_LM) || type.equals(LMType.NGRAM_COUNTS))
            {
                textPath = CEProperties.getProperty("ESLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM) || type.equals(LMType.NGRAM_COUNTS_POS))
            {
                textPath = CEProperties.getProperty("ESPOSLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM) || type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                textPath = CEProperties.getProperty("ESPOSNolexLanguageModelTextPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        
        return textPath;
    }

    public static String getLMPath(LMType type, Language language)
    {
        String lmPath = null;

        if(language.equals(Language.ENGLISH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                lmPath = CEProperties.getProperty("ENLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                lmPath = CEProperties.getProperty("ENPOSLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                lmPath = CEProperties.getProperty("ENPOSLMNolexPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.FRENCH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                lmPath = CEProperties.getProperty("FRLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                lmPath = CEProperties.getProperty("FRPOSLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                lmPath = CEProperties.getProperty("FRPOSLMNolexPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.SPANISH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                lmPath = CEProperties.getProperty("ESLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                lmPath = CEProperties.getProperty("ESPOSLMPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                lmPath = CEProperties.getProperty("ESPOSLMNolexPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        
        if(lmPath == null || lmPath.equals(""))
        {
            lmPath = MiscellaneousUtils.getResourcePath(ngramLMTextPath) + ".lm";
        }

        return lmPath;
    }

    public static String getLMBinaryPath(LMType type, Language language)
    {
        String binaryLMPath = "";

        if(language.equals(Language.ENGLISH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                binaryLMPath = CEProperties.getProperty("ENLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                binaryLMPath = CEProperties.getProperty("ENPOSLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                binaryLMPath = CEProperties.getProperty("ENPOSLMNolexBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.FRENCH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                binaryLMPath = CEProperties.getProperty("FRLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                binaryLMPath = CEProperties.getProperty("FRPOSLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                binaryLMPath = CEProperties.getProperty("FRPOSLMNolexBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.SPANISH))
        {
            if(type.equals(LMType.TEXT_LM))
            {
                binaryLMPath = CEProperties.getProperty("ESLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_TAGGED_LM))
            {
                binaryLMPath = CEProperties.getProperty("ESPOSLMBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.POS_NOLEX_LM))
            {
                binaryLMPath = CEProperties.getProperty("ESPOSLMNolexBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        
        if(binaryLMPath == null || binaryLMPath.equals(""))
        {
            binaryLMPath = MiscellaneousUtils.getResourcePath(ngramLMPath) + ".bin";
        }

        return binaryLMPath;
    }
    
    public static String getNGramLMCountsPath(LMType type, Language language)
    {
        String lmPath = null;

        if(language.equals(Language.ENGLISH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                lmPath = CEProperties.getProperty("ENLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                lmPath = CEProperties.getProperty("ENPOSLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                lmPath = CEProperties.getProperty("ENPOSLMNolexCountsPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.FRENCH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                lmPath = CEProperties.getProperty("FRLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                lmPath = CEProperties.getProperty("FRPOSLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                lmPath = CEProperties.getProperty("FRPOSLMNolexCountsPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.SPANISH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                lmPath = CEProperties.getProperty("ESLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                lmPath = CEProperties.getProperty("ESPOSLMCountsPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                lmPath = CEProperties.getProperty("ESPOSLMNolexCountsPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        
        if(lmPath == null || lmPath.equals(""))
        {
            lmPath = MiscellaneousUtils.getResourcePath(ngramLMPath) + ".counts";
        }

        return lmPath;
    }
    
    public static String getNGramLMCountsBinaryPath(LMType type, Language language)
    {
        String binaryLMPath = null;

        if(language.equals(Language.ENGLISH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                binaryLMPath = CEProperties.getProperty("ENLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                binaryLMPath = CEProperties.getProperty("ENPOSLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                binaryLMPath = CEProperties.getProperty("ENPOSLMNolexCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.FRENCH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                binaryLMPath = CEProperties.getProperty("FRLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                binaryLMPath = CEProperties.getProperty("FRPOSLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                binaryLMPath = CEProperties.getProperty("FRPOSLMNolexCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        else if(language.equals(Language.SPANISH))
        {
            if(type.equals(LMType.NGRAM_COUNTS))
            {
                binaryLMPath = CEProperties.getProperty("ESLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS))
            {
                binaryLMPath = CEProperties.getProperty("ESPOSLMCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
            else if(type.equals(LMType.NGRAM_COUNTS_POS_NOLEX))
            {
                binaryLMPath = CEProperties.getProperty("ESPOSLMNolexCountsBinaryPath", CEProperties.CORE_DATA_PATHS);
            }
        }
        
        if(binaryLMPath == null || binaryLMPath.equals(""))
        {
            binaryLMPath = MiscellaneousUtils.getResourcePath(ngramCountsPath) + ".bin";
        }

        return binaryLMPath;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
