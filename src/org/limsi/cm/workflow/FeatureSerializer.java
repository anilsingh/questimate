/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.limsi.cm.avro.AvroDatasetOperations;
import org.limsi.cm.avro.AvroRecordOperations;
import org.limsi.cm.avro.AvroUtils;
import org.limsi.cm.avro.PostEditedCorpusEntry;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.AvroOperationType;
import org.limsi.types.AvroType;
import org.limsi.types.FeatureType;
import sanchay.corpus.ssf.SSFSentence;

/**
 *
 * @author anil
 */
public class FeatureSerializer<T> {
    
    private FeatureExtractionJob featureExtractionJob;

    private DatumWriter<T> datumWriter;
    private DataFileWriter<T> dataFileWriter;
    
    private Schema avroSchema;
    
    private boolean reverse;
    
    public FeatureSerializer(FeatureExtractionJob featureExtractionJob, boolean reverse) {

        this.reverse = reverse;        
        
        this.featureExtractionJob = featureExtractionJob;
    }    

    /**
     * @return the featureExtractionJob
     */
    FeatureExtractionJob getFeatureExtractionJob() {
        return featureExtractionJob;
    }
   
    void init(FeatureType featureType) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        avroSchema = AvroUtils.parseSchema(new File(getFeatureExtractionJob().getAvroType().getSchemaPath()));
        
        if(featureType.equals(FeatureType.LABEL) && !reverse)
        {
            return;
        }

        if(getFeatureExtractionJob().getAvroPath() != null)
        {
            String avroPath = getFeatureExtractionJob().getAvroPath() + "." + featureType.getFeatureName();

            if(reverse)
            {
                avroPath += ".tgt";
            }
            else
            {
                avroPath += ".src";
            }

            datumWriter = new SpecificDatumWriter<T>(avroSchema);
            dataFileWriter = new DataFileWriter<T>(datumWriter);
            
            dataFileWriter.setCodec(getFeatureExtractionJob().getAvroType().getCodecFactory());
            
            dataFileWriter.create(avroSchema, new File(avroPath));
        }
    }
    
    public void serialize(FeatureExtractionCorpus featureExtractionCorpus, 
            int srcIndex, FeatureType featureType, List<HypothesisFeatures> hypothesisFeatureList) throws IOException
    {
        if(featureType.equals(FeatureType.LABEL) && !reverse)
        {
            return;
        }

        serializeAvro(featureExtractionCorpus, srcIndex, featureType, hypothesisFeatureList);
    }
    
    void serializeAvro(FeatureExtractionCorpus featureExtractionCorpus, int srcIndex,
            FeatureType featureType, List<HypothesisFeatures> hypothesisFeatureList) throws IOException
    {
        if(getFeatureExtractionJob().getAvroType().equals(AvroType.NBEST_LIST))
        {
            T avroRecord = (T) AvroUtils.getAvroNBestList(srcIndex, featureExtractionCorpus.getSourceText(),
                    hypothesisFeatureList, featureExtractionCorpus.getNbestList(), reverse);

            dataFileWriter.append(avroRecord);        
        }
        else if(getFeatureExtractionJob().getAvroType().equals(AvroType.POST_EDITED_CORPUS_ENTRY))
        {
            PostEditedCorpusEntry peCorpusRecord = new PostEditedCorpusEntry();
 
            SSFSentence srcSentence = featureExtractionCorpus.getSourceText().getSentence(srcIndex);
            
            peCorpusRecord.setSrcFte(srcSentence.convertToRawText().trim());

            if(featureExtractionCorpus.getOriginalSourceText() != null)
            {
                SSFSentence sentence = featureExtractionCorpus.getOriginalSourceText().getSentence(srcIndex);
                
                peCorpusRecord.setSrc(sentence.convertToRawText().trim());
            }
            
            if(featureExtractionCorpus.getTargetText() != null)
            {
                SSFSentence tgtSentence = featureExtractionCorpus.getTargetText().getSentence(srcIndex);
                
                peCorpusRecord.setTgtFte(tgtSentence.convertToRawText().trim());
            }

            if(featureExtractionCorpus.getOriginalTargetText() != null)
            {
                SSFSentence sentence = featureExtractionCorpus.getOriginalTargetText().getSentence(srcIndex);
                
                peCorpusRecord.setTgt(sentence.convertToRawText().trim());
            }
            
//
//            if(referenceText != null)
//            {
//                SSFSentence refSentence = referenceText.getSentence(srcIndex);
//                
//                peCorpusRecord.set(refSentence.convertToRawText().trim());
//            }

            if(featureExtractionCorpus.getPostEditedText() != null)
            {
                SSFSentence peSentence = featureExtractionCorpus.getPostEditedText().getSentence(srcIndex);
                
                peCorpusRecord.setPeTgtFte(peSentence.convertToRawText().trim());
            }

            if(featureExtractionCorpus.getOriginalPostEditedText() != null)
            {
                SSFSentence sentence = featureExtractionCorpus.getOriginalPostEditedText().getSentence(srcIndex);
                
                peCorpusRecord.setPeTgt(sentence.convertToRawText().trim());
            }
            
            peCorpusRecord.setFeatures(hypothesisFeatureList.get(0).getFeatureValuesMap());

            peCorpusRecord.setLabels(hypothesisFeatureList.get(0).getLabelValuesMap());

            T avroRecord = (T) peCorpusRecord;

            dataFileWriter.append(avroRecord);                    
        }
    }
        
    public void mergeFiles(FeatureExtractionCorpus featureExtractionCorpus, boolean addRatioFeatures) throws IOException
    {
        List<String> inpaths = new ArrayList<String>();
        
        List<FeatureType> featureTypes = featureExtractionJob.getFeatureTypes();
        
        for (FeatureType featureType : featureTypes) {

            if(featureType.equals(FeatureType.LABEL) && !reverse)
            {
                continue;
            }
            
            if(getFeatureExtractionJob().getAvroPath() != null)
            {
                String featurePath = getFeatureExtractionJob().getAvroPath() + "." + featureType.getFeatureName();
                
                featurePath += ".tgt";

                if(MiscellaneousUtils.fileExists(featurePath))
                {
                    inpaths.add(featurePath);
                }

                featurePath = getFeatureExtractionJob().getAvroPath() + "." + featureType.getFeatureName();
                
                featurePath += ".src";
                
                if(MiscellaneousUtils.fileExists(featurePath))
                {
                    inpaths.add(featurePath);
                }
            }
        }

        String tmpFilePath = getFeatureExtractionJob().getAvroPath() + ".merged";
        
        AvroDatasetOperations.mergeHorizontal(getFeatureExtractionJob().getAvroType(), inpaths,
                tmpFilePath);
        
        System.out.println("Adding ratio features...");
        
        for (String string : inpaths) {
            File file = new File(string);

            file.delete();            
        }

        inpaths.clear();

        inpaths.add(tmpFilePath);

        AvroRecordOperations.applyOperation(inpaths, getFeatureExtractionJob().getAvroPath(),
                null, getFeatureExtractionJob().getAvroType(),
                AvroOperationType.ADD_RATIO_FEATURES);
        
        File file = new File(tmpFilePath);
        
        file.delete();
    }
    
    public void cleanup(FeatureType featureType) throws IOException
    {
        if(featureType.equals(FeatureType.LABEL) && !reverse)
        {
            return;
        }

        dataFileWriter.close();
    }
}
