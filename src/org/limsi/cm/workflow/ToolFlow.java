/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import org.limsi.cm.tools.LinuxCommand;
import org.limsi.cm.tools.POSTaggerWrapper;
import org.limsi.mt.cm.features.sentence.SoulLMFeatureExtractor;
import org.limsi.mt.cm.props.CEProperties;
import org.limsi.types.Language;
import org.limsi.types.POSTaggerType;

/**
 *
 * @author anil
 */
public class ToolFlow {

   protected static EnumMap<Language, POSTaggerWrapper> posTaggerWrappers = new EnumMap<Language, POSTaggerWrapper>(Language.class);
   
   private static void createPosTaggerWrapper(POSTaggerType type, Language l) throws IOException, ClassNotFoundException
   {
       POSTaggerWrapper posTaggerWrapper = posTaggerWrappers.get(l);
       
       if(posTaggerWrapper == null)
       {
            posTaggerWrapper = new POSTaggerWrapper(type, l);       
            
            posTaggerWrapper.prepareTagger();
            
            posTaggerWrappers.put(l, posTaggerWrapper);
       }
   }

   private static void clearPosTaggerWrapper(Language l)
   {
       posTaggerWrappers.remove(l);
   }

    /**
     * @return the posTaggerWrapper
     */
    public static POSTaggerWrapper getPosTaggerWrapper(POSTaggerType type, Language l) throws IOException, ClassNotFoundException {
        createPosTaggerWrapper(type, l);
        
        return posTaggerWrappers.get(l);
    }

    /**
     * @param posTagger the posTaggerWrapper to set
     */
    public static void setPosTaggerWrapper(Language l, POSTaggerWrapper posTagger) {
        posTaggerWrappers.put(l, posTagger);
    }  
    
    public static String [] runSoulLM(Language l, String filePath)
    {
        System.out.println("Getting Soul LM scores for file: " + filePath);
        
        String toolCommand = getSoulLMCommand();
        
        File file = new File(filePath);
        
        toolCommand += " " + filePath + " " + getSoulLMModel(l) + " "
                + file.getParentFile().getAbsolutePath() + "/" + file.getName() + SoulLMFeatureExtractor.SOUL_PREFIX;
        
        System.out.println("*********************************************************");
        System.out.println(toolCommand);
        System.out.println("*********************************************************");
        
        String [] response = LinuxCommand.executeCommand(toolCommand, true);
        
        return response;
    }

    private static String getSoulLMCommand()
    {
        String toolCommand = "sh " + CEProperties.getProperty("SoulLM", CEProperties.TOOL_COMMANDS);
        
        return toolCommand;
    }
    
    private static String getSoulLMModel(Language language)
    {
        String modelPath = null;
        
        if(language.equals(Language.ENGLISH))
        {
            modelPath = CEProperties.getProperty("SoulModelEnglish", CEProperties.CORE_DATA_PATHS);
        }
        else if(language.equals(Language.FRENCH))
        {
            modelPath = CEProperties.getProperty("SoulModelFrench", CEProperties.CORE_DATA_PATHS);
        }
        else if(language.equals(Language.SPANISH))
        {
            modelPath = CEProperties.getProperty("SoulModelSpanish", CEProperties.CORE_DATA_PATHS);
        }
        
        return modelPath;
    }
    
    public static String [] runLatticeFst(String pathPrefix, int whichGram)
    {
        System.out.println("Computing n-gram FSt posterior probabilities for path prefix: " + pathPrefix);
        
        String toolCommand = getLatticeFstCommand();
        
        toolCommand += " " + "-o " + whichGram + " -p " + pathPrefix;
        
        String [] response = LinuxCommand.executeCommand(toolCommand, true);
        
        return response;
    }

    private static String getLatticeFstCommand()
    {
        String toolCommand = CEProperties.getProperty("LatticeFst", CEProperties.TOOL_COMMANDS);
        
        return toolCommand;
    }
    
}
