/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import java.beans.PropertyChangeSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.resources.NBestList;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.Formats;
import org.limsi.types.FeatureType;
import sanchay.corpus.ssf.SSFStory;
import sanchay.corpus.ssf.impl.SSFStoryImpl;
import sanchay.properties.PropertiesTable;
import sanchay.properties.PropertyTokens;
import sanchay.common.types.CorpusType;

/**
 *
 * @author anil
 */
public class FeatureExtractionCorpus {
    public static final String PROP_SOURCETEXT = "PROP_SOURCETEXT";
    public static final String PROP_TARGETTEXT = "PROP_TARGETTEXT";
    public static final String PROP_REFERENCETEXT = "PROP_REFERENCETEXT";
    public static final String PROP_POSTEDITEDTEXT = "PROP_POSTEDITEDTEXT";
    public static final String PROP_NBESTLIST = "PROP_NBESTLIST";
    public static final String PROP_ORIGINALSOURCETEXT = "PROP_ORIGINALSOURCETEXT";
    public static final String PROP_ORIGINALTARGETTEXT = "PROP_ORIGINALTARGETTEXT";
    public static final String PROP_ORIGINALREFERENCETEXT = "PROP_ORIGINALREFERENCETEXT";
    public static final String PROP_ORIGINALPOSTEDITEDTEXT = "PROP_ORIGINALPOSTEDITEDTEXT";
    public static final String PROP_FEATUREEXTRACTIONJOB = "PROP_FEATUREEXTRACTIONJOB";
    public static final String PROP_LABELS = "PROP_LABELS";

    private SSFStory sourceText;
    private SSFStory targetText;

    private SSFStory referenceText;
    private SSFStory postEditedText;
    
    private NBestList nbestList;

    private SSFStory originalSourceText;
    private SSFStory originalTargetText;

    private SSFStory originalReferenceText;
    private SSFStory originalPostEditedText;
    
    private PropertyTokens labels;
    private PropertiesTable multipleLabels;
    private PropertiesTable multipleLabelWeights;
    
    private FeatureExtractionJob featureExtractionJob;
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    
    public FeatureExtractionCorpus(FeatureExtractionJob featureExtractionJob)
    {
        this.featureExtractionJob = featureExtractionJob;
    }
        
    void loadCorpus(FeatureType featureType) throws FileNotFoundException, IOException, Exception
    {
        String value = featureExtractionJob.getSrcPath();
        String cs = featureExtractionJob.getSrcCharset();

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setSourceText(new SSFStoryImpl());
            getSourceText().setSSFFile(value);
            getSourceText().setCharset(cs);

            System.out.println("Loading source corpus...");
            getSourceText().readFile(value, cs);
            System.out.printf("Loaded %d source sentences\n", getSourceText().countSentences());
        }

        value = featureExtractionJob.getTgtPath();        
        cs = featureExtractionJob.getTgtCharset() == null ? "UTF-8": featureExtractionJob.getTgtCharset();
       
        if(value != null && MiscellaneousUtils.fileExists(value))
        { 
            setTargetText(new SSFStoryImpl());
            getTargetText().setSSFFile(value);
            getTargetText().setCharset(cs);

            System.out.println("Loading target corpus...");
            getTargetText().readFile(value, cs);
            System.out.printf("Loaded %d target sentences\n", getTargetText().countSentences());
        }

        value = featureExtractionJob.getReferencePath();
        cs = featureExtractionJob.getTgtCharset() == null ? "UTF-8": featureExtractionJob.getTgtCharset();

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setReferenceText(new SSFStoryImpl());
            getReferenceText().setSSFFile(value);
            getReferenceText().setCharset(cs);

            System.out.println("Loading reference corpus...");
            getReferenceText().readFile(value, cs);
            System.out.printf("Loaded %d reference sentences\n", getReferenceText().countSentences());
        }

        value = featureExtractionJob.getPostEditedPath();
        
        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setPostEditedText(new SSFStoryImpl());
            getPostEditedText().setSSFFile(value);
            getPostEditedText().setCharset(cs);

            System.out.println("Loading post-edited corpus...");
            getPostEditedText().readFile(value, cs);
            System.out.printf("Loaded %d post-edited sentences\n", getPostEditedText().countSentences());
        }

        if(featureType.equals(FeatureType.MODEL))
        {
            value = featureExtractionJob.getNbestListPath();

            if(value != null && MiscellaneousUtils.fileExists(value))
            {
                setNbestList(new NBestList(false, Formats.MOSES_NBEST_FORMAT, value, cs));
            }
        }
    }
    
    public void loadLabels()
    {
        String value = featureExtractionJob.getLabelsPath();        
        String cs = featureExtractionJob.getLabelCharset() == null ? "UTF-8": featureExtractionJob.getLabelCharset();
       
        if(value != null && MiscellaneousUtils.fileExists(value))
        { 
            System.out.println("Loading labels...");
            
            try {
                setLabels(new PropertyTokens(value, cs));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.printf("Loaded %d labels\n", getLabels().countTokens());
        }        

        value = featureExtractionJob.getMultipleLabelsPath();        
       
        if(value != null && MiscellaneousUtils.fileExists(value))
        { 
            System.out.println("Loading multiple labels...");

            PropertiesTable table = new PropertiesTable();
            
            try {
                table.read(value, cs);
                
                setMultipleLabels(table);
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.printf("Loaded %d labels\n", getLabels().countTokens());
        }        

        value = featureExtractionJob.getMultipleLabelWeightsPath();        
       
        if(value != null && MiscellaneousUtils.fileExists(value))
        { 
            System.out.println("Loading multiple label weights...");
            
            PropertiesTable table = new PropertiesTable();
            
            try {
                table.read(value, cs);
                
                setMultipleLabelWeights(table);
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FeatureExtractionCorpus.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.printf("Loaded %d label weights\n", getLabels().countTokens());
        }        
    }
    
    void loadCorpusExtra(FeatureType featureType) throws FileNotFoundException, IOException, Exception
    {
        String value = featureExtractionJob.getOriginalSrcPath();
        String cs = featureExtractionJob.getSrcCharset();

	if(cs == null) { cs = "UTF-8" ;}

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setOriginalSourceText(new SSFStoryImpl());
            getOriginalSourceText().setSSFFile(value);
            getOriginalSourceText().setCharset(cs);

            System.out.println("Loading original source corpus...");
            getOriginalSourceText().readFile(value, cs, CorpusType.RAW);
            System.out.printf("Loaded %d original source sentences\n", getOriginalSourceText().countSentences());
        }

        value = featureExtractionJob.getOriginalTgtPath();
        cs = featureExtractionJob.getTgtCharset();

	if(cs == null) { cs = "UTF-8" ;}

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setOriginalTargetText(new SSFStoryImpl());
            getOriginalTargetText().setSSFFile(value);
            getOriginalTargetText().setCharset(cs);

            System.out.println("Loading original target corpus...");
            getOriginalTargetText().readFile(value, cs, CorpusType.RAW);
            System.out.printf("Loaded %d original target sentences\n", getOriginalTargetText().countSentences());
        }

        value = featureExtractionJob.getOriginalReferencePath();

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setOriginalReferenceText(new SSFStoryImpl());
            getOriginalReferenceText().setSSFFile(value);
            getOriginalReferenceText().setCharset(cs);

            System.out.println("Loading original reference corpus...");
            getOriginalReferenceText().readFile(value, cs, CorpusType.RAW);
            System.out.printf("Loaded %d original reference sentences\n", getOriginalReferenceText().countSentences());
        }

        value = featureExtractionJob.getOriginalPostEditedPath();

        if(value != null && MiscellaneousUtils.fileExists(value))
        {
            setOriginalPostEditedText(new SSFStoryImpl());
            getOriginalPostEditedText().setSSFFile(value);
            getOriginalPostEditedText().setCharset(cs);

            System.out.println("Loading original post-edited corpus...");
            getOriginalPostEditedText().readFile(value, cs, CorpusType.RAW);
            System.out.printf("Loaded %d original post-edited sentences\n", getOriginalPostEditedText().countSentences());
        }
    }

    /**
     * @return the sourceText
     */
    public SSFStory getSourceText() {
        return sourceText;
    }

    /**
     * @param sourceText the sourceText to set
     */
    void setSourceText(SSFStory sourceText) {
        sanchay.corpus.ssf.SSFStory oldSourceText = this.sourceText;
        this.sourceText = sourceText;
        propertyChangeSupport.firePropertyChange(PROP_SOURCETEXT, oldSourceText, sourceText);
    }

    /**
     * @return the targetText
     */
    public SSFStory getTargetText() {
        return targetText;
    }

    /**
     * @param targetText the targetText to set
     */
    public void setTargetText(SSFStory targetText) {
        sanchay.corpus.ssf.SSFStory oldTargetText = this.targetText;
        this.targetText = targetText;
        propertyChangeSupport.firePropertyChange(PROP_TARGETTEXT, oldTargetText, targetText);
    }

    /**
     * @return the referenceText
     */
    public SSFStory getReferenceText() {
        return referenceText;
    }

    /**
     * @param referenceText the referenceText to set
     */
    void setReferenceText(SSFStory referenceText) {
        sanchay.corpus.ssf.SSFStory oldReferenceText = this.referenceText;
        this.referenceText = referenceText;
        propertyChangeSupport.firePropertyChange(PROP_REFERENCETEXT, oldReferenceText, referenceText);
    }

    /**
     * @return the postEditedText
     */
    public SSFStory getPostEditedText() {
        return postEditedText;
    }

    /**
     * @param postEditedText the postEditedText to set
     */
    void setPostEditedText(SSFStory postEditedText) {
        sanchay.corpus.ssf.SSFStory oldPostEditedText = this.postEditedText;
        this.postEditedText = postEditedText;
        propertyChangeSupport.firePropertyChange(PROP_POSTEDITEDTEXT, oldPostEditedText, postEditedText);
    }

    /**
     * @return the nbestList
     */
    public NBestList getNbestList() {
        return nbestList;
    }

    /**
     * @param nbestList the nbestList to set
     */
    void setNbestList(NBestList nbestList) {
        org.limsi.cm.resources.NBestList oldNbestList = this.nbestList;
        this.nbestList = nbestList;
        propertyChangeSupport.firePropertyChange(PROP_NBESTLIST, oldNbestList, nbestList);
    }

    /**
     * @return the originalSourceText
     */
    public SSFStory getOriginalSourceText() {
        return originalSourceText;
    }

    /**
     * @param originalSourceText the originalSourceText to set
     */
    void setOriginalSourceText(SSFStory originalSourceText) {
        sanchay.corpus.ssf.SSFStory oldOriginalSourceText = this.originalSourceText;
        this.originalSourceText = originalSourceText;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALSOURCETEXT, oldOriginalSourceText, originalSourceText);
    }

    /**
     * @return the originalTargetText
     */
    public SSFStory getOriginalTargetText() {
        return originalTargetText;
    }

    /**
     * @param originalTargetText the originalTargetText to set
     */
    void setOriginalTargetText(SSFStory originalTargetText) {
        sanchay.corpus.ssf.SSFStory oldOriginalTargetText = this.originalTargetText;
        this.originalTargetText = originalTargetText;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALTARGETTEXT, oldOriginalTargetText, originalTargetText);
    }

    /**
     * @return the originalReferenceText
     */
    public SSFStory getOriginalReferenceText() {
        return originalReferenceText;
    }

    /**
     * @param originalReferenceText the originalReferenceText to set
     */
    void setOriginalReferenceText(SSFStory originalReferenceText) {
        sanchay.corpus.ssf.SSFStory oldOriginalReferenceText = this.originalReferenceText;
        this.originalReferenceText = originalReferenceText;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALREFERENCETEXT, oldOriginalReferenceText, originalReferenceText);
    }

    /**
     * @return the originalPostEditedText
     */
    public SSFStory getOriginalPostEditedText() {
        return originalPostEditedText;
    }

    /**
     * @param originalPostEditedText the originalPostEditedText to set
     */
    void setOriginalPostEditedText(SSFStory originalPostEditedText) {
        sanchay.corpus.ssf.SSFStory oldOriginalPostEditedText = this.originalPostEditedText;
        this.originalPostEditedText = originalPostEditedText;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALPOSTEDITEDTEXT, oldOriginalPostEditedText, originalPostEditedText);
    }

    /**
     * @return the labels
     */
    public PropertyTokens getLabels() {
        return labels;
    }

    /**
     * @param labels the labels to set
     */
    public void setLabels(PropertyTokens labels) {
        sanchay.properties.PropertyTokens oldLabels = this.labels;
        this.labels = labels;
        propertyChangeSupport.firePropertyChange(PROP_LABELS, oldLabels, labels);
    }

    /**
     * @return the multipleLabels
     */
    public PropertiesTable getMultipleLabels() {
        return multipleLabels;
    }

    /**
     * @param multipleLabels the multipleLabels to set
     */
    public void setMultipleLabels(PropertiesTable multipleLabels) {
        sanchay.properties.PropertiesTable oldMultipleLabels = multipleLabels;
        this.multipleLabels = multipleLabels;
        propertyChangeSupport.firePropertyChange(PROP_MULTIPLELABELS, oldMultipleLabels, multipleLabels);
    }

    /**
     * @return the multipleLabelWeights
     */
    public PropertiesTable getMultipleLabelWeights() {
        return multipleLabelWeights;
    }

    /**
     * @param multipleLabelWeights the multipleLabelWeights to set
     */
    public void setMultipleLabelWeights(PropertiesTable multipleLabelWeights) {
        sanchay.properties.PropertiesTable oldMultipleLabelWeights = multipleLabelWeights;
        this.multipleLabelWeights = multipleLabelWeights;
        propertyChangeSupport.firePropertyChange(PROP_MULTIPLELABELWEIGHTS, oldMultipleLabelWeights, multipleLabelWeights);
    }
    public static final String PROP_MULTIPLELABELS = "PROP_MULTIPLELABELS";
    public static final String PROP_MULTIPLELABELWEIGHTS = "PROP_MULTIPLELABELWEIGHTS";

    /**
     * @return the featureExtractionJob
     */
    FeatureExtractionJob getFeatureExtractionJob() {
        return featureExtractionJob;
    }

    /**
     * @param featureExtractionJob the featureExtractionJob to set
     */
    void setFeatureExtractionJob(FeatureExtractionJob featureExtractionJob) {
        org.limsi.cm.workflow.FeatureExtractionJob oldFeatureExtractionJob = this.featureExtractionJob;
        this.featureExtractionJob = featureExtractionJob;
        propertyChangeSupport.firePropertyChange(PROP_FEATUREEXTRACTIONJOB, oldFeatureExtractionJob, featureExtractionJob);
    }
    
}
