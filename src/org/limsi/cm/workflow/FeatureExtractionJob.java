/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.props.CEProperties;
import org.limsi.types.AvroType;
import org.limsi.types.FeatureType;
import org.limsi.types.FormatType;
import org.limsi.types.LMType;
import org.limsi.types.LabelType;
import org.limsi.types.Language;
import sanchay.properties.KeyValueProperties;
import sanchay.util.UtilityFunctions;

/**
 *
 * @author anil
 */
public class FeatureExtractionJob {
    public static final String PROP_SRCPATH = "PROP_SRCPATH";
    public static final String PROP_TGTPATH = "PROP_TGTPATH";
    public static final String PROP_REFERENCEPATH = "PROP_REFERENCEPATH";
    public static final String PROP_POSTEDITEDPATH = "PROP_POSTEDITEDPATH";
    public static final String PROP_ORIGINALSRCPATH = "PROP_ORIGINALSRCPATH";
    public static final String PROP_ORIGINALTGTPATH = "PROP_ORIGINALTGTPATH";
    public static final String PROP_ORIGINALREFERENCEPATH = "PROP_ORIGINALREFERENCEPATH";
    public static final String PROP_ORIGINALPOSTEDITEDPATH = "PROP_ORIGINALPOSTEDITEDPATH";
    public static final String PROP_OUTPATH = "PROP_OUTPATH";
    public static final String PROP_AVROPATH = "PROP_AVROPATH";
    public static final String PROP_AVROTYPE = "PROP_AVROTYPE";
    public static final String PROP_NBESTLISTPATH = "PROP_NBESTLISTPATH";
    public static final String PROP_FEATURETYPES = "PROP_FEATURETYPES";
    public static final String PROP_LABELTYPE = "PROP_LABELTYPE";
    public static final String PROP_JOBSPECIFICATIONS = "PROP_JOBSPECIFICATIONS";
    public static final String PROP_FORMAT = "PROP_FORMAT";
    public static final String PROP_VERTICAL = "PROP_VERTICAL";
    public static final String PROP_SAVEALL = "PROP_SAVEALL";
    public static final String PROP_PROPERTYCHANGESUPPORT = "PROP_PROPERTYCHANGESUPPORT";
    public static final String PROP_SRCCHARSET = "PROP_SRCCHARSET";
    public static final String PROP_TGTCHARSET = "PROP_TGTCHARSET";

    private String srcPath;
    private String tgtPath;
    private String referencePath;
    private String postEditedPath;

    private String originalSrcPath;
    private String originalTgtPath;
    private String originalReferencePath;
    private String originalPostEditedPath;

    private String labelsPath;
    private String multipleLabelsPath;
    private String multipleLabelWeightsPath;

    private String outPath;
    private String avroPath;
    
    private String srcCharset;
    private String tgtCharset;
    private String labelsCharset;

    private AvroType avroType;

    private String nbestListPath;
    private boolean oneBest = true;
    
    private String wordGraphPath;
    private String wordGraphSuffix;
    
    private FormatType.WordGraphFormat wordGraphFormat;

    private String tempDirPath;
    
    private List<FeatureType> featureTypes;
    private LabelType labelType;
    
    private KeyValueProperties jobSpecifications;
    
    private FormatType.FeatureCollectionFormat format = FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT;
    
    private boolean vertical = true;
    private boolean saveAll = false;
    
    private transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);    

    public FeatureExtractionJob(KeyValueProperties jobSpecifications) {
        
        this.jobSpecifications = jobSpecifications;
        
        init();
    }

    /**
     * @return the jobSpecifications
     */
    KeyValueProperties getJobSpecifications() {
        return jobSpecifications;
    }
    
    private void init()
    {
        loadJob();
        loadJobExtra();
    }
    
    FeatureExtractionCorpus loadFeatureExtractionCorpus(FeatureType featureType) throws FileNotFoundException, IOException, Exception
    {
        FeatureExtractionCorpus featureExtractionCorpus = new FeatureExtractionCorpus(this);
        
        featureExtractionCorpus.loadCorpus(featureType);
        featureExtractionCorpus.loadCorpusExtra(featureType);
        
        return featureExtractionCorpus;
    }
    
    void loadJob()
    {
        setFeatureTypes(getFeatureTypes(getJobSpecifications()));

        System.out.println("Selected feature types: ");
        MiscellaneousUtils.printList(getFeatureTypes(), System.out, ",");     
                
        String value = MiscellaneousUtils.getOptionValue("-s", "-srcPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                srcPath = value;
                
                srcCharset = MiscellaneousUtils.getOptionValue("-cs -charset -scs -srcCharset", getJobSpecifications());

                if(srcCharset == null)
                {
                    srcCharset = "UTF-8";
                }
            }
            else
            {
                System.err.printf("Source file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-t", "-tgtPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                tgtPath = value;
                
                tgtCharset = MiscellaneousUtils.getOptionValue("-cs -charset -tcs -tgtCharset", getJobSpecifications());

                if(tgtCharset == null)
                {
                    tgtCharset = "UTF-8";
                }
            }
            else
            {
                System.err.printf("Target file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-lp", "-labelsPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                labelsPath = value;
                
                setLabelsCharset(MiscellaneousUtils.getOptionValue("-cs -charset -lcs -labelsCharset", getJobSpecifications()));

                if(getLabelsCharset() == null)
                {
                    setLabelsCharset("UTF-8");
                }
            }
            else
            {
                System.err.printf("Labels file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-mlp", "-multipleLabelsPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                setMultipleLabelsPath(value);
            }
            else
            {
                System.err.printf("Multiple labels file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-mlwp", "-multipleLabelWeightsPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                setMultipleLabelWeightsPath(value);
            }
            else
            {
                System.err.printf("Multiple label weights file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-r", "-referencePath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                referencePath = value;
            }
            else
            {
                System.err.printf("Reference file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-pe", "-postEditedPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                postEditedPath = value;
            }
            else
            {
                System.err.printf("Post-edited file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-nb", "-nbestListPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                nbestListPath = value;

                setAvroType(AvroType.NBEST_LIST);
               
                value = MiscellaneousUtils.getOptionValue("-1b", "-oneBest", getJobSpecifications());

                if(value != null)
                {
                    setOneBest(Boolean.parseBoolean(value));
                    setAvroType(AvroType.POST_EDITED_CORPUS_ENTRY);   
                }
            }
            else
            {
                System.out.println("No path supplied for n-best list");            

                if(getFeatureTypes().contains(FeatureType.MODEL))
                {
                    getFeatureTypes().remove(FeatureType.MODEL);

                    System.out.println("Since no nbest list is provided this feature type is not applicable: "
                            + FeatureType.MODEL.getFeatureName());
                }
            }
        }
        else
        {
            setAvroType(AvroType.POST_EDITED_CORPUS_ENTRY);   
        }

        value = MiscellaneousUtils.getOptionValue("-wgf", "-wordGraphFormat", getJobSpecifications());

        if(value != null)
        {
            wordGraphFormat = FormatType.WordGraphFormat.valueOf(value);
        }
        else
        {
            System.out.println("No format mentioned for word graphs (lattices)");            

            if(getFeatureTypes().contains(FeatureType.LATTICE))
            {
                getFeatureTypes().remove(FeatureType.LATTICE);

                System.out.println("Since word graph format is not provided, this feature type will not be used: "
                        + FeatureType.LATTICE.getFeatureName());
            }
        }

        value = MiscellaneousUtils.getOptionValue("-wgp", "-wordGraphPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value) || wordGraphFormat.equals(FormatType.WordGraphFormat.OPEN_FST_FORMAT))
            {
                wordGraphPath = value;
            }
            else
            {
                System.out.println("No path supplied for word graphs (lattices)");            

                if(getFeatureTypes().contains(FeatureType.LATTICE))
                {
                    getFeatureTypes().remove(FeatureType.LATTICE);

                    System.out.println("Since no word graph is provided this feature type is not applicable: "
                            + FeatureType.LATTICE.getFeatureName());
                }
            }
        }

        value = MiscellaneousUtils.getOptionValue("-wgs", "-wordGraphSuffix", getJobSpecifications());

        if(value != null)
        {
            wordGraphSuffix = value;
        }
        else
        {
            System.out.println("No path suffix supplied for word graphs (lattices)");            

            if(getFeatureTypes().contains(FeatureType.LATTICE))
            {
                getFeatureTypes().remove(FeatureType.LATTICE);

                System.out.println("Since no word graph path suffix is provided this feature type will not be used: "
                        + FeatureType.LATTICE.getFeatureName());
            }
        }

        value = MiscellaneousUtils.getOptionValue("-tmp", "-tempDirPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                tempDirPath = value;
            }
            else
            {
                System.out.println("No path supplied for temporary directory");            

                if(getFeatureTypes().contains(FeatureType.LATTICE))
                {
                    getFeatureTypes().remove(FeatureType.LATTICE);

                    System.out.println("Since no temporary directory path is provided this feature type will not be used: "
                            + FeatureType.LATTICE.getFeatureName());
                }
            }
        }
        
        Language srcLang = Language.FRENCH;
        Language tgtLang = Language.ENGLISH;

        value = MiscellaneousUtils.getOptionValue("-sl", "-srcLanguage", getJobSpecifications());

        if(value != null)
        {
            srcLang = Language.getLanguage(value);
        }

        value = MiscellaneousUtils.getOptionValue("-tl", "-tgtLanguage", getJobSpecifications());

        if(value != null)
        {
            tgtLang = Language.getLanguage(value);
        }

        LMType.LMOrder lmorder;

        value = MiscellaneousUtils.getOptionValue("-lmo", "-lmOrder", getJobSpecifications());

        if(value != null)
        {
            lmorder = LMType.LMOrder.getLMOrder(Integer.parseInt(value));

            try {
                LMType.TEXT_LM.setOrder(lmorder);
                LMType.POS_TAGGED_LM.setOrder(lmorder);
                LMType.POS_NOLEX_LM.setOrder(lmorder);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//        setFeatureTypes(getFeatureTypes(getJobSpecifications()));

//        value = MiscellaneousUtils.getOptionValue("-ft", "-featureTypes", getJobSpecifications());
//
//        if(value != null)
//        {
//            setFeatureTypes(FeatureType.getFeatureTypes(value));
//        }

//        System.out.println("Selected feature types: " + value);
        
        MiscellaneousUtils.setLanguages(getFeatureTypes(), srcLang, tgtLang);
        
//        setFeatureTypes(getFeatureTypes());

        boolean detailedLabel;
        setLabelType(null);

        value = MiscellaneousUtils.getOptionValue("-lt", "-labelType", getJobSpecifications());

        if(value != null)
        {
            setLabelType(LabelType.valueOf(value));
        }

        if(getLabelType() != null)
        {
            value = MiscellaneousUtils.getOptionValue("-ltd", "-labelTypeDetailed", getJobSpecifications());

            if(value != null)
            {
                detailedLabel = true;

                getLabelType().setDetailed(detailedLabel);
            }
        }
        else
        {
            if(getFeatureTypes().contains(FeatureType.LABEL))
            {
                getFeatureTypes().remove(FeatureType.LABEL);

                System.out.println("Since no label type is provided this feature type is not applicable: "
                        + FeatureType.LABEL.getFeatureName());
            }            
        }
        
        setLabelType(getLabelType());
        
        if(!MiscellaneousUtils.fileExists(CEProperties.getProperty("SoulLM", CEProperties.TOOL_COMMANDS)))
        {
            featureTypes.remove(FeatureType.SOUL_LM);
        }
               
        value = MiscellaneousUtils.getOptionValue("-h", "-horizontal", getJobSpecifications());

        if(value != null)
        {
            setVertical(false);
        }

        value = MiscellaneousUtils.getOptionValue("-sa", "-saveAll", getJobSpecifications());

        if(value != null)
        {
            setSaveAll(true);
        }

        setFormat(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT);

        value = MiscellaneousUtils.getOptionValue("-f", "-format", getJobSpecifications());

        if(value != null)
        {
            setFormat(FormatType.FeatureCollectionFormat.valueOf(value));
        }

        value = MiscellaneousUtils.getOptionValue("-o", "-outputPath", getJobSpecifications());

        if(value != null)
        {
            setOutPath(value);
        }

        value = MiscellaneousUtils.getOptionValue("-a", "-avroPath", getJobSpecifications());

        if(value != null)
        {
            setAvroPath(value);
        }
    }
    
    void loadJobExtra()
    {
        String value = MiscellaneousUtils.getOptionValue("-os", "-originalSourcePath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                originalSrcPath = value;
            }
            else
            {
                System.err.printf("Original source file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-ot", "-originalTargetPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                originalTgtPath = value;
            }
            else
            {
                System.err.printf("Original target file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-or", "-originalReferencePath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                originalReferencePath = value;
            }
            else
            {
                System.err.printf("Original reference file not found: %s\n", value);
            }
        }

        value = MiscellaneousUtils.getOptionValue("-ope", "-originalPostEditedPath", getJobSpecifications());

        if(value != null)
        {
            if(MiscellaneousUtils.fileExists(value))
            {
                originalPostEditedPath = value;
            }
            else
            {
                System.err.printf("Original post-edited file not found: %s\n", value);
            }
        }
    }

    public static List<FeatureType> getFeatureTypes(KeyValueProperties jobSpecification)
    {
        String ftypes = MiscellaneousUtils.getOptionValue("-ft", "-featureTypes", jobSpecification);

        if(ftypes != null)
        {
            return FeatureType.getFeatureTypes(ftypes);
        }

//        String ftypes = jobSpecification.getPropertyValue("-ft");
//        
//        if(ftypes == null)
//        {
//            List<FeatureType> featureTypes = FeatureType.getFeatureTypes();
//            
//            String value = MiscellaneousUtils.getOptionValue("-nb", "-nbestListPath", jobSpecification);
//
//            if(value == null)
//            {
//                featureTypes.remove(FeatureType.MODEL);
//            }
//            
//            if(!MiscellaneousUtils.fileExists(CEProperties.getProperty("SoulLM", CEProperties.TOOL_COMMANDS)))
//            {
//                featureTypes.remove(FeatureType.SOUL_LM);
//            }
//               
//            value = MiscellaneousUtils.getOptionValue("-lt", "-labelType", jobSpecification);
//
//            if(value == null)
//            {
//                featureTypes.remove(FeatureType.LABEL);
//            }
//            
//            return featureTypes;
//        }
        
//        return FeatureType.getFeatureTypes(ftypes);
        
        return FeatureType.getFeatureTypes();
    }
    
    static List<FeatureType> getFeatureTypesUnion(List<KeyValueProperties> jobSpecifications)
    {
        List<FeatureType> featureTypes = new ArrayList<FeatureType>();
        
        for (KeyValueProperties keyValueProperties : jobSpecifications) {
            featureTypes.addAll(getFeatureTypes(keyValueProperties));
        }
        
        featureTypes = UtilityFunctions.getUnique(featureTypes);
        
        if(featureTypes.isEmpty())
        {
            featureTypes = FeatureType.getFeatureTypes();
        }
        
        return featureTypes;
    }

    /**
     * @return the srcPath
     */
    public String getSrcPath() {
        return srcPath;
    }

    /**
     * @param srcPath the srcPath to set
     */
    void setSrcPath(String srcPath) {
        java.lang.String oldSrcPath = this.srcPath;
        this.srcPath = srcPath;
        propertyChangeSupport.firePropertyChange(PROP_SRCPATH, oldSrcPath, srcPath);
    }

    /**
     * @return the tgtPath
     */
    public String getTgtPath() {
        return tgtPath;
    }

    /**
     * @param tgtPath the tgtPath to set
     */
    void setTgtPath(String tgtPath) {
        java.lang.String oldTgtPath = this.tgtPath;
        this.tgtPath = tgtPath;
        propertyChangeSupport.firePropertyChange(PROP_TGTPATH, oldTgtPath, tgtPath);
    }

    /**
     * @return the referencePath
     */
    public String getReferencePath() {
        return referencePath;
    }

    /**
     * @param referencePath the referencePath to set
     */
    void setReferencePath(String referencePath) {
        java.lang.String oldReferencePath = this.referencePath;
        this.referencePath = referencePath;
        propertyChangeSupport.firePropertyChange(PROP_REFERENCEPATH, oldReferencePath, referencePath);
    }

    /**
     * @return the postEditedPath
     */
    public String getPostEditedPath() {
        return postEditedPath;
    }

    /**
     * @param postEditedPath the postEditedPath to set
     */
    void setPostEditedPath(String postEditedPath) {
        java.lang.String oldPostEditedPath = this.postEditedPath;
        this.postEditedPath = postEditedPath;
        propertyChangeSupport.firePropertyChange(PROP_POSTEDITEDPATH, oldPostEditedPath, postEditedPath);
    }

    /**
     * @return the originalSrcPath
     */
    public String getOriginalSrcPath() {
        return originalSrcPath;
    }

    /**
     * @param originalSrcPath the originalSrcPath to set
     */
    void setOriginalSrcPath(String originalSrcPath) {
        java.lang.String oldOriginalSrcPath = this.originalSrcPath;
        this.originalSrcPath = originalSrcPath;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALSRCPATH, oldOriginalSrcPath, originalSrcPath);
    }

    /**
     * @return the originalTgtPath
     */
    public String getOriginalTgtPath() {
        return originalTgtPath;
    }

    /**
     * @param originalTgtPath the originalTgtPath to set
     */
    void setOriginalTgtPath(String originalTgtPath) {
        java.lang.String oldOriginalTgtPath = this.originalTgtPath;
        this.originalTgtPath = originalTgtPath;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALTGTPATH, oldOriginalTgtPath, originalTgtPath);
    }

    /**
     * @return the originalReferencePath
     */
    public String getOriginalReferencePath() {
        return originalReferencePath;
    }

    /**
     * @param originalReferencePath the originalReferencePath to set
     */
    void setOriginalReferencePath(String originalReferencePath) {
        java.lang.String oldOriginalReferencePath = this.originalReferencePath;
        this.originalReferencePath = originalReferencePath;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALREFERENCEPATH, oldOriginalReferencePath, originalReferencePath);
    }

    /**
     * @return the originalPostEditedPath
     */
    public String getOriginalPostEditedPath() {
        return originalPostEditedPath;
    }

    /**
     * @param originalPostEditedPath the originalPostEditedPath to set
     */
    void setOriginalPostEditedPath(String originalPostEditedPath) {
        java.lang.String oldOriginalPostEditedPath = this.originalPostEditedPath;
        this.originalPostEditedPath = originalPostEditedPath;
        propertyChangeSupport.firePropertyChange(PROP_ORIGINALPOSTEDITEDPATH, oldOriginalPostEditedPath, originalPostEditedPath);
    }

    /**
     * @return the labelsPath
     */
    public String getLabelsPath() {
        return labelsPath;
    }

    /**
     * @param labelsPath the labelsPath to set
     */
    public void setLabelsPath(String labelsPath) {
        java.lang.String oldLabelsPath = this.labelsPath;
        this.labelsPath = labelsPath;
        propertyChangeSupport.firePropertyChange(PROP_LABELSPATH, oldLabelsPath, labelsPath);
    }

    /**
     * @return the multipleLabelsPath
     */
    public String getMultipleLabelsPath() {
        return multipleLabelsPath;
    }

    /**
     * @param multipleLabelsPath the multipleLabelsPath to set
     */
    public void setMultipleLabelsPath(String multipleLabelsPath) {
        java.lang.String oldMultipleLabelsPath = multipleLabelsPath;
        this.multipleLabelsPath = multipleLabelsPath;
        propertyChangeSupport.firePropertyChange(PROP_MULTIPLELABELSPATH, oldMultipleLabelsPath, multipleLabelsPath);
    }

    /**
     * @return the multipleLabelWeightsPath
     */
    public String getMultipleLabelWeightsPath() {
        return multipleLabelWeightsPath;
    }

    /**
     * @param multipleLabelWeightsPath the multipleLabelWeightsPath to set
     */
    public void setMultipleLabelWeightsPath(String multipleLabelWeightsPath) {
        java.lang.String oldMultipleLabelWeightsPath = multipleLabelWeightsPath;
        this.multipleLabelWeightsPath = multipleLabelWeightsPath;
        propertyChangeSupport.firePropertyChange(PROP_MULTIPLELABELWEIGHTSPATH, oldMultipleLabelWeightsPath, multipleLabelWeightsPath);
    }
    public static final String PROP_MULTIPLELABELSPATH = "PROP_MULTIPLELABELSPATH";
    public static final String PROP_MULTIPLELABELWEIGHTSPATH = "PROP_MULTIPLELABELWEIGHTSPATH";

    /**
     * @return the labelCharset
     */
    public String getLabelCharset() {
        return getLabelsCharset();
    }

    /**
     * @param labelCharset the labelCharset to set
     */
    public void setLabelCharset(String labelCharset) {
        java.lang.String oldLabelCharset = this.getLabelsCharset();
        this.setLabelsCharset(labelCharset);
        propertyChangeSupport.firePropertyChange(PROP_LABELCHARSET, oldLabelCharset, labelCharset);
    }
    public static final String PROP_LABELSPATH = "PROP_LABELSPATH";
    public static final String PROP_LABELCHARSET = "PROP_LABELCHARSET";

    /**
     * @return the outPath
     */
    public String getOutPath() {
        return outPath;
    }

    /**
     * @param outPath the outPath to set
     */
    void setOutPath(String outPath) {
        java.lang.String oldOutPath = this.outPath;
        this.outPath = outPath;
        propertyChangeSupport.firePropertyChange(PROP_OUTPATH, oldOutPath, outPath);
    }

    /**
     * @return the avroPath
     */
    public String getAvroPath() {
        return avroPath;
    }

    /**
     * @param avroPath the avroPath to set
     */
    void setAvroPath(String avroPath) {
        java.lang.String oldAvroPath = this.avroPath;
        this.avroPath = avroPath;
        propertyChangeSupport.firePropertyChange(PROP_AVROPATH, oldAvroPath, avroPath);
    }

    /**
     * @return the avroType
     */
    public AvroType getAvroType() {
        return avroType;
    }

    /**
     * @param avroType the avroType to set
     */
    void setAvroType(AvroType avroType) {
        org.limsi.types.AvroType oldAvroType = this.avroType;
        this.avroType = avroType;
        propertyChangeSupport.firePropertyChange(PROP_AVROTYPE, oldAvroType, avroType);
    }

    /**
     * @return the srcCharset
     */
    public String getSrcCharset() {
        return srcCharset;
    }

    /**
     * @param srcCharset the srcCharset to set
     */
    void setSrcCharset(String srcCharset) {
        java.lang.String oldSrcCharset = this.srcCharset;
        this.srcCharset = srcCharset;
        propertyChangeSupport.firePropertyChange(PROP_SRCCHARSET, oldSrcCharset, srcCharset);
    }

    /**
     * @return the tgtCharset
     */
    public String getTgtCharset() {
        return tgtCharset;
    }

    /**
     * @param tgtCharset the tgtCharset to set
     */
    void setTgtCharset(String tgtCharset) {
        java.lang.String oldTgtCharset = this.tgtCharset;
        this.tgtCharset = tgtCharset;
        propertyChangeSupport.firePropertyChange(PROP_TGTCHARSET, oldTgtCharset, tgtCharset);
    }

    /**
     * @return the labelsCharset
     */
    public String getLabelsCharset() {
        return labelsCharset;
    }

    /**
     * @param labelsCharset the labelsCharset to set
     */
    public void setLabelsCharset(String labelsCharset) {
        this.labelsCharset = labelsCharset;
    }

    /**
     * @return the nbestListPath
     */
    public String getNbestListPath() {
        return nbestListPath;
    }

    /**
     * @param nbestListPath the nbestListPath to set
     */
    void setNbestListPath(String nbestListPath) {
        java.lang.String oldNbestListPath = this.nbestListPath;
        this.nbestListPath = nbestListPath;
        propertyChangeSupport.firePropertyChange(PROP_NBESTLISTPATH, oldNbestListPath, nbestListPath);
    }

    /**
     * @return the oneBest
     */
    public boolean isOneBest() {
        return oneBest;
    }

    /**
     * @param oneBest the oneBest to set
     */
    public void setOneBest(boolean oneBest) {
        this.oneBest = oneBest;
    }

    /**
     * @return the wordGraphPath
     */
    public String getWordGraphPath() {
        return wordGraphPath;
    }

    /**
     * @param wordGraphPath the wordGraphPath to set
     */
    public void setWordGraphPath(String wordGraphPath) {
        java.lang.String oldWordGraphPath = wordGraphPath;
        this.wordGraphPath = wordGraphPath;
        propertyChangeSupport.firePropertyChange(PROP_WORDGRAPHPATH, oldWordGraphPath, wordGraphPath);
    }

    /**
     * @return the wordGraphSuffix
     */
    public String getWordGraphSuffix() {
        return wordGraphSuffix;
    }

    /**
     * @param wordGraphSuffix the wordGraphSuffix to set
     */
    public void setWordGraphSuffix(String wordGraphSuffix) {
        this.wordGraphSuffix = wordGraphSuffix;
    }

    /**
     * @return the wordGraphFormat
     */
    public FormatType.WordGraphFormat getWordGraphFormat() {
        return wordGraphFormat;
    }

    /**
     * @param wordGraphFormat the wordGraphFormat to set
     */
    public void setWordGraphFormat(FormatType.WordGraphFormat wordGraphFormat) {
        this.wordGraphFormat = wordGraphFormat;
    }

    /**
     * @return the tempDirPath
     */
    public String getTempDirPath() {
        return tempDirPath;
    }

    /**
     * @param tempDirPath the tempDirPath to set
     */
    public void setTempDirPath(String tempDirPath) {
        this.tempDirPath = tempDirPath;
    }
    public static final String PROP_WORDGRAPHPATH = "PROP_WORDGRAPHPATH";

    /**
     * @return the featureTypes
     */
    public List<FeatureType> getFeatureTypes() {
        return featureTypes;
    }

    /**
     * @param featureTypes the featureTypes to set
     */
    void setFeatureTypes(List<FeatureType> featureTypes) {
        java.util.List<org.limsi.types.FeatureType> oldFeatureTypes = this.featureTypes;
        this.featureTypes = featureTypes;
        propertyChangeSupport.firePropertyChange(PROP_FEATURETYPES, oldFeatureTypes, featureTypes);
    }

    /**
     * @return the labelType
     */
    public LabelType getLabelType() {
        return labelType;
    }

    /**
     * @param labelType the labelType to set
     */
    void setLabelType(LabelType labelType) {
        org.limsi.types.LabelType oldLabelType = this.labelType;
        this.labelType = labelType;
        propertyChangeSupport.firePropertyChange(PROP_LABELTYPE, oldLabelType, labelType);
    }

    /**
     * @param jobSpecifications the jobSpecifications to set
     */
    void setJobSpecifications(KeyValueProperties jobSpecifications) {
        sanchay.properties.KeyValueProperties oldJobSpecifications = this.jobSpecifications;
        this.jobSpecifications = jobSpecifications;
        propertyChangeSupport.firePropertyChange(PROP_JOBSPECIFICATIONS, oldJobSpecifications, jobSpecifications);
    }

    /**
     * @return the format
     */
    public FormatType.FeatureCollectionFormat getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    void setFormat(FormatType.FeatureCollectionFormat format) {
        org.limsi.types.FormatType.FeatureCollectionFormat oldFormat = this.format;
        this.format = format;
        propertyChangeSupport.firePropertyChange(PROP_FORMAT, oldFormat, format);
    }

    /**
     * @return the vertical
     */
    public boolean isVertical() {
        return vertical;
    }

    /**
     * @param vertical the vertical to set
     */
    void setVertical(boolean vertical) {
        boolean oldVertical = this.vertical;
        this.vertical = vertical;
        propertyChangeSupport.firePropertyChange(PROP_VERTICAL, oldVertical, vertical);
    }

    /**
     * @return the saveAll
     */
    boolean isSaveAll() {
        return saveAll;
    }

    /**
     * @param saveAll the saveAll to set
     */
    void setSaveAll(boolean saveAll) {
        boolean oldSaveAll = this.saveAll;
        this.saveAll = saveAll;
        propertyChangeSupport.firePropertyChange(PROP_SAVEALL, oldSaveAll, saveAll);
    }

    /**
     * @return the propertyChangeSupport
     */
    PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    /**
     * @param propertyChangeSupport the propertyChangeSupport to set
     */
    void setPropertyChangeSupport(PropertyChangeSupport propertyChangeSupport) {
        java.beans.PropertyChangeSupport oldPropertyChangeSupport = this.propertyChangeSupport;
        this.propertyChangeSupport = propertyChangeSupport;
        propertyChangeSupport.firePropertyChange(PROP_PROPERTYCHANGESUPPORT, oldPropertyChangeSupport, propertyChangeSupport);
    }
}
