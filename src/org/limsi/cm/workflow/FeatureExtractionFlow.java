/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import edu.stanford.nlp.mt.base.IString;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.annolab.tt4j.TreeTaggerException;
import org.limsi.cm.avro.NBestListAvro;
import org.limsi.cm.avro.PostEditedCorpusEntry;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.mt.cm.props.CEProperties;
import org.limsi.types.AvroType;
import org.limsi.types.FeatureType;
import org.limsi.types.FormatType;
import org.limsi.types.LabelType;
import org.limsi.types.Language;
import sanchay.properties.KeyValueProperties;

/**
 *
 * @author anil
 */
public class FeatureExtractionFlow implements Runnable {
    
    private FeatureExtractionJob featureExtractionJob;
    private FeatureExtractionCorpus featureExtractionCorpus;
    
    private List<KeyValueProperties> jobSpecifications;
    
    protected FeatureCollector featureCollector;

    /*
     * For holding features for hypotheses for a single source language sentence.
     * This is used for both vertical and horizontal flows.
     */
    protected List<HypothesisFeatures> hypothesisFeaturesList;

    /*
     * For holding features of all types for a single source language sentence.
     */
    protected EnumMap<FeatureType, List<HypothesisFeatures>> hypothesisFeaturesListMap;

    protected List<FeatureSerializer> srcFeatureSerializers;
    protected List<FeatureSerializer> tgtFeatureSerializers;

    private List<FeatureType> allFeatureTypes;
    
    private boolean first = true;
    
    public static final String PROP_FEATUREEXTRACTIONJOB = "PROP_FEATUREEXTRACTIONJOB";
    public static final String PROP_FEATURETYPES = "PROP_FEATURETYPES";

    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    public FeatureExtractionFlow(List<KeyValueProperties> jobSpecifications)
    {
        this.jobSpecifications = jobSpecifications;        
    }

    public FeatureExtractionFlow(List<String> jobSpecs, String charset) throws FileNotFoundException, IOException
    {
        this.jobSpecifications = new ArrayList<KeyValueProperties>();
        
        for (String string : jobSpecs) {
            
            KeyValueProperties jobSpec = new KeyValueProperties(string, charset);
            
            jobSpecifications.add(jobSpec);
        }
    }
    
    private FeatureSerializer getSerializer(int jobIndex, boolean reverse)
    {                
        List<FeatureSerializer> featureSerializers = srcFeatureSerializers;
        
        if(reverse)
        {
            featureSerializers = tgtFeatureSerializers;
        }

        FeatureSerializer featureSerializer = featureSerializers.get(jobIndex);
        
        return featureSerializer;
    }

    private void createSerializers() throws IOException
    {
        int count = jobSpecifications.size();
        
        for (int i = 0; i < count; i++) {
            FeatureSerializer<NBestListAvro> featureSerializerNBest;
            FeatureSerializer<PostEditedCorpusEntry> featureSerializerPostEdit;
            
            featureExtractionJob = new FeatureExtractionJob(jobSpecifications.get(i));
            
            featureExtractionJob.loadJob();
            featureExtractionJob.loadJobExtra();

            if(featureExtractionJob.getAvroType().equals(AvroType.NBEST_LIST))
            {
                featureSerializerNBest = new FeatureSerializer<NBestListAvro>(featureExtractionJob, false);
                srcFeatureSerializers.add(featureSerializerNBest);

                featureSerializerNBest = new FeatureSerializer<NBestListAvro>(featureExtractionJob, true);
                tgtFeatureSerializers.add(featureSerializerNBest);
            }
            else if(featureExtractionJob.getAvroType().equals(AvroType.POST_EDITED_CORPUS_ENTRY))
            {
                featureSerializerPostEdit = new FeatureSerializer<PostEditedCorpusEntry>(featureExtractionJob, false);
                srcFeatureSerializers.add(featureSerializerPostEdit);

                featureSerializerPostEdit = new FeatureSerializer<PostEditedCorpusEntry>(featureExtractionJob, true);
                tgtFeatureSerializers.add(featureSerializerPostEdit);
            }
        }        
    }                

    private void initSerializers(FeatureType featureType) throws IOException
    {
        int count = jobSpecifications.size();
        
        for (int i = 0; i < count; i++) {
            FeatureSerializer featureSerializer = srcFeatureSerializers.get(i);
            featureSerializer.init(featureType);

            featureSerializer = tgtFeatureSerializers.get(i);
            featureSerializer.init(featureType);
        }                
    }

    private void closeSerializers(FeatureType featureType) throws IOException
    {
        int count = jobSpecifications.size();
        
        for (int i = 0; i < count; i++) {
            FeatureSerializer featureSerializer = srcFeatureSerializers.get(i);
            featureSerializer.cleanup(featureType);

            featureSerializer = tgtFeatureSerializers.get(i);
            featureSerializer.cleanup(featureType);
        }        
    }                

    /**
     * @return the hypothesisFeatures
     */
    public List<HypothesisFeatures> getHypothesisFeatureList() {
        return hypothesisFeaturesList;
    }
    
    public List<HypothesisFeatures> getHypothesisFeatureList(FeatureType featureType) {
        return hypothesisFeaturesListMap.get(featureType);
    }

    public HypothesisFeatures getHypothesisFeatures(FeatureType featureType,
            int hypIndex) {

        HypothesisFeatures hf = hypothesisFeaturesListMap.get(featureType).get(hypIndex);
        
        return hf;
    }

    public HypothesisFeatures getMergedHypothesisFeatures(int srcIndex, int hypIndex) {
    
        HypothesisFeatures hf = new HypothesisFeatures();
        
        for (Iterator<FeatureType> it = featureExtractionJob.getFeatureTypes().iterator(); it.hasNext();) {
            FeatureType featureType = it.next();                       
            
            HypothesisFeatures hfFT = getHypothesisFeatures(featureType, hypIndex);
            
            hf = HypothesisFeatures.merge(hf, hfFT);
        }
        
        return hf;
    }

    public List<HypothesisFeatures> getMergedHypothesisFeatureList(int srcIndex) {
    
        List<HypothesisFeatures> hfList = new ArrayList<HypothesisFeatures>();

        for (Iterator<FeatureType> it = featureExtractionJob.getFeatureTypes().iterator(); it.hasNext();) {
            
            FeatureType featureType = it.next();                       
            
            List<HypothesisFeatures> hfListFT = getHypothesisFeatureList(featureType);
            
            hfList = HypothesisFeatures.merge(hfList, hfListFT);
        }
        
        return hfList;
    }

    /**
     * Collects features of a one kind for all source sentences and
     * all the hypotheses for each source sentence. It then proceeds
     * to collect features of another kind for all the data, and so on.
     * The advantage is that the resources do not remain in memory
     * unnecessarily. More useful for offline processing of data.
     * @param ps 
     */
    public void collectAllFeaturesVertical() throws IOException, TreeTaggerException, ClassNotFoundException, FileNotFoundException, Exception
    {
        System.out.println("Starting to extract features...");
        
        for (Iterator<FeatureType> it = allFeatureTypes.iterator(); it.hasNext();) {
            
            FeatureType featureType = it.next();                       
            
            if(featureType.equals(FeatureType.MODEL) && featureExtractionJob.getNbestListPath() == null)
            {
                continue;
            }
            
            if(featureType.equals(FeatureType.SOUL_LM) && 
                    !MiscellaneousUtils.fileExists(CEProperties.getProperty("SoulLM", CEProperties.TOOL_COMMANDS)))
            {
                continue;
            }

            if(featureType.equals(FeatureType.LATTICE) && featureExtractionJob.getWordGraphPath() == null)
            {
                continue;
            }
                
            System.out.println("Extracting features of type: " + featureType.name());

            System.out.println("Extracting features for the source language data ...");

            System.out.println("Loading resource: " + featureType.getResourceType().name());
            
            initSerializers(featureType);

            ResourceFlow.loadResource(featureType.getResourceType(), featureType.getResourceSubType(), true);

            collectAllFeaturesVertical(featureType, false);

            ResourceFlow.freeResource(featureType.getResourceType());                                            
                
            try {
                Language l = featureType.getResourceType().getSrcLangauge();

                featureType.getResourceType().setSrcLangauge(featureType.getResourceType().getTgtLangauge());

                featureType.getResourceType().setTgtLangauge(l);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
            }            

            System.out.println("Extracting features for the target language data ...");                

            System.out.println("Loading resource: " + featureType.getResourceType().name());

            ResourceFlow.loadResource(featureType.getResourceType(), featureType.getResourceSubType(), true);
            
            collectAllFeaturesVertical(featureType, true);
            
            ResourceFlow.freeResource(featureType.getResourceType());
            
            closeSerializers(featureType);
        }
    }

    public void collectAllFeaturesVertical(FeatureType featureType, boolean reverse) throws IOException, TreeTaggerException, ClassNotFoundException, FileNotFoundException, Exception
    {
        for (int jobIndex = 0; jobIndex < jobSpecifications.size(); jobIndex++) {

            System.out.printf("Working on job %d: %s\n", (jobIndex + 1), jobSpecifications.get(jobIndex).getFilePath());

            List<FeatureType> jobFeatureTypes = FeatureExtractionJob.getFeatureTypes(jobSpecifications.get(jobIndex));

            if(jobSpecifications.size() > 1)
            {    
                if(jobFeatureTypes.contains(featureType) == false)
                {
                    System.out.printf("Job %d does not have features of type: %s\n", jobIndex, featureType.name());
                    continue;
                }

                loadJob(jobIndex, featureType);                        
            }

            int count = featureExtractionCorpus.getSourceText().countSentences();

            if(featureExtractionCorpus.getNbestList() != null)
            {
                List<List<TranslationHypothesis<IString, String>>> nbestLists = featureExtractionCorpus.getNbestList().getNbestLists();

                int nbsize = nbestLists.size();

                if(count != nbsize)
                {
                    System.err.format("The number of sentences in the source corpus not the same as in the nbest list: %d, %d\n",
                            count, nbsize);

                    System.exit(1);
                }
            }
            
            collectAllFeaturesVertical(jobIndex, featureType, reverse);
        }        
    }

    public void collectAllFeaturesVertical(int jobIndex, FeatureType featureType, boolean reverse) throws IOException, TreeTaggerException, ClassNotFoundException
    {
        SentenceLevelFeatureExtractor featureExtractor = featureCollector.getFeatureExtractor(featureType);
        
        if(featureExtractor == null)
        {
            featureExtractor = SentenceLevelFeatureExtractor.createFeatureExtractor(featureType,
                    featureExtractionJob, reverse);
            
            featureCollector.setFeatureExtractor(featureExtractionCorpus,
                    featureType, featureExtractor, reverse, true);
        }
        else
        {
            featureCollector.setFeatureExtractor(featureExtractionCorpus, featureType, featureExtractor, reverse, false);            
        }

        SentenceLevelFeatureExtractor.setResources(featureExtractor, featureType, reverse);
        
        featureExtractor.preprocessData();
        
        int count = featureExtractionCorpus.getSourceText().countSentences();

        List<List<TranslationHypothesis<IString, String>>> nbestLists = null;

        if(featureExtractionCorpus.getNbestList() != null)
        {           
            nbestLists = featureExtractionCorpus.getNbestList().getNbestLists();
        }
        
        for (int i = 0; i < count; i++) {

            List<TranslationHypothesis<IString, String>> nblist;

            int hcount = 1;
            
            if(featureExtractionCorpus.getNbestList() != null)
            {    
                if(!featureExtractionJob.isOneBest())
                {
                    nblist = nbestLists.get(i);
                    hcount = nblist.size();
                }
            }

//            if(!reverse)
//            {
                hypothesisFeaturesList = new ArrayList<HypothesisFeatures>();

                for (int j = 0; j < hcount; j++) {
                    hypothesisFeaturesList.add(new HypothesisFeatures());
                }                    
                
                hypothesisFeaturesListMap.put(featureType, hypothesisFeaturesList);
//            }
//            else
//            {
//                hypothesisFeaturesList = hypothesisFeaturesListMap.get(featureType);
//            }

            hypothesisFeaturesList = featureCollector.collectFeatures(featureExtractionCorpus,
                    i, hypothesisFeaturesList, featureType, reverse);

            int hfcount = hypothesisFeaturesList.size();

            if(hcount != hfcount)
            {
                System.err.format("The number of hypotheses for the source sentence %d not the same as the number of feature sets: %d, %d\n",
                        (i+1), hcount, hfcount);

                System.exit(1);
            }

//            if(reverse)
//            {
//                addRatioFeatures();
                
                FeatureSerializer featureSerializer = getSerializer(jobIndex, reverse);

                featureSerializer.serialize(featureExtractionCorpus, 
                        i, featureType, hypothesisFeaturesList);
//            }
        }
    }

    /**
     * Collects all the features for all the hypotheses of one source sentence at a
     * time and returns that. The disadvantage is that the resources have to be kept
     * in the memory till the end. The advantage is that any pair of sentences can be
     * processed on demand. More useful for online processing.
     * @param ps 
     */
    public void collectAllFeaturesHorizontal(PrintStream ps) throws FileNotFoundException, IOException
    {
        System.out.println("Starting to extract features...");
        
        int count = featureExtractionCorpus.getSourceText().countSentences();
        
        List<List<TranslationHypothesis<IString, String>>> nbestLists = featureExtractionCorpus.getNbestList().getNbestLists();

        int nbsize = nbestLists.size();

        if(count != nbsize)
        {
            System.err.format("The number of sentences in the source corpus not the same as in the nbest list: %d, %d\n",
                    count, nbsize);

            System.exit(1);
        }

        for (int i = 0; i < count; i++) {

            List<TranslationHypothesis<IString, String>> nblist = nbestLists.get(i);
            
            hypothesisFeaturesList = featureCollector.collectFeaturesHorizontal(featureExtractionCorpus,
                    i, hypothesisFeaturesList);
                
            int hcount = nblist.size();
            int hfcount = hypothesisFeaturesList.size();

            if(hcount != hfcount)
            {
                System.err.format("The number of hypotheses for the source sentence %d not the same as the number of feature sets: %d, %d\n",
                        (i+1), hcount, hfcount);

                System.exit(1);
            }
            
            if(ps == null)
            {
                ps = System.out;
            }
            
            for (int j = 0; j < hcount; j++) {
                HypothesisFeatures hf = hypothesisFeaturesList.get(i);

                if(i == 0 && j == 0)
                {
                    if(featureExtractionJob.getFormat().equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
                    {
                        hf.printHeader(ps, featureExtractionJob.getFormat());
                    }
                    else if(featureExtractionJob.getFormat().equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
                    {
                        hf.printHeader(ps, featureExtractionJob.getFormat());
                    }
                }
                
                if(featureExtractionJob.getFormat().equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
                {
                    hf.printFeatures(ps, featureExtractionJob.getFormat());
                }
                else if(featureExtractionJob.getFormat().equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT)
                        || featureExtractionJob.getFormat().equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
                {
                    if(hcount > 1)
                    {
                        ps.print(i + " ");
                    }

                    hf.printFeatures(ps, featureExtractionJob.getFormat());
                }
            }
        }
        
//        addRatioFeatures();
    }

    public void collectAndSaveFeatures() throws FileNotFoundException, UnsupportedEncodingException, IOException, TreeTaggerException, ClassNotFoundException, Exception
    {
        loadJob(0, FeatureExtractionJob.getFeatureTypes(jobSpecifications.get(0)).get(0));    
        
        if(jobSpecifications.size() == 1)
        {
            allFeatureTypes = featureExtractionJob.getFeatureTypes();
        }
        else
        {
            allFeatureTypes = FeatureExtractionJob.getFeatureTypesUnion(jobSpecifications);
        }
        
        if(featureExtractionJob.isVertical())
        {
            collectAllFeaturesVertical();
            
            mergeFiles(true);
         }
    }
    
    public void mergeFiles(boolean addRatioFeatures) throws IOException
    {
        System.out.println("Merging files for feature types...");
        
        int count = jobSpecifications.size();
        
        for (int i = 0; i < count; i++) {
            FeatureSerializer featureSerializer = tgtFeatureSerializers.get(i);
            featureSerializer.mergeFiles(featureExtractionCorpus, addRatioFeatures);
        }        
    }

    public void addRatioFeatures() throws IOException
    {
        HypothesisFeatures hf = hypothesisFeaturesList.get(0);
        
        HypothesisFeatures.findFeaturePairs(featureExtractionJob.getFeatureTypes(), hf.getFeatureValues());

        int hcount = hypothesisFeaturesList.size();

        for (int j = 0; j < hcount; j++) {
            hf = hypothesisFeaturesList.get(j);

            hf.addRatioFeatures();
        }
    }
    
    public String getSrcCharset()
    {
        FeatureType ft = featureExtractionJob.getFeatureTypes().get(0);
        
        return ft.getSrcCharset();
    }

    public String getTgtCharset()
    {
        FeatureType ft = featureExtractionJob.getFeatureTypes().get(0);
        
        return ft.getTgtCharset();        
    }

    private void loadJob(int jobIndex, FeatureType featureType)
            throws FileNotFoundException, IOException, Exception
    {        
        if(featureCollector == null)
        {
            hypothesisFeaturesListMap = new EnumMap<FeatureType, List<HypothesisFeatures>>(FeatureType.class);
            featureCollector = new FeatureCollector(featureExtractionJob);            
            srcFeatureSerializers = new ArrayList<FeatureSerializer>();           
            tgtFeatureSerializers = new ArrayList<FeatureSerializer>();
            
            createSerializers();
        }

        FeatureSerializer featureSerializer = getSerializer(jobIndex, false);
        
        featureExtractionJob = featureSerializer.getFeatureExtractionJob();

        featureExtractionCorpus = featureExtractionJob.loadFeatureExtractionCorpus(featureType);
        
        if(featureCollector != null)
        {
            featureCollector.setFeatureExtractionJob(featureExtractionJob);
            featureCollector.setFeatureExtractionCorpus(featureExtractionCorpus);
        }
     }
    
    @Override
    public void run() {
        try {
            collectAndSaveFeatures();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 100);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("srcLanguage", "sl", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tgtLanguage", "tl", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("lmOrder", "lmo", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("featureTypes", "ft", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("labelType", "lt", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("labelTypeDetailed", "ltd", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("horizontal", "h", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("saveAll", "sa", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("nbestListPath", "nb", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("oneBest", "1b", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("wordGraphPath", "wgp", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("wordGraphSuffix", "wgs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("wordGraphFromat", "wgf", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("format", "f", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("referencePath", "r", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("postEditedPath", "pe", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("originalSourcePath", "os", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("originalTargetPath", "ot", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("originalReferencePath", "or", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("originalPostEditedPath", "ope", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("labelsPath", "lp", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("multipleLabelsPath", "mlp", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("multipleLabelWeightsPath", "mlwp", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("labelsCharset", "lcs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("outputPath", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("avroPath", "a", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("tempDirPath", "tmp", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("batchMode", "bm", Options.Multiplicity.ZERO_OR_ONE);

        if (!opt.check()) {
            System.out.println("FeatureExtractionFlow [-cs charset] -sl srcLang -tl tgtLang [-nb nbestlist] [-1b <?ifOneBest>]");
            System.out.println("\t [-wgp wordGraphPath] [-wgs wordGraphSuffix] [-wgf wordGraphformat] [-lcs labelsCharset -lp labelsPath]");
            System.out.println("\t [-mlp multipleLabelsPath -mlwp multipleLabelWeightsPath][-lmo ngramorder] [-ft featuretypes]");
            System.out.println("\t [-o outfile] [-f outputformat] [-a avrofile] [-tmp tempDirPath] [srcInfile tgtInfile]");
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }
        
        FeatureExtractionFlow pipeline;

        boolean batchMode = false;

        if (opt.getSet().isSet("bm")) {
            batchMode = true;  
            System.out.println("Batch mode set to " + batchMode);
        }

        try {

            if(batchMode)
            {
                String charset = "UTF-8";

                if (opt.getSet().isSet("cs")) {
                    charset = opt.getSet().getOption("cs").getResultValue(0);
                    System.out.println("Charset: " + charset);
                }

                pipeline = new FeatureExtractionFlow(opt.getSet().getData(), charset);            
            }
            else
            {
                KeyValueProperties jobSpecifications = KeyValueProperties.readOptions(opt.getSet());

                String srcInfile = opt.getSet().getData().get(0);
                String tgtInfile = opt.getSet().getData().get(1);

                jobSpecifications.addProperty("-s", srcInfile);
                jobSpecifications.addProperty("-t", tgtInfile);

                List<KeyValueProperties> jobSpecs = new ArrayList<KeyValueProperties>();

                jobSpecs.add(jobSpecifications);

                pipeline = new FeatureExtractionFlow(jobSpecs);
            }

            pipeline.collectAndSaveFeatures();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {
//        args = new String[]{"-bm", "wmt-1-3.job.txt"};
//        args = new String[]{"-bm", "nbest-fr-en.sample.job.txt"};
//        args = new String[]{"-bm", "en-fr-first.job.txt"};
//        args = new String[]{"-bm", "en-fr-first.job.txt", "en-fr-second.job.txt"};
//        args = new String[]{"-sl", "fr", "-tl", "en", "-ft", "POSCOUNTS", "-nb",
//            "/extra/work/wmt13/nbest-sample.txt",
//            "-o", "tmp/nbest-features.svmlight",
//            "-a", "tmp/nbest-features.avro",
//            "/extra/work/wmt13/fr-sample.txt", "/extra/work/wmt13/en-sample.txt"};
        
        if(true)
//        if(args != null && args.length > 1)
        {
            runWithCmdLineOptions(args);
        }
        else
        {        
//            String srcPath = "/extra/work/wmt13/fr-sample.txt";
//            String tgtPath = "/extra/work/wmt13/en-sample.txt";
            String srcPath = "/more/work/trace-qe/tok-noalt/en/en_first.data.src.fte.utf8";
            String tgtPath = "/more/work/trace-qe/tok-noalt/fr/en_first.data.trans.fte.utf8";
            String refPath = null;
            String pePath = "/more/work/trace-qe/tok-noalt/fr/en_first.data.pedit.fte.utf8";
//            String nbestListPath = "/extra/work/wmt13/nbest-sample.txt";
            String nbestListPath = null;

            String outPath = "tmp/features.svmlight";
            String avroPath = "tmp/features.avro";

            String srcCS = "UTF-8";
            String tgtCS = "UTF-8";
            String nbestCS = "UTF-8";

            String featureTypes = "";
            
//            featureTypes += FeatureType.SURFACE.name();
//            featureTypes += "," + FeatureType.NGRAM_LM.name();
//            featureTypes += "," + FeatureType.NGRAM_LM_POS.name();
            featureTypes += FeatureType.NGRAM_LM_POS_NOLEX.name();
//            featureTypes += "," + FeatureType.NGRAM_COUNTS.name();
//            featureTypes += "," + FeatureType.NGRAM_COUNTS_POS.name();
//            featureTypes += "," + FeatureType.NGRAM_COUNTS_POS_NOLEX.name();
//            featureTypes += "," + FeatureType.IBM1.name();
//            featureTypes += "," + FeatureType.MODEL.name();
//            featureTypes += "," + FeatureType.POSCOUNTS.name();
//            featureTypes += "," + FeatureType.SOUL_LM.name();
//            featureTypes += "," + FeatureType.LABEL.name();

            String labelType = LabelType.HTER.name();

            KeyValueProperties jobSpecification = new KeyValueProperties();
            
            Language srcLanguage = Language.ENGLISH;
            Language tgtLanguage = Language.FRENCH;

            jobSpecification.addProperty("-sl", srcLanguage.getThreeLetterName());
            jobSpecification.addProperty("-tl", tgtLanguage.getThreeLetterName());

            jobSpecification.addProperty("-s", srcPath);
            jobSpecification.addProperty("-t", tgtPath);
            jobSpecification.addProperty("-nb", nbestListPath);
            jobSpecification.addProperty("-r", refPath);
            jobSpecification.addProperty("-pe", pePath);
            
            jobSpecification.addProperty("-ft", featureTypes);
            jobSpecification.addProperty("-lt", labelType);

            jobSpecification.addProperty("-o", outPath);
            jobSpecification.addProperty("-a", avroPath);
            
            List<KeyValueProperties> jobSpecs = new ArrayList<KeyValueProperties>();

            jobSpecs.add(jobSpecification);

            FeatureExtractionFlow pipeline = new FeatureExtractionFlow(jobSpecs);

            try {
                pipeline.collectAndSaveFeatures();

            } catch (FileNotFoundException ex) {
                Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @return the featureExtractionJob
     */
    public FeatureExtractionJob getFeatureExtractionJob() {
        return featureExtractionJob;
    }

    /**
     * @param featureExtractionJob the featureExtractionJob to set
     */
    public void setFeatureExtractionJob(FeatureExtractionJob featureExtractionJob) throws PropertyVetoException {
        org.limsi.cm.workflow.FeatureExtractionJob oldFeatureExtractionJob = this.featureExtractionJob;
        vetoableChangeSupport.fireVetoableChange(PROP_FEATUREEXTRACTIONJOB, oldFeatureExtractionJob, featureExtractionJob);
        this.featureExtractionJob = featureExtractionJob;
        propertyChangeSupport.firePropertyChange(PROP_FEATUREEXTRACTIONJOB, oldFeatureExtractionJob, featureExtractionJob);
    }
}
