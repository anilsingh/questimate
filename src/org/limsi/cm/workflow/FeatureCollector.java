/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.workflow;

import org.limsi.mt.cm.features.SentenceLevelFeatureExtractor;
import java.io.*;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.FeatureType;
import org.limsi.types.FormatType;

/**
 *
 * @author anil
 */
public class FeatureCollector {
    
    private FeatureExtractionJob featureExtractionJob;

    protected FeatureExtractionCorpus featureExtractionCorpus;
    
    protected EnumMap<FeatureType, SentenceLevelFeatureExtractor> featureExtractors;
    
    protected SentenceLevelFeatureExtractor featureExtractor;

    protected List<HypothesisFeatures> hypothesisFeaturesList;
    
    public FeatureCollector(FeatureExtractionJob featureExtractionJob) {
        
        this.featureExtractionJob = featureExtractionJob;
        
        hypothesisFeaturesList = new ArrayList<HypothesisFeatures>();
 
        featureExtractors = new EnumMap<FeatureType, SentenceLevelFeatureExtractor>(FeatureType.class);
    }

    /**
     * @return the featureExtractionJob
     */
    public FeatureExtractionJob getFeatureExtractionJob() {
        return featureExtractionJob;
    }

    /**
     * @param featureExtractionJob the featureExtractionJob to set
     */
    void setFeatureExtractionJob(FeatureExtractionJob featureExtractionJob) {
        this.featureExtractionJob = featureExtractionJob;
    }

    /**
     * @return the featureExtractionCorpus
     */
    public FeatureExtractionCorpus getFeatureExtractionCorpus() {
        return featureExtractionCorpus;
    }

    /**
     * @param featureExtractionCorpus the featureExtractionCorpus to set
     */
    public void setFeatureExtractionCorpus(FeatureExtractionCorpus featureExtractionCorpus) {
        this.featureExtractionCorpus = featureExtractionCorpus;
    }

    /**
     * @return the featureExtractor
     */
    public SentenceLevelFeatureExtractor getFeatureExtractor() {
        return featureExtractor;
    }

    /**
     * @param featureExtractor the featureExtractor to set
     */
    public void setFeatureExtractor(SentenceLevelFeatureExtractor featureExtractor) {
        this.featureExtractor = featureExtractor;
    }

    public List<HypothesisFeatures> collectFeaturesHorizontal(FeatureExtractionCorpus featureExtractionCorpus,
            int srcIndex, List<HypothesisFeatures> hypothesisFeaturesList) throws FileNotFoundException, IOException, IOException {

        for(FeatureType featureType: FeatureType.values())
        {            
            // Source side
            hypothesisFeaturesList = collectFeatures(featureExtractionCorpus, srcIndex, hypothesisFeaturesList, featureType, false);
            
            // Target side
            hypothesisFeaturesList = collectFeatures(featureExtractionCorpus, srcIndex, hypothesisFeaturesList, featureType, true);
        }        
        
        return hypothesisFeaturesList;
    }
    
    public SentenceLevelFeatureExtractor getFeatureExtractor(FeatureType featureType)
    {
        return featureExtractors.get(featureType);
    }
    
    public void setFeatureExtractor(FeatureExtractionCorpus featureExtractionCorpus,
            FeatureType featureType, SentenceLevelFeatureExtractor featureExtractor,
            boolean reverse, boolean add)
    {
        featureExtractor.setFeatureType(featureType);
        featureExtractor.setReverse(reverse);

        featureExtractor.setFeatureCollector(this);

        if(add)
        {
            featureExtractors.put(featureType, featureExtractor);
        }
    }

    public List<HypothesisFeatures> collectFeatures(FeatureExtractionCorpus featureExtractionCorpus,
            int srcIndex, List<HypothesisFeatures> hypothesisFeaturesList,
            FeatureType ftype, boolean reverse) throws FileNotFoundException, IOException {

        featureExtractor = featureExtractors.get(ftype);

//        featureExtractor.setFeatureType(ftype);
//        featureExtractor.setReverse(reverse);
//
//        featureExtractor.setSourceText(sourceText);
//        featureExtractor.setTargetText(targetText);
//        featureExtractor.setNbestList(nbestList);
        
        hypothesisFeaturesList = getFeatureExtractor().extractAllFeatures(srcIndex, hypothesisFeaturesList);

        if(featureExtractionCorpus.getNbestList() != null)
        {
            if(!reverse)
            {
                System.out.printf("Feature collection for type %s for the source side finished. %d hypotheses found.\n",
                        ftype.name(), hypothesisFeaturesList.size());
            }
            else
            {
                System.out.printf("Feature collection for type %s for the target side finished. %d hypotheses found.\n",
                        ftype.name(), hypothesisFeaturesList.size());
            }
        }
        
        return hypothesisFeaturesList;
    }
    
    public void readFeatureFile(String path, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        LineNumberReader reader;

        if (path.endsWith(".gz")) {
            reader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(path)), encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    path), encoding));
        }
        
        Pattern space = Pattern.compile(" ");
        
        int h = 0;
        for (String inline; (inline = reader.readLine()) != null;)
        {
            Double hl = 0.0;
            HypothesisFeatures hf = null;
            
            if(h >= hypothesisFeaturesList.size() - 1)
            {
                hf = new HypothesisFeatures();
//                hypothesisLabels.add(hl);
                hypothesisFeaturesList.add(hf);
            }
            else
            {
//                hl = hypothesisLabels.get(h);
                hf = hypothesisFeaturesList.get(h);
            }
            
            h++;
            
            String fields[] = space.split(inline);
            
            for (int i = 0; i < fields.length; i++) {
                if(i == 0)
                {
//                    System.out.println("Label: " + fields[0]);
//                    hypothesisLabels.set(i, Double.parseDouble(fields[0]));
//                    hypothesisLabels.add(Double.parseDouble(fields[0]));
                }
                else
                {
                    String ifields[] = fields[i].split(":");
                    
                    if(ifields.length != 2)
                    {
                        System.err.println("Error in feature file, two fields required, line number: " + reader.getLineNumber());
                    }
                    
                    hf.addFeature(ifields[0], ifields[1]);
                }
            }
        }
    }
    
    public void readFeatureFiles(String labelFile, String[] featureFiles, String encoding) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        LineNumberReader labelReader;
        LineNumberReader[] featureReaders = new LineNumberReader[featureFiles.length];

        if (labelFile.endsWith(".gz")) {
            labelReader = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(labelFile)), encoding));
        } else {
            labelReader = new LineNumberReader(new InputStreamReader(new FileInputStream(
                    labelFile), encoding));
        }
        
        for (int i = 0; i < featureFiles.length; i++)
        {
            if (featureFiles[i].endsWith(".gz")) {
                featureReaders[i] = new LineNumberReader(new InputStreamReader(new GZIPInputStream(
                        new FileInputStream(featureFiles[i])), encoding));
            } else {
                featureReaders[i] = new LineNumberReader(new InputStreamReader(new FileInputStream(
                        featureFiles[i]), encoding));
            }            
        }
        
        Pattern space = Pattern.compile(" ");
        
        int h = 0;
        String featuresLines[] = new String[featureFiles.length];
        
        for (String labelLine; (labelLine = labelReader.readLine()) != null;)
        {
//            hypothesisLabels.add(Double.parseDouble(labelLine.trim()));
            
            HypothesisFeatures hf = null;
            
            if(h >= hypothesisFeaturesList.size() - 1)
            {
                hf = new HypothesisFeatures();
//                hypothesisLabels.add(hl);
                hypothesisFeaturesList.add(hf);
            }
            else
            {
//                hl = hypothesisLabels.get(h);
                hf = hypothesisFeaturesList.get(h);
            }
            
            h++;

            for (int i = 0; i < featureFiles.length; i++)
            {
                featuresLines[i] = featureReaders[i].readLine();
            
                String fields[] = space.split(featuresLines[i]);
            
                for (int j = 0; j < fields.length; j++)
                {
                    String ifields[] = fields[j].split(":");

                    if(ifields.length != 2)
                    {
                        System.err.println("Error in feature file, two fields required, line number: " + labelReader.getLineNumber());
                    }

                    hf.addFeature(ifields[0], ifields[1]);
                }
            }
        }
    }

    public void printFeatures(PrintStream ps)
    {
        int hcount = hypothesisFeaturesList.size();
        
        for (int i = 0; i < hcount; i++)
        {
//            ps.print(hypothesisLabels.get(i) + " ");
            hypothesisFeaturesList.get(i).printFeatures(ps, FormatType.FeatureCollectionFormat.ARFF_FORMAT);
        }
    }
    
    public void writeFeatureFile(String path, String encoding) throws UnsupportedEncodingException, FileNotFoundException
    {
        PrintStream ps = new PrintStream(path, encoding);
        
        printFeatures(ps);
    }
    
    public static void main(String args[])
    {
//        FeatureCollector featureCollector = new FeatureCollector();
        
//        System.out.println("Reading the existing feature file ...");
        
//        try {
//            featureCollector.readFeatureFile("/people/wisniews/workspace/pred/data/a_debordeliser/train_normalize_ter.svmlight", "UTF-8");
//            featureCollector.readFeatureFile("/people/wisniews/workspace/pred/data/a_debordeliser/test_normalize_ter.svmlight", "UTF-8");
//            featureCollector.readFeatureFile("/people/wisniews/workspace/pred/data/test_normalize_ter.svmlight", "UTF-8");
//            featureCollector.readFeatureFiles("/people/wisniews/workspace/pred/data/train_score.label",
//                    new String[]{"/people/wisniews/workspace/pred/data/train_baseline_features.svmlight"},
//                    "UTF-8");
//            featureCollector.readFeatureFiles("/people/wisniews/workspace/pred/data/test_score.label",
//                    new String[]{"/people/wisniews/workspace/pred/data/test_baseline_features.svmlight"},
//                    "UTF-8");
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
//        featureCollector.collectFeaturesHorizontal();
        
//        featureCollector.printFeatures(System.out);
        
//        try {
//            featureCollector.writeFeatureFile("/people/anil/work/confidence-estimation/extracted-features/guillaume/wmt12-train-10-07-12a", "UTF-8");
//            featureCollector.writeFeatureFile("/people/anil/work/confidence-estimation/extracted-features/guillaume/wmt12-train-13-07-12a", "UTF-8");
//            featureCollector.writeFeatureFile("/people/anil/work/confidence-estimation/extracted-features/artem/newstest2012-17-07-12a", "UTF-8");
            
    //        featureCollector.testFeatureExtraction();
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(FeatureCollector.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
