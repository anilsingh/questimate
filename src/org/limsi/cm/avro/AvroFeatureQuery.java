/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import org.limsi.types.AvroSearchOperationType;
import org.limsi.types.AvroType;

/**
 *
 * @author anil
 */
public class AvroFeatureQuery implements Cloneable {

    public String key;
    public String value;
    public Float minValue;
    public Float maxValue;
    public AvroType avroType;
    public AvroSearchOperationType avroSearchOperation;
    public AvroTextQuery avroTextQuery;
    
    public AvroFeatureQuery(AvroType type)
    {
        this.avroType = type;        
    }
    
    public AvroFeatureQuery(AvroType type, AvroSearchOperationType avroSearchOperation,
            AvroTextQuery avroTextQuery)
    {
        this.avroType = type;        
        this.avroSearchOperation = avroSearchOperation;
        this.avroTextQuery = avroTextQuery;
    }

    public AvroFeatureQuery(String key, String value, Float minValue, Float maxValue,
            AvroType type, AvroSearchOperationType avroSearchOperation, AvroTextQuery avroTextQuery)
    {
        this.key = key;
        this.value = value;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.avroType = type;
        this.avroSearchOperation = avroSearchOperation;
        this.avroTextQuery = avroTextQuery;
    }
    
    public boolean isActive()
    {
        if(key != null)
        {
            if(value != null || minValue != null || maxValue != null)
            {
                return true;
            }
        }
        
        if(avroTextQuery != null)
        {
            return true;
        }
        
        if(avroSearchOperation != null)
        {
            return true;
        }
        
        return false;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        AvroFeatureQuery query = (AvroFeatureQuery) super.clone();
        
        query.key = key;
        query.value = value;
        query.maxValue = maxValue;
        query.minValue= minValue;
        query.avroType = avroType;
        
        return query;
    }

    public static AvroFeatureQuery createQuery(AvroType type)
    {
        AvroFeatureQuery query = new AvroFeatureQuery(type);
                
        return query;
    }

    public static AvroFeatureQuery createQuery(AvroType type, AvroSearchOperationType avroSearchOperation,
            AvroTextQuery avroTextQuery)
    {
        AvroFeatureQuery query = new AvroFeatureQuery(type, avroSearchOperation, avroTextQuery);
                
        return query;
    }

    public static AvroFeatureQuery createQuery(String key, String value, Float minValue,
            Float maxValue, AvroType type, AvroSearchOperationType avroSearchOperation,
            AvroTextQuery avroTextQuery)
    {
        AvroFeatureQuery query = new AvroFeatureQuery(key, value, minValue, maxValue, type,
                avroSearchOperation, avroTextQuery);
        
        return query;
    }
}
