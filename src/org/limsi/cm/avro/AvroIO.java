/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.types.AvroType;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class AvroIO {
    public static <T> Instances readInstances(String avroPath, AvroType type) throws IOException
    {
        int capacity = 0;
        FastVector attInfo = new FastVector();
        
        Instances instances = null;

        T avroPtr = (T) AvroType.createInstance(type);

        SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
        DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(avroPath), userDatumReader);

        int r = 0;
        
        while (dataFileReader.hasNext()) {

            System.out.printf("Processing record: %d\r", ++r);

            avroPtr = dataFileReader.next(avroPtr);
            
            if(type.equals(AvroType.POST_EDITED_CORPUS_FEATURES) || type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
            {
                PostEditedCorpusRecord postEditedCorpusRecord = null;
                PostEditedCorpusFeatures postEditedCorpusFeatures;

                if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
                {
                    postEditedCorpusRecord = (PostEditedCorpusRecord) avroPtr;
                    
                    postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();
                }
                else
                {
                    postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroPtr;
                }                
                
                Map featureMap = postEditedCorpusFeatures.getFeatures();
                
                FeatureValues featureValues = FeatureValueUtils.getFeatureValues(featureMap);

                if(r == 1)
                {
                    Iterator<Integer> itr = featureValues.getFeatureIndices();
                    
                    capacity = featureValues.size();
                    
                    while (itr.hasNext()) {
                        Integer featureIndex = itr.next();
                        
                        String featureName = featureValues.getFeatureNameFromIndex(featureIndex);
                        
                        Attribute attribute = new Attribute(featureName);
                        
                        attInfo.addElement(attribute);
                    }

                    instances = new Instances(MiscellaneousUtils.getFileName(avroPath), attInfo, 1000);
                }
                                
                Instance instance = new Instance(capacity);

                Iterator<Integer> itr = featureValues.getFeatureIndices();

                while (itr.hasNext()) {
                    Integer featureIndex = itr.next();

                    String featureName = featureValues.getFeatureNameFromIndex(featureIndex);

                    Float featureValue = (Float) featureValues.getFeatureValue(featureName);                    
                    
                    instance.setValue(instances.attribute(featureName), featureValue);
                }                

                if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
                {
                    Map labelMap = postEditedCorpusRecord.getLabels();
                    
                    if(labelMap != null)
                    {
                        FeatureValues labelValues = FeatureValueUtils.getFeatureValues(labelMap);

                        itr = labelValues.getFeatureIndices();

                        while (itr.hasNext()) {
                            Integer labelIndex = itr.next();

                            String labelName = featureValues.getFeatureNameFromIndex(labelIndex);

                            Float labelValue = (Float) featureValues.getFeatureValue(labelName);                    

                            instance.setValue(instances.attribute(labelName), labelValue);
                        }                    
                    }
                }
                    
                instances.add(instance);
            }
            else if(type.equals(AvroType.POST_EDITED_CORPUS_ENTRY))
            {
                PostEditedCorpusEntry postEditedCorpusEntry = (PostEditedCorpusEntry) avroPtr;

                Map featureMap = postEditedCorpusEntry.getFeatures();
                Map labelMap = postEditedCorpusEntry.getLabels();
                
                FeatureValues featureValues = FeatureValueUtils.getFeatureValues(featureMap);
                FeatureValues labelValues = FeatureValueUtils.getFeatureValues(labelMap);

                if(r == 1)
                {
                    Iterator<Integer> itr = featureValues.getFeatureIndices();
                    
                    capacity = featureValues.size();
                    
                    while (itr.hasNext()) {
                        Integer featureIndex = itr.next();
                        
                        String featureName = featureValues.getFeatureNameFromIndex(featureIndex);
                        
                        Attribute attribute = new Attribute(featureName);
                        
                        attInfo.addElement(attribute);
                    }

                    itr = labelValues.getFeatureIndices();
                    
                    capacity += labelValues.size();
                    
                    while (itr.hasNext()) {
                        Integer labelIndex = itr.next();
                        
                        String labelName = labelValues.getFeatureNameFromIndex(labelIndex);
                        
                        Attribute attribute = new Attribute(labelName);
                        
                        attInfo.addElement(attribute);
                    }

                    instances = new Instances(MiscellaneousUtils.getFileName(avroPath), attInfo, 1000);
                }
                                
                Instance instance = new Instance(capacity);

                Iterator<Integer> itr = featureValues.getFeatureIndices();

                while (itr.hasNext()) {
                    Integer featureIndex = itr.next();

                    String featureName = featureValues.getFeatureNameFromIndex(featureIndex);

                    Float featureValue = (Float) featureValues.getFeatureValue(featureName);                    
                    
                    instance.setValue(instances.attribute(featureName), featureValue);
                }                

                if(labelMap != null)
                {
                    itr = labelValues.getFeatureIndices();

                    while (itr.hasNext()) {
                        Integer labelIndex = itr.next();

                        String labelName = labelValues.getFeatureNameFromIndex(labelIndex);

                        Float labelValue = (Float) labelValues.getFeatureValue(labelName);                    

                        instance.setValue(instances.attribute(labelName), labelValue);
                    }                    
                }
                    
                instances.add(instance);
            }
        }

        return instances;
    }
}
