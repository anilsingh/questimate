/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.apache.avro.Schema;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.limsi.cm.workflow.FeatureExtractionFlow;
import org.limsi.mt.cm.HypothesisConfidence;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.AvroOperationType;
import org.limsi.types.AvroSearchOperationType;
import org.limsi.types.AvroType;
import org.limsi.types.FormatType;
import weka.core.Instances;

/**
 *
 * @author anil
 */
public class AvroDatasetOperations {
    
    public static List<NBestListAvro> getNBestLists(String avroPath) throws IOException
    {
        List<NBestListAvro> nblists = new ArrayList<NBestListAvro>();
        
        DatumReader<NBestListAvro> userDatumReader = new SpecificDatumReader<NBestListAvro>(NBestListAvro.class);
        DataFileReader<NBestListAvro> dataFileReader = new DataFileReader<NBestListAvro>(new File(avroPath), userDatumReader);

        NBestListAvro nBestListAvro = null;
        
        while (dataFileReader.hasNext()) {

            nBestListAvro = dataFileReader.next(nBestListAvro);
//            System.out.println(user);
        }
        
        return nblists;
    }
    
    public static <T> void mergeHorizontal(AvroType type, List<String> inpaths, String outpath) throws IOException
    {
        System.out.println("Merging AVRO files to: " + outpath);

        List<DataFileReader<T>> dataFileReaders = new ArrayList<DataFileReader<T>>();
        
        int inFileCount = inpaths.size();
        
        T[] avroPtrs = (T[]) AvroType.createArray(type, inFileCount);

        File nbestSchemaFile = new File(type.getSchemaPath());

        Schema nbestSchema = AvroUtils.parseSchema(nbestSchemaFile);

        for (int i = 0; i < inFileCount; i++) {
            String string = inpaths.get(i);
        
            SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
//            DatumReader<NBestListAvro> userDatumReader = new SpecificDatumReader<NBestListAvro>(nbestSchema);
            DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(string), userDatumReader);
            
            dataFileReaders.add(dataFileReader);
        }
                
        File avroFile = new File(outpath);

        DatumWriter<T> userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());
        DataFileWriter<T> dataFileWriter = new DataFileWriter<T>(userDatumWriter);

        dataFileWriter.setCodec(type.getCodecFactory());
        
        dataFileWriter.create(nbestSchema, avroFile);
        
        int r = 0;
        while (dataFileReaders.get(0).hasNext()) {
                
            System.out.printf("Processing record: %d\r", ++r);

            for (int i = 0; i < inFileCount; i++) {

                try
                {
                    avroPtrs[i] = dataFileReaders.get(i).next(avroPtrs[i]);
                }
                catch(NoSuchElementException ex)
                {
                    System.out.format("This file has some problem: %s\n", inpaths.get(i));
                    System.out.format("Please fix it.\n");
                    System.out.format("Aborting.\n");
                    System.exit(1);
                }
            }
             
            T avroMerged = AvroUtils.mergeAvroRecords(avroPtrs, type);
        
            dataFileWriter.append(avroMerged);        
        } 

        System.out.println("Finished merging the files. Total records found: " + r);

        for (int i = 0; i < inFileCount; i++) {
            dataFileReaders.get(i).close();
        }
        
        dataFileWriter.close();        
    }
    
    public static <T> void mergeVertical(AvroType type, List<String> inpaths, String outpath) throws IOException
    {
        System.out.println("Concatenating AVRO files to: " + outpath);
        
        int inFileCount = inpaths.size();
        
        T avroPtr = (T) AvroType.createInstance(type);

        File nbestSchemaFile = new File(type.getSchemaPath());

        Schema nbestSchema = AvroUtils.parseSchema(nbestSchemaFile);
                
        File avroFile = new File(outpath);

        DatumWriter<T> userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());
        DataFileWriter<T> dataFileWriter = new DataFileWriter<T>(userDatumWriter);

        dataFileWriter.setCodec(type.getCodecFactory());
        
        dataFileWriter.create(nbestSchema, avroFile);

        int rt = 0;
        for (int i = 0; i < inFileCount; i++) {
            String string = inpaths.get(i);
        
            SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
            DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(string), userDatumReader);
            
            int r = 0;
            while (dataFileReader.hasNext()) {

                System.out.printf("Processing record: %d for file %s\r", ++r, string);

                try
                {
                    avroPtr = dataFileReader.next(avroPtr);
        
                    dataFileWriter.append(avroPtr);        
                }
                catch(NoSuchElementException ex)
                {
                    System.out.format("This file has some problem: %s\n", inpaths.get(i));
                    System.out.format("Please fix it.\n");
                    System.out.format("Aborting.\n");
                    System.exit(1);
                }
            }

            System.out.printf("\nFound %d records in file %s\n ", r, string);
            
            rt += r;
            
            dataFileReader.close();
        }

        System.out.printf("\nFound a total of %d records in the files\n ", rt);
        
        dataFileWriter.close();
    }
    
    public static <T> void splitHorizontal(AvroType type, List<String> inpaths) throws IOException
    {
        for (String string : inpaths) {
            splitHorizontal(type, string);
        }
    }

    public static <T> void splitHorizontal(AvroType type, String inpath) throws IOException
    {
        System.out.println("Horizontally splitting the AVRO file: " + inpath);

        T avroPtr = (T) AvroType.createInstance(type);

        File nbestSchemaFile = new File(type.getSchemaPath());

        Schema nbestSchema = AvroUtils.parseSchema(nbestSchemaFile);

        SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
        DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(inpath), userDatumReader);        
                            
//        File avroFile = new File(outpath);
//
//        DatumWriter<T> userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());
//        DataFileWriter<T> dataFileWriter = new DataFileWriter<T>(userDatumWriter);
//        dataFileWriter.create(nbestSchema, avroFile);
//        
//        int r = 0;
//        while (dataFileReaders.get(0).hasNext()) {
//                
//            System.out.printf("Processing record: %d\r", ++r);
//
//            for (int i = 0; i < inFileCount; i++) {
//
//                try
//                {
//                    avroPtr[i] = dataFileReaders.get(i).next(avroPtr[i]);
//                }
//                catch(NoSuchElementException ex)
//                {
//                    System.out.format("This file has some problem: %s\n", inpaths.get(i));
//                    System.out.format("Please fix it.\n");
//                    System.out.format("Aborting.\n");
//                    System.exit(1);
//                }
//            }
//            
//            T avroMerged = null;
//            
//            if(sample instanceof NBestListAvro)
//            {
//                NBestListAvro avroSpecificPtrs[] = (NBestListAvro[]) avroPtr;
//                NBestListAvro avroMergedSpecific = AvroUtils.mergeNBestLists(avroSpecificPtrs);
//                avroMerged = (T) avroMergedSpecific;
//            }
//            else if(sample instanceof PostEditedCorpusFeatures)
//            {
//                PostEditedCorpusFeatures avroSpecificPtrs[] = (PostEditedCorpusFeatures[]) avroPtr;
//                PostEditedCorpusFeatures avroMergedSpecific = AvroUtils.mergePostEditedCorpusFeatures(avroSpecificPtrs);                
//                avroMerged = (T) avroMergedSpecific;
//            }
//        
//            dataFileWriter.append(avroMerged);        
//        } 
//
//        System.out.println("Finished merging the files. Total records found: " + r);
//        
//        dataFileWriter.close();                
    }
    
    public static <T> void splitVertical(AvroType type, AvroOperationType avroOperationType, 
            List<String> inpaths, float splitSize) throws IOException
    {
        for (String string : inpaths) {
            splitVertical(type, avroOperationType, string, splitSize);
        }
    }
    
    public static <T> void splitVertical(AvroType type, AvroOperationType avroOperationType,
            String inpath, float splitSize) throws IOException
    {
        System.out.println("Splitting AVRO file: " + inpath);

        T avroPtr = (T) AvroType.createInstance(type);
        
        SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
        DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(inpath), userDatumReader);
        
        AvroFeatureQuery query = AvroFeatureQuery.createQuery(type, AvroSearchOperationType.COUNT_RECORDS, null);

        int numRecords = AvroSearch.countRecords(inpath, query);

        File nbestSchemaFile = new File(type.getSchemaPath());

        Schema nbestSchema = AvroUtils.parseSchema(nbestSchemaFile);

        DatumWriter<T> userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());

        int size = 0;
        if(splitSize > 0 && splitSize < 1)
        {
            size = Math.round(splitSize * numRecords);
        }
        
        DataFileWriter<T> dataFileWriter = null;

        int r = 0;
        int s = 0;
        
        while (dataFileReader.hasNext()) {

            System.out.printf("Processing record %d in file %s\r", ++r, inpath);

            if(r == 1 || (avroOperationType.equals(AvroOperationType.SPLIT_VERTICAL_2_WAY) && r == size + 1)
                     || (avroOperationType.equals(AvroOperationType.SPLIT_VERTICAL_N_WAY) && r % size == 1))
            {
                if(dataFileWriter != null)
                {
                    dataFileWriter.close();
                }
                
                dataFileWriter = new DataFileWriter<T>(userDatumWriter);
        
                dataFileWriter.setCodec(type.getCodecFactory());
                
                dataFileWriter.create(nbestSchema, new File(inpath + "." + ++s));                
            }
 
            avroPtr = dataFileReader.next(avroPtr);
           
            dataFileWriter.append(avroPtr);
        }

        System.out.printf("Split the input file into %d files\n", s);

        dataFileWriter.close();
        dataFileReader.close();
    }
    
    public static <T> void convert(AvroType type, List<String> inpaths,
            FormatType.FeatureCollectionFormat inFormat,
            FormatType.FeatureCollectionFormat outFormat, String charset) throws IOException
    {
        if(inFormat.equals(FormatType.FeatureCollectionFormat.AVRO_FORMAT)
                && (outFormat.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT)
                    || outFormat.equals(FormatType.FeatureCollectionFormat.ARFF_FORMAT))
                    || outFormat.equals(FormatType.FeatureCollectionFormat.CSV_FORMAT))
        {
            printAvro(type, inpaths, outFormat, charset);
        }
        else
        {
            System.err.println("Sorry, this format pair is not yet implemented.");
        }
    }

    public static <T> void printAvro(AvroType type, List<String> inpaths,
            FormatType.FeatureCollectionFormat format, String charset) throws IOException
    {
        for (String string : inpaths) {
            printAvro(type, string, format, charset);
        }                
    }

    public static <T> void printAvro(AvroType type, String inpath,
            FormatType.FeatureCollectionFormat format, String charset) throws IOException
    {
        System.out.println("Converting the file: " + inpath);
        
//        T avroPtr = (T) AvroType.createInstance(type);
//
//        SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
//        DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(inpath), userDatumReader);
//        
//        PrintStream ps = new PrintStream(inpath + "." + format.getFormatExtension(), charset);
//
//        int r = 0;
//        while (dataFileReader.hasNext()) {
//
//            System.out.printf("Processing record %d in file %s\r", ++r, inpath);
//
//            avroPtr = dataFileReader.next(avroPtr);
//            
//            printAvro((r - 1), type, avroPtr, format, ps);
//        }
//
//        dataFileReader.close();
//        ps.close();
        
        Instances instances = HypothesisConfidence.getInstances(inpath, null);
        
        HypothesisConfidence.saveInstances(instances, inpath + "." + format.getFormatExtension(), format);

        System.out.println("\nFinished converting the file.");
        
    }

    public static <T> void printAvro(int srcIndex, AvroType type, T avroRecord, 
            FormatType.FeatureCollectionFormat format, PrintStream ps) throws IOException
    {
        if(avroRecord instanceof NBestListAvro)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;

            List<Hypothesis> avroHyps = nBestListAvro.getHypotheses();

            int hcount = avroHyps.size();
            
            List<HypothesisFeatures> hfList = new ArrayList<HypothesisFeatures>(hcount);

            for (int j = 0; j < hcount; j++) {  
                Hypothesis avroHyp = avroHyps.get(j);

                Map<CharSequence, Float> featureValues = avroHyp.getFeatures();
                
                HypothesisFeatures hf = new HypothesisFeatures();
                
                hf.setFeatureValues(featureValues);
                
                hfList.add(hf);
            }
                
            printFeatures(srcIndex, hfList, format, ps);
        }
        else if(avroRecord instanceof PostEditedCorpusFeatures)
        {
            PostEditedCorpusFeatures postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroRecord;

            Map<CharSequence, Float> featureValues = postEditedCorpusFeatures.getFeatures();

            List<HypothesisFeatures> hfList = new ArrayList<HypothesisFeatures>(1);

            HypothesisFeatures hf = new HypothesisFeatures();

            hf.setFeatureValues(featureValues);

            hfList.add(hf);
                
            printFeatures(srcIndex, hfList, format, ps);
        }        
        else if(avroRecord instanceof PostEditedCorpusRecord)
        {
            PostEditedCorpusRecord postEditedCorpusRecord = (PostEditedCorpusRecord) avroRecord;

            PostEditedCorpusFeatures postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();
            
            printAvro(srcIndex, type, postEditedCorpusFeatures, format, ps);

            Map<CharSequence, Float> labelValues = postEditedCorpusRecord.getLabels();

            List<HypothesisFeatures> hfList = new ArrayList<HypothesisFeatures>(1);

            HypothesisFeatures hf = new HypothesisFeatures();

            hf.setFeatureValues(labelValues);

            hfList.add(hf);
                
            printFeatures(srcIndex, hfList, format, ps);
        }        
    }

    public static void printFeatures(int srcIndex, List<HypothesisFeatures> hypothesisFeatureList,
            FormatType.FeatureCollectionFormat format, PrintStream ps)
    {
        int hcount = hypothesisFeatureList.size();

        for (int j = 0; j < hcount; j++) {
            HypothesisFeatures hf = hypothesisFeatureList.get(j);

            if(srcIndex == 0 && j == 0)
            {
                hf.printHeader(ps, format);
            }

            if(format.equals(FormatType.FeatureCollectionFormat.SVMLIGHT_FORMAT))
            {
                if(hcount > 1)
                {
                    ps.print(srcIndex + " ");
                }
            }

            hf.printFeatures(ps, format);
        }
    }
    
    public static <T> void applyOperation(AvroType type, AvroOperationType operationType,
            float splitSize, List<String> inpaths, String outpath,
            FormatType.FeatureCollectionFormat inFormat,
            FormatType.FeatureCollectionFormat outFormat, String charset) throws IOException
    {
        if(operationType.equals(AvroOperationType.MERGE_HORIZANOTAL))
        {
            mergeHorizontal(type, inpaths, outpath);
        }
        else if(operationType.equals(AvroOperationType.MERGE_VERTICAL))
        {
            mergeVertical(type, inpaths, outpath);
        }
        else if(operationType.equals(AvroOperationType.CONVERT_FORMAT))
        {
            convert(type, inpaths, inFormat, outFormat, charset);
        }
        else if(operationType.equals(AvroOperationType.SPLIT_HORIZONTAL))
        {
            splitHorizontal(type, inpaths);
        }
        else if(operationType.equals(AvroOperationType.SPLIT_VERTICAL_2_WAY)
                || operationType.equals(AvroOperationType.SPLIT_VERTICAL_N_WAY))
        {
            splitVertical(type, operationType, inpaths, splitSize);            
        }        
    }
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 10000);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("inFormat", "if", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("outFormat", "of", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("type", "t", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("splitSize", "s", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("operationType", "ot", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        String charset = "UTF-8";
        List<String> infiles;
        String outfile = "";
        AvroType type = null;
        AvroOperationType operationType = null;
        float splitSize = 0.5F;
        
        FormatType.FeatureCollectionFormat inFormat = FormatType.FeatureCollectionFormat.AVRO_FORMAT;
        FormatType.FeatureCollectionFormat outFormat = FormatType.FeatureCollectionFormat.AVRO_FORMAT;

        if (!opt.check()) {
            System.out.println("AvroIO [-if inFormat] [-of outFormat] [-cs charset] -t type [-s splitSize] -ot operationType -o [outfile] infiles ...");
            System.out.println("\tTypes:");
            System.out.println("\t\t" + AvroType.NBEST_LIST);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_FEATURES);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_RECORD);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_ENTRY);
            System.out.println("\tOperation Types:");
            System.out.println("\t\t" + AvroOperationType.CONVERT_FORMAT);
            System.out.println("\t\t" + AvroOperationType.MERGE_HORIZANOTAL);
            System.out.println("\t\t" + AvroOperationType.MERGE_VERTICAL);
            System.out.println("\t\t" + AvroOperationType.SPLIT_HORIZONTAL);
            System.out.println("\t\t" + AvroOperationType.SPLIT_VERTICAL_2_WAY);
            System.out.println("\t\t" + AvroOperationType.SPLIT_VERTICAL_N_WAY);
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }

        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }

        if (opt.getSet().isSet("if")) {
            String fmt = opt.getSet().getOption("if").getResultValue(0);  
            inFormat = FormatType.FeatureCollectionFormat.valueOf(fmt);
            System.out.println("Input format: " + inFormat);
        }

        if (opt.getSet().isSet("of")) {
            String fmt = opt.getSet().getOption("of").getResultValue(0);  
            outFormat = FormatType.FeatureCollectionFormat.valueOf(fmt);
            System.out.println("Output format: " + outFormat);
            
            operationType = AvroOperationType.CONVERT_FORMAT;
        }

        if (opt.getSet().isSet("t")) {
            String typeString = opt.getSet().getOption("t").getResultValue(0);
            type = AvroType.valueOf(typeString);
            System.out.println("Avro type: " + type);
        }

        if (opt.getSet().isSet("ot")) {
            String typeString = opt.getSet().getOption("ot").getResultValue(0);
            operationType = AvroOperationType.valueOf(typeString);
            System.out.println("Operation type: " + type);
        }

        if (opt.getSet().isSet("s")) {
            String string = opt.getSet().getOption("s").getResultValue(0);
            splitSize = Float.parseFloat(string);
            System.out.println("Split size: " + splitSize);
        }

        infiles = opt.getSet().getData();

        System.out.println("Input files: " + infiles);

        try {

            AvroDatasetOperations.applyOperation(type, operationType, splitSize,
                    infiles, outfile, inFormat, outFormat, charset);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {
//        args = new String[] {"-o", "tmp/nblist-features.avro", "tmp/nblist-features.avro.Surface",
//                "tmp/nblist-features.avro.ModelScores"};
//        args = new String[] {"-t", "POST_EDITED_CORPUS_FEATURES", "-ot", "SPLIT_VERTICAL_N_WAY",
//                "-s", "0.2", "tmp/features.avro"};
//        args = new String[] {"-t", "POST_EDITED_CORPUS_FEATURES", "-ot", "CONVERT_FORMAT", "-if", "AVRO_FORMAT",
//                "-of", "ARFF_FORMAT", "/extra/work/wmt-13-quality-estimation/task1-1/features/feature-set-1.avro"};
                
        runWithCmdLineOptions(args);
    }
}
