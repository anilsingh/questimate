/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import edu.stanford.nlp.mt.base.FeatureValue;
import edu.stanford.nlp.mt.base.FeatureValueCollection;
import edu.stanford.nlp.mt.base.IString;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.avro.Schema;
import org.limsi.cm.resources.NBestList;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.mt.cm.TranslationHypothesis;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.AvroType;
import sanchay.corpus.ssf.SSFStory;

/**
 *
 * @author kaz
 */
public class AvroUtils {

    private static Map<String, Schema> schemas = new HashMap<String, Schema>();

    private AvroUtils() {
    }

    public static void addSchema(String name, Schema schema) {
        schemas.put(name, schema);

    }

    public static Schema getSchema(String name) {
        return schemas.get(name);

    }

    public static String resolveSchema(String sc) {

        String result = sc;
        for (Map.Entry<String, Schema> entry : schemas.entrySet()) {
            result = replace(result, entry.getKey(),
                    entry.getValue().toString());
        }
        return result;

    }

    static String replace(String str, String pattern, String replace) {

        int s = 0;
        int e = 0;
        StringBuffer result = new StringBuffer();
        while ((e = str.indexOf(pattern, s)) >= 0) {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e + pattern.length();

        }
        result.append(str.substring(s));
        return result.toString();

    }

    public static Schema parseSchema(String schemaString) {

        String completeSchema = resolveSchema(schemaString);
        Schema schema = Schema.parse(completeSchema);
        String name = schema.getFullName();
        schemas.put(name, schema);
        return schema;

    }

    public static Schema parseSchema(InputStream in) throws IOException {

        StringBuffer out = new StringBuffer();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1;) {
            out.append(new String(b, 0, n));
        }
        return parseSchema(out.toString());

    }

    public static Schema parseSchema(File file) throws IOException {

        FileInputStream fis = new FileInputStream(file);
        return parseSchema(fis);
    }
    
    public static NBestListAvro getAvroNBestList(int srcIndex, SSFStory srcCorpus,
            List<HypothesisFeatures> hypothesisFeatureList, NBestList nbestList, boolean reverse)
    {
        NBestListAvro nblist = new NBestListAvro();
        
//        nblist.setSrc(srcCorpus.getSentence(srcIndex).convertToRawText());
        nblist.src = srcCorpus.getSentence(srcIndex).convertToRawText().trim();
        
        List<Hypothesis> hypotheses = new ArrayList<Hypothesis>(hypothesisFeatureList.size());

        int count = hypothesisFeatureList.size();
        
        for (int i = 0; i < count; i++) {
            
            HypothesisFeatures hf = hypothesisFeatureList.get(i);
            
            Hypothesis hypothesis = new Hypothesis();

            TranslationHypothesis<IString, String> translation = nbestList.getNbestLists().get(srcIndex).get(i);
            
//            hypothesis.setTgt(translation.toString());
            hypothesis.tgt = translation.getTranslation().toString();
//            hypothesis.setRank(i);
            hypothesis.rank = i;
            
//            hypothesis.setFeatures(hf.getFeatureValuesMap());
            hypothesis.features = hf.getFeatureValuesMap();

            // Don't store emtpy hypotheses
//            if(reverse)
//            {
//                Object val = hf.getFeatureValues().getFeatureValue("tgtSenLength");
//
//                if(val != null && val instanceof Double && ((Double) val) == 0)
//                {
//                    continue;
//                }
//            }
            
            if(MiscellaneousUtils.emptySentence(translation.getTranslation().toString()))
            {
                continue;
            }

            hypotheses.add(hypothesis);
        }
        
//        nblist.setHypotheses(hypotheses);
        nblist.Hypotheses = hypotheses;
        
        return nblist;
    }
    
    public static Map<CharSequence, Float> getFeatureAvro(FeatureValueCollection<String> features)
    {
        Map<CharSequence, Float> featureAvro = new LinkedHashMap<CharSequence, Float>();
        
        Iterator<FeatureValue<String>> itr = features.iterator();
        
        while(itr.hasNext())
        {
            FeatureValue<String> fv = itr.next();
            
            featureAvro.put(fv.name, (float) fv.value);
        }
        
        return featureAvro;
    }    

    public static <T> T mergeAvroRecords(T[] avroRecords, AvroType type)
    {
        T mergedRecord = null;
        
        if(type.equals(AvroType.NBEST_LIST))
        {
            mergedRecord = mergeNBestLists(avroRecords);
        }
        else if(type.equals(AvroType.POST_EDITED_CORPUS_FEATURES) || type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
        {
            mergedRecord = mergePostEditedCorpusFeatures(avroRecords);
        }                
        else if(type.equals(AvroType.POST_EDITED_CORPUS_ENTRY))
        {
            mergedRecord = mergePostEditedCorpusEntries(avroRecords);
        }                
        
        return mergedRecord;
    }

    public static <T> T mergeNBestLists(T[] avroRecords)
    {
        NBestListAvro avroObjOut = new NBestListAvro();
        
        List<Hypothesis> hypothesesOut = new ArrayList<Hypothesis>();

        NBestListAvro record = (NBestListAvro) avroRecords[0];
//        nblistOut.setSrc(nblist[0].getSrc());
        avroObjOut.src = record.src;

        for (int i = 0; i < avroRecords.length; i++) {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecords[i];

//            List<Hypothesis> hypotheses = nBestListAvro.getHypotheses();
            List<Hypothesis> hypotheses = nBestListAvro.Hypotheses;
            
            hypothesesOut = mergetHypothesisLists(hypothesesOut, hypotheses);
        }
        
//        nblistOut.setHypotheses(hypothesesOut);
        avroObjOut.Hypotheses = hypothesesOut;
        
        return (T) avroObjOut;
    }

    public static <T> T mergePostEditedCorpusFeatures(T[] avroRecords)
    {
        PostEditedCorpusRecord avroObjOutRecord = getPERecordInstanceWithFeatures();
        PostEditedCorpusFeatures avroObjOutFeatures = avroObjOutRecord.getCorpusFeatures();

        PostEditedCorpusRecord record = null;
        PostEditedCorpusFeatures features;
        
        if(avroRecords[0] instanceof PostEditedCorpusRecord)
        {
            record = (PostEditedCorpusRecord) avroRecords[0];

            features = record.getCorpusFeatures();
        }
        else
        { 
            features = (PostEditedCorpusFeatures) avroRecords[0];           
        }

        avroObjOutFeatures.setSrc(features.getSrc());
        avroObjOutFeatures.setSrcFte(features.getSrcFte());
        avroObjOutFeatures.setTgt(features.getTgt());
        avroObjOutFeatures.setTgtFte(features.getTgtFte());
        avroObjOutFeatures.setPeTgt(features.getPeTgt());
        avroObjOutFeatures.setPeTgtFte(features.getPeTgtFte());
        avroObjOutFeatures.setFeatures(features.getFeatures());

        if(avroRecords[0] instanceof PostEditedCorpusRecord)
        {
            avroObjOutRecord.setLabels(record.getLabels());
        }

        for (int i = 1; i < avroRecords.length; i++) {
            if(avroRecords[0] instanceof PostEditedCorpusRecord)
            {
                record = (PostEditedCorpusRecord) avroRecords[i];
                
                features = record.getCorpusFeatures();
            }
            else
            {
                features = (PostEditedCorpusFeatures) avroRecords[i];
            }

            avroObjOutFeatures.getFeatures().putAll(features.getFeatures());

            if(avroRecords[0] instanceof PostEditedCorpusRecord)
            {
                avroObjOutRecord.getLabels().putAll(record.getLabels());
            }
        }

        if(avroRecords[0] instanceof PostEditedCorpusFeatures)
        {
            return (T) avroObjOutFeatures;
        }
        
        return (T) avroObjOutRecord;
    }

    public static <T> T mergePostEditedCorpusEntries(T[] avroRecords)
    {
        PostEditedCorpusEntry avroObjOutRecord = new PostEditedCorpusEntry();

        PostEditedCorpusEntry record = (PostEditedCorpusEntry) avroRecords[0];

        avroObjOutRecord.setSrc(record.getSrc());
        avroObjOutRecord.setSrcFte(record.getSrcFte());
        avroObjOutRecord.setTgt(record.getTgt());
        avroObjOutRecord.setTgtFte(record.getTgtFte());
        avroObjOutRecord.setPeTgt(record.getPeTgt());
        avroObjOutRecord.setPeTgtFte(record.getPeTgtFte());
        
        avroObjOutRecord.setFeatures(record.getFeatures());
        avroObjOutRecord.setLabels(record.getLabels());

        for (int i = 1; i < avroRecords.length; i++) {

            record = (PostEditedCorpusEntry) avroRecords[i];

            avroObjOutRecord.getFeatures().putAll(record.getFeatures());

            avroObjOutRecord.getLabels().putAll(record.getLabels());
        }
        
        return (T) avroObjOutRecord;
    }
    
    public static NBestListAvro mergeNBestLists(List<NBestListAvro> nblist)
    {
        NBestListAvro nblistOut = new NBestListAvro();
        
        List<Hypothesis> hypothesesOut = new ArrayList<Hypothesis>();

//        nblistOut.setSrc(nblist.get(0).getSrc());
        nblistOut.src = nblist.get(0).src;

        for (int i = 0; i < nblist.size(); i++) {
            NBestListAvro nBestListAvro = nblist.get(i);

//            List<Hypothesis> hypotheses = nBestListAvro.getHypotheses();
            List<Hypothesis> hypotheses = nBestListAvro.Hypotheses;
            
            hypothesesOut = mergetHypothesisLists(hypothesesOut, hypotheses);
        }
        
//        nblistOut.setHypotheses(hypothesesOut);
        nblistOut.Hypotheses = hypothesesOut;
        
        return nblistOut;
    }
    
    public static List<Hypothesis> mergetHypothesisLists(List<Hypothesis> hypList1, List<Hypothesis> hypList2)
    {
        List<Hypothesis> hypList = new ArrayList<Hypothesis>(hypList1.size());

        if(hypList1.isEmpty())
        {
            for (int i = 0; i < hypList2.size(); i++){
                hypList1.add(new Hypothesis());
            }            
        }
        else if(hypList2.isEmpty())
        {
            for (int i = 0; i < hypList1.size(); i++){
                hypList2.add(new Hypothesis());
            }            
        }

        if(hypList1.size() != hypList2.size())
        {
            System.err.println("Hypotheses are not of the same size: "
                    + hypList1.size() + " and " + hypList2.size());
        }

        for (int i = 0; i < hypList1.size(); i++){
            Hypothesis hyp1 = hypList1.get(i);
            Hypothesis hyp2 = hypList2.get(i);
            
            Hypothesis hyp = mergeHypotheses(hyp1, hyp2);
            
            hypList.add(hyp);
        }
        
        return hypList;        
    }

    public static Hypothesis mergeHypotheses(Hypothesis hyp1, Hypothesis hyp2)
    {
        Hypothesis hyp = new Hypothesis();
        
//        if(hyp1 == null || hyp1.getRank() == null || hyp1.getFeatures() == null
//                || hyp1.getFeatures().isEmpty())
        if(hyp1 == null || hyp1.features == null
                || hyp1.features.isEmpty())
        {
//            hyp.setRank(hyp2.getRank());
            hyp.rank = hyp2.rank;
//            hyp.setTgt(hyp2.getTgt());
            hyp.tgt = hyp2.tgt;
            
//            if(hyp2.getTgt() == null)
            if(hyp2.tgt== null)
            {
                int stop = 1;
            }
        }
//        else if(hyp2 == null|| hyp2.getRank() == null || hyp2.getFeatures() == null
//                || hyp2.getFeatures().isEmpty())
        else if(hyp2 == null|| hyp2.features == null
                || hyp2.features.isEmpty())
        {
//            hyp.setRank(hyp1.getRank());
            hyp.rank = hyp1.rank;
//            hyp.setTgt(hyp1.getTgt());
            hyp.tgt = hyp1.tgt;
            
//            if(hyp1.getTgt() == null)
            if(hyp1.tgt == null)
            {
                int stop = 1;
            }
        }
        else
        {
//            hyp.setRank(hyp2.getRank());
            hyp.rank = hyp2.rank;
//            hyp.setTgt(hyp2.getTgt());            
            hyp.tgt = hyp2.tgt;            
        }

        if(hyp1 != null)
        {
//            hyp.setFeatures(hyp1.getFeatures());
            hyp.features = hyp1.features;
        }
        
        if(hyp2 != null)
        {
//            if(hyp.getFeatures() != null)
            if(hyp.features != null)
            {
//                hyp.getFeatures().putAll(hyp2.getFeatures());
                hyp.features.putAll(hyp2.features);
            }
            else
            {
//                hyp.setFeatures(hyp2.getFeatures());            
                hyp.features = hyp2.features;            
            }
        }
        
//        if(hyp.getTgt() == null)
        if(hyp.tgt == null)
        {
            int stop = 1;
        }
        
        return hyp;
    }
    
    public static PostEditedCorpusRecord getPERecordInstanceWithFeatures()
    {
        PostEditedCorpusRecord postEditedCorpusRecord = new PostEditedCorpusRecord();
        
        PostEditedCorpusFeatures postEditedCorpusFeatures = new PostEditedCorpusFeatures();
        
        postEditedCorpusRecord.setCorpusFeatures(postEditedCorpusFeatures);
        
        return postEditedCorpusRecord;
    }
}
