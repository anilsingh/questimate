/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.workflow.FeatureExtractionFlow;
import org.limsi.types.AvroSearchOperationType;
import org.limsi.types.AvroType;
import sanchay.util.Pair;

/**
 *
 * @author anil
 */
public class AvroSearch {
    
    public static <T> void searchRecords(List<String> inpaths, int beg, int end,
            AvroFeatureQuery query) throws IOException
    {
        if(query.avroSearchOperation != null 
                && query.avroSearchOperation.equals(AvroSearchOperationType.COUNT_RECORDS))
        {
            countRecords(inpaths, query);
            
            return;
        }
        
        List<DataFileReader<T>> dataFileReaders = new ArrayList<DataFileReader<T>>();
        
        int inFileCount = inpaths.size();

        T[] avroPtrs = (T[]) AvroType.createArray(query.avroType, inFileCount);

        File schemaFile = new File(query.avroType.getSchemaPath());

        Schema schema = AvroUtils.parseSchema(schemaFile);

        for (int i = 0; i < inFileCount; i++) {
            String string = inpaths.get(i);
        
            SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(query.avroType.getSchemaClass());
//            DatumReader<NBestListAvro> userDatumReader = new SpecificDatumReader<NBestListAvro>(nbestSchema);
            DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(string), userDatumReader);
            
            dataFileReaders.add(dataFileReader);
        }

        int r = 1;
        
        if(end < 0 || (end == beg && query.isActive()))
        {
            end = Integer.MAX_VALUE;
        }
        else if(end < beg)
        {
            end = beg;
        }

        while (dataFileReaders.get(0).hasNext()) {

            if(r > end)
            {
                break;
            }
                
            System.out.printf("Processing record: %d\r", r);

            for (int i = 0; i < inFileCount; i++) {
                
                avroPtrs[i] = dataFileReaders.get(i).next(avroPtrs[i]);
                
                if(r >= beg)
                {                    
                    String peek = peekARecord(avroPtrs[i], query.avroType);
                    
                    if(query.isActive())
                    {
                        peek = matchRecord(avroPtrs[i], query);
                        
                        if(peek != null)
                        {
                            printResult(inpaths.get(i), r, peek, System.out);
                        }
                    }
                    else
                    {
                        printResult(inpaths.get(i), r, peek, System.out);                        
                    }
                }                
            }
            
            r++;
        }         
    }

    public static <T> int[] countRecords(List<String> inpaths, AvroFeatureQuery query) throws IOException
    {
        int[] counts = new int[inpaths.size()];
        
        int inFileCount = inpaths.size();
        
        for (int i = 0; i < inFileCount; i++) {
            String string = inpaths.get(i);
            
            countRecords(string, query);
        }
        
        return counts;
    }

    public static <T> int countRecords(String inpath, AvroFeatureQuery query) throws IOException
    {
        System.out.println("Counting records for file: " + inpath);
        
        T avroPtr = (T) AvroType.createInstance(query.avroType);

        SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(query.avroType.getSchemaClass());
        DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(inpath), userDatumReader);

        int r = 0;
        while (dataFileReader.hasNext()) {

            System.out.printf("Processing record %d in file %s\r ", ++r, inpath);

            avroPtr = dataFileReader.next(avroPtr);
        }

        dataFileReader.close();

        System.out.printf("\nFound %d records in file %s\n ", r, inpath);
        
        return r;
    }
    
    public static <T> String matchRecord(T avroRecord, AvroFeatureQuery query)
    {
        String matchedValue = null;
        
        if(query.avroType == AvroType.NBEST_LIST)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;

            List<Hypothesis> hypotheses = nBestListAvro.getHypotheses();
            
            for (Hypothesis hypothesis : hypotheses) {

                if(query.avroTextQuery != null)
                {
                    Pair<String, Float> matchedEntry = matchText(hypothesis.getTgt().toString(), query);

                    if(matchedEntry != null)
                    {
                        matchedValue = matchedEntry.toString();
                    }                    
                }
                else
                {                                    
                    Map<CharSequence, Float> featuresAvro = hypothesis.getFeatures();

                    Map<String, Float> featuresAvroString = FeatureValueUtils.getStringMap(featuresAvro);
                    
                    Pair<String, Float> matchedEntry = matchFeatureMap(featuresAvroString, query);

                    if(matchedEntry != null)
                    {
                        matchedValue = matchedEntry.toString();
                    }
                }
            }
        }
        else if(query.avroType == AvroType.POST_EDITED_CORPUS_FEATURES || query.avroType == AvroType.POST_EDITED_CORPUS_RECORD)
        {
            PostEditedCorpusRecord postEditedCorpusRecord = null;
            PostEditedCorpusFeatures postEditedCorpusFeatures;
            
            if(query.avroType.equals(AvroType.POST_EDITED_CORPUS_RECORD))
            {
                postEditedCorpusRecord = (PostEditedCorpusRecord) avroRecord;

                postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();
            }
            else
            {
                postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroRecord;
            }                
            
            if(query.avroTextQuery != null)
            {
                Pair<String, Float> matchedEntry = matchText(postEditedCorpusFeatures.getTgtFte().toString(), query);

                if(matchedEntry != null)
                {
                    matchedValue = matchedEntry.toString();
                }
            }
            else
            {                                    
                Map<CharSequence, Float> featuresAvro = postEditedCorpusFeatures.getFeatures();

                Map<String, Float> featuresAvroString = FeatureValueUtils.getStringMap(featuresAvro);

                Pair<String, Float> matchedEntry = matchFeatureMap(featuresAvroString, query);

                if(matchedEntry != null)
                {
                    matchedValue = matchedEntry.toString();
                }
                else if(query.avroType.equals(AvroType.POST_EDITED_CORPUS_RECORD))
                {
                    featuresAvro = postEditedCorpusRecord.getLabels();
                    
                    if(featuresAvro != null)
                    {
                        featuresAvroString = FeatureValueUtils.getStringMap(featuresAvro);

                        matchedEntry = matchFeatureMap(featuresAvroString, query);

                        if(matchedEntry != null)
                        {
                            matchedValue = matchedEntry.toString();
                        }                    
                    }
                }
            }
        }        
        else if(query.avroType == AvroType.POST_EDITED_CORPUS_ENTRY)
        {
            PostEditedCorpusEntry postEditedCorpusEntry = (PostEditedCorpusEntry) avroRecord;
            
            if(query.avroTextQuery != null)
            {
                Pair<String, Float> matchedEntry = matchText(postEditedCorpusEntry.getTgtFte().toString(), query);

                if(matchedEntry != null)
                {
                    matchedValue = matchedEntry.toString();
                }
            }
            else
            {                                    
                Map<CharSequence, Float> featuresAvro = postEditedCorpusEntry.getFeatures();

                Map<String, Float> featuresAvroString = FeatureValueUtils.getStringMap(featuresAvro);

                Pair<String, Float> matchedEntry = matchFeatureMap(featuresAvroString, query);

                if(matchedEntry != null)
                {
                    matchedValue = matchedEntry.toString();
                }
                else if(postEditedCorpusEntry.getLabels() != null)
                {
                    featuresAvro = postEditedCorpusEntry.getLabels();
                    
                    if(featuresAvro != null)
                    {
                        featuresAvroString = FeatureValueUtils.getStringMap(featuresAvro);

                        matchedEntry = matchFeatureMap(featuresAvroString, query);

                        if(matchedEntry != null)
                        {
                            matchedValue = matchedEntry.toString();
                        }                    
                    }
                }
            }
        }        
        
        return matchedValue;
    }

    public static Pair matchEntry(AvroFeatureQuery query, String key, Float entryValue)
    {
        Pair matchedEntry = null;
        
        if(entryValue != null)
        {
            if(query.value != null)
            {
                Float qvalue = null;
                
                if(!query.value.equalsIgnoreCase("nan"))
                {
                    qvalue = Float.parseFloat(query.value);
                }

                if(qvalue != null)
                {
                    if(entryValue.floatValue() == qvalue.floatValue())
                    {                
                        matchedEntry = createEntry(key, entryValue);
                    }
                    else if(entryValue.isNaN() && qvalue.isNaN())
                    {                
                        matchedEntry = createEntry(key, entryValue);
                    }
                    else if(entryValue.isInfinite() && qvalue.isInfinite())
                    {                
                        matchedEntry = createEntry(query.key, entryValue);
                    }
                }
                else if(entryValue.isNaN() && query.value.equalsIgnoreCase("nan"))
                {                
                    matchedEntry = createEntry(key, entryValue);
                }
                else if(entryValue.isInfinite()
                        && (query.value.equalsIgnoreCase("nan")
                            || (query.value.equalsIgnoreCase("infinity"))
                            || (query.value.equalsIgnoreCase("-infinity"))))
                {                
                    matchedEntry = createEntry(key, entryValue);
                }
            }
            else if(query.maxValue == null && query.minValue != null)
            {
                if(entryValue >= query.minValue)
                {
                    matchedEntry = createEntry(key, entryValue);
                }
            }
            else if(query.minValue == null && query.maxValue != null)
            {
                if(entryValue <= query.maxValue)
                {
                    matchedEntry = createEntry(key, entryValue);
                }
            }
            else if(query.minValue != null && query.maxValue != null)
            {
                if(entryValue >= query.minValue && entryValue <= query.maxValue)
                {
                    matchedEntry = createEntry(key, entryValue);
                }
            }
        }       
        
        return matchedEntry;
    }

    public static Pair matchText(String text, AvroFeatureQuery query)
    {
        Pair matchedEntry = null;
        
        if(query.avroTextQuery != null)
        {
            if(query.avroSearchOperation != null
                    && query.avroSearchOperation.equals(AvroSearchOperationType.CHECK_LENGTH))
            {
                Integer length;

                if(query.avroTextQuery.charSearch)
                {
                    length = text.length();
                }
                else
                {
                    String words[] = text.split("[\\s+]");

                    length = words.length;
                }
            
                if(matchLength(length, query))
                {
                    matchedEntry = createEntry(length, text);
                }
            }
            else if(query.avroTextQuery.text != null)
            {
                if(text.contains(query.avroTextQuery.text))
                {
                    matchedEntry = createEntry(query.avroTextQuery.text, text);
                }
            }
        }

        return matchedEntry;
    }

    public static boolean matchLength(int length, AvroFeatureQuery query)
    {
        boolean matched = false;
        
        if(query.maxValue == null && query.minValue != null)
        {
            if(length >= query.minValue)
            {
                return true;
            }
        }
        else if(query.maxValue == null && query.minValue != null)
        {
            if(length >= query.minValue)
            {
                return true;
            }
        }
        else if(query.minValue == null && query.maxValue != null)
        {
            if(length <= query.maxValue)
            {
                return true;
            }
        }
        else if(query.minValue != null && query.maxValue != null)
        {
            if(length >= query.minValue && length <= query.maxValue)
            {
                return true;
            }
        }
        else if(query.value != null)
        {
            if(length == Integer.parseInt(query.value))
            {
                return true;
            }
        }
        
        return matched;
    }

    public static <T> Pair<String, Float> matchFeatureMap(Map<String, Float> featuresAvro,
            AvroFeatureQuery query)
    {
        Pair<String, Float> matchedEntry = null;

        if(query.value != null && (query.key.equals("*") || query.key.equalsIgnoreCase("_ANY_")))
        {
            Iterator<String> itr = featuresAvro.keySet().iterator();
            
            while(itr.hasNext())
            {
                String key = itr.next();
                
                Float avalue = featuresAvro.get(key);
                
                matchedEntry = matchEntry(query, key, avalue);
                
                if(matchedEntry != null)
                {
                    return matchedEntry;
                }
            }
        }
        else if(query.value != null && (query.value.equals("*") || query.value.equalsIgnoreCase("_ANY_")))
        {
            if(featuresAvro.containsKey(query.key))
            {
                matchedEntry = createEntry(query.key, featuresAvro.get(query.key));                
            }
        }
        else
        {
            Float avalue = featuresAvro.get(query.key);
            
            matchedEntry = matchEntry(query, query.key, avalue);
        }
        
        return matchedEntry;
    }
    
    public static <K, V> Pair<K, V> createEntry(K key, V value)
    {
        Pair<K, V> entry = new Pair<K, V>();

        entry.first = key;
        entry.second = value;        
        
        return entry;
    }
    
    public static <T> String peekARecord(T avroRecord, AvroType type)
    {
        String peek = "";
        
        if(type == AvroType.NBEST_LIST)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;
            peek = nBestListAvro.getSrc() + "\n" + nBestListAvro.getHypotheses().get(0).toString();
        }
        else if(type.equals(AvroType.POST_EDITED_CORPUS_FEATURES) || type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
        {
            PostEditedCorpusRecord postEditedCorpusRecord = null;
            PostEditedCorpusFeatures postEditedCorpusFeatures;

            if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
            {
                postEditedCorpusRecord = (PostEditedCorpusRecord) avroRecord;

                postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();
            }
            else
            {
                postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroRecord;
            }                

            if(postEditedCorpusFeatures.getSrc() != null)
            {
                peek += postEditedCorpusFeatures.getSrc() + "\n";
            }

            if(postEditedCorpusFeatures.getTgt() != null)
            {
                peek += postEditedCorpusFeatures.getTgt() + "\n";
            }
 
            if(postEditedCorpusFeatures.getPeTgt() != null)
            {
                peek += postEditedCorpusFeatures.getPeTgt() + "\n";
            }
 
            if(postEditedCorpusFeatures.getSrcFte() != null)
            {
                peek += postEditedCorpusFeatures.getSrcFte() + "\n";
            }
 
            if(postEditedCorpusFeatures.getTgtFte() != null)
            {
                peek += postEditedCorpusFeatures.getTgtFte() + "\n";
            }
 
            if(postEditedCorpusFeatures.getPeTgtFte()!= null)
            {
                peek += postEditedCorpusFeatures.getPeTgtFte() + "\n";
            }
 
            peek += postEditedCorpusFeatures.getFeatures();

            if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD) && postEditedCorpusRecord.getLabels() != null)
            {
                peek += "\n" + postEditedCorpusRecord.getLabels();
            }
       }        
       else if(type.equals(AvroType.POST_EDITED_CORPUS_ENTRY))
       {
            PostEditedCorpusEntry postEditedCorpusEntry = (PostEditedCorpusEntry) avroRecord;

            if(postEditedCorpusEntry.getSrc() != null)
            {
                peek += postEditedCorpusEntry.getSrc() + "\n";
            }

            if(postEditedCorpusEntry.getTgt() != null)
            {
                peek += postEditedCorpusEntry.getTgt() + "\n";
            }
 
            if(postEditedCorpusEntry.getPeTgt() != null)
            {
                peek += postEditedCorpusEntry.getPeTgt() + "\n";
            }
 
            if(postEditedCorpusEntry.getSrcFte() != null)
            {
                peek += postEditedCorpusEntry.getSrcFte() + "\n";
            }
 
            if(postEditedCorpusEntry.getTgtFte() != null)
            {
                peek += postEditedCorpusEntry.getTgtFte() + "\n";
            }
 
            if(postEditedCorpusEntry.getPeTgtFte()!= null)
            {
                peek += postEditedCorpusEntry.getPeTgtFte() + "\n";
            }
 
            peek += postEditedCorpusEntry.getFeatures();

            if(postEditedCorpusEntry.getLabels() != null)
            {
                peek += "\n" + postEditedCorpusEntry.getLabels();
            }
       }        
        
        return peek;
    }    
    
    public static void printResult(String filePath, int recordNum, String peek, PrintStream ps)
    {
        ps.println("File " + filePath + ", record " + recordNum + ":");
        ps.println("-------------------------------------------------");
        ps.println(peek);
        ps.println("-------------------------------------------------");
    }
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 10000);

        opt.getSet().addBooleanOption("countRecored", "c", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("charSearch", "cs", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addBooleanOption("checkLength", "cl", Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("textSearch", "ts", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("from", "b", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("to", "e", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("key", "k", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("value", "v", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("low", "l", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("high", "h", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("type", "t", Options.Separator.BLANK, Options.Multiplicity.ONCE);

        List<String> infiles;
        boolean countRecords;
        boolean charSearch = false;
        boolean checkLength = false;
        int beg = 1;
        int end = 1;
        String key = null;
        String value = null;
        Float minValue = null;
        Float maxValue = null;
        AvroType type = null;
        AvroSearchOperationType searchOperationType = null;
        AvroTextQuery avroTextQuery = null;

        if (!opt.check()) {
            System.out.println("AvroSearch -t type [-c <?countRecords>] [-cs <?charSearch>] [-cl <?checkLength>] [-k key -v value [-min minvalue -max maxvalue]]");
            System.out.println("\t\t[-ts textSearch] [-b beginRecord] [-e endRecord] avrofiles ...");
            System.out.println("\tTypes:");
            System.out.println("\t\t" + AvroType.NBEST_LIST);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_FEATURES);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_RECORD);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_ENTRY);
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("c")) {
            countRecords = true;
            searchOperationType = AvroSearchOperationType.COUNT_RECORDS;
            System.out.println("Count records: " + countRecords);
        }

        if (opt.getSet().isSet("t")) {
            String typeString = opt.getSet().getOption("t").getResultValue(0);
            type = AvroType.valueOf(typeString);
            System.out.println("Avro type: " + type);
        }

        if (opt.getSet().isSet("b")) {
            String vString = opt.getSet().getOption("b").getResultValue(0);
            beg = Integer.parseInt(vString);
            System.out.println("Beginning at record: " + beg);
        }

        if (opt.getSet().isSet("e")) {
            String vString = opt.getSet().getOption("e").getResultValue(0);
            end = Integer.parseInt(vString);
            System.out.println("Ending at record: " + end);
        }

        if (opt.getSet().isSet("k")) {
            key = opt.getSet().getOption("k").getResultValue(0);
            System.out.println("Search key: " + key);
            searchOperationType = AvroSearchOperationType.SEARCH;
        }

        if (opt.getSet().isSet("v")) {
            value = opt.getSet().getOption("v").getResultValue(0);
            System.out.println("Search value: " + value);
        }

        if (opt.getSet().isSet("l")) {
            String qString = opt.getSet().getOption("l").getResultValue(0);
            minValue = Float.parseFloat(qString);
            System.out.println("Minimum value: " + qString);
        }

        if (opt.getSet().isSet("h")) {
            String qString = opt.getSet().getOption("h").getResultValue(0);
            maxValue = Float.parseFloat(qString);
            System.out.println("Maximum value: " + qString);
        }

        if (opt.getSet().isSet("cl")) {
            checkLength = true;
            searchOperationType = AvroSearchOperationType.CHECK_LENGTH;
            System.out.println("Check length: " + checkLength);
        }

        if (opt.getSet().isSet("ts")) {
            String qString = opt.getSet().getOption("ts").getResultValue(0);
            System.out.println("Text search: " + qString);

            if (opt.getSet().isSet("cs")) {
                charSearch = true;
                System.out.println("Char search: " + charSearch);
            }
            
            avroTextQuery = AvroTextQuery.createQuery(qString, charSearch);
        }

        infiles = opt.getSet().getData();
    
        System.out.println("Input files: " + infiles);

        try {
            AvroFeatureQuery avroQuery = AvroFeatureQuery.createQuery(key, value, minValue,
                    maxValue, type, searchOperationType, avroTextQuery);

            AvroSearch.searchRecords(infiles, beg, end, avroQuery);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {                
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-b", "2", "-e", "4", "tmp/features.avro"};
//        args = new String[]{"-t", "NBEST_LIST", "-b", "2", "-e", "2", "tmp/nbest-features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-k", "srcSenLength",
//            "-l", "2", "-h", "40", "tmp/features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-k", "srcSenLength",
//            "-v", "23.0", "tmp/features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-k", "srcSenLength",
//            "-v", "nan", "tmp/features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-k", "_ANY_",
//            "-v", "23.0", "tmp/features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-k", "tgtSenLength",
//            "-v", "_ANY_", "tmp/features.avro"};
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-ts", "a", "-cl",
//            "-v", "28", "/more/work/trace-qe/features/feature-set-1/en-fr-first-data-features.avro"};
//        args = new String[]{"-t", "NBEST_LIST", "-ts", "negative", 
//            "tmp/nbest-features.avro"};
        
        runWithCmdLineOptions(args);
    }    
    
}
