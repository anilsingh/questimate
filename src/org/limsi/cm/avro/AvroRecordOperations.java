/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ml.options.Options;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.limsi.cm.resources.NBestList;
import org.limsi.cm.util.FeatureValueUtils;
import org.limsi.cm.util.MiscellaneousUtils;
import org.limsi.cm.workflow.FeatureExtractionFlow;
import org.limsi.mt.cm.Formats;
import org.limsi.mt.cm.features.FeatureValues;
import org.limsi.mt.cm.features.sentence.HypothesisFeatures;
import org.limsi.types.AvroOperationType;
import org.limsi.types.AvroType;

/**
 *
 * @author anil
 */
public class AvroRecordOperations {

    protected NBestList nbestList;

    public void applyOperation(List<String> inpaths, String outpath,
            String nbestListPath, String cs, AvroType type, AvroOperationType operationType) throws FileNotFoundException, IOException, Exception
    {        
        if(nbestListPath != null && MiscellaneousUtils.fileExists(nbestListPath))
        {
            nbestList = new NBestList(false, Formats.MOSES_NBEST_FORMAT, nbestListPath, cs);
        }
        
        applyOperation(inpaths, outpath, nbestList, type, operationType);
    }
    
    private static <T> void trimStrings(int srcIndex, T avroRecord,
            AvroType type)
    {
        if(avroRecord instanceof NBestListAvro)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;
            
            String src = "" + nBestListAvro.getSrc();

            src = src.trim();

            nBestListAvro.setSrc(src);

            List<Hypothesis> avroHyps = nBestListAvro.getHypotheses();

            int hcount = avroHyps.size();

            if(hcount != avroHyps.size())
            {
                System.err.println("Number of hypotheses is not the same in the nbest list and the avro file: "
                        + hcount + " and " + avroHyps.size());
            }

            for (int j = 0; j < hcount; j++) {  
                Hypothesis avroHyp = avroHyps.get(j);

                avroHyp.setTgt(avroHyp.getTgt().toString().trim());
            }
        }
    }
    
    private static <T> void removeEmptyHypothesis(int srcIndex, T avroRecord,
            AvroType type)
    {
        if(avroRecord instanceof NBestListAvro)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;

            List<Hypothesis> avroHyps = nBestListAvro.getHypotheses();

            int hcount = avroHyps.size();

            for (int j = 0; j < hcount; j++) {  

                Hypothesis avroHyp = avroHyps.get(j);

                Map<CharSequence, Float> featureValueMap = avroHyp.getFeatures();
                
                FeatureValues featureValues = FeatureValueUtils.getFeatureValues(featureValueMap);
                
                Object val = featureValues.getFeatureValue("tgtSenLength");
                
                if(val != null && val instanceof Float && ((Float) val) == 0)
                {
                    avroHyps.remove(j--);
                    hcount--;
                }
            }
        }
    }
    
    private static <T> void removeNaN(int srcIndex, T avroRecord, AvroType type)
    {
        if(avroRecord instanceof NBestListAvro)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;

            List<Hypothesis> avroHyps = nBestListAvro.getHypotheses();
        
            int hcount = avroHyps.size();

            for (int j = 0; j < hcount; j++) {  
                
                Hypothesis avroHyp = avroHyps.get(j);

                Map<CharSequence, Float> featureValues = avroHyp.getFeatures();

                FeatureValueUtils.fixNaNValues(featureValues);
            }
        }
        else if(avroRecord instanceof PostEditedCorpusFeatures)
        {
            PostEditedCorpusFeatures postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroRecord;

            Map<CharSequence, Float> featureValues = postEditedCorpusFeatures.getFeatures();

            FeatureValueUtils.fixNaNValues(featureValues);
        }
        else if(avroRecord instanceof PostEditedCorpusRecord)
        {
            PostEditedCorpusRecord postEditedCorpusRecord = (PostEditedCorpusRecord) avroRecord;

            PostEditedCorpusFeatures postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();

            Map<CharSequence, Float> featureValues = postEditedCorpusFeatures.getFeatures();

            FeatureValueUtils.fixNaNValues(featureValues);

            Map<CharSequence, Float> labelValues = postEditedCorpusRecord.getLabels();

            FeatureValueUtils.fixNaNValues(labelValues);
        }
    }
    
    private static <T> void addRatioFeatures(int srcIndex, T avroRecord, AvroType type)
    {
        if(avroRecord instanceof NBestListAvro)
        {
            NBestListAvro nBestListAvro = (NBestListAvro) avroRecord;

            List<Hypothesis> avroHyps = nBestListAvro.getHypotheses();
        
            int hcount = avroHyps.size();

            for (int j = 0; j < hcount; j++) {  

                Hypothesis avroHyp = avroHyps.get(j);

                Map<CharSequence, Float> featureValues = avroHyp.getFeatures();

                featureValues = HypothesisFeatures.addRatioFeatures(featureValues);

                avroHyp.setFeatures(featureValues);
            }
        }
        else if(avroRecord instanceof PostEditedCorpusFeatures)
        {
            PostEditedCorpusFeatures postEditedCorpusFeatures = (PostEditedCorpusFeatures) avroRecord;

            Map<CharSequence, Float> featureValues = postEditedCorpusFeatures.getFeatures();

            featureValues = HypothesisFeatures.addRatioFeatures(featureValues);
            
            postEditedCorpusFeatures.setFeatures(featureValues);
        }
        else if(avroRecord instanceof PostEditedCorpusRecord)
        {
            PostEditedCorpusRecord postEditedCorpusRecord = (PostEditedCorpusRecord) avroRecord;
            PostEditedCorpusFeatures postEditedCorpusFeatures = postEditedCorpusRecord.getCorpusFeatures();

            Map<CharSequence, Float> featureValues = postEditedCorpusFeatures.getFeatures();

            featureValues = HypothesisFeatures.addRatioFeatures(featureValues);
            
            postEditedCorpusFeatures.setFeatures(featureValues);
        }
        else if(avroRecord instanceof PostEditedCorpusEntry)
        {
            PostEditedCorpusEntry postEditedCorpusRecord = (PostEditedCorpusEntry) avroRecord;

            Map<CharSequence, Float> featureValues = postEditedCorpusRecord.getFeatures();

            featureValues = HypothesisFeatures.addRatioFeatures(featureValues);
            
            postEditedCorpusRecord.setFeatures(featureValues);
        }
    }
    
    public static <T> void applyOperation(List<String> inpaths, String outpath,
            NBestList nbestList, AvroType type, AvroOperationType operationType) throws IOException
    {
        System.out.println("Operating on AVRO files and writing to: " + outpath);

        List<DataFileReader<T>> dataFileReaders = new ArrayList<DataFileReader<T>>();

        DatumWriter<T> userDatumWriter;
        List<DataFileWriter<T>> dataFileWriters = new ArrayList<DataFileWriter<T>>();
        
        int inFileCount = inpaths.size();
        
        T[] avroPtrs = (T[]) AvroType.createArray(type, inFileCount);

        File schemaFile = new File(type.getSchemaPath());

        Schema schema = AvroUtils.parseSchema(schemaFile);

        for (int i = 0; i < inFileCount; i++) {
            String string = inpaths.get(i);
        
            SpecificDatumReader<T> userDatumReader = new SpecificDatumReader<T>(type.getSchemaClass());
//            DatumReader<NBestListAvro> userDatumReader = new SpecificDatumReader<NBestListAvro>(nbestSchema);
            DataFileReader<T> dataFileReader = new DataFileReader<T>(new File(string), userDatumReader);
            
            userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());
            DataFileWriter<T> dataFileWriter = new DataFileWriter<T>(userDatumWriter);

            dataFileWriter.setCodec(type.getCodecFactory());

            dataFileWriter.create(schema, new File(string + ".fixed"));
            
            dataFileReaders.add(dataFileReader);
            dataFileWriters.add(dataFileWriter);
        }
                
        File avroFile = new File(outpath);

        userDatumWriter = new SpecificDatumWriter<T>(type.getSchemaClass());
        DataFileWriter<T> dataFileWriter = new DataFileWriter<T>(userDatumWriter);

        dataFileWriter.setCodec(type.getCodecFactory());
        
        dataFileWriter.create(schema, avroFile);
        
        int r = 0;
        while (dataFileReaders.get(0).hasNext()) {
                
            System.out.printf("Processing record: %d\r", ++r);

            for (int i = 0; i < inFileCount; i++) {

                avroPtrs[i] = dataFileReaders.get(i).next(avroPtrs[i]);
                
                if(operationType.equals(AvroOperationType.FIX_NAN))
                {   
                    removeNaN(r-1, avroPtrs[i], type);
                }
                else if(operationType.equals(AvroOperationType.REMOVE_EMPTY_HYPOTHESIS))
                {   
                    removeEmptyHypothesis(r-1, avroPtrs[i], type);
                }
                else if(operationType.equals(AvroOperationType.TRIM_STRINGS))
                {   
                    trimStrings(r-1, avroPtrs[i], type);
                }
                else if(operationType.equals(AvroOperationType.ADD_RATIO_FEATURES))
                {   
                    addRatioFeatures(r-1, avroPtrs[i], type);
                }
                
                dataFileWriters.get(i).append(avroPtrs[i]);
            }
            
            T mergedRecord = AvroUtils.mergeAvroRecords(avroPtrs, type);
        
            dataFileWriter.append(mergedRecord);        
        } 

        for (int i = 0; i < inFileCount; i++) {
            dataFileWriters.get(i).close();
            
            File file = new File(inpaths.get(i) + ".fixed");
            
            file.renameTo(new File(inpaths.get(i)));
        }

        System.out.println("Finished fixing and merging the files. Total records found: " + r);
        
        dataFileWriter.close();        

//        File file = new File(outpath + ".fixed");
//
//        file.renameTo(new File(outpath));
    }
    
    private static void runWithCmdLineOptions(String[] args)
    {
        Options opt = new Options(args, 1, 10000);

        opt.getSet().addOption("charset", "cs", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("nbest", "nb", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("operationType", "ot", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("type", "t", Options.Separator.BLANK, Options.Multiplicity.ONCE);
        opt.getSet().addOption("output", "o", Options.Separator.BLANK, Options.Multiplicity.ZERO_OR_ONE);

        String charset = "UTF-8";
        List<String> infiles;
        String nbestpath = "";
        String outfile = "";
        AvroType type = null;
        AvroOperationType operationType = null;

        if (!opt.check()) {
            System.out.println("AvroFormatFix -t avroType -ot operationType");
            System.out.println("\t\t[-nb nbestpath] [-cs charset] [-o outfile] avrofiles ...");
            System.out.println("\tTypes:");
            System.out.println("\t\t" + AvroType.NBEST_LIST);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_FEATURES);
            System.out.println("\t\t" + AvroType.POST_EDITED_CORPUS_ENTRY);
            System.out.println("\tOperation Types:");
            System.out.println("\t\t" + AvroOperationType.FIX_NAN);
            System.out.println("\t\t" + AvroOperationType.ADD_RATIO_FEATURES);
            System.out.println("\t\t" + AvroOperationType.REMOVE_EMPTY_HYPOTHESIS);
            System.out.println("\t\t" + AvroOperationType.TRIM_STRINGS);
            System.out.println(opt.getCheckErrors());
            System.exit(1);
        }

        if (opt.getSet().isSet("cs")) {
            charset = opt.getSet().getOption("cs").getResultValue(0);
            System.out.println("Charset " + charset);
        }

        if (opt.getSet().isSet("t")) {
            String typeString = opt.getSet().getOption("t").getResultValue(0);
            type = AvroType.valueOf(typeString);
            System.out.println("Avro type: " + type);
        }

        if (opt.getSet().isSet("ot")) {
            String typeString = opt.getSet().getOption("ot").getResultValue(0);
            operationType = AvroOperationType.valueOf(typeString);
            System.out.println("Operation type: " + type);
        }

        if (opt.getSet().isSet("o")) {
            outfile = opt.getSet().getOption("o").getResultValue(0);  
            System.out.println("Output file: " + outfile);
        }

        if (opt.getSet().isSet("nb")) {
            nbestpath = opt.getSet().getOption("nb").getResultValue(0);  
            System.out.println("N-best list file: " + nbestpath);
        }

        infiles = opt.getSet().getData();

        System.out.println("Input files: " + infiles);

        try {
            AvroRecordOperations avroFormatFix = new AvroRecordOperations();

            avroFormatFix.applyOperation(infiles, outfile, nbestpath, charset, type, operationType);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FeatureExtractionFlow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args)
    {                
//        args = new String[]{"-t", "POST_EDITED_CORPUS_FEATURES", "-ot", "ADD_RATIO_FEATURES", "/more/work/trace-qe/features/feature-set-1/en-fr-first-data-features.avro.Surface.src"};
//        args = new String[]{"-t", "NBEST_LIST", "-ot", "REMOVE_EMPTY_HYPOTHESIS",
//            "-o", "tmp/nblist-features.avro.fixed",
//            "tmp/nblist-features.avro"};
        
        runWithCmdLineOptions(args);
    }    
}
