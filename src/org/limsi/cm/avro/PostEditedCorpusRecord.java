/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package org.limsi.cm.avro;  
@SuppressWarnings("all")
/** Schema to represent a corpus of post-editions annotated with features, labels and other meta-data */
@org.apache.avro.specific.AvroGenerated
public class PostEditedCorpusRecord extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"PostEditedCorpusRecord\",\"namespace\":\"org.limsi.cm.avro\",\"doc\":\"Schema to represent a corpus of post-editions annotated with features, labels and other meta-data\",\"fields\":[{\"name\":\"corpusFeatures\",\"type\":{\"type\":\"record\",\"name\":\"PostEditedCorpusFeatures\",\"fields\":[{\"name\":\"src\",\"type\":[\"string\",\"null\"],\"doc\":\"the source sentence\"},{\"name\":\"tgt\",\"type\":[\"string\",\"null\"],\"doc\":\"the target sentence produced by the MT system\"},{\"name\":\"peTgt\",\"type\":[\"string\",\"null\"],\"doc\":\"the target sentence post-edited by an human annotator\"},{\"name\":\"srcFte\",\"type\":[\"string\",\"null\"],\"doc\":\"src tokenized in FTE\"},{\"name\":\"tgtFte\",\"type\":[\"string\",\"null\"],\"doc\":\"tgt tokenized in FTE\"},{\"name\":\"peTgtFte\",\"type\":[\"string\",\"null\"],\"doc\":\"peTgt tokenized in FTE\"},{\"name\":\"features\",\"type\":{\"type\":\"map\",\"values\":\"float\"},\"doc\":\"features vector (feat_name:feat_value representation)\"}]},\"doc\":\"features for the post-edited corpus\"},{\"name\":\"labels\",\"type\":{\"type\":\"map\",\"values\":\"float\"},\"doc\":\"labels for classification or regression (label_name:label_value representation)\"}]}");
  /** features for the post-edited corpus */
  @Deprecated public org.limsi.cm.avro.PostEditedCorpusFeatures corpusFeatures;
  /** labels for classification or regression (label_name:label_value representation) */
  @Deprecated public java.util.Map<java.lang.CharSequence,java.lang.Float> labels;

  /**
   * Default constructor.
   */
  public PostEditedCorpusRecord() {}

  /**
   * All-args constructor.
   */
  public PostEditedCorpusRecord(org.limsi.cm.avro.PostEditedCorpusFeatures corpusFeatures, java.util.Map<java.lang.CharSequence,java.lang.Float> labels) {
    this.corpusFeatures = corpusFeatures;
    this.labels = labels;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return corpusFeatures;
    case 1: return labels;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: corpusFeatures = (org.limsi.cm.avro.PostEditedCorpusFeatures)value$; break;
    case 1: labels = (java.util.Map<java.lang.CharSequence,java.lang.Float>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'corpusFeatures' field.
   * features for the post-edited corpus   */
  public org.limsi.cm.avro.PostEditedCorpusFeatures getCorpusFeatures() {
    return corpusFeatures;
  }

  /**
   * Sets the value of the 'corpusFeatures' field.
   * features for the post-edited corpus   * @param value the value to set.
   */
  public void setCorpusFeatures(org.limsi.cm.avro.PostEditedCorpusFeatures value) {
    this.corpusFeatures = value;
  }

  /**
   * Gets the value of the 'labels' field.
   * labels for classification or regression (label_name:label_value representation)   */
  public java.util.Map<java.lang.CharSequence,java.lang.Float> getLabels() {
    return labels;
  }

  /**
   * Sets the value of the 'labels' field.
   * labels for classification or regression (label_name:label_value representation)   * @param value the value to set.
   */
  public void setLabels(java.util.Map<java.lang.CharSequence,java.lang.Float> value) {
    this.labels = value;
  }

  /** Creates a new PostEditedCorpusRecord RecordBuilder */
  public static org.limsi.cm.avro.PostEditedCorpusRecord.Builder newBuilder() {
    return new org.limsi.cm.avro.PostEditedCorpusRecord.Builder();
  }
  
  /** Creates a new PostEditedCorpusRecord RecordBuilder by copying an existing Builder */
  public static org.limsi.cm.avro.PostEditedCorpusRecord.Builder newBuilder(org.limsi.cm.avro.PostEditedCorpusRecord.Builder other) {
    return new org.limsi.cm.avro.PostEditedCorpusRecord.Builder(other);
  }
  
  /** Creates a new PostEditedCorpusRecord RecordBuilder by copying an existing PostEditedCorpusRecord instance */
  public static org.limsi.cm.avro.PostEditedCorpusRecord.Builder newBuilder(org.limsi.cm.avro.PostEditedCorpusRecord other) {
    return new org.limsi.cm.avro.PostEditedCorpusRecord.Builder(other);
  }
  
  /**
   * RecordBuilder for PostEditedCorpusRecord instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<PostEditedCorpusRecord>
    implements org.apache.avro.data.RecordBuilder<PostEditedCorpusRecord> {

    private org.limsi.cm.avro.PostEditedCorpusFeatures corpusFeatures;
    private java.util.Map<java.lang.CharSequence,java.lang.Float> labels;

    /** Creates a new Builder */
    private Builder() {
      super(org.limsi.cm.avro.PostEditedCorpusRecord.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(org.limsi.cm.avro.PostEditedCorpusRecord.Builder other) {
      super(other);
    }
    
    /** Creates a Builder by copying an existing PostEditedCorpusRecord instance */
    private Builder(org.limsi.cm.avro.PostEditedCorpusRecord other) {
            super(org.limsi.cm.avro.PostEditedCorpusRecord.SCHEMA$);
      if (isValidValue(fields()[0], other.corpusFeatures)) {
        this.corpusFeatures = data().deepCopy(fields()[0].schema(), other.corpusFeatures);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.labels)) {
        this.labels = data().deepCopy(fields()[1].schema(), other.labels);
        fieldSetFlags()[1] = true;
      }
    }

    /** Gets the value of the 'corpusFeatures' field */
    public org.limsi.cm.avro.PostEditedCorpusFeatures getCorpusFeatures() {
      return corpusFeatures;
    }
    
    /** Sets the value of the 'corpusFeatures' field */
    public org.limsi.cm.avro.PostEditedCorpusRecord.Builder setCorpusFeatures(org.limsi.cm.avro.PostEditedCorpusFeatures value) {
      validate(fields()[0], value);
      this.corpusFeatures = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'corpusFeatures' field has been set */
    public boolean hasCorpusFeatures() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'corpusFeatures' field */
    public org.limsi.cm.avro.PostEditedCorpusRecord.Builder clearCorpusFeatures() {
      corpusFeatures = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'labels' field */
    public java.util.Map<java.lang.CharSequence,java.lang.Float> getLabels() {
      return labels;
    }
    
    /** Sets the value of the 'labels' field */
    public org.limsi.cm.avro.PostEditedCorpusRecord.Builder setLabels(java.util.Map<java.lang.CharSequence,java.lang.Float> value) {
      validate(fields()[1], value);
      this.labels = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'labels' field has been set */
    public boolean hasLabels() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'labels' field */
    public org.limsi.cm.avro.PostEditedCorpusRecord.Builder clearLabels() {
      labels = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    public PostEditedCorpusRecord build() {
      try {
        PostEditedCorpusRecord record = new PostEditedCorpusRecord();
        record.corpusFeatures = fieldSetFlags()[0] ? this.corpusFeatures : (org.limsi.cm.avro.PostEditedCorpusFeatures) defaultValue(fields()[0]);
        record.labels = fieldSetFlags()[1] ? this.labels : (java.util.Map<java.lang.CharSequence,java.lang.Float>) defaultValue(fields()[1]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
