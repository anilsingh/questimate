/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.cm.avro;

/**
 *
 * @author anil
 */
public class AvroTextQuery {
    public String text;
    public boolean charSearch;
    
    public AvroTextQuery(String text)
    {
        this.text = text;
    }
    
    public AvroTextQuery(String text, boolean charLength)
    {
        this.text = text;
        this.charSearch = charLength;
    }

    public static AvroTextQuery createQuery(String text)
    {
        return new AvroTextQuery(text);
    }

    public static AvroTextQuery createQuery(String text, boolean charLength)
    {
        return new AvroTextQuery(text, charLength);
    }
}
