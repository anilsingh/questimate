/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anil
 */
public class FeatureSubTypes {

    protected FeatureType featureType;

    public FeatureSubTypes(FeatureType featureType) {
        this.featureType = featureType;
    }

    /**
     * @return the featureType
     */
    public FeatureType getFeatureType() {
        return featureType;
    }

    /**
     * @param featureType the featureType to set
     */
    public void setFeatureType(FeatureType featureType) {
        this.featureType = featureType;
    }
    
    public List<FeatureSubType> getFeatureSubTypes()
    {
        List<FeatureSubType> featureSubTypes = new ArrayList<FeatureSubType>();

        if(featureType.equals(FeatureType.SURFACE))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.SENTENCE_LENGH);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.AVG_TOKEN_LENGH);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TYPE_TOKEN_RATIO);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUMBER_PUNC);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUMBER_CAPS);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_LM))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_LOGPROB);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_PERPLEXITY);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_BACKOFF_COUNT);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_LOGPROB_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_PERPLEXITY_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_BACKOFF_COUNT_POS);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_LM_POS_NOLEX))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_LOGPROB_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_PERPLEXITY_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LM_BACKOFF_COUNT_POS_NOLEX);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_UNIGRAM);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_BIGRAM);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_TRIGRAM);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_UNIGRAM_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_BIGRAM_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_TRIGRAM_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE2_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE3_POS);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4_POS);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.NGRAM_COUNTS_POS_NOLEX))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_UNIGRAM_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE1_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE2_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE3_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UNIGRAM_FREQ_QUARTILE4_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_BIGRAM_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE1_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE2_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE3_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BIGRAM_FREQ_QUARTILE4_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.OOV_TRIGRAM_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE1_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE2_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE3_POS_NOLEX);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.TRIGRAM_FREQ_QUARTILE4_POS_NOLEX);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.SOUL_LM))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.SOUL_LM_SCORE);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.IBM1))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.IBM1_SCORE);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.AVG_NUM_TRANS02);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.AVG_NUM_TRANS001);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.POSCOUNTS))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_VERB_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_FUNCTION_WORD_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_NOUN_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_PRONOUN_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_PREPOSITION_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_MODIFIER_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_WH_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_NUMBER_WORD_COUNT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.POS_SYMBOL_COUNT);
            featureSubTypes.add(featureSubType);
        }
//        else if(featureType.equals(FeatureType.LATTICE))
//        {
//            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LATTICE_NUM_STATES);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LATTICE_NUM_ARCS);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LATTICE_NUM_BIGRAMS);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LATTICE_NUM_TRIGRAMS);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LATTICE_NUM_FOUR_GRAMS);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.UG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.FORWARD_BG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BACKWARD_BG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.FORWARD_TG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BACKWARD_TG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.FORWARD_FG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//
//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.BACKWARD_FG_POSTERIOR_PROB);
//            featureSubTypes.add(featureSubType);
//        }
        else if(featureType.equals(FeatureType.MODEL))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.MODEL_SCORE);
            featureSubTypes.add(featureSubType);
        }
        else if(featureType.equals(FeatureType.LABEL))
        {
            FeatureSubType featureSubType =  new FeatureSubType(FeatureSubTypesEnum.LABEL);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.HTER_REF);
            featureSubTypes.add(featureSubType);

//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.CLIPPED_HTER_REF);
//            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_DEL_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_INS_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_SUB_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_SHIFTS_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_WSHIFTS_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_EDITS_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_WORDS_HTER_REF);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

//            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.CLIPPED_HTER_POST_EDIT);
//            featureSubTypes.add(featureSubType);
 
            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_DEL_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_INS_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_SUB_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_SHIFTS_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_WSHIFTS_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_EDITS_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);

            featureSubType =  new FeatureSubType(FeatureSubTypesEnum.NUM_WORDS_HTER_POST_EDIT);
            featureSubTypes.add(featureSubType);
       }
        
        return featureSubTypes;
    }
    
    public static List<FeatureSubType> getFeatureSubTypes(FeatureType featureType)
    {
        FeatureSubTypes featureSubTypes = new FeatureSubTypes(featureType);
        
        List<FeatureSubType> featureSubTypesList = featureSubTypes.getFeatureSubTypes();
        
        return featureSubTypesList;
    }
}
