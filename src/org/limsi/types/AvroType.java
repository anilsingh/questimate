/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.lang.reflect.Array;
import org.apache.avro.file.CodecFactory;
import org.limsi.cm.avro.AvroUtils;
import org.limsi.cm.avro.NBestListAvro;
import org.limsi.cm.avro.PostEditedCorpusEntry;
import org.limsi.cm.avro.PostEditedCorpusFeatures;
import org.limsi.cm.avro.PostEditedCorpusRecord;

/**
 *
 * @author anil
 */
public enum AvroType {
        
    NBEST_LIST ("data/avro-schemas/nbest.avsc", NBestListAvro.class, CodecFactory.snappyCodec()),
    POST_EDITED_CORPUS_FEATURES ("data/avro-schemas/post-edited-corpus-features.avsc", PostEditedCorpusFeatures.class, CodecFactory.snappyCodec()),
    POST_EDITED_CORPUS_RECORD ("data/avro-schemas/post-edited-corpus-record.avsc", PostEditedCorpusRecord.class, CodecFactory.snappyCodec()),
    POST_EDITED_CORPUS_ENTRY ("data/avro-schemas/post-edited-corpus-entry.avsc", PostEditedCorpusEntry.class, CodecFactory.snappyCodec());
    
    public static final String PROP_SCHEMAPATH = "PROP_SCHEMAPATH";
    public static final String PROP_SCHEMACLASS = "PROP_SCHEMACLASS";
    
    private String schemaPath;
    private Class schemaClass;
    private CodecFactory codecFactory;
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    AvroType(String schemaPath, Class schemaClass, CodecFactory codecFactory) {
        this.schemaPath = schemaPath;
        this.schemaClass = schemaClass;
        this.codecFactory = codecFactory;
    }

    /**
     * @return the schemaPath
     */
    public String getSchemaPath() {
        return schemaPath;
    }

    /**
     * @param schemaPath the schemaPath to set
     */
    public void setSchemaPath(String schemaPath) throws PropertyVetoException {
        java.lang.String oldSchemaPath = this.schemaPath;
        vetoableChangeSupport.fireVetoableChange(PROP_SCHEMAPATH, oldSchemaPath, schemaPath);
        this.schemaPath = schemaPath;
        propertyChangeSupport.firePropertyChange(PROP_SCHEMAPATH, oldSchemaPath, schemaPath);
    }

    /**
     * @return the schemaClass
     */
    public Class getSchemaClass() {
        return schemaClass;
    }

    /**
     * @param schemaClass the schemaClass to set
     */
    public void setSchemaClass(Class schemaClass) {
        this.schemaClass = schemaClass;
    }

    /**
     * @return the codecFactory
     */
    public CodecFactory getCodecFactory() {
        return codecFactory;
    }

    /**
     * @param codecFactory the codecFactory to set
     */
    public void setCodecFactory(CodecFactory codecFactory) {
        this.codecFactory = codecFactory;
    }
    
    public static Object getSampleInstance(AvroType type)
    {
        System.out.println("Type: " + type.getSchemaClass().getCanonicalName());
        if(type == AvroType.NBEST_LIST)
        {
            return new NBestListAvro();
        }
        else if(type == AvroType.POST_EDITED_CORPUS_FEATURES)
        {
            return new PostEditedCorpusFeatures();
        }
        else if(type == AvroType.POST_EDITED_CORPUS_RECORD)
        {
            return AvroUtils.getPERecordInstanceWithFeatures();
        }
        else if(type == AvroType.POST_EDITED_CORPUS_ENTRY)
        {
            return new PostEditedCorpusEntry();
        }
        
        return null;
    }

    public static Object createInstance(AvroType type)
    {
        Object avroObj = null;
        
        if(type.equals(AvroType.NBEST_LIST))
        {
            avroObj = new NBestListAvro();
        }
        else if(type.equals(AvroType.POST_EDITED_CORPUS_FEATURES))
        {
            avroObj = new PostEditedCorpusFeatures();
        }        
        else if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
        {
            avroObj = AvroUtils.getPERecordInstanceWithFeatures();
        }        
        else if(type.equals(AvroType.POST_EDITED_CORPUS_RECORD))
        {
            avroObj = new PostEditedCorpusEntry();
        }        
        
        return avroObj;
    }
    
    public static Object[] createArray(AvroType type, int size)
    {
        Object[] avroArray = (Object[]) Array.newInstance(type.getSchemaClass(), size);
        
        return avroArray;
    }
}
