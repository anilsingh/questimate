/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

/**
 *
 * @author anil
 */
public enum AvroOperationType {
    FIX_NAN,
    TRIM_STRINGS,
    REMOVE_EMPTY_HYPOTHESIS,
    ADD_RATIO_FEATURES,
    MERGE_HORIZANOTAL,
    MERGE_VERTICAL,
    CONVERT_FORMAT,
    SPLIT_HORIZONTAL,
    SPLIT_VERTICAL_2_WAY,
    SPLIT_VERTICAL_N_WAY;
}
