/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

/**
 *
 * @author anil
 */
public enum FormatType {

    COMMON_FORMAT( CommonFormat.KEY_VALUE_PROPERTY, FormatConfig.NO_CONFIG ),
    CORPUS_FORMAT( CorpusFormat.RAW_CORPUS, FormatConfig.NO_CONFIG ),
    LANGUAGE_MODEL_FORMAT( LanguageModelFormat.ARPA_FORMAT, FormatConfig.NO_CONFIG ),
    ALIGNMENT_FORMAT( AlignmentFormat.GIZA_PP_FORMAT1, FormatConfig.NO_CONFIG ),
    NBEST_LIST_FORMAT( NBestListFormat.MOSES_FORMAT, FormatConfig.NO_CONFIG ),
    WORD_GRAPH_FORMAT( WordGraphFormat.NCODE_FORMAT, FormatConfig.NO_CONFIG ),
    LATTICE_FST_FORMAT( LatticeFstFormat.OPEN_FST_FORMAT, FormatConfig.NO_CONFIG ),
    FEATURE_COLLECTION_FORMAT( FeatureCollectionFormat.ARFF_FORMAT, FormatConfig.NO_CONFIG ),
    RESULTS_FORMAT( ResultsFormat.CSV_FORMAT, FormatConfig.NO_CONFIG );
    
    protected Format format;
    protected FormatConfig formatConfig;

    public enum CommonFormat implements Format {
        PROPERTY_TOKEN ("ptokens"),
        KEY_VALUE_PROPERTY ("kvp"),
        PROPERTY_TABLE ("prop.table"),
        PROPERTY_TREE ("prop.tree"),
        XML_PROPERTIES ("prop.xml");

        private String formatExtension;
        
        CommonFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum CorpusFormat implements Format {
        RAW_CORPUS ("raw"),
        TOKENIZED_CORPUS ("tok"),
        POS_TAGGED_CORPUS ("postagged"),
        CHUNKED_CORPUS ("chunked"),
        PARSED_CORPUS ("parsed");

        private String formatExtension;
        
        CorpusFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum LanguageModelFormat implements Format {
        ARPA_FORMAT ("lm"),
        ARPA_FORMAT_EX ("lm.ext"),
        BERKELEY_BINARY_FORMAT ("bin"),
        SRILM_BINARY_FORMAT ("bin");

        private String formatExtension;
        
        LanguageModelFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum AlignmentFormat implements Format {
        GIZA_PP_FORMAT1 ("giza"),
        GIZA_PP_FORMAT2 ("giza"),
        MOSES_FORMAT ("moses");

        private String formatExtension;
        
        AlignmentFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum NBestListFormat implements Format {
        MOSES_FORMAT ("NBEST");

        private String formatExtension;
        
        NBestListFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum WordGraphFormat implements Format {
        WMT12_FORMAT ("wmt"),
        WMT13_FORMAT ("wmt13"),
        MOSES_FORMAT ("plf"),
        NCODE_FORMAT ("GRAPH"),
        OPEN_FST_FORMAT ("fst");

        private String formatExtension;
        
        WordGraphFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum LatticeFstFormat implements Format {
        OPEN_FST_FORMAT ("fst");

        private String formatExtension;
        
        LatticeFstFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
    };

    public enum FeatureCollectionFormat implements Format {
        ARFF_FORMAT ("arff"),
        SVMLIGHT_FORMAT ("svmlight"),
        CSV_FORMAT ("csv"),
        AVRO_FORMAT ("avro");
        
        private String formatExtension;

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String formatExtension) { this.formatExtension = formatExtension; }
        
        FeatureCollectionFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }
    };

    public enum ResultsFormat implements Format {
        CSV_FORMAT ("csv");

        private String formatExtension;
        
        ResultsFormat(String formatExtension)
        {
            this.formatExtension = formatExtension;
        }

        @Override
        public String getFormatExtension() { return formatExtension; }
        @Override
        public void setFormatExtension(String ext) { this.formatExtension = formatExtension; }
    };

    /**
     * @return the format
     */
    public Format getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(Format format) throws PropertyVetoException {
        org.limsi.types.Format oldFormat = this.format;
        vetoableChangeSupport.fireVetoableChange(PROP_FORMAT, oldFormat, format);
        this.format = format;
        propertyChangeSupport.firePropertyChange(PROP_FORMAT, oldFormat, format);
    }

    /**
     * @return the formatConfig
     */
    public FormatConfig getFormatConfig() {
        return formatConfig;
    }

    /**
     * @param formatConfig the formatConfig to set
     */
    public void setFormatConfig(FormatConfig formatConfig) throws PropertyVetoException {
        org.limsi.types.FormatConfig oldFormatConfig = this.formatConfig;
        vetoableChangeSupport.fireVetoableChange(PROP_FORMAT_CONFIG, oldFormatConfig, formatConfig);
        this.formatConfig = formatConfig;
        propertyChangeSupport.firePropertyChange(PROP_FORMAT_CONFIG, oldFormatConfig, formatConfig);
    }
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    public static final String PROP_FORMAT = "PROP_KEY_VALUE";
    public static final String PROP_FORMAT_CONFIG = "NONE";

    FormatType(Format format, FormatConfig formatConfig) {
        this.format = format;
        this.formatConfig = formatConfig;
    }
}
