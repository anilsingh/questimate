/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

/**
 *
 * @author anil
 */
public enum FeatureDataType {

    NUMERIC_INT("Numeric", "Integer"),
    NUMERIC_FLOAT("Numeric", "Float"),
    NOMINAL_INT("Nominal", "Integer"),
    NOMINAL_STRING("Nominal", "String");
    public static final String PROP_TYPE = "PROP_TYPE";
    public static final String PROP_SUBTYPE = "PROP_SUBTYPE";
    protected String type;
    protected String subType;
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    FeatureDataType(String type, String subType) {
        this.type = type;
        this.subType = subType;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) throws PropertyVetoException {
        java.lang.String oldType = this.type;
        vetoableChangeSupport.fireVetoableChange(PROP_TYPE, oldType, type);
        this.type = type;
        propertyChangeSupport.firePropertyChange(PROP_TYPE, oldType, type);
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) throws PropertyVetoException {
        java.lang.String oldSubType = this.subType;
        vetoableChangeSupport.fireVetoableChange(PROP_SUBTYPE, oldSubType, subType);
        this.subType = subType;
        propertyChangeSupport.firePropertyChange(PROP_SUBTYPE, oldSubType, subType);
    }
}
