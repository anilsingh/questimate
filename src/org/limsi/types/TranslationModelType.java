/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;

/**
 *
 * @author anil
 */
public enum TranslationModelType {
    IBM1 (Granularity.WORD, false);
    
    private Granularity granularity;
    private boolean lowercase;
    
    private TranslationModelType(Granularity granularity, boolean lowercase)
    {
        this.granularity = granularity;
        this.lowercase = lowercase;
    }

    /**
     * @return the granularity
     */
    public Granularity getGranularity() {
        return granularity;
    }

    /**
     * @param granularity the granularity to set
     */
    public void setGranularity(Granularity granularity) {
        org.limsi.types.Granularity oldGranularity = this.granularity;
        this.granularity = granularity;
        propertyChangeSupport.firePropertyChange(PROP_GRANULARITY, oldGranularity, granularity);
    }

    /**
     * @return the lowercase
     */
    public boolean isLowercase() {
        return lowercase;
    }

    /**
     * @param lowercase the lowercase to set
     */
    public void setLowercase(boolean lowercase) {
        boolean oldLowercase = this.lowercase;
        this.lowercase = lowercase;
        propertyChangeSupport.firePropertyChange(PROP_LOWERCASE, oldLowercase, lowercase);
    }
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    public static final String PROP_GRANULARITY = "PROP_GRANULARITY";
    public static final String PROP_LOWERCASE = "PROP_LOWERCASE";
            
}
