/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

/**
 *
 * @author anil
 */
public interface Format {
        
    String getFormatExtension();
    void setFormatExtension(String ext);
    
}
