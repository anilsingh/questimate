/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anil
 */
public enum ResourceType {
        NONE (null, null),
        ANY (null, null),
        NGRAM_LM_TRAINING_TEXT (Language.ENGLISH, null),
        NGRAM_LM_COUNTS (Language.ENGLISH, null),
        NGRAM_LM_MODEL (Language.ENGLISH, null),
        SOUL_LM_MODEL (Language.ENGLISH, null),
        IBM1_SCORES (Language.ENGLISH, Language.FRENCH),
        POS_TAGGED_TEXT (Language.ENGLISH, null),
        POS_TAGGED_NOLEX (Language.ENGLISH, null),
        POS_TAGGED_NGRAM_COUNTS (Language.ENGLISH, null),
        NGRAM_POS_LM_MODEL (Language.ENGLISH, null),
        POS_NOLEX_NGRAM_COUNTS (Language.ENGLISH, null),
        NGRAM_POS_NOLEX_LM_MODEL (Language.ENGLISH, null),
        SENTENCE_ALIGNED_PARALLEL_TEXT (Language.ENGLISH, Language.FRENCH),
        WORD_ALIGNED_PARALLEL_TEXT (Language.ENGLISH, Language.FRENCH),
        NBEST_LIST (Language.ENGLISH, Language.FRENCH),
        TRANSLATION_LATTICES (Language.ENGLISH, Language.FRENCH),
        NGRAMS_FSTS (Language.ENGLISH, Language.FRENCH),
        REFERENCES (null, Language.FRENCH);

        protected Language srcLangauge;
        protected Language tgtLangauge;
        
        ResourceType(Language srcLangauge, Language tgtLangauge)
        {
            this.srcLangauge = srcLangauge;
            this.tgtLangauge = tgtLangauge;
        }

    /**
     * @return the srcLangauge
     */
    public Language getSrcLangauge() {
        return srcLangauge;
    }

    /**
     * @param srcLangauge the srcLangauge to set
     */
    public void setSrcLangauge(Language srcLangauge) throws PropertyVetoException {
        org.limsi.types.Language oldSrcLangauge = this.srcLangauge;
        vetoableChangeSupport.fireVetoableChange(PROP_SRCLANGAUGE, oldSrcLangauge, srcLangauge);
        this.srcLangauge = srcLangauge;
        propertyChangeSupport.firePropertyChange(PROP_SRCLANGAUGE, oldSrcLangauge, srcLangauge);
    }

    /**
     * @return the tgtLangauge
     */
    public Language getTgtLangauge() {
        return tgtLangauge;
    }

    /**
     * @param tgtLangauge the tgtLangauge to set
     */
    public void setTgtLangauge(Language tgtLangauge) throws PropertyVetoException {
        org.limsi.types.Language oldTgtLangauge = this.tgtLangauge;
        vetoableChangeSupport.fireVetoableChange(PROP_TGTLANGAUGE, oldTgtLangauge, tgtLangauge);
        this.tgtLangauge = tgtLangauge;
        propertyChangeSupport.firePropertyChange(PROP_TGTLANGAUGE, oldTgtLangauge, tgtLangauge);
    }
    
    public static void setLanguages(Language srcLanguage, Language tgtLanguage)
    {
        for(ResourceType rt: ResourceType.values())
        {
            try {
                rt.setSrcLangauge(srcLanguage);
                rt.setTgtLangauge(tgtLanguage);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ResourceType.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    
    public static final String PROP_SRCLANGAUGE = "PROP_SRCLANGAUGE";
    public static final String PROP_TGTLANGAUGE = "PROP_TGTLANGAUGE";
}
