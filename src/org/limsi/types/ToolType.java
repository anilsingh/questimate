/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

/**
 *
 * @author anil
 */
public enum ToolType {
    
    NONE (null),
    NGRAM_LM (LMType.TEXT_LM),
    SOUL_LM (null),
    POS_TAGGER (POSTaggerType.TREE_TAGGER),
    CFG_PARSER (null),
    DEPENDENCY_PARSER (null),
    TERCOM (null);    

    private Enum subType;

    ToolType(Enum subType) {
        this.subType = subType;
    }    

    /**
     * @return the subType
     */
    public Enum getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(Enum subType) {
        this.subType = subType;
    }
}

