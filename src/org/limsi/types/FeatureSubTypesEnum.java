/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 *
 * @author anil
 */
public enum FeatureSubTypesEnum {
   SENTENCE_LENGH ("SenLength", FeatureType.SURFACE, FeatureDataType.NUMERIC_INT),
   AVG_TOKEN_LENGH ("AvgTokenLength", FeatureType.SURFACE, FeatureDataType.NUMERIC_FLOAT),
   TYPE_TOKEN_RATIO ("TypeTokenRatio", FeatureType.SURFACE, FeatureDataType.NUMERIC_FLOAT),
   NUMBER_PUNC ("NumPunc", FeatureType.SURFACE, FeatureDataType.NUMERIC_INT),
   NUMBER_CAPS ("NumCaps", FeatureType.SURFACE, FeatureDataType.NUMERIC_INT),
   LM_LOGPROB ("LMLogprob", FeatureType.NGRAM_LM, FeatureDataType.NUMERIC_FLOAT),
   LM_PERPLEXITY ("LMPpl", FeatureType.NGRAM_LM, FeatureDataType.NUMERIC_FLOAT),
   LM_BACKOFF_COUNT ("LMBackoffCount", FeatureType.NGRAM_LM, FeatureDataType.NUMERIC_INT),
   LM_LOGPROB_POS ("POSLMLogprob", FeatureType.NGRAM_LM_POS, FeatureDataType.NUMERIC_FLOAT),
   LM_PERPLEXITY_POS ("POSLMPpl", FeatureType.NGRAM_LM_POS, FeatureDataType.NUMERIC_FLOAT),
   LM_BACKOFF_COUNT_POS ("POSLMBackoffCount", FeatureType.NGRAM_LM_POS, FeatureDataType.NUMERIC_INT),
   LM_LOGPROB_POS_NOLEX ("POSNolexLMLogprob", FeatureType.NGRAM_LM_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   LM_PERPLEXITY_POS_NOLEX ("POSNolexLMPpl", FeatureType.NGRAM_LM_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   LM_BACKOFF_COUNT_POS_NOLEX ("POSNolexLMBackoffCount", FeatureType.NGRAM_LM_POS_NOLEX, FeatureDataType.NUMERIC_INT),
   OOV_UNIGRAM ("OOVUnigrams", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE1 ("UnigramsFreqQuartile1", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE2 ("UnigramsFreqQuartile2", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE3 ("UnigramsFreqQuartile3", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE4 ("UnigramsFreqQuartile4", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   OOV_BIGRAM ("OOVBigrams", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE1 ("BigramsFreqQuartile1", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE2 ("BigramsFreqQuartile2", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE3 ("BigramsFreqQuartile3", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE4 ("BigramsFreqQuartile4", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   OOV_TRIGRAM ("OOVTrigrams", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE1 ("TrigramsFreqQuartile1", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE2 ("TrigramsFreqQuartile2", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE3 ("TrigramsFreqQuartile3", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE4 ("TrigramsFreqQuartile4", FeatureType.NGRAM_COUNTS, FeatureDataType.NUMERIC_FLOAT),
   OOV_UNIGRAM_POS ("OOVUnigramsPOS", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE1_POS ("POSUnigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE2_POS ("POSUnigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE3_POS ("POSUnigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE4_POS ("POSUnigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   OOV_BIGRAM_POS ("OOVBigramsPOS", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE1_POS ("POSBigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE2_POS ("POSBigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE3_POS ("POSBigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE4_POS ("POSBigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   OOV_TRIGRAM_POS ("OOVTrigramsPOS", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE1_POS ("POSTrigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE2_POS ("POSTrigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE3_POS ("POSTrigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE4_POS ("POSTrigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS, FeatureDataType.NUMERIC_FLOAT),
   OOV_UNIGRAM_POS_NOLEX ("OOVUnigramsPOSNolex", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE1_POS_NOLEX ("POSNolexUnigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE2_POS_NOLEX ("POSNolexUnigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE3_POS_NOLEX ("POSNolexUnigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   UNIGRAM_FREQ_QUARTILE4_POS_NOLEX ("POSNolexUnigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   OOV_BIGRAM_POS_NOLEX ("OOVBigramsPOSNolex", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE1_POS_NOLEX ("POSNolexBigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE2_POS_NOLEX ("POSNolexBigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE3_POS_NOLEX ("POSNolexBigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   BIGRAM_FREQ_QUARTILE4_POS_NOLEX ("POSNolexBigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   OOV_TRIGRAM_POS_NOLEX ("OOVTrigramsPOSNolex", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE1_POS_NOLEX ("POSNolexTrigramsFreqQuartile1", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE2_POS_NOLEX ("POSNolexTrigramsFreqQuartile2", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE3_POS_NOLEX ("POSNolexTrigramsFreqQuartile3", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   TRIGRAM_FREQ_QUARTILE4_POS_NOLEX ("POSNolexTrigramsFreqQuartile4", FeatureType.NGRAM_COUNTS_POS_NOLEX, FeatureDataType.NUMERIC_FLOAT),
   POS_VERB_COUNT ("VerbCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_PREPOSITION_COUNT ("PrepositionCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_PRONOUN_COUNT ("PronounCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_FUNCTION_WORD_COUNT ("FuntionWordCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_NOUN_COUNT ("NounCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_MODIFIER_COUNT ("ModifierCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_WH_COUNT ("WHCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_NUMBER_WORD_COUNT ("NumberWordCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   POS_SYMBOL_COUNT ("SymbolCount", FeatureType.POSCOUNTS, FeatureDataType.NUMERIC_INT),
   SOUL_LM_SCORE ("SoulLMScore", FeatureType.SOUL_LM, FeatureDataType.NUMERIC_FLOAT),
   IBM1_SCORE ("IBM1Score", FeatureType.IBM1, FeatureDataType.NUMERIC_FLOAT),
   AVG_NUM_TRANS02 ("AvgNumTrans02", FeatureType.IBM1, FeatureDataType.NUMERIC_FLOAT),
   AVG_NUM_TRANS001 ("AvgNumTrans001", FeatureType.IBM1, FeatureDataType.NUMERIC_FLOAT),
   LATTICE_NUM_STATES ("LatticeNumStates", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_ARCS ("LatticeNumArcs", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   UG_POSTERIOR_PROB ("UGPosteriorProb", FeatureType.LATTICE, FeatureDataType.NUMERIC_FLOAT),
   BG_POSTERIOR_PROB ("BGPosteriorProb", FeatureType.LATTICE, FeatureDataType.NUMERIC_FLOAT),
   TG_POSTERIOR_PROB ("TGPosteriorProb", FeatureType.LATTICE, FeatureDataType.NUMERIC_FLOAT),
   FG_POSTERIOR_PROB ("FGPosteriorProb", FeatureType.LATTICE, FeatureDataType.NUMERIC_FLOAT),
   LATTICE_NUM_UNIGRAMS ("LatticeNumUnigrams", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_BIGRAMS ("LatticeNumBigrams", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_TRIGRAMS ("LatticeNumTrigrams", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_FOUR_GRAMS ("LatticeNumFourGrams", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_UNIGRAMS1 ("LatticeNumUnigrams1", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_BIGRAMS1 ("LatticeNumBigrams1", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_TRIGRAMS1 ("LatticeNumTrigrams1", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_FOUR_GRAMS1 ("LatticeNumFourGrams1", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_UNIGRAMS4 ("LatticeNumUnigrams4", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_BIGRAMS4 ("LatticeNumBigrams4", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_TRIGRAMS4 ("LatticeNumTrigrams4", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   LATTICE_NUM_FOUR_GRAMS4 ("LatticeNumFourGrams4", FeatureType.LATTICE, FeatureDataType.NUMERIC_INT),
   MODEL_SCORE ("ModelScore", FeatureType.MODEL, FeatureDataType.NUMERIC_FLOAT),
   HTER_REF ("HTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
//   CLIPPED_HTER_REF ("ClippedHTERRef", FeatureDataType.NUMERIC_FLOAT),
   NUM_INS_HTER_REF ("NumInsHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_DEL_HTER_REF ("NumDelHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_SUB_HTER_REF ("NumSubHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_SHIFTS_HTER_REF ("NumShiftsHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_WSHIFTS_HTER_REF ("NumWShiftsHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_EDITS_HTER_REF ("NumEditsHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_WORDS_HTER_REF ("NumWordsHTERRef", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   HTER_POST_EDIT ("HTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
//   CLIPPED_HTER_POST_EDIT ("ClippedHTERPostEdit", FeatureDataType.NUMERIC_FLOAT),
   NUM_INS_HTER_POST_EDIT ("NumInsHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_DEL_HTER_POST_EDIT ("NumDelHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_SUB_HTER_POST_EDIT ("NumSubHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_SHIFTS_HTER_POST_EDIT ("NumShiftsHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_WSHIFTS_HTER_POST_EDIT ("NumWShiftsHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_EDITS_HTER_POST_EDIT ("NumEditsHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   NUM_WORDS_HTER_POST_EDIT ("NumWordsHTERPostEdit", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT),
   LABEL ("HumanScore", FeatureType.LABEL, FeatureDataType.NUMERIC_FLOAT);
   
   FeatureSubTypesEnum(String featureName, FeatureType featureType, FeatureDataType featureDataType)
   {
       this.featureName = featureName;
       this.featureType = featureType;
       this.featureDataType = featureDataType;
   }
   
   public String featureName;
   public FeatureDataType featureDataType;
   public FeatureType featureType;
   
   public static boolean isValidName(String name)
   {
       for (FeatureSubTypesEnum subType : FeatureSubTypesEnum.values()) {
           if(subType.featureName.equals(name))
           {
               return true;
           }
       }
           
        return false;
   }

   public static List<FeatureSubTypesEnum> getFeatureSubTypes(FeatureType featureType)
   {
       List<FeatureSubTypesEnum> featureSubTypes = new ArrayList<FeatureSubTypesEnum>();
       
       FeatureSubTypesEnum[] allFeatureSubTypes = values();
 
       for (int i = 0; i < allFeatureSubTypes.length; i++) {
           FeatureSubTypesEnum featureSubType = allFeatureSubTypes[i];
           
           if(featureSubType.featureType!= null && featureSubType.featureType.equals(featureType))
           {
               featureSubTypes.add(featureSubType);
           }
       }
        
       return featureSubTypes;
   }
}
