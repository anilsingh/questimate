/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

/**
 *
 * @author anil
 */
public enum LabelType {
    MANUAL (FeatureDataType.NUMERIC_FLOAT, false, false, false),
    MANUAL_WEIGHTED_AVG (FeatureDataType.NUMERIC_FLOAT, false, false, false),
    HTER (FeatureDataType.NUMERIC_FLOAT, true, false, false);
    
    protected FeatureDataType label;
    protected boolean derived;
    protected boolean lowercase;
    protected boolean detailed;
    
    LabelType(FeatureDataType label, boolean derived, boolean lowercase, boolean detailed) {
        this.label = label;
        this.derived = derived;
        this.lowercase = lowercase;
        this.detailed = detailed;
    }

    /**
     * @return the label
     */
    public FeatureDataType getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(FeatureDataType label) throws PropertyVetoException {
        org.limsi.types.FeatureDataType oldLabel = this.label;
        vetoableChangeSupport.fireVetoableChange(PROP_LABEL, oldLabel, label);
        this.label = label;
        propertyChangeSupport.firePropertyChange(PROP_LABEL, oldLabel, label);
    }

    /**
     * @return the derived
     */
    public boolean isDerived() {
        return derived;
    }

    /**
     * @param derived the derived to set
     */
    public void setDerived(boolean derived) throws PropertyVetoException {
        boolean oldDerived = this.derived;
        vetoableChangeSupport.fireVetoableChange(PROP_DERIVED, oldDerived, derived);
        this.derived = derived;
        propertyChangeSupport.firePropertyChange(PROP_DERIVED, oldDerived, derived);
    }
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    public static final String PROP_LABEL = "PROP_LABEL";
    public static final String PROP_DERIVED = "PROP_DERIVED";    

    /**
     * @return the lowercase
     */
    public boolean isLowercase() {
        return lowercase;
    }

    /**
     * @param lowercase the lowercase to set
     */
    public void setLowercase(boolean lowercase) {
        this.lowercase = lowercase;
    }

    /**
     * @return the detailed
     */
    public boolean isDetailed() {
        return detailed;
    }

    /**
     * @param detailed the detailed to set
     */
    public void setDetailed(boolean detailed) {
        this.detailed = detailed;
    }
}
