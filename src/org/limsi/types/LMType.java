/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

/**
 *
 * @author anil
 */
public enum LMType {
    
    TEXT_LM (LMTool.BERKELEY, LMOrder.THREE, LMSmoothing.KN_DISCOUNT, false),
    POS_TAGGED_LM (LMTool.BERKELEY, LMOrder.THREE, LMSmoothing.KN_DISCOUNT, false),
    POS_NOLEX_LM (LMTool.BERKELEY, LMOrder.FIVE, LMSmoothing.WITTEN_BELL, false),
    NGRAM_COUNTS (LMTool.SANCHAY, LMOrder.THREE, LMSmoothing.WITTEN_BELL, false),
    NGRAM_COUNTS_POS (LMTool.SANCHAY, LMOrder.THREE, LMSmoothing.KN_DISCOUNT, false),
    NGRAM_COUNTS_POS_NOLEX (LMTool.SANCHAY, LMOrder.THREE, LMSmoothing.KN_DISCOUNT, false),
    SOUL_LM (LMTool.SOUL, LMOrder.TEN, LMSmoothing.SOUL, false);
    
    public static final String PROP_LMTOOL = "PROP_LMTOOL";
    public static final String PROP_ORDER = "PROP_ORDER";
    public static final String PROP_SMOOTHING = "PROP_SMOOTHING";
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    /**
     * @return the lmtool
     */
    public LMTool getLMTool() {
        return lmtool;
    }

    /**
     * @param lmtool the lmtool to set
     */
    public void setLMTool(LMTool lmtool) throws PropertyVetoException {
        org.limsi.types.LMType.LMTool oldLmtool = lmtool;
        vetoableChangeSupport.fireVetoableChange(PROP_LMTOOL, oldLmtool, lmtool);
        this.lmtool = lmtool;
        propertyChangeSupport.firePropertyChange(PROP_LMTOOL, oldLmtool, lmtool);
    }

    /**
     * @return the order
     */
    public LMOrder getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(LMOrder order) throws PropertyVetoException {
        org.limsi.types.LMType.LMOrder oldOrder = order;
        vetoableChangeSupport.fireVetoableChange(PROP_ORDER, oldOrder, order);
        this.order = order;
        propertyChangeSupport.firePropertyChange(PROP_ORDER, oldOrder, order);
    }

    /**
     * @return the smoothing
     */
    public LMSmoothing getSmoothing() {
        return smoothing;
    }

    /**
     * @param smoothing the smoothing to set
     */
    public void setSmoothing(LMSmoothing smoothing) throws PropertyVetoException {
        org.limsi.types.LMType.LMSmoothing oldSmoothing = smoothing;
        vetoableChangeSupport.fireVetoableChange(PROP_SMOOTHING, oldSmoothing, smoothing);
        this.smoothing = smoothing;
        propertyChangeSupport.firePropertyChange(PROP_SMOOTHING, oldSmoothing, smoothing);
    }

    /**
     * @return the lowercase
     */
    public boolean isLowercase() {
        return lowercase;
    }

    /**
     * @param lowercase the lowercase to set
     */
    public void setLowercase(boolean lowercase) {
        this.lowercase = lowercase;
    }
    
    public enum LMTool { SRILM, BERKELEY, SANCHAY, SOUL };
    
    public enum LMOrder {
        ONE (1),
        TWO (2),
        THREE (3),
        FOUR (4),
        FIVE (5),
        SIX (6),
        SEVEN (7),
        EIGHT (8),
        NINE (9),
        TEN (10);
        
        private final int order;

        private LMOrder(int order) {
            this.order = order;
        }
        
        public static LMOrder getLMOrder(int order)
        {
            for(LMOrder lmo: LMOrder.values())
            {
                if(lmo.order == order)
                {
                    return lmo;
                }
            }
                
            return null;
        }
        
        public int order() { return order; }
    };
    
    public enum LMSmoothing { GOOD_TURING, WITTEN_BELL, KN_DISCOUNT, STUPID_BACKOFF, SOUL };

    protected LMTool lmtool;
    protected LMOrder order;
    protected LMSmoothing smoothing;
    private boolean lowercase;
    
    LMType(LMTool lmtool, LMOrder order, LMSmoothing smoothing, boolean lowercase)
    {
        this.lmtool = lmtool;
        this.order = order;
        this.smoothing = smoothing;
        this.lowercase = lowercase;
    }
    
}
