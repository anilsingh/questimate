/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;

/**
 *
 * @author anil
 */
public enum Language {

    ENGLISH ("English", "eng", "en", Charset.UTF_8),
    FRENCH ("French", "fre", "fr", Charset.UTF_8),
    SPANISH ("Spanish", "esp", "es", Charset.UTF_8);
    
    private String name;
    private String threeLetterName;
    private String twoLetterName;
    private Charset charset;
    
    Language(String name, String threeLetterName, String twoLetterName, Charset cs)
    {
        this.name = name;
        this.threeLetterName = threeLetterName;
        this.twoLetterName = twoLetterName;
        charset = cs;
    }

    /**
     * @return the charsetName
     */
    public String getName() {
        return name;
    }

    /**
     * @param charsetName the charsetName to set
     */
    public void setName(String name) throws PropertyVetoException {
        java.lang.String oldName = name;
        vetoableChangeSupport.fireVetoableChange(PROP_NAME, oldName, name);
        this.name = name;
        propertyChangeSupport.firePropertyChange(PROP_NAME, oldName, name);
    }

    /**
     * @return the threeLetterName
     */
    public String getThreeLetterName() {
        return threeLetterName;
    }

    /**
     * @param threeLetterName the threeLetterName to set
     */
    public void setThreeLetterName(String threeLetterName) throws PropertyVetoException {
        java.lang.String oldThreeLetterName = threeLetterName;
        vetoableChangeSupport.fireVetoableChange(PROP_THREELETTERNAME, oldThreeLetterName, threeLetterName);
        this.threeLetterName = threeLetterName;
        propertyChangeSupport.firePropertyChange(PROP_THREELETTERNAME, oldThreeLetterName, threeLetterName);
    }

    /**
     * @return the twoLetterName
     */
    public String getTwoLetterName() {
        return twoLetterName;
    }

    /**
     * @param twoLetterName the twoLetterName to set
     */
    public void setTwoLetterName(String twoLetterName) throws PropertyVetoException {
        java.lang.String oldTwoLetterName = twoLetterName;
        vetoableChangeSupport.fireVetoableChange(PROP_TWOLETTERNAME, oldTwoLetterName, twoLetterName);
        this.twoLetterName = twoLetterName;
        propertyChangeSupport.firePropertyChange(PROP_TWOLETTERNAME, oldTwoLetterName, twoLetterName);
    }

    /**
     * @return the charset
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    
    public static final String PROP_NAME = "PROP_NAME";
    public static final String PROP_THREELETTERNAME = "PROP_THREELETTERNAME";
    public static final String PROP_TWOLETTERNAME = "PROP_TWOLETTERNAME";
    public static final String PROP_CHARSET = "PROP_CHARSET";

    public static Language getLanguage(String language)
    {
        Language l = Language.ENGLISH;
        
        for(Language lang: Language.values())
        {
            if(lang.getName().equalsIgnoreCase(language)
                    || lang.getThreeLetterName().equalsIgnoreCase(language)
                    || lang.getTwoLetterName().equalsIgnoreCase(language))

                return lang;
        }
        
        return l;
    }
    
    public enum Charset {
        UTF_8 ("UTF-8"),
        ISO_8859_1 ("ISO-8859-1");
        
        public final String charsetName;
        
        Charset(String n)
        {
            charsetName = n;
        }
    }
    
}
