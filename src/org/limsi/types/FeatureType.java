/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.limsi.types;

import edu.stanford.nlp.util.HashIndex;
import edu.stanford.nlp.util.Index;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;

/**
 *
 * @author anil
 */
public enum FeatureType {
    
    SURFACE ("Surface", Linguality.MONOLINGUAL, ResourceType.NONE, null, ToolType.NONE, Composite.NO),
    NGRAM_COUNTS ("NGramCounts", Linguality.MONOLINGUAL, ResourceType.NGRAM_LM_COUNTS, LMType.NGRAM_COUNTS, ToolType.NONE, Composite.NO),
    NGRAM_COUNTS_POS ("NGramCountsPOS", Linguality.MONOLINGUAL, ResourceType.POS_TAGGED_NGRAM_COUNTS, LMType.NGRAM_COUNTS_POS, ToolType.NONE, Composite.NO),
    NGRAM_COUNTS_POS_NOLEX ("NGramCountsPOSNolex", Linguality.MONOLINGUAL, ResourceType.POS_NOLEX_NGRAM_COUNTS, LMType.NGRAM_COUNTS_POS_NOLEX, ToolType.NONE, Composite.NO),
    NGRAM_LM ("NGramLM", Linguality.MONOLINGUAL, ResourceType.NGRAM_LM_MODEL, LMType.TEXT_LM, ToolType.NONE, Composite.NO),
    NGRAM_LM_POS ("NGramLMPOS", Linguality.MONOLINGUAL, ResourceType.NGRAM_POS_LM_MODEL, LMType.POS_TAGGED_LM, ToolType.NONE, Composite.NO),
    NGRAM_LM_POS_NOLEX ("NGramLMPOSNolex", Linguality.MONOLINGUAL, ResourceType.NGRAM_POS_NOLEX_LM_MODEL, LMType.POS_NOLEX_LM, ToolType.NONE, Composite.NO),
    SOUL_LM ("SoulLM", Linguality.MONOLINGUAL, ResourceType.SOUL_LM_MODEL, null, ToolType.SOUL_LM, Composite.NO),
    IBM1 ("IBM1Scores", Linguality.BILINGUAL, ResourceType.IBM1_SCORES, null, ToolType.NONE, Composite.WORD_TRANSLATION_MODEL),
    POSCOUNTS ("POSCounts", Linguality.MONOLINGUAL, ResourceType.POS_TAGGED_TEXT, null, ToolType.POS_TAGGER, Composite.NO),
    MODEL ("ModelScores", Linguality.BILINGUAL, ResourceType.NBEST_LIST, null, ToolType.NONE, Composite.WORD_TRANSLATION_MODEL),
    LATTICE ("LatticeFeatures", Linguality.BILINGUAL, ResourceType.TRANSLATION_LATTICES, null, ToolType.NONE, Composite.WORD_TRANSLATION_MODEL),
//    POSTERIOR_PROBS ("PosteriorProbs", Linguality.BILINGUAL, ResourceType.NGRAMS_FSTS, ToolType.NONE, Composite.WORD_TRANSLATION_MODEL),
//    ALIGNMENT ("Alignment", Linguality.BILINGUAL, ResourceType.WORD_ALIGNED_PARALLEL_TEXT, ToolType.NONE, Composite.WORD_TRANSLATION_MODEL),
//    META ("Meta", Linguality.ANY, ResourceType.ANY, ToolType.NONE, Composite.POSSIBLY),
    LABEL ("Label", Linguality.BILINGUAL, ResourceType.REFERENCES, null, ToolType.TERCOM, Composite.NO);
    
    public static final String PROP_FEATURE_NAME = "PROP_FEATURE_NAME";
    public static final String PROP_LINGUALITY = "PROP_LINGUALITY";
    public static final String PROP_RESOURCE_TYPE = "PROP_RESOURCE_TYPE";
    public static final String PROP_RESOURCE_SUB_TYPE = "PROP_RESOURCE_SUB_TYPE";
    public static final String PROP_EXTERNALTOOL = "PROP_EXTERNALTOOL";
    public static final String PROP_COMPOSITE = "PROP_COMPOSITE";

    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    
   protected static final EnumMap<FeatureType, Index<String>> registeredFeatures = new EnumMap<FeatureType, Index<String>>(FeatureType.class);
   
   private void registerInBuiltFeatures()
   {           
        List<FeatureSubTypesEnum> fsubtypes = getFeatureSubTypes();
        
        Index<String> featureIndex = new HashIndex<String>();
        
        for (FeatureSubTypesEnum featureSubType : fsubtypes) {            
           featureIndex.add(featureSubType.featureName);
       }
        
       registeredFeatures.put(this, featureIndex);
   }
   
   public Index<String> getRegisteredFeatureNames()
   {
       if(registeredFeatures.isEmpty())
       {
           registerInBuiltFeatures();
       }
       
       return registeredFeatures.get(this);
   }

   public void registerFeatureName(String featureName)
   {           
        Index<String> featureIndex = getRegisteredFeatureNames();
        
        featureIndex.add(featureName);
   }

    /**
     * @return the linguality
     */
    public Linguality getLinguality() {
        return linguality;
    }

    /**
     * @param linguality the linguality to set
     */
    public void setLinguality(Linguality linguality) throws PropertyVetoException {
        org.limsi.types.FeatureType.Linguality oldLinguality = linguality;
        vetoableChangeSupport.fireVetoableChange(PROP_LINGUALITY, oldLinguality, linguality);
        this.linguality = linguality;
        propertyChangeSupport.firePropertyChange(PROP_LINGUALITY, oldLinguality, linguality);
    }

    /**
     * @return the featureName
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * @param featureName the featureName to set
     */
    public void setFeatureName(String featureName) throws PropertyVetoException {
        String oldFeatureName = featureName;
        vetoableChangeSupport.fireVetoableChange(PROP_FEATURE_NAME, oldFeatureName, featureName);
        this.featureName = featureName;
        propertyChangeSupport.firePropertyChange(PROP_FEATURE_NAME, oldFeatureName, featureName);
    }

    /**
     * @return the externalTool
     */
    public ToolType getExternalTool() {
        return externalTool;
    }

    /**
     * @param externalTool the externalTool to set
     */
    public void setExternalTool(ToolType externalTool) throws PropertyVetoException {
        ToolType oldExternalTool = externalTool;
        vetoableChangeSupport.fireVetoableChange(PROP_EXTERNALTOOL, oldExternalTool, externalTool);
        this.externalTool = externalTool;
        propertyChangeSupport.firePropertyChange(PROP_EXTERNALTOOL, oldExternalTool, externalTool);
    }

    /**
     * @return the resourceType
     */
    public ResourceType getResourceType() {
        return resource;
    }

    /**
     * @param resourceType the resourceType to set
     */
    public void setResourceType(ResourceType resourceType) throws PropertyVetoException {
        ResourceType oldResourceType = resourceType;
        vetoableChangeSupport.fireVetoableChange(PROP_RESOURCE_TYPE, oldResourceType, resourceType);
        this.resource = resourceType;
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_TYPE, oldResourceType, resourceType);
    }

    /**
     * @return the resourceSubType
     */
    public Enum getResourceSubType() {
        return resourceSubType;
    }

    /**
     * @param resourceSubType the resourceSubType to set
     */
    public void setResourceSubType(Enum resourceSubType) throws PropertyVetoException {
        Enum oldResourceSubType = resourceSubType;
        vetoableChangeSupport.fireVetoableChange(PROP_RESOURCE_TYPE, oldResourceSubType, resourceSubType);
        this.resourceSubType = resourceSubType;
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_TYPE, oldResourceSubType, resourceSubType);
    }

    /**
     * @return the composite
     */
    public Composite getComposite() {
        return composite;
    }

    /**
     * @param composite the composite to set
     */
    public void setComposite(Composite composite) throws PropertyVetoException {
        org.limsi.types.FeatureType.Composite oldComposite = composite;
        vetoableChangeSupport.fireVetoableChange(PROP_COMPOSITE, oldComposite, composite);
        this.composite = composite;
        propertyChangeSupport.firePropertyChange(PROP_COMPOSITE, oldComposite, composite);
    }
    
    public enum Linguality {
        ANY,
        MONOLINGUAL,
        BILINGUAL,
        MULTI_LINGUAL;
    }

    public enum Composite {
        NO,
        POSSIBLY,
        WORD_TRANSLATION_MODEL;
    }
    
    FeatureType(String featureName, Linguality linguality, ResourceType resource, Enum resourceSubType,
            ToolType externalTool, Composite composite)
    {
        this.featureName = featureName;
        this.linguality = linguality;
        this.resource = resource;
        this.resourceSubType = resourceSubType;
        this.externalTool = externalTool;
        this.composite = composite;
    }
    
    public static List<FeatureType> getFeatureTypes()
    {
        List<FeatureType> ftypes = new ArrayList<FeatureType>();
        
        ftypes.addAll(Arrays.asList(FeatureType.values()));
        
        return ftypes;
    }
    
    public List<FeatureSubTypesEnum> getFeatureSubTypes()
    {
        if(featureSubTypes == null)
        {
            featureSubTypes = FeatureSubTypesEnum.getFeatureSubTypes(this);
        }
        
        return featureSubTypes;
    }
    
    private String featureName;
    private Linguality linguality;
    private ResourceType resource;
    private Enum resourceSubType;
    private ToolType externalTool;
    private Composite composite;
    private List<FeatureSubTypesEnum> featureSubTypes;
    
    public String getSrcCharset()
    {
        return getResourceType().getSrcLangauge().getCharset().charsetName;
    }
    
    public String getTgtCharset()
    {
        return getResourceType().getTgtLangauge().getCharset().charsetName;
    }
    
    public static List<FeatureType> getFeatureTypes(String featureTypesString)
    {
        if(featureTypesString == null || featureTypesString.equals(""))
        {
            return null;
        }
        
        List<FeatureType> featureTypes = new ArrayList<FeatureType>();
        
        String ft[] = featureTypesString.split("[,;:\\-.]");
        
        for (int i = 0; i < ft.length; i++) {
            String string = ft[i];
            
            FeatureType ftype = FeatureType.valueOf(string);
            
            featureTypes.add(ftype);
        }
        
        return featureTypes;
    }
}
