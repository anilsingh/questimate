import avro.schema
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from avro.datafile import DataFileReader
from avro.io import DatumReader

schema = avro.schema.parse(open("data/avro-schemas/nbest.avsc", "rt").read())


#reader = DataFileReader(open("test.avro", "r"), DatumReader())
reader = DataFileReader(open("tmp/nblist-features.avro.Surface", "r"), DatumReader())

for nbest in reader:
	print nbest

reader.close()
