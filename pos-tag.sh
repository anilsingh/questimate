#!/bin/sh

# Change the java heap size, if necessary
mem=800m

java "-Xmx$mem"  -cp ".:dist/Questimate.jar:lib/*:lib-nodistrib/*" org.limsi.cm.tools.POSTaggerWrapper $@


